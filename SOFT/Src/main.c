/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */
#include "board.h"
#include "_debug.h"
#include "_fifo.h"
#include "xprintf.h"
#include "halI2C.h"

#include "drvMPU6050.h"
#include "drvBME280.h"
#include "drvBH1750FVI.h"
//#include "drvNRF8001.h"

#include "modParser.h"
#include "modPaint.h"
#include "halPaint.h"
#include "mod3D_func.h" 

//������������ Allpcb pcbshopper  HLK-PM01 SPIFFS ��� RingFS
      
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
volatile static const char compile_version[] = "1.0.2";
volatile static const char compile_date[12] = __DATE__;
volatile static const char compile_time[10] = __TIME__;
volatile static const char device_descriptor[] = { "NRFXXXX test" };
char _str[64];

U32 testU32;

#define I2C_SEARCH_SIZE                 6
volatile U8 I2C_search_buf [I2C_SEARCH_SIZE];
volatile U8 I2C_search_cnt = 0;
volatile U8 I2C_search_adress = 0;
volatile U8 I2C_search_f = FALSE;
U8 I2C_buf[128];
U16 size;

U32 test_core_ticks_old;
U32 test_core_ticks_new;

S16 calibration [12+6]; //BME280
S32 temperature;
S32 pressure; 
U8 humidity;
    
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
void error (void)
{
    static U32 errors = 0;
    errors++;
    while (1);
}


void halUSART1_setBaud (U32 baud)
{
    USART1->BRR = ((U32)72000000) / baud; // APB2 = 72MHz
}


void halUSART1_init (U32 baud)
{
    RCC->APB2ENR |= (RCC_APB2ENR_IOPAEN | RCC_APB2ENR_USART1EN);

  //���������������� PORTA.9 ��� TX
    GPIOA->CRH &= ~GPIO_CRH_MODE9;   //�������� ������� MODE
    GPIOA->CRH &= ~GPIO_CRH_CNF9;    //�������� ������� CNF
    GPIOA->CRH |=  GPIO_CRH_MODE9;   //�����, 50MHz
    GPIOA->CRH |=  GPIO_CRH_CNF9_1;  //�������������� �������, �����������

    //���������������� GPIOA.10 ��� RX 
    /*
    GPIOA->CRH &= ~GPIO_CRH_MODE10;  //�������� ������� MODE
    GPIOA->CRH &= ~GPIO_CRH_CNF10;   //�������� ������� CNF
    GPIOA->CRH |=  GPIO_CRH_CNF10_1; //���������� ����, �������� � �����
    GPIOA->BSRR =  GPIO_BSRR_BS10;   //�������� ������������� ��������
    */
    //������� ������ ������
    if ((110 > baud) && (921600 < baud))
    {
        baud = 9600;
    }
    halUSART1_setBaud (baud);

    USART1->CR1 &= ~USART_CR1_M;        //8 ��� ������
    USART1->CR2 &= ~USART_CR2_STOP;     //���������� ����-�����: 1
    //���������� �������
    USART1->CR1 |= USART_CR1_UE;        //��������� ������ USART1
    USART1->CR1 |= USART_CR1_TE;        //��������� �����������
    //USART1->CR1 |= USART_CR1_RE;        //��������� ���������
    //NVIC_EnableIRQ (USART1_IRQn);
    //NVIC_SetPriority (USART1_IRQn, 6);
    //USART1->CR1 |= USART_CR1_RXNEIE;
    
    //halUSART1_flush ();
}


void halUSART1_send_byte (S8 data)
{
    while(!(USART1->SR & USART_SR_TC)); //���� ���� ��� TC � �������� SR ������ 1
    USART1->DR = data; //�������� ���� ����� UART
}


void halSPI1_init (void)
{
    // SPI1 - PA5 - SCK, PA6 - MISO, PA7 - MOSI
    // SPI1 - PB3 - SCK, PB4 - MISO, PB5 - MOSI, PA15 - CS
        
    // Enable clocks
	RCC->APB2ENR |= (RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_SPI1EN);
    //remap
    RCC->APB2ENR |= (RCC_APB2ENR_AFIOEN);
    AFIO->MAPR |= AFIO_MAPR_SPI1_REMAP;
        
	// Configure SPI pins
	GPIOB->CRL &= ~(GPIO_CRL_CNF3 | GPIO_CRL_CNF4 | GPIO_CRL_CNF5); //clear
	GPIOB->CRL |= (GPIO_CRL_CNF3_1 | GPIO_CRL_CNF4_1 | GPIO_CRL_CNF5_1); // AF, Output, PP
	GPIOB->CRL &= ~(GPIO_CRL_MODE4); //input pull-up
    GPIOB->CRL |= (GPIO_CRL_MODE3 | GPIO_CRL_MODE5); // OUT- 50MHz
    //CS
    GPIOA->CRH   &= ~GPIO_CRH_CNF15;
    GPIOA->CRH   |= GPIO_CRH_MODE15;
    
    // Set SPI1
	SPI1->CR1 =
		SPI_CR1_CPHA * 0 |		// Clock Phase
		SPI_CR1_CPOL * 0 |		// Clock Polarity
		SPI_CR1_MSTR * 1 |		// Master Selection
		SPI_CR1_BR_0 * 1 |		// Baud Rate Control - 72 / 8 = 9 MHz
		SPI_CR1_BR_1 * 0 |		//
		SPI_CR1_BR_2 * 0 |		//
		SPI_CR1_SPE * 0 |		// SPI Enable
		SPI_CR1_LSBFIRST * 0 |		// Frame Format
		SPI_CR1_SSI * 1 |		// Internal slave select
		SPI_CR1_SSM * 1 |		// Software slave management
		SPI_CR1_RXONLY * 0 |		// Receive only
		SPI_CR1_DFF * 0 |		// Data Frame Format
		SPI_CR1_CRCNEXT * 0 |		// Transmit CRC next
		SPI_CR1_CRCEN * 0 |		// Hardware CRC calculation enable
		SPI_CR1_BIDIOE * 0 |		// Output enable in bidirectional mode
		SPI_CR1_BIDIMODE * 0;		// Bidirectional data mode enable

	SPI1->CR2 =
		SPI_CR2_RXDMAEN * 0 |		// Rx Buffer DMA Enable
		SPI_CR2_TXDMAEN * 0 |		// Tx Buffer DMA Enable
		SPI_CR2_SSOE * 0 |		// SS Output Enable
		//SPI_CR2_FRF * 0 |	// Protocol format - 0: SPI Motorola mode, 1: SPI TI mode
		SPI_CR2_ERRIE * 0 |		// Error Interrupt Enable
		SPI_CR2_RXNEIE * 0 |		// RX buffer Not Empty Interrupt Enable
		SPI_CR2_TXEIE * 0;		// Tx buffer Empty Interrupt Enable

	SPI1->CR1 |= SPI_CR1_SPE;	// ���������, ������
    
}


U8 halSPI1_xspi (U8 byte)
{
    while (0 == (SPI1->SR & SPI_SR_TXE)) {};
    SPI1->DR = byte;
    while (!(SPI1->SR & SPI_SR_RXNE)) {};  //todo! - remove this!
    return SPI1->DR;
}


void halADC1_init (void)
{
    //init pin
    RCC->APB2ENR  |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRL &= ~(GPIO_CRL_CNF0);
    GPIOB->CRL &= ~(GPIO_CRL_MODE0);
    
    //init ADC
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;     // ������ ����� �� ���
	ADC1->CR2 |= ADC_CR2_ADON;              // ������ ������� �� ���
	ADC1->CR2 |= ADC_CR2_TSVREFE;   // ������ ������� �� ����. ������ � ������ ����.
	ADC1->CR2 |= ADC_CR2_EXTSEL;    // ������ �������������� �� ��������� ���� swstart
	ADC1->CR2 |= ADC_CR2_EXTTRIG;   // �������� ������ �� �������� ������� (� ��� ��� ������)
	ADC1->SMPR1 |= ADC_SMPR1_SMP16; // ������ ����. ���-�� ������ (239.5) �� �������������� ��� 16 ������ ��� ���. ������
	ADC1->SQR3 &= ~ADC_SQR3_SQ1;
	ADC1->SQR3 |= ADC_SQR3_SQ1_3;    // ch8 (0b01000) - 1

    //TODO add calibration!
    
    ADC1->CR2 |= ADC_CR2_SWSTART; // start
    while (!(ADC1->SR & ADC_SR_EOC)) {}; //waiting
    
}


#define ADC_FILTER_SIZE                 8 //������ ������ ������� ��� battary
U32 ADC_filter_buf [ADC_FILTER_SIZE + 1];
//#define MEDIAN_FILTER_SIZE              ADC_FILTER_SIZE


// BaseType - ����� ������������� ��� 
// typedef int BaseType - ������
void shell_sort (S32 *A, U32 N)
{
	S32 i,j,k;
    
	S32 t;
    
	for (k = N/2; k > 0; k /=2)
    {
        for (i = k; i < N; i+=1)
        {
            t = A[i];
            for (j = i; j>=k; j-=k)
            {
                if(t < A[j-k])
                    A[j] = A[j-k];
                else
                    break;
            }
            A[j] = t;
        }
    }
}

static S16 adc_copy [ADC_FILTER_SIZE];
U32 filter_median (U32 *adc_buf, U32 size, S32 sample)
{
    S32 tmpS32;
    //uint_fast16_t
    

    U32 ADC_filter_pointer = adc_buf [size];

    if (++ADC_filter_pointer >= size)
    {
        ADC_filter_pointer = 0;
    }
    adc_buf [ADC_filter_pointer] = sample;
    adc_buf [size] = ADC_filter_pointer; //store pointer

    /* Copy the data */
    //memcpy (adc_copy, adc_buf, MEDIAN_FILTER_SIZE);
    for (U32 i = 0; i < size; i++)
    {
        adc_copy [i] = adc_buf [i];
    }

    /* ��������� */
    shell_sort ((S32 *)&adc_copy[0], size);

    /*  ������� �������������� */
    tmpS32 = 0;
    //tmpS32 = adc_copy [(size) / 2U - 2U];
    tmpS32 += adc_copy [(size) / 2U - 1U];
    tmpS32 += adc_copy [(size) / 2U];
    tmpS32 += adc_copy [(size) / 2U + 1U];
    //tmpS32 += adc_copy [(size) / 2U + 2U];

    return tmpS32 / 3;
}


U32 ADC_val = 0;

void halADC1_run (void)
{
    static U32 val = 0;
    static U8 ADC_cnt;
    
    ADC1->CR2 |= ADC_CR2_SWSTART; // start
    while (!(ADC1->SR & ADC_SR_EOC)) {}; //waiting
    val += (U32)ADC1->DR;
    ADC_cnt++;
    if (ADC_cnt >=  16) //12 + 4bit
    {
        U32 tmp32 = val; //filter_median (&ADC_filter_buf[0], ADC_FILTER_SIZE, val);
        val = val / 16;
        ADC_val = (tmp32 * 34000 * 2) / (4096);
        val = 0;
        ADC_cnt = 0;
    }
}


U32 halADC1_get (void)
{
    return ADC_val;
}

    
void halUSB_pullup_ON (void)
{
    RCC->APB2ENR  |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRH &= ~GPIO_CRH_CNF9;
    GPIOB->CRH |= GPIO_CRH_MODE9;
    PIN_USB_PULLUP_H; //USB on
}


void halUSB_pullup_OFF (void)
{
    RCC->APB2ENR  |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRH &= ~GPIO_CRH_CNF9;
    GPIOB->CRH &= ~GPIO_CRH_MODE9;
}


void halLED_init (void)
{
    // test LED - output push-pull
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
#if LED_FULL_POWER
    GPIOB->CRH &= ~GPIO_CRH_CNF8;
    GPIOB->CRH |= GPIO_CRH_MODE8;
#else
    GPIOB->CRH &= ~GPIO_CRH_CNF8 | GPIO_CRH_MODE8;
    GPIOB->CRH |= GPIO_CRH_CNF8_1;
#endif
    TEST_LED_OFF;
}


void halKEY_init (void)
{
    //KEY - input pull-up
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
    GPIOC->CRH &= ~GPIO_CRH_CNF13;
    GPIOC->CRH |= GPIO_CRH_CNF13_1;
    GPIOC->CRH &= ~GPIO_CRH_MODE13;
    GPIOC->ODR |= GPIO_ODR_ODR13;
}


void halLCD_backlight_ON (void)
{
    RCC->APB2ENR  |= RCC_APB2ENR_IOPAEN;
    GPIOA->CRH &= ~GPIO_CRH_CNF15;
    GPIOA->CRH |= GPIO_CRH_MODE15;
    GPIOA->BSRR = GPIO_BSRR_BS15;
}


void halLCD_backlight_OFF (void)
{
    RCC->APB2ENR  |= RCC_APB2ENR_IOPAEN;
    GPIOA->CRH &= ~GPIO_CRH_CNF15;
    GPIOA->CRH |= GPIO_CRH_MODE15;
    GPIOA->BSRR = GPIO_BSRR_BR15;
}


BOOL LCD_backlight_status = FALSE;

void halLCD_backlight_set (BOOL status)
{
    if (TRUE != status)
    {
        LCD_backlight_status = FALSE;
        halLCD_backlight_OFF ();
    }
    else
    {
        LCD_backlight_status = TRUE;
        halLCD_backlight_ON ();
    }
}


BOOL halLCD_backlight_get (void)
{
    return LCD_backlight_status;
}


void halI2C_power_ON (void)
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRL &= ~GPIO_CRL_CNF1;
    GPIOB->CRL |= GPIO_CRL_MODE1;
    GPIOB->BSRR = GPIO_BSRR_BR1;
}


void halI2C_power_OFF (void)
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRL &= ~GPIO_CRL_CNF1;
    GPIOB->CRL |= GPIO_CRL_MODE1;
    GPIOB->BSRR = GPIO_BSRR_BS1;
}




extern S16 X, Y, Z;
S16 aX, aY, aZ;
S16 gX, gY, gZ;


U32 USB_error_cnt;
U8 show_message (char *buf)
{
    U32 test_cnt = 0;
    U16 size;
    
    size = _strlen (buf);
    //memcpy ((void *)_str, buf, size);
    strcat ((char *)buf, "\r\n");
    size += 2;
    /*
    if (HAL_OK != HAL_UART_Transmit_IT (&huart1, (uint8_t *)&buf[0], size))
    {
        //do nothing
    }
    
    else
    */
    {
repeat: //TODO
        if (USBD_OK != CDC_Transmit_FS((uint8_t *)&buf[0], size))
        {
            USB_error_cnt++;
            if (++test_cnt >= 10)
            {
                return 1;
            }
            else
            {
                HAL_Delay (1); //~10ms
                goto repeat;
            }
        }
        else
        {
            return 0;
        }
    }
    return 0;
}


void show_message_c (const char *str)
{
#if _DEBUG
    xsprintf (_str, "%s", str);
    show_message (_str);
#endif
}



typedef enum {
    CMD_TEST                            = 'T',
    CMD_GET_DEV_ID                      = 'I',
    CMD_GET_FIRMWARE_VER                = 'V',
    CMD_ADC_GET                         = 'B', //2,3 - number channel
    
} FMT_CMD;


parser_t parser;
U8 aRxBuffer [128];


U8 fifo_UART_RX_buf [fifo_UART_RX_SIZE];
FIFO_t fifo_UART_RX;


static uint32_t *UID = (uint32_t *)0x1FFFF7E8;

U8 IOBuf [64];
U8 IOBuf_cnt = 0;;




/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */
    //nRF8001
    //CE
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRH &= ~GPIO_CRH_CNF12;
    GPIOB->CRH |= GPIO_CRH_MODE12;
    GPIOB->BSRR = GPIO_BSRR_BS12;
    //ACTIVE
    GPIOB->CRL &= ~GPIO_CRL_CNF2;
    GPIOB->CRL |= GPIO_CRL_MODE2;
    GPIOB->BSRR = GPIO_BSRR_BR2;
    
    //test pin - output push-pull
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    GPIOA->CRL &= ~GPIO_CRL_CNF1;
    GPIOA->CRL |= GPIO_CRL_MODE1;
    TEST_PIN_L;
    halI2C_power_ON ();
    
    halLED_init ();
    halKEY_init ();
    
    halI2C_power_OFF ();
    halADC1_init ();
                     
    
    // for debug & test
    //halUSART1_init (921600); //921600);
    // init CDC out struct
    /*
    parser_data.start = PIK_START;
    parser_data.reciv_cnt = 8;
    parser_data.crc = 0x4545;
    parser_data.end = PIK_FIN;

    modParser_init (&parser, '#'); //'#' - address
    modParser_reset (&parser);
    */
    
    //TODO check init
    FIFO_init (&fifo_UART_RX, &fifo_UART_RX_buf[0], fifo_UART_RX_SIZE);
    /*
    //test fifo
    FIFO_puts (&fifo, (U8 *) &tmps16, 2); tmps16_ = 37677;
    if (TRUE != FIFO_puts (&fifo, (U8 *) &tmps16_, 2))
    {
        while (1)
        {
            TEST_LED_ON;  
            HAL_Delay(500); //for (U32 i=0; i<100000; i++) {};
            TEST_LED_OFF;
            HAL_Delay(500); //for (U32 i=0; i<100000; i++) {};
        }
    }
    */
    FIFO_flush (&fifo_UART_RX);
    
    //halUSB_pullup_ON ();
    
    
    halLCD_backlight_set (TRUE);
    paint_init (SCREEN_ORIENTATION_GLOBAL);
    paint_setBackgroundColor (COLOR_WHITE);
    paint_clearScreen ();
    hal_paint_update ();
    paint_setColor (COLOR_BLACK);
    paint_setFont (PAINT_FONT_Generic_8pt, PAINT_FONT_MS);
    paint_putStrColRow (0, 0, "Hell world...");
    paint_putStrColRow (0, 1, __DATE__);
    paint_putStrColRow (0, 2, __TIME__);
    hal_paint_update ();
    
    HAL_Delay (1000);
    paint_clearScreen ();
    paint_putStrColRow (0, 0, "I2C scan start");
    hal_paint_update ();

jjj:
    // I2C test
    halI2C_init ();
    if (FUNCTION_RETURN_OK == soft_halI2C_re_init ())
    {
        xsprintf (_str, "I2C reinit");
        paint_putStrColRow (0, 1, _str);
        hal_paint_update ();
    }
        
    I2C_search_cnt = 0;
    I2C_search_adress = 0;
    
    while (1)
    {
        if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_search_adress, &I2C_buf[0], 0, 0))
        {
            I2C_search_buf [I2C_search_cnt] = I2C_search_adress;
            I2C_search_cnt++;
        }
        I2C_search_adress += 2;
        if (I2C_search_adress >= 128)
        {
            break;
        }
    }
    
    size = xsprintf (_str, "%u.%02X %02X %02X %02X", 
            I2C_search_cnt,           
            I2C_search_buf[0], 
            I2C_search_buf[1],
            I2C_search_buf[2],
            I2C_search_buf[3]);
    paint_putStrColRow (0, 2, _str);
    CDC_Transmit_FS ((uint8_t  *)_str, size); //test send
    hal_paint_update ();
    HAL_Delay (2500);

   
    

    U32 LED_tickstart = 0;
    U32 repaint_tickstart = 0;
    
    
    if (FUNCTION_RETURN_OK == drvBME280_calibration (&calibration[0]))
    {
        if (FUNCTION_RETURN_OK == drvBME280_get_all (&calibration[0], &temperature, &pressure, &humidity))
        {
            size = xsprintf (_str, "%8d C", temperature);
            paint_putStrColRow (0, 3, _str);
            size = xsprintf (_str, "%8d P", pressure);
            paint_putStrColRow (0, 4, _str);
            size = xsprintf (_str, "%8d H", humidity);
            paint_putStrColRow (0, 5, _str);
            hal_paint_update ();
            HAL_Delay (3000);
        }
    }
    
    paint_clearScreen ();
    mod3D_init ();
    drvMPU6050_init ();
    if (FALSE == drvMPU6050_testConnection ())
    {
        paint_putStrColRow (0, 0, "MPU not found!");
        hal_paint_update ();
        HAL_Delay (2000);
        goto jjj; // ololo ))0)))
    }
    
    test_core_ticks_new = 0;
    test_core_ticks_old = 0;
    
    while (1)
    {
        //test FPS
        RESET_CORE_COUNT;
        
        U8 tmpchar;
        // 0A 23 09 47 00 00 01 00 30 30 30 30 35 35 0D
        if (TRUE == FIFO_gets (&fifo_UART_RX, &tmpchar, 1)) 
        {
            IOBuf [IOBuf_cnt] = tmpchar;
            if (++IOBuf_cnt >= 64)
            {
                IOBuf_cnt = 0;
            }
            
            //if (FUNCTION_RETURN_OK == modParser_reciv (&parser, tmpchar, &IOBuf[0], &size8))
            if ((IOBuf [IOBuf_cnt - 1] == '\r') || (IOBuf [IOBuf_cnt - 1] == '\n'))
            {
                //U32 digit_uint32 = (U32)(IOBuf [1] << 24 | IOBuf [2] << 16 | IOBuf [3] << 8 | IOBuf [4]);
                switch (IOBuf [0]) // parsing command
                {
                case CMD_TEST: //T
                    size = xsprintf (_str, "[%u.%u]%u ", HAL_GetTick (), test_core_ticks_old);
                    break;
                    
                case CMD_GET_DEV_ID: //I
                    size = xsprintf (_str, "ID:0x%08X%08X%08X ", UID [0], UID [1], UID [2]);
                    break;
                    
                case CMD_GET_FIRMWARE_VER: //V
                    size = xsprintf (_str, "date %u.%u.%u v[%u] ", compile_date [0], compile_date [1], compile_date [2], compile_version [0]);
                    break;
                    
                case CMD_ADC_GET: //B2(3)
                    
                    switch (IOBuf [1])
                    {
                      case '0':
                        testU32 = halADC1_get ();
                        xsprintf (_str, "%0u.%02u V", 
                            testU32 / 10000,
                            (testU32 % 10000) / 10);
                        
                        //paint_putStrColRow (0, 3, _str);
                        break;
                        
                      default:
                          size = xsprintf (_str, "ADC wrong channel!");
                          //drvBUZZER_peep (1);
                          break;
                    }
                    break;
                
                default: 
                    size = xsprintf (_str, "Unknown command!");
                    break;
                } // switch (IOBuf [0]) // parsing command
                
                // create & send respond
                CDC_Transmit_FS ((U8 *)_str, size);
                /*
                modParser_transmit (&parser, (uint8_t *)_str, size8, &IOBuf [0], &size8);
                //respond
                if (HAL_UART_Transmit_IT (&huart1, &IOBuf[0], size8)!= HAL_OK)
                {
                    Error_Handler();
                }
                */
                IOBuf_cnt = 0;
            }//if (HAL_OK != HAL_UART_Transmit (&huart1, (uint8_t *)&buf[0], size, 0xFFFF))
        }
        
        //test FPS
        test_core_ticks_new = GET_CORE_COUNT;
        if (test_core_ticks_new > test_core_ticks_old)
        {
            test_core_ticks_old = test_core_ticks_new;
        }
        
        
        //key - baklight
        if (0 == KEY_IN)
        {
            if (TRUE == halLCD_backlight_get ())
                halLCD_backlight_set (FALSE);
            else
                halLCD_backlight_set (TRUE);
            HAL_Delay (250);
        }
        
        //check BATT voltage
        halADC1_run ();
    
        //test blink
        if((HAL_GetTick() - LED_tickstart) >= 100)
        {
            LED_tickstart = HAL_GetTick();
            
            TEST_LED_INV;
        }

        //repaint LCD
        if((HAL_GetTick() - repaint_tickstart) >= 200)
        {
            repaint_tickstart = HAL_GetTick();
            
            drvMPU6050_getRawData (&aX, &aY, &aZ, &gX, &gY, &gZ);
            
            size = xsprintf (_str, "%6d\r\n", aX);
            CDC_Transmit_FS ((uint8_t  *)_str, size); 
            //paint_putStrColRow (0, 0, _str);
            size = xsprintf (_str, "%6d\r\n", aY);
            CDC_Transmit_FS ((uint8_t  *)_str, size); 
            //paint_putStrColRow (0, 1, _str);
            size = xsprintf (_str, "%6d\r\n", aZ);
            CDC_Transmit_FS ((uint8_t  *)_str, size); 
            //paint_putStrColRow (0, 2, _str);
            
            /*
            S32 vector = sqrt ((aX * aX) + (aY * aY) + (aZ * aZ)); //16 bit!
            size = xsprintf (_str, "%6d\r\n", vector);
            CDC_Transmit_FS ((uint8_t  *)_str, size); 
            */
            aX = aX * -1;
            Y = aX / 256;
            X = aY / 256;
            Z = 0;//aZ / 256;
            
            if (X < 0) X = 63 - X;
            if (Y < 0) Y = 63 - Y;
            if (Z < 0) Z = 63 - Z;
            
            mod3D_run ();

            hal_paint_update ();
        }
        
    }
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
      
    
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
