#include "board.h"
#include "drvMAX30100.h"
#include "halI2C.h"

#include "_debug.h"


#if I2C_MAX30100


U16 rawIRValue;
U16 rawRedValue;
    
MSG drvMAX30100_read8 (U8 address, U8 *data8)
{
    MSG respond = FUNCTION_RETURN_ERROR;

    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_MAX30100_ADDRESS, &address, 1, 100))
    {
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_MAX30100_ADDRESS, data8, 1, 100))
        {
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}


MSG drvMAX30100_write8 (U8 address, U8 data8)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[2];

    I2C_buf[0] = address;
    I2C_buf[1] = data8;
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_MAX30100_ADDRESS, &I2C_buf[0], 2, 100))
    {
        respond = FUNCTION_RETURN_OK;
    }
    return respond;
}


MSG drvMAX30100_burstRead (U8 baseAddress, U8 *buffer, U8 length)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_MAX30100_ADDRESS, &baseAddress, 1, 100))
    {
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_MAX30100_ADDRESS, buffer, length, 100))
        {
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}


void drvMAX30100_setMode(Mode mode)
{
    drvMAX30100_write8 (MAX30100_REG_MODE_CONFIGURATION, mode);
}


void drvMAX30100_setLedsPulseWidth(LEDPulseWidth ledPulseWidth)
{
    U8 previous;
    drvMAX30100_read8 (MAX30100_REG_SPO2_CONFIGURATION, &previous);
    drvMAX30100_write8 (MAX30100_REG_SPO2_CONFIGURATION, (previous & 0xfc) | ledPulseWidth);
}


void drvMAX30100_setSamplingRate(SamplingRate samplingRate)
{
    U8 previous;
    drvMAX30100_read8 (MAX30100_REG_SPO2_CONFIGURATION, &previous);
    drvMAX30100_write8 (MAX30100_REG_SPO2_CONFIGURATION, (previous & 0xe3) | (samplingRate << 2));
}


void drvMAX30100_setLedsCurrent(LEDCurrent irLedCurrent, LEDCurrent redLedCurrent)
{
    drvMAX30100_write8 (MAX30100_REG_LED_CONFIGURATION, redLedCurrent << 4 | irLedCurrent);
}


void drvMAX30100_setHighresModeEnabled(bool enabled)
{
    U8 previous;
    drvMAX30100_read8 (MAX30100_REG_SPO2_CONFIGURATION, &previous);
    if (enabled) {
        drvMAX30100_write8 (MAX30100_REG_SPO2_CONFIGURATION, previous | MAX30100_SPC_SPO2_HI_RES_EN);
    } else {
        drvMAX30100_write8 (MAX30100_REG_SPO2_CONFIGURATION, previous & ~MAX30100_SPC_SPO2_HI_RES_EN);
    }
}


void drvMAX30100_readFifoData (void)
{
    U8 buffer[4];

    drvMAX30100_burstRead (MAX30100_REG_FIFO_DATA, buffer, 4);

    // Warning: the values are always left-aligned
    rawIRValue = (buffer[0] << 8) | buffer[1];
    rawRedValue = (buffer[2] << 8) | buffer[3];
}


void drvMAX30100_update()
{
    drvMAX30100_readFifoData();
}


void drvMAX30100_startTemperatureSampling (void)
{
    U8 modeConfig;
    drvMAX30100_read8 (MAX30100_REG_MODE_CONFIGURATION, &modeConfig);
    modeConfig |= MAX30100_MC_TEMP_EN;
    drvMAX30100_write8 (MAX30100_REG_MODE_CONFIGURATION, modeConfig);
}


bool drvMAX30100_isTemperatureReady (void)
{
    U8 modeConfig;
    drvMAX30100_read8 (MAX30100_REG_MODE_CONFIGURATION, &modeConfig);
    return !(modeConfig & MAX30100_MC_TEMP_EN);
}


float drvMAX30100_retrieveTemperature (void)
{
    S8 tempInteger;
    drvMAX30100_read8 (MAX30100_REG_TEMPERATURE_DATA_INT, &tempInteger);
    S8 tempFrac;
    drvMAX30100_read8 (MAX30100_REG_TEMPERATURE_DATA_FRAC, &tempFrac);

    return (float)tempFrac * 0.0625 + tempInteger;
}


void drvMAX30100_init (void)
{
    drvMAX30100_setMode(DEFAULT_MODE);
    drvMAX30100_setLedsPulseWidth(DEFAULT_PULSE_WIDTH);
    drvMAX30100_setSamplingRate(DEFAULT_SAMPLING_RATE);
    drvMAX30100_setLedsCurrent(DEFAULT_IR_LED_CURRENT, DEFAULT_RED_LED_CURRENT);
    drvMAX30100_setHighresModeEnabled(1);
}



/*
// http://www.schwietering.com/jayduino/filtuino/
// Low pass butterworth filter order=1 alpha1=0.1
// Fs=100Hz, Fc=6Hz
class  FilterBuLp1
{
	public:
		FilterBuLp1()
		{
			v[0]=0.0;
		}
	private:
		float v[2];
	public:
		float step(float x) //class II
		{
			v[0] = v[1];
			v[1] = (2.452372752527856026e-1 * x)
				 + (0.50952544949442879485 * v[0]);
			return
				 (v[0] + v[1]);
		}
};

// http://sam-koblenski.blogspot.de/2015/11/everyday-dsp-for-programmers-dc-and.html
class DCRemover
{
public:
	DCRemover() : alpha(0), dcw(0)
	{
	}
	DCRemover(float alpha_) : alpha(alpha_), dcw(0)
	{
	}

	float step(float x)
	{
		float olddcw = dcw;
		dcw = (float)x + alpha * dcw;

		return dcw - olddcw;
	}

	float getDCW()
	{
		return dcw;
	}

private:
	float alpha;
	float dcw;
};

*/


#endif
