#include "board.h"
#include "drvBME280.h"
#include "halI2C.h"
#include "_debug.h"


#if I2C_BME280

#define OSS 3 //�������� - ���������

MSG drvBME280_ReadShort (U8 address, U16 *data16)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[2];

    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_BME280_ADDRESS, &address, 1, 100))
    {
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_BME280_ADDRESS, &I2C_buf[0], 2, 100))
        {
            *data16 = (((U16)I2C_buf[0] << 8) | I2C_buf[1]); //combine world
            //*data16 = tmp16;
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}


MSG drvBME280_ReadLong (U8 address, U32 *data32)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[3];

    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_BME280_ADDRESS, &address, 1, 100))
    {
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_BME280_ADDRESS, &I2C_buf[0], 3, 100))
        {
            *data32 = (((U32)I2C_buf[0] << 16) +
                ((U32)I2C_buf[1] << 8) +
                (U32)I2C_buf[2]) >> (8 - OSS);
            respond = FUNCTION_RETURN_OK;
        }
    }
	return respond;
}


MSG drvBME280_getTemp (S32 *temperature)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[2];
	S16 tmp16 = 0;
    
    *temperature = 0;
    I2C_buf[0] = 0xF4; //CMD - start measure temperature
    I2C_buf[1] = 0x2E;
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_BME280_ADDRESS, &I2C_buf[0], 2, 100))
    {
        HAL_Delay (5); // min time is 4.5ms
        if (FUNCTION_RETURN_OK == drvBME280_ReadShort (0xF6, (U16 *)&tmp16))
        {
            *temperature = tmp16;
            respond = FUNCTION_RETURN_OK;
        }
    }
	return respond;
}


MSG drvBME280_getPressure (S32 *pressure)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[2];

    *pressure = 0;
    I2C_buf [0] = 0xF4; //CMD - start measure temperature
    I2C_buf [1] = 0x34 + (OSS << 6); // write 0x34+(OSS<<6)
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_BME280_ADDRESS, &I2C_buf[0], 2, 100))
    {
        HAL_Delay (25); // min time is 25ms
        if (FUNCTION_RETURN_OK == drvBME280_ReadLong (0xF6, (U32 *)pressure))
        {
            respond = FUNCTION_RETURN_OK;
        }
    }
	return respond;
}


MSG drvBME280_get_all (S16 *calibration,
    S32 *temperature,
    S32 *pressure,
    U8 *humidity) 
{
	S8 i;
	S32 ut = 0;
	S32 up = 0;
	S32 x1, x2, b5, b6, x3, b3, p;
	U32 b4, b7;
	S16 ac1 = calibration [0];
	S16 ac2 = calibration [1]; 
	S16 ac3 = calibration [2]; 
	U16 ac4 = calibration [8+0];
	U16 ac5 = calibration [8+1];
	U16 ac6 = calibration [8+2];
	S16 b1 = calibration [3]; 
	S16 b2 = calibration [4];
	//S16 mb=calibration [5];
	S16 mc = calibration [6];
	S16 md = calibration [7];

	if (FUNCTION_RETURN_OK != drvBME280_getTemp (&ut))
    {
        return FUNCTION_RETURN_ERROR;
	}
	if (FUNCTION_RETURN_OK != drvBME280_getPressure (&up))
    {
        return FUNCTION_RETURN_ERROR;
	}
	
	x1 = ((S32)ut - (S32)ac6) * (S32)ac5 >> 15;
	x2 = ((S32)mc << 11) / (x1 + md);
	b5 = x1 + x2;
	*temperature = (b5 + 8) >> 4;
	
	b6 = b5 - 4000;
	x1 = (b2 * ((b6 * b6) >> 12)) >> 11;
	x2 = (ac2 * b6) >> 11;
	x3 = x1 + x2;
	b3 = (((((S32) ac1) * 4 + x3)<<OSS) + 2)>> 2;
	x1 = (ac3 * b6) >> 13;
	x2 = (b1 * ((b6 * b6) >> 12)) >> 16;
	x3 = ((x1 + x2) + 2) >> 2;
	b4 = (ac4 * (U32) (x3 + 32768)) >> 15;
	b7 = ((U32) (up - b3) * (50000 >> OSS));
	//p = b7 < 0x80000000 ? (b7 * 2) / b4 : (b7 / b4) * 2;
	
	if (b7 < 0x80000000)
	{
		p = (b7 << 1) / b4;
	}
	else
	{ 
		p = (b7 / b4) << 1;
	}

	x1 = (p >> 8) * (p >> 8);
	x1 = (x1 * 3038) >> 16;
	x2 = (-7357 * p) >> 16;
	*pressure = p + ((x1 + x2 + 3791) >> 4);
    
    return FUNCTION_RETURN_OK;
}


MSG drvBME280_calibration (S16 *calibration)
{
    U8 errorcode = 0;
    
    if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xAA, (U16 *)&calibration[0]))//ac1
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xAC, (U16 *)&calibration[1]))//ac2
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xAE, (U16 *)&calibration[2])) //ac3
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xB0, (U16 *)&calibration[8+0])) //ac4
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xB2, (U16 *)&calibration[8+1])) //ac5
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xB4, (U16 *)&calibration[8+2])) //ac6
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xB6, (U16 *)&calibration[3])) //b1
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xB8, (U16 *)&calibration[4])) ///b2
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xBA, (U16 *)&calibration[5])) //mb
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xBC, (U16 *)&calibration[6])) //mc
    {
        errorcode += 1;
	}	
	if (FUNCTION_RETURN_OK != drvBME280_ReadShort (0xBE, (U16 *)&calibration[7])) //md
    {
        errorcode += 1;
	}
    if (0 == errorcode)
        return FUNCTION_RETURN_OK;
    else
        return FUNCTION_RETURN_ERROR;
}
//----------------------------------------

#include "math.h"
//http://www.avislab.com/blog/bmp085/
S32 drvBME280_get_altitude (S32 pressure)
{
	float temp;
	S32 altitude;
	temp = (float) pressure / 101325;

	temp = 1 - pow ((float)temp, (float)0.190294957);

	//altitude = round(44330*temp*10);
	altitude = 44330 * temp * 100;
	
	return altitude; //get altitude in dm
}


#endif
