#include "modParser.h"
#include "board.h"
#include "_crc.h"


enum PARSER_STATE {
    PARSER_STATE_RESET = 0,
    PARSER_STATE_IDLE,
    PARSER_STATE_CH_ADR,
    PARSER_STATE_CH_LEN_L, 
    PARSER_STATE_CH_LEN_H, 
    PARSER_STATE_REC_DATA,
    PARSER_STATE_CRC_L,
    PARSER_STATE_CRC_H,
    PARSER_STATE_CH_END
};


void modParser_init (parser_t *pp, U8 adress)
{
    pp->adress = adress;
    modParser_reset (pp);
}


void modParser_reset (parser_t *pp)
{
    pp->pos = PARSER_STATE_IDLE;
}


MSG modParser_reciv (parser_t *pp, U8 rchar, U8 *bufIN, U16 *size_bufIN)
{
    MSG respond = FUNCTION_RETURN_ERROR;

    switch (pp->pos)
    { 
        case PARSER_STATE_IDLE: // START ����
            if (PIK_START == rchar) // ������� ������ ���������
            {
                pp->pos = PARSER_STATE_CH_ADR;
            }
            break;
            
        case PARSER_STATE_CH_ADR:
            if (pp->adress == rchar) // ����� ����������
            {
                pp->pos = PARSER_STATE_CH_LEN_L;
            }
            else
            {
                if (PIK_START != rchar) // ���� ��� ��� ��� ���������, �� ��������
                {
                    pp->pos = PARSER_STATE_IDLE; // ������
                }
            }
            break;
						
        case PARSER_STATE_CH_LEN_L:
            //if (0 != rchar) // @todo �������� �� ������� (pp->message_size > rchar) &&
            pp->reciv_cnt = 0;	                
            *size_bufIN = rchar;
            pp->pos = PARSER_STATE_CH_LEN_H;
            break;
                              
        case PARSER_STATE_CH_LEN_H:
            pp->reciv_cnt = 0;	                
            *size_bufIN |= (U16)(rchar << 8);
            pp->crcA = 0xFFFF; // ��. �������� ������
            pp->pos = PARSER_STATE_REC_DATA;
            break;

        case PARSER_STATE_REC_DATA: // ��� ��������� �����
            bufIN[pp->reciv_cnt] = rchar; // ���������
            crc16_CITT_s(&pp->crcA, &rchar);
            pp->reciv_cnt++;
            if (pp->reciv_cnt >= *size_bufIN)
            {
                pp->crcB = 0;
                pp->pos = PARSER_STATE_CRC_L;
            }
            break;
						
       case PARSER_STATE_CRC_L: // crc
            pp->crcB = (uint16_t)rchar;
            pp->pos = PARSER_STATE_CRC_H;
            break;
            
       case PARSER_STATE_CRC_H: // check crc
            pp->crcB |= (uint16_t)(rchar << 8);
#if CRC_CHECK
            if (pp->crcA == pp->crcB)
#endif
            {
                pp->pos = PARSER_STATE_CH_END;
            }
#if CRC_CHECK
            else { // error CRC
            
                ParsPos = PARSER_STATE_IDLE;
            }
#endif
            break;
            
        case PARSER_STATE_CH_END:
            if (PIK_FIN == rchar)
            { 
                respond = FUNCTION_RETURN_OK; // pak rec succ!
            }
            pp->pos = PARSER_STATE_IDLE; // to start othewise
            break;
            
        default:
            pp->pos = PARSER_STATE_IDLE;
            break;
    }
		
    return respond;  
}


MSG modParser_transmit (parser_t *pp, U8 *bufIN, U16 size_bufIN, U8 *bufOUT, U16 *size_bufOUT)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U32 i;
    
    if (0 != size_bufIN) // ((pp->message_size >= size) &&
    { 
        bufOUT [0] = PIK_START;
        bufOUT [1] = PIK_START;
        bufOUT [2] = pp->adress;
        bufOUT [3] = (uint8_t)(size_bufIN & 0x00FF); // LEN
        bufOUT [4] = (uint8_t)(size_bufIN >> 8); // LEN
        pp->crcA = 0xFFFF; // ��. �������� ������ ��� ������� ���
        for (i = 0; i < size_bufIN; i++)
        {
            bufOUT [i + 5] = bufIN[i];
            crc16_CITT_s (&pp->crcA, &bufIN [i]);
        }
        bufOUT [5 + size_bufIN]  = (U8)(pp->crcA & 0x00FF); // crc
        bufOUT [6 + size_bufIN]  = (U8)(pp->crcA >> 8);
        bufOUT [7 + size_bufIN]  = PIK_FIN;
        *size_bufOUT = PARSER_STRUCT_SIZE + size_bufIN;
        
        respond = FUNCTION_RETURN_OK; 
    }
    
    return respond;  
}
