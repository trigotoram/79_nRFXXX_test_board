#include "modRandom.h"
#include "board.h"


// ������������ ����� ������� �������������
#define RAND_MAX_ATTEMPTS 5

#define POLINOM_32 0xE0000200

//#define SORS_RAND(n)   (*((volatile U32 *)(0x20000000 + 4 * n)))

//
U32 modRnd_rand32;

/**
int _rand(void) // RAND_MAX assumed to be 32767.
{
  static unsigned long next = 1;
  next = next * 1103515245 + 12345;
  return next >> 16;
}
*/

/**
 * �������-����������� 32 ���
 * @return ��������� ����� � ��������� �� 0 to 429467296-1
 */
U32 _rand32 (void)
{
    U32 i;
    
    for (i = 0; i < 32; i++)
    {
        if (modRnd_rand32 & 0x00000001)
        {
            modRnd_rand32 = (modRnd_rand32 >> 1) ^ POLINOM_32;
        }
        else
        {
            modRnd_rand32 = (modRnd_rand32 >> 1);
        }
    }
    
    return modRnd_rand32;
}


uint16_t _rand16 (void)
{
    U32 i;
    
    for (i = 0; i < 16; i++)
    {
        if (modRnd_rand32 & 0x00000001)
        {
            modRnd_rand32 = (modRnd_rand32 >> 1) ^ POLINOM_32;
        }
        else
        {
            modRnd_rand32 = (modRnd_rand32 >> 1);
        }
    }
    
    return modRnd_rand32;
}


U8  _rand8 (void)
{
    U32 i;
    
    for (i = 0; i < 8; i++)
    {
        if (modRnd_rand32 & 0x00000001)
        {
            modRnd_rand32 = (modRnd_rand32 >> 1) ^ POLINOM_32;
        }
        else
        {
            modRnd_rand32 = (modRnd_rand32 >> 1);
        }
    }
    
    return modRnd_rand32;
}


U8  _rand1 (void)
{
    if (modRnd_rand32 & 0x00000001)
    {
        modRnd_rand32 = (modRnd_rand32 >> 1) ^ POLINOM_32;
    }
    else
    {
        modRnd_rand32 = (modRnd_rand32 >> 1);
    }

    return modRnd_rand32 & 0x01;
}


void  _rand_str (char *buf, U32 num)
{
    U32 i, j;
    if (NULL != buf)
    {
        for (i = 0; i < num; i++)
        {
            for (j = 0; j < 8; j++)
            {
                if (modRnd_rand32 & 0x00000001)
                {
                    modRnd_rand32 = (modRnd_rand32 >> 1) ^ POLINOM_32;
                }
                else
                {
                    modRnd_rand32 = (modRnd_rand32 >> 1);
                }
            }
            buf [i] = modRnd_rand32;
        }
    }
}


void  _rand_real (U8 *buf, U32 num) //ADC conf. & work DESTROED!!!
{
#if (BOARD_STM32F4DISCOVERY || BOARD_STM32F411_REBORN)
    U32 i, j, tmp8;
    
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;     // ������ ����� �� ���
	ADC1->CR2 |= ADC_CR2_ADON;              // ������ ������� �� ���
	//ADC1->CR2 |= ADC_CR2_TSVREFE;   // ������ ������� �� ����. ������ � ������ ����.
	ADC1->CR2 |= ADC_CR2_EXTSEL;    // ������ �������������� �� ��������� ���� swstart
	//ADC1->CR2 |= ADC_CR2_EXTTRIG;   // �������� ������ �� �������� ������� (� ��� ��� ������)
	ADC1->SMPR1 |= ADC_SMPR1_SMP16; // ������ ����. ���-�� ������ (239.5) �� �������������� ��� 16 ������ ��� ���. ������
	ADC1->SQR3 &= ~ADC_SQR3_SQ1;
	ADC1->SQR3 |= 6; //ADC_SQR3_SQ1_4;    // �������� 16 (0b10000) ����� ��� 1 ��������������     (���-�� �������������� ��-��������� 1)
    
    for (j = 0; j < num; j++)
    {
        buf [j] = 0;
        for (i = 0; i < 8; i++)
        {
            ADC1->CR2 |= ADC_CR2_SWSTART; // ������ ��������������
            while (!(ADC1->SR & ADC_SR_EOC)) {}; // ���� ����� ��������������
            //buf [j] ^tmp8 = (U8)((ADC1->DR << i) & (1 << i));
            tmp8 = (uint16_t)(ADC1->DR & 0x0001);
            buf [j] |= tmp8;
            buf [j] = buf [j] << 1;
        }
    }

#endif
}


#if MODRANDOM_4096BIT_EN

#define MODRAND_4096BIT_SIZE            (4096UL / 32UL) //128 32bits

static const U32 c_rand [MODRAND_4096BIT_SIZE] = {
    0, 0, 0, 0, 
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0, //16
    
    0, 0, 0, 0, 
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0, //32
    
    0, 0, 0, 0, 
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0, 
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0, //64
    
    0, 0, 0, 0, 
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0, 
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    
    
    0, 0, 0, 0, 
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0x00800600
};


 //����������� ��������� ����� 0x00000000 !!!
U32 _rand4096 [(MODRAND_4096BIT_SIZE + 1)]; // = {676667, 15655567 ,0};

U32 _rand4096_32 (void)
{
    U32 i, j, m;
    U32 n;
	  
    for (i = 0; i < 32; i++)
    {
        m = _rand4096 [0] & 0x00000001;
        for (j = 0; j < MODRAND_4096BIT_SIZE; j++)
        {
            _rand4096 [j] = _rand4096 [j] >> 1;
            n = (_rand4096 [j + 1] & 0x00000001);
            _rand4096 [j] |= (n << 31);
        } 
        if (0 != m)
        { 
            for (j = 0; j < MODRAND_4096BIT_SIZE; j++)
            {
                _rand4096 [j] = _rand4096 [j] ^ c_rand [j];
            }
        }
    }
    
    return _rand4096 [0];
}


U8 _rand4096_8 (void)
{
    U32 i, j, m;
    U32 n;
	  
    for (i = 0; i < 8; i++)
    {
        m = _rand4096 [0] & 0x01;
        for (j = 0; j < MODRAND_4096BIT_SIZE; j++)
        {
            _rand4096 [j] = _rand4096 [j] >> 1;
            n = (_rand4096 [j + 1] & 0x00000001);
            _rand4096 [j] |= (n << 31);
        } 
        if (0 != m)
        { 
            for (j = 0; j < MODRAND_4096BIT_SIZE; j++)
            {
                _rand4096 [j] = _rand4096 [j] ^ c_rand [j];
            }
        }
    }
    
    return _rand4096 [0];
}


U8 _rand4096_1 (void)
{
    U32 j, m;
    U32 n;
    
    m = _rand4096 [0] & 0x01;
    for (j = 0; j < MODRAND_4096BIT_SIZE; j++)
    {
        _rand4096 [j] = _rand4096 [j] >> 1;
        n = (_rand4096 [j + 1] & 0x00000001);
        _rand4096 [j] |= (n << 31);
    } 
    if (0 != m)
    { 
        for (j = 0; j < MODRAND_4096BIT_SIZE; j++)
        {
            _rand4096 [j] = _rand4096 [j] ^ c_rand [j];
        }
    }
    
    return _rand4096 [0];
}


void  _rand4096_str (char *buf, U32 n)
{
    if (NULL != buf)
    {
        for (; 0 < n; n--)
            *buf++ = _rand4096_8 ();
    }
}
#endif



U32 _srand (void)
{
#if BOARD_TEST
    static const U8 TIME[10] = __TIME__;
    U8 *pST32 = (U8 *) &modRnd_rand32;
    //for (i = 0; i < sizeof(U32); i++)
    {
        //modRnd_rand32 += TIME[i]; //@todo �������� �������� �� 0!
        pST32[3] = TIME[3] - 0x30;
        pST32[2] = TIME[4] - 0x30;
        pST32[1] = TIME[6] - 0x30;
        pST32[0] = TIME[7] - 0x30;
    }  
#endif
    
#if BOARD_STM32F4DISCOVERY_
    //PA0 � ���� ���
    GPIO_InitTypeDef GPIO_InitStructure; 
    ADC_InitTypeDef  ADC_InitStructure;   
    ADC_CommonInitTypeDef  ADC_CommonInitStructure;
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    /* PCLK2 is the APB2 clock */
    /* ADCCLK = PCLK2/6 = 72/6 = 12MHz*/
    ///RCC_ADCCLKConfig(RCC_PCLK2_Div6);

    /* Enable ADC1 clock so that we can talk to it */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    /* Put everything back to power-on defaults */
    ADC_DeInit();

//     /* ADC1 Configuration ------------------------------------------------------*/
//     /* ADC1 and ADC2 operate independently */
//     // ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
//     /* Disable the scan conversion so we do one at a time */
//     ADC_InitStructure.ADC_ScanConvMode = DISABLE;
//     /* Don't do contimuous conversions - do them on demand */
//     ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
//     /* Start conversin by software, not an external trigger */
//     // ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
//     /* Conversions are 12 bit - put them in the lower 12 bits of the result */
//     ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
//     /* Say how many channels would be used by the sequencer */
//     ADC_InitStructure.ADC_NbrOfConversion = 1;


    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStructure.ADC_ExternalTrigConvEdge =  ADC_ExternalTrigConvEdge_None;
    ADC_InitStructure.ADC_ExternalTrigConv =      ADC_ExternalTrigConvEdge_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfConversion = 1;
    
    ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_9Cycles;
    
    ADC_CommonInit(&ADC_CommonInitStructure);
    /* Now do the setup */
    ADC_Init(ADC1, &ADC_InitStructure);

    /* Enable ADC1 */
    ADC_Cmd(ADC1, ENABLE);

//         /* Enable ADC1 reset calibaration register */
//         ADC_ResetCalibration(ADC1);
//         /* Check the end of ADC1 reset calibration register */
//         while(ADC_GetResetCalibrationStatus(ADC1));
//         /* Start ADC1 calibaration */
//         ADC_StartCalibration(ADC1);
//         /* Check the end of ADC1 calibration */
//         while(ADC_GetCalibrationStatus(ADC1));


    while(1)//attempts++ < MAX_ATTEMPTS) 
    {
        U32 i;
        // ������ ���, ����� ������� ��� ����� ��������, �� � ��������������
        for (i = 0; i < 32; i++)
        {
            // ������ ������� �����
            ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_3Cycles);
            // Start the conversion
            ADC_SoftwareStartConv(ADC1);
            // Wait until conversion completion
            while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
            // Get the conversion value
            modRnd_rand32 ^= (U32)((ADC_GetConversionValue(ADC1) << i) & (1 << i));
        }
        if (modRnd_rand32 != 0) // �������� �� 0
        {
#if MODRANDOM_TRUE_4096
            U32 j = 0;
            _rand[j] = modRnd_rand32;
            j++;
            if (j>= SIZE_)
                break;
#endif
            break;
        }
    }
#endif
		
#if BOARD_STM32EV103_V2
    extern ADC_HandleTypeDef hadc1;
    
    HAL_ADC_Start (&hadc1);
    while(1) 
    {
        U32 i;
        // ������ ���, ����� ������� ��� ����� ��������, �� � ��������������
        for (i = 0; i < 32; i++)
        {
            
            HAL_ADC_PollForConversion (&hadc1, 10);
            modRnd_rand32 ^= (U32)((HAL_ADC_GetValue (&hadc1) << i) & (1<<i)); //@todo!!!
        }
        if (modRnd_rand32 != 0) // �������� �� 0
        {
#if MODRANDOM_TRUE_4096
            U32 j = 0;
            _rand[j] = modRnd_rand32;
            j++;
            if (j>= SIZE_)
                break;
#endif
            break;
        }
         
    }
#endif
#if (BOARD_OSCILLOSCOPE_MKII || BOARD_EVAL_V1 || BOARD_STM32EV103 || BOARD_STM32F100_REBORN || BOARD_USBDEV_FLESHKA_VB)
	
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;     // ������ ����� �� ���
	ADC1->CR2 |= ADC_CR2_ADON;              // ������ ������� �� ���
	ADC1->CR2 |= ADC_CR2_TSVREFE;   // ������ ������� �� ����. ������ � ������ ����.
	ADC1->CR2 |= ADC_CR2_EXTSEL;    // ������ �������������� �� ��������� ���� swstart
	ADC1->CR2 |= ADC_CR2_EXTTRIG;   // �������� ������ �� �������� ������� (� ��� ��� ������)
	ADC1->SMPR1 |= ADC_SMPR1_SMP16; // ������ ����. ���-�� ������ (239.5) �� �������������� ��� 16 ������ ��� ���. ������
	ADC1->SQR3 &= ~ADC_SQR3_SQ1;
	ADC1->SQR3 |= ADC_SQR3_SQ1_4;    // �������� 16 (0b10000) ����� ��� 1 ��������������     (���-�� �������������� ��-��������� 1)

    while(1)
    {
        U32 i;
        //
        for (i = 0; i < 32; i++)
        {
        	ADC1->CR2 |= ADC_CR2_SWSTART; // ������ ��������������
			// ���� ����� ��������������
			while (!(ADC1->SR & ADC_SR_EOC)) {};
            modRnd_rand32 ^= (U32)((ADC1->DR << i) & (1 << i)); //TODO!!!
        }
        if (modRnd_rand32 != 0)
        {
            break;
        }

    }
#endif
    
#if (BOARD_STM32F4DISCOVERY || BOARD_STM32F411_REBORN || BOARD_F407EZT)
#if MODRANDOM_4096BIT_EN
    U32 j = 0;
#endif
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;     // ������ ����� �� ���
	ADC1->CR2 |= ADC_CR2_ADON;              // ������ ������� �� ���
	//ADC1->CR2 |= ADC_CR2_TSVREFE;   // ������ ������� �� ����. ������ � ������ ����.
	ADC1->CR2 |= ADC_CR2_EXTSEL;    // ������ �������������� �� ��������� ���� swstart
	//ADC1->CR2 |= ADC_CR2_EXTTRIG;   // �������� ������ �� �������� ������� (� ��� ��� ������)
    /*
	ADC1->SMPR1 |= ADC_SMPR1_SMP16; // ������ ����. ���-�� ������ (239.5) �� �������������� ��� 16 ������ ��� ���. ������
	ADC1->SQR3 &= ~ADC_SQR3_SQ1;
	ADC1->SQR3 |= 16; //ADC_SQR3_SQ1_4;    // �������� 16 (0b10000) ����� ��� 1 ��������������     (���-�� �������������� ��-��������� 1)
    */
    
    ADC1->SMPR2 |= ADC_SMPR2_SMP1_2; 
	ADC1->SQR3 &= ~ADC_SQR3_SQ1;
	ADC1->SQR3 = 1; //ADC_SQR3_SQ1_4;
    
    
    while(1)
    {
        U32 i;
        //
        for (i = 0; i < 32; i++)
        {
        	ADC1->CR2 |= ADC_CR2_SWSTART; // ������ ��������������
			// ���� ����� ��������������
			while (!(ADC1->SR & ADC_SR_EOC)) {};
            modRnd_rand32 ^= (U32)((ADC1->DR << i) & (1 << i)); //TODO!!!
        }
        if (modRnd_rand32 != 0)
        {
#if MODRANDOM_4096BIT_EN
            _rand4096 [j] = modRnd_rand32;
            j++;
            if (j>= MODRAND_4096BIT_SIZE)
                break;
#else
            break;
#endif
        }

    }
#endif
    
#if (BOARD_STM8MICRO_V1 || BOARD_STM8TEST || BOARD_STM8RGBTEMP)
    //modRnd_rand32 = 0x5648A654; return;
    ADC1_Init(ADC1_CONVERSIONMODE_SINGLE,
        ADC1_CHANNEL_12,
        ADC1_PRESSEL_FCPU_D2,
        ADC1_EXTTRIG_TIM, DISABLE,
        ADC1_ALIGN_RIGHT,
        ADC1_SCHMITTTRIG_CHANNEL12, DISABLE); // ENABLE
    ADC1_Cmd(ENABLE);
    while (1) 
    {
        U32 tmp32;
        // ������ ���, ����� ������� ��� ����� ��������, �� � ��������������
        for (U32 i = 0; i < 32; i++)
        {
            ADC1_StartConversion(); //��� ������� ���
            while (ADC1_GetFlagStatus(ADC1_FLAG_EOC) == RESET); //����
            tmp32 = (ADC1_GetConversionValue() & 0x00000001);
            modRnd_rand32 ^= tmp32;
            modRnd_rand32 = modRnd_rand32 << 1;
        }
        if (modRnd_rand32 != 0) // �������� �� 0
        {
#if MODRANDOM_TRUE_4096
            U32 j = 0;
            _rand[j] = modRnd_rand32;
            j++;
            if (j>= SIZE_)
                break;
#endif
            break;
        }
    }
#endif
    
    return modRnd_rand32;
}


void _rand_set_seed (U32 val)
{
     if (0 == val)
         while (1) {};
         modRnd_rand32 = val;
}

void _rand4096_set_seed (U32 *val)
{
#if MODRANDOM_4096BIT_EN
    U32 i;
    //TODO add check for non zero
    for (i = 0; i < MODRAND_4096BIT_SIZE; i++)
    {
        _rand4096 [i] = val [i];
    }
#endif //#if MODRANDOM_4096BIT_EN
}
