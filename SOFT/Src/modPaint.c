#include "modPaint.h"
#include "board.h"
#include "halPaint.h"

#if PAINT_FONT_x3y5
    #include "fonts\font_x3y5.h"
#endif
#if PAINT_FONT_Generic_8pt
    #include "fonts\font_Generic_8pt.h"
#endif
#if PAINT_FONT_Arial_14pt
    #include "fonts\font_Arial_14pt.h"
#endif
#if PAINT_FONT_Arial_20pt
    #include "fonts\font_Arial_20pt.h"
#endif
#if PAINT_FONT_Arial_120pt
    #include "fonts\font_Arial_120pt.h"
#endif
#if PAINT_FONT_BookmanOldStyle_120pt
    #include "fonts\font_BookmanOldStyle_120pt.h"
#endif
#if PAINT_FONT_OmniblackOutline_120pt
    #include "fonts\font_OmniblackOutline_120pt.h"
#endif


struct FontSettings_t
{ // ��������� �������
    //U8   header_offset;
    //U8   char_offset;
    U8             numFont;
    U8             width;
    U8             height;
    U8             vSpacing;
    U8             hSpacing;
    U16            offsetX;
    U16            offsetY;
    U16            maxCol;
    U16            maxRow;
    U32            offset; // �������� ��-������
    COORD             charX; // ������� ���������� �������
    COORD             charY;
    font_info_t        *pFontInfo;
    
    U8             type;
};

struct tsPaint
{
    COLOR                 fColor; // �������� ����
    COLOR                 bColor; // ���� ����
    struct FontSettings_t   font;
    U8                 orientation;

} sPaint;


#pragma pack(push, 1)
COLOR paintBuf [PAINT_BUF_SIZE]; // ����� ��� ��������, ����� ����������
#pragma pack(pop)


MSG paint_init (U8 mode)
{
    hal_paint_init (mode);
    return mode;
}


void paint_clearScreen (void)
{
    //paint_setColor (sPaint.bColor);
    hal_paint_cls (sPaint.bColor);
}


void paint_repaint (void) {
    hal_paint_repaint ();
}


void  paint_pixel (COORD x, COORD y)
{
#if 1//CHECK_BORDER
    if (x >= SCREEN_W  ||
        y >= SCREEN_H)
    {
        // error
    } else {
#endif
    hal_paint_setPixelColor (x, y, sPaint.fColor);
#if 1//CHECK_BORDER
    }
#endif
}


void paint_pixelColor (COORD x, COORD y, COLOR color)
{
#if 1//CHECK_BORDER
    if (x >= SCREEN_W  ||
        y >= SCREEN_H)
    {
        // error
    } else {
#endif
    hal_paint_setPixelColor (x, y, color);
#if 1//CHECK_BORDER
    }
#endif
}


void paint_setColor (COLOR color)
{
    sPaint.fColor = color;
    //halLCD_setColor (color);
}


COLOR     paint_getColor (void)
{
    return sPaint.fColor;
}


void paint_setBackgroundColor (COLOR color)
{
    sPaint.bColor = color;
}


COLOR paint_getBackgroundColor (void)
{
    return sPaint.bColor;
}



U8 paint_RGB888_RGB8 (U8 r, U8 g, U8 b)
{
    return (U8)((((r & 0xE0) >> 5) << 5) | (((g & 0xE0) >> 5) << 2) | ((b & 0xC0) >> 6));
}


COLOR paint_RGB888_RGB16 (U8 r, U8 g, U8 b)
{
    return (U16)((r & 0xF8) << 8) | (U16)((g & 0xFC) << 3) | (U16)(b >> 3);
}


void paint_RGB16_RGB888 (COLOR color, U8 *pColor)
{
    pColor[0] = ((color & 0x001F) << 3) | (color & 0x07);          /* Blue value */
    pColor[1] = ((color & 0x07E0) >> 3) | ((color >> 7) & 0x03);   /* Green value */
    pColor[2] = ((color & 0xF800) >> 8) | ((color >> 11) & 0x07);  /* Red value */
}


#if (LCD_COLOR > 1) //TODO
COLOR paint_blendColor (COLOR fg, COLOR bg, U8 alpha)
{
    U8 colorBuf[3];
    U16 r, g, b;
    U16 fg_ratio = alpha + 1;
    U16 bg_ratio;
    
    paint_RGB16_RGB888 (fg, &colorBuf[0]);
    bg_ratio = 256 - alpha;
    r = PAINT_RGB_COLOR_RED(fg) * fg_ratio;
    g = PAINT_RGB_COLOR_GREEN(fg) * fg_ratio;
    b = PAINT_RGB_COLOR_BLUE(fg) * fg_ratio;
    r += PAINT_RGB_COLOR_RED(bg) * bg_ratio;
    g += PAINT_RGB_COLOR_GREEN(bg) * bg_ratio;
    b += PAINT_RGB_COLOR_BLUE(bg) * bg_ratio;
    r /= 256;
    g /= 256;
    b /= 256;

    return paint_RGB888_RGB16 (r, g, b);
}
#endif


//@todo #ifdef CHECK_BORDER
void paint_bitmapMono (COORD x1, COORD y1, U16 xw, U16 yh, const U8 *buf) {
    U16 i, j, m;
    U8 ch;

    for (j = 0; j < yh; j++)
    {
        for (i = 0; i < xw; i++)
        {
            ch = *buf++;
            for (m = 0; m < 8; m++)
            {
                if (ch & 0x80) {
                    paintBuf[i * 8 + m] = sPaint.fColor;
                } else {
                    paintBuf[i * 8 + m] = sPaint.bColor;
                }
                ch = ch << 1;
            }
        }
        hal_paint_fillBlock //halLCD_fillBlockDMA 
        ((COORD)x1,
            (COORD)(y1 + j),
            xw * 8,
            1,
            &paintBuf[0]);
    }
}


void setBitmapColor (COORD x1, COORD y1, U16 xw, U16 yh, COLOR *buf) {
    hal_paint_fillBlock(x1, y1, xw, yh, buf);
}



#if PAINT_NEED_LINE
void paint_lineX (COORD x0, COORD y0, U16 w) {
#if CHECK_BORDER
    if (x0 >= SCREEN_W  ||
        w >= SCREEN_W  ||
        y0 >= SCREEN_H) {
        // error
    } else {
#endif
    hal_paint_fillBlockColor (x0, y0, w, 1, sPaint.fColor);
#if CHECK_BORDER
    }
#endif
}


void paint_lineY (COORD x0, COORD y0, U16 h) {
#if CHECK_BORDER
    if (x0 >= SCREEN_W  ||
        y0 >= SCREEN_H  ||
        h >= SCREEN_H)
    {
        // error
    } else {
#endif
    hal_paint_fillBlockColor (x0, y0, 1, h, sPaint.fColor);
#if CHECK_BORDER
    }
#endif
}


void paint_line (COORD x1, COORD y1, COORD x2, COORD y2) {
#if CHECK_BORDER
    if (x1 >= SCREEN_W  ||
        x2 >= SCREEN_W  ||
        y1 >= SCREEN_H ||
        y2 >= SCREEN_H  )
    {
        // error
    } else {
#endif
        S16 dy = 0; // all signed short!
        S16 dx = 0;
        S16 stepx = 0;
        S16 stepy = 0;
        S16 fraction = 0;

        dy = y2 - y1;
        dx = x2 - x1;
        if (dy < 0)
        {
            dy = -dy;
            stepy = -1;
        }
        else
            stepy = 1;

        if (dx < 0)
        {
            dx = -dx;
            stepx = -1;
        }
        else
            stepx = 1;
        dy <<= 1;
        dx <<= 1;

        paint_pixel (x1, y1);

        if (dx > dy)
        {
            fraction = dy - (dx >> 1);
            while (x1 != x2) {
                if (fraction >= 0)
                {
                    y1 += stepy;
                    fraction -= dx;
                }
                x1 += stepx;
                fraction += dy;
                paint_pixel (x1, y1);
            }
        }
        else
        {
            fraction = dx - (dy >> 1);
            while (y1 != y2)
            {
                if (fraction >= 0)
                {
                    x1 += stepx;
                    fraction -= dy;
                }
                y1 += stepy;
                fraction += dx;
                paint_pixel (x1, y1);
            }
        }
#if CHECK_BORDER
    }
#endif
}
#endif //PAINT_NEED_LINE


#if PAINT_NEED_RECT
void paint_rect (COORD x1, COORD y1, U16 xw, U16 yh) {
    U16 x2, y2;

    x2 = x1 + (xw - 1);
    y2 = y1 + (yh - 1);
#if CHECK_BORDER
    if (x1 >= SCREEN_W  ||
        x2 >= SCREEN_W  ||
        y1 >= SCREEN_H ||
        y2 >= SCREEN_H) 
    {
        // error
    } else {
#endif
        paint_lineX (x1, y1, xw);
        paint_lineX (x1, y2, xw);
        paint_lineY (x1, y1, yh);
        paint_lineY (x2, y1, yh);
#if CHECK_BORDER
    }
#endif
}


void paint_rectFill (COORD x1, COORD y1, U16 xw, U16 yh) {
#if CHECK_BORDER
    if (x1 >= SCREEN_W  ||
        xw >= SCREEN_W  ||
        y1 >= SCREEN_H || // @todo ������� �������� ����������!
        yh >= SCREEN_H)
    {
        // error
    } else {
#endif
        hal_paint_fillBlockColor (x1, y1, xw, yh, sPaint.fColor);
#if CHECK_BORDER
    }
#endif
}


#if PAINT_NEED_CIRCLE
void paint_rectRound (COORD x, COORD y, U16 w, U16 h, U16 r) {
    paint_lineX (x + r, y, w - 2 * r); // Top
    paint_lineX (x + r, y + h - 1, w - 2 * r); // Bottom
    paint_lineY (x, y + r, h - 2 * r); // Left
    paint_lineY (x + w - 1, y + r , h - 2 * r); // Right
    paint_circ (x+r    , y+r    , r, PAINT_QUARTERS_II); // draw four corners
    paint_circ (x+w-r-1, y+r    , r, PAINT_QUARTERS_I);
    paint_circ (x+w-r-1, y+h - r-1, r, PAINT_QUARTERS_IV);
    paint_circ (x+r    , y+h - r-1, r, PAINT_QUARTERS_III);
}


void paint_rectFillRound (COORD x, COORD y, U16 w, U16 h, U16 r) {
    paint_rectFill (x + r, y, w - 2 * r, h); // @todo
    //paint_setColor (COLOR_BLUE);
    paint_circFill (x+r    , y + r        , r, PAINT_QUARTERS_II); // draw four corners
    paint_circFill (x+w-r-1, y + r        , r, PAINT_QUARTERS_I);
    paint_circFill (x+w-r-1, y + h - r - 1, r, PAINT_QUARTERS_IV);
    paint_circFill (x+r    , y + h - r - 1, r, PAINT_QUARTERS_III);
    //paint_setColor (COLOR_GREEN);
    paint_rectFill (x, y+r, r, h-2*r);
    paint_rectFill (x+w-r-1, y+r, r, h-2*r);
}
#endif // PAINT_NEED_CIRCLE
#endif // PAINT_NEED_RECT


#if PAINT_NEED_CIRCLE
//������������ �������� ����������.
void paint_circ (COORD x, COORD y, U16 radius, MODPAINT_QUARTERS corner_type)
{
    S16 xc = 0; // signed int!
    S16 yc;
    S16 p;

#if CHECK_BORDER
    if (x >= SCREEN_W  ||
        y >= SCREEN_H ||
        radius >= SCREEN_W) {
        // error
    } else {
#endif
        yc = radius;
        p = 3 - (radius << 1);
        while (xc <= yc) {
            if (corner_type & PAINT_QUARTERS_I) {
                paint_pixel(x + xc, y - yc);
                paint_pixel(x + yc, y - xc);
            }
            if (corner_type & PAINT_QUARTERS_IV) {
                paint_pixel(x + xc, y + yc);
                paint_pixel(x + yc, y + xc);
            }
            if (corner_type & PAINT_QUARTERS_III) {
                paint_pixel(x - xc, y + yc);
                paint_pixel(x - yc, y + xc);
            }
            if (corner_type & PAINT_QUARTERS_II) {
                paint_pixel(x - xc, y - yc);
                paint_pixel(x - yc, y - xc);
            }
            if (p < 0) {
                p += (xc++ << 2) + 6;
            } else {
                p += ((xc++ - yc--) << 2) + 10;
            }
        }
#if CHECK_BORDER
    }
#endif
}


void paint_circFill (COORD x, COORD y, U16 radius, MODPAINT_QUARTERS corner_type) {
    S16 xc = 0; // signed int!
    S16 yc;
    S16 p;
#if CHECK_BORDER
    if (x >= SCREEN_W  ||
        y >= SCREEN_H ||
        radius >= SCREEN_W) {
        // error
    } else {
#endif
        yc = radius;
        p = 3 - (radius << 1);
        while (xc <= yc) {
            /**
            paint_lineX(x - xc, y + yc, xc + xc);
            paint_lineX(x - yc, y + xc, yc + yc);
            paint_lineX(x - xc, y - yc, xc + xc);
            paint_lineX(x - yc, y - xc, yc + yc);
            */
            if (corner_type & PAINT_QUARTERS_I) {
                paint_lineX (x, y - yc, xc);
                paint_lineX (x, y - xc, yc);
            }
            if (corner_type & PAINT_QUARTERS_IV) {
                paint_lineX (x, y + yc, xc);
                paint_lineX (x, y + xc, yc);
            }
            if (corner_type & PAINT_QUARTERS_III) {
                paint_lineX (x - xc, y + yc, xc);
                paint_lineX (x - yc, y + xc, yc);
            }
            if (corner_type & PAINT_QUARTERS_II) {
                paint_lineX (x - xc, y - yc, xc);
                paint_lineX (x - yc, y - xc, yc);
            }
            if (p < 0)
            {
                p += (xc++ << 2) + 6;
            } else {
                p += ((xc++ - yc--) << 2) + 10;
            }
        }
#if CHECK_BORDER
    }
#endif
}
#endif // PAINT_NEED_CIRCLE


#if PAINT_NEED_ELLIPS
void paint_ellipse (COORD X, COORD Y, COORD A, COORD B) {
	S16 Xc = 0, Yc = B;
	long A2 = (long)A*A, B2 = (long)B*B;
	long C1 = -(A2/4 + A % 2 + B2);
	long C2 = -(B2/4 + B % 2 + A2);
	long C3 = -(B2/4 + B % 2);
	long t = -A2 * Yc;
	long dXt = B2*Xc*2, dYt = -A2*Yc*2;
	long dXt2 = B2*2, dYt2 = A2*2;
    
    //LCD_pixel_mode = GDI_ROP_OR;
    
	while (Yc >= 0 && Xc <= A)
    {
		paint_pixel (X + Xc, Y + Yc);
		if (Xc != 0 || Yc != 0)
          paint_pixel (X - Xc, Y - Yc);
		if (Xc != 0 && Yc != 0)
        {
			paint_pixel (X + Xc, Y - Yc);
			paint_pixel (X - Xc, Y + Yc);
		}
		if (t + Xc*B2 <= C1 || t + Yc*A2 <= C3)
        {
			Xc++;
			dXt += dXt2;
			t   += dXt;
		} else if (t - Yc*A2 > C2) {
			Yc--;
			dYt += dYt2;
			t   += dYt;
		} else {
			Xc++;
			Yc--;
			dXt += dXt2;
			dYt += dYt2;
			t   += dXt;
			t   += dYt;
		}
	}
}
#endif


//subdivision rasterization
#ifdef PAINT_NEED_TRIANGLE
void paint_triangle (COORD x1, COORD y1, COORD x2, COORD y2, COORD x3, COORD y3)
{
#if CHECK_BORDER
    if (x1 >= SCREEN_W ||
        y1 >= SCREEN_H ||
        x2 >= SCREEN_W ||
        y2 >= SCREEN_H ||
        x3 >= SCREEN_W ||
        y3 >= SCREEN_H) {
        // error
    } else {
#endif
    paint_line (x1, y1, x2, y2);
    paint_line (x2, y2, x3, y3);
    paint_line (x3, y3, x1, y1);
#if CHECK_BORDER
    }
#endif
}


void paint_triangleFill_A (COORD x1, COORD y1, COORD x2, COORD y2, COORD x3, COORD y3)
{
    S32 x, y, addx, dx, dy;
    S32 P;
    U16 i;
    U32 a1, a2, b1, b2;
    
    if (y1 > y2)
    { b1 = y2; b2 = y1; a1 = x2; a2 = x1; }
    else
    { b1 = y1; b2 = y2; a1 = x1; a2 = x2; }
    dx = a2 -a1;
    dy = b2 - b1;
    if (dx < 0) dx=-dx;
    if (dy < 0) dy=-dy;
    x = a1;
    y = b1;
   
    if(a1 > a2)
        addx = -1;
    else
        addx = 1;
   
    if (dx >= dy)
    {
        P = 2*dy - dx;
        for (i = 0; i <= dx; ++i)
        {
            paint_line ((COORD)x, (COORD)y, x3, y3);
            if(P < 0)
            {
                P += 2*dy;
                x += addx;
            }
            else
            {
                P += 2*dy - 2*dx;
                x += addx;
                y++;
            }
        }
    }
    else
    {
        P = 2*dx - dy;
        for (i = 0; i <= dy; ++i)
        {
            paint_line ((COORD)x, (COORD)y, x3, y3);
            if (P < 0)
            {
                P += 2*dx;
                y++;
            }
            else
            {
                P += 2*dx - 2*dy;
                x += addx;
                y ++;
            }
        }
    }
}


//http://compgraphics.info/2D/triangle_rasterization.php
void __swap (COORD *a, COORD *b)
{
      COORD *t;
      *t = *a;
      *a = *b;
      *b = *t;
}
 

void paint_triangleFill (COORD x1, COORD y1, COORD x2, COORD y2, COORD x3, COORD y3)
{
#if CHECK_BORDER
    if (x1 >= SCREEN_W ||
        y1 >= SCREEN_H ||
        x2 >= SCREEN_W ||
        y2 >= SCREEN_H ||
        x3 >= SCREEN_W ||
        y3 >= SCREEN_H) {
        // error
    } else {
#endif
    // ������������� ����� p1(x1, y1),
    // p2(x2, y2), p3(x3, y3)
    if (y2 < y1) {
        __swap (&y1, &y2);
        __swap (&x1, &x2);
    } // ����� p1, p2 �����������
    if (y3 < y1) {
        __swap (&y1, &y3);
        __swap (&x1, &x3);
    } // ����� p1, p3 �����������
    // ������ p1 ����� �������
    // �������� ����������� p2 � p3
    if (y2 > y3) {
        __swap (&y2, &y3);
        __swap (&x2, &x3);
    }
    
//        Private Sub PaintTriangle(v1 As Vec2, v2 As Vec2, v3 As Vec2, c As Long)
// Dim t1 As Vec2, t2 As Vec2, t3 As Vec2
// Dim x As Long, y As Long, x1 As Long, x2 As Long
//     U32 x, y;
//     U32 x1, x2;
//   t1 = v1: t2 = v2: t3 = v3
// //   If t1.y > t3.y Then Swap t1, t3
// //   If t1.y > t2.y Then Swap t1, t2
// //   If t2.y > t3.y Then Swap t2, t3
//   For y = t1.y To t2.y - 1&
//     x1 = t1.x + (t2.x - t1.x) * (y - t1.y) / (t2.y - t1.y)
//     x2 = t1.x + (t3.x - t1.x) * (y - t1.y) / (t3.y - t1.y)
//     Line (x1, y)-(x2, y), c
//   Next y
//   For y = t2.y To t3.y - 1&
//     x1 = t2.x + (t3.x - t2.x) * (y - t2.y) / (t3.y - t2.y)
//     x2 = t1.x + (t3.x - t1.x) * (y - t1.y) / (t3.y - t1.y)
//     Line (x1, y)-(x2, y), c
//   Next y
// End Sub

// Private Sub Swap(v1 As Vec2, v2 As Vec2)
// Dim p As Vec2
//   p = v1
//   v1 = v2
//   v2 = p
// End Sub


    //paint_triangleFill_A (x1, y1, x2, y2, x3, y3);
    //paint_triangleFill_A (x3, y3, x1, y1, x2, y2);
    //paint_triangleFill_A (x3, y3, x2, y2, x1, y1);
#if CHECK_BORDER
    }
#endif
}

#endif // PAINT_NEED_TRIANGLE


#if PAINT_NEED_ELLIPS
void paint_ellipsFill (COORD origin_x, COORD origin_y, U16 w, U16 h, U8 corner_type)
{
    S16 hh = h * h;
    S16 ww = w * w;
    S16 hhww = hh * ww;
    S16 x0 = w;
    S16 dx = 0;
    S16 x, y;
    // do the horizontal diameter
    //for (int x = -w; x <= w; x++)
    paint_lineX (origin_x - w, origin_y, w * 2);

    // now do both halves at the same time, away from the diameter
    for (y = 1; y <= h; y++)
    {
        int x1 = x0 - (dx - 1);  // try slopes of dx - 1 or more
        for ( ; x1 > 0; x1--)
            if (x1*x1*hh + y*y*ww <= hhww)
                break;
        dx = x0 - x1;  // current approximation of the slope
        x0 = x1;

        for (x = -x0; x <= x0; x++)
        {
            paint_pixel (origin_x + x, origin_y - y);
            paint_pixel (origin_x + x, origin_y + y);
        }
    }
}
#endif // PAINT_NEED_ELLIPS


//---------------------------- TEXT -------------------------------
void paint_setFont (U8 num, PAINT_FONT_TYPE type) { // ����������� �����
    switch (num) {
#if PAINT_FONT_Generic_8pt
    case PAINT_FONT_Generic_8pt:
        sPaint.font.pFontInfo = (font_info_t *)Generic_8ptFontInfo;
        break;
#endif
#if PAINT_FONT_Arial_14pt
    case PAINT_FONT_Arial_14pt:
        sPaint.font.pFontInfo = (font_info_t *)Arial_14ptFontInfo;
        break;
#endif
#if PAINT_FONT_Arial_20pt
    case PAINT_FONT_Arial_20pt:
        sPaint.font.pFontInfo = (font_info_t *)Arial_20ptFontInfo;
        break;
#endif
#if PAINT_FONT_Arial_120pt
    case PAINT_FONT_Arial_120pt:
        sPaint.font.pFontInfo = (font_info_t *)Arial_120ptFontInfo;
    break;
#endif
#if PAINT_FONT_BookmanOldStyle_120pt
    case PAINT_FONT_BookmanOldStyle_120pt:
        sPaint.font.pFontInfo = (font_info_t *)BookmanOldStyle_120ptFontInfo;
        break;
#endif
#if PAINT_FONT_OmniblackOutline_120pt
    case PAINT_FONT_OmniblackOutline_120pt:
        sPaint.font.pFontInfo = (font_info_t *)OmniblackOutline_120ptFontInfo;
        break;
#endif
        //default: while(1); break; // ���������, ���� ��� ������ �� ����� ���
    }

    sPaint.font.numFont   = num;
    sPaint.font.width     = sPaint.font.pFontInfo->width; //pFont_settings[0];
    sPaint.font.height    = sPaint.font.pFontInfo->height; //pFont_settings[1];
    sPaint.font.vSpacing  = (sPaint.font.width + sPaint.font.pFontInfo->vSpacing);
    sPaint.font.hSpacing  = (sPaint.font.height + sPaint.font.pFontInfo->hSpacing);
    sPaint.font.offset    = sPaint.font.pFontInfo->offset;
    sPaint.font.maxCol    = (paint_getWidth() / sPaint.font.vSpacing)  - 0; // 1; // �������
    sPaint.font.maxRow    = (paint_getHeight() / sPaint.font.hSpacing) - 0; // 1; // ������
    sPaint.font.offsetX   = (paint_getWidth() - sPaint.font.maxCol * sPaint.font.vSpacing) / 2; // �������� �������������
    sPaint.font.offsetY   = (paint_getHeight() - sPaint.font.maxRow * sPaint.font.hSpacing) / 2;
    //sPaint.font.pFont     = &pFont[0];
    //sPaint.font.pFontInfo = pFontInfo;
    //sPaint.font.mashtab   = 1;
    sPaint.font.type = type;
    paint_gotoColRow (0, 0);
    
#if PAINT_FONT_x3y5
    sPaint.font.pFontInfo = (font_info_t *)console_font_4x6;
    
    sPaint.font.numFont   = num;
    sPaint.font.width     = 3; //sPaint.font.pFontInfo->width;
    sPaint.font.height    = 5; //sPaint.font.pFontInfo->height;
    sPaint.font.vSpacing  = 4; //(sPaint.font.width + sPaint.font.pFontInfo->vSpacing);
    sPaint.font.hSpacing  = 5; //(sPaint.font.height + sPaint.font.pFontInfo->hSpacing);
    sPaint.font.offset    = 6; //sPaint.font.pFontInfo->offset;
    sPaint.font.maxCol    = (paint_getWidth() / sPaint.font.vSpacing); // �������
    sPaint.font.maxRow    = (paint_getHeight() / sPaint.font.hSpacing); // ������
    sPaint.font.offsetX   = (paint_getWidth() - sPaint.font.maxCol * sPaint.font.vSpacing) / 2; // �������� �������������
    sPaint.font.offsetY   = (paint_getHeight() - sPaint.font.maxRow * sPaint.font.hSpacing) / 2;
    //sPaint.font.pFont     = &pFont[0];
    //sPaint.font.pFontInfo = pFontInfo;
    //sPaint.font.mashtab   = 1;
    //paint_gotoColRow (0, 0);
#endif
}


COORD paint_getMaxCol (void)
{
    return sPaint.font.maxCol;
}


COORD paint_getMaxRow (void)
{
    return sPaint.font.maxRow;
}


MSG paint_gotoColRow (COORD col, COORD row)
{
#if CHECK_BORDER
    if ((col >= paint_getMaxCol()) ||
         (row >= paint_getMaxRow())) {
        return FUNCTION_RETURN_ERROR;
    } else {
#endif
        sPaint.font.charX = sPaint.font.offsetX + (sPaint.font.vSpacing * col);
        sPaint.font.charY = sPaint.font.offsetY + (sPaint.font.hSpacing * row);
        return FUNCTION_RETURN_OK;
#if CHECK_BORDER
    }
#endif
}


COORD paint_getCursorCol (void) {
    return (sPaint.font.charX - sPaint.font.offsetX) / ( sPaint.font.vSpacing);
}


COORD paint_getCursorRow (void) {
    return (sPaint.font.charY - sPaint.font.offsetY) / (sPaint.font.hSpacing);
}


void paint_putChar (char c) {
    U8 ch;
    U32 smesh;
    const U8 *bitmap_ptr;
    const U8 *__bitmap_ptr;
    U8 tmp;
    U16 i, j, k, font_height_bytes, current_char_width;
#if PAINT_FONT_Generic_8pt
    if (PAINT_FONT_Generic_8pt == sPaint.font.numFont)
    {
        ch = (U8)c;
        if (192 <= (U8)ch) // ������������� '�'
            ch = (U8)(ch - (96 - 6)); //(6 - �������������)
        else
            ch = (U8)ch - 32;
        smesh = (U32)ch * sPaint.font.offset;
        for (j = 0; j < sPaint.font.width; j++)
        { // every column of the character
            ch = sPaint.font.pFontInfo->font_bitmap_array[smesh + j];
            for (i = 0; i < sPaint.font.height; i++)
            { // i = y
                if (ch & 0x01) {
                    paintBuf[i] = sPaint.fColor;
                } else {
                    paintBuf[i] = sPaint.bColor;
                }
                ch = ch >> 1; // @todo �������� ���������� ����� ���������
            }
            hal_paint_fillBlock (sPaint.font.charX + j,
                sPaint.font.charY,
                1,
                sPaint.font.height,
                &paintBuf[0]);
        }
    }
    else
    {
        // Get height in bytes
        font_height_bytes = (sPaint.font.height) / 8;
        if (((sPaint.font.height) % 8)) 
            font_height_bytes++;
        
        current_char_width = 0;
        if (' ' != (U8)c)
        {
            if ((U8)'�' <= (U8)c) // ������������� '�' - �������
                c = (U8)(c - (98)); //(6 - �������������)
            else
                c = (U8)c - sPaint.font.pFontInfo->start_char;
            
            // Get width of current character
            current_char_width = sPaint.font.pFontInfo->descr_array[c].char_width;
            // Get pointer to data
            bitmap_ptr = (const U8 *)&(sPaint.font.pFontInfo->font_bitmap_array[(sPaint.font.pFontInfo->descr_array[c].char_offset)]);
            __bitmap_ptr = bitmap_ptr; // Copy to temporary

            for (k = 0; k < current_char_width; k++)
            { // Width of character
                for (i = 0; i < font_height_bytes; i++)
                { // Height of character
                    tmp = *__bitmap_ptr; // Get data to "shift"
                    __bitmap_ptr += current_char_width;
                    for (j = 0; j < 8; j++)
                    { // Push byte
                        if ((tmp & 0x01))
                        {
                        //    if (vram_put_point(__p, mode) == GRAPH_ERROR) err = GRAPH_ERROR;};
                            paintBuf[i * 8 + j] = sPaint.fColor;
                        } else {
                            paintBuf[i * 8 + j] = sPaint.bColor;
                        }
                        tmp >>= 1;
                    }
                }
                hal_paint_fillBlock (sPaint.font.charX + k,
                    sPaint.font.charY,
                    1,
                    sPaint.font.height,
                    &paintBuf[0]);
                __bitmap_ptr = ++bitmap_ptr;
            }
        }
        //paint_setBackgroundColor (sPaint.bgColor);
        hal_paint_fillBlockColor (sPaint.font.charX + current_char_width, // clear other
            sPaint.font.charY,
            sPaint.font.width - current_char_width,
            sPaint.font.height,
            sPaint.bColor);
    }
#endif
}


void  paint_putCharColRow (COORD col, COORD row, char c)
{
    if (FUNCTION_RETURN_OK == paint_gotoColRow (col, row))
    {
        paint_putChar (c);
    }
}


void paint_putStr (const char *str)
{
    U16 i = 0;
    COORD x, y;

    x = paint_getCursorCol ();
    y = paint_getCursorRow ();
    while ('\0' != str[i]) {
        if ('\r' == str[i]) {
            x = 0;
            paint_gotoColRow (x, y);
        } else if ('\n' == str[i]) {
            if (++y >= paint_getMaxRow ())
                break;
            else
                paint_gotoColRow (x, y);
        } else { // symbol
            if (FUNCTION_RETURN_OK == paint_gotoColRow (x, y)) {
                paint_putChar (str[i]);
            }
            if (++x >= paint_getMaxCol ()) {
                x = 1; //0; TODO!!!!!!!!!!!!!!!!!!!
                if (++y >= paint_getMaxRow ())
                    break;
            }
            if (FUNCTION_RETURN_OK != paint_gotoColRow (x, y)) {
                break;
            }
        }
        i++;
    }
}


void paint_putCharAuto (char ch)
{
    char pf[2];
    
    pf[0] = ch;
    pf[1] = 0;
    paint_putStr (pf);
}


void paint_putStrColRow (COORD x, COORD y, const char *str)
{
    paint_gotoColRow (x, y);
    paint_putStr (str);
}


void paint_strClearRow (COORD row) {
    COORD i = 0;

    for (i = 0; i < paint_getMaxCol(); i++) {
        if (FUNCTION_RETURN_OK == paint_gotoColRow (i, row))
        {
            paint_putChar (' ');
        }
    }
}


COORD paint_putCharXY  (COORD x, COORD y, char c)
{
    U8 ch;
    U32 smesh;
    const U8 *bitmap_ptr;
    const U8 *__bitmap_ptr;
    unsigned char tmp;
    U16 i, j, k, font_height_bytes, current_char_width;

#if PAINT_FONT_x3y5
    current_char_width = sPaint.font.width;
    if (1)//PAINT_FONT_Generic_8pt == sPaint.font.numFont)
    {
        ch = (U8)c;
        //if (192 <= (U8)ch) // ������������� '�'
        //    ch = (U8)(ch - (98 - 6)); //(6 - �������������)
        //else
        //    ch = (U8)ch - 31;
        smesh = (U32)ch * sPaint.font.offset;
        for (j = 0; j < sPaint.font.height; j++)
        { // every column of the character
            //ch = sPaint.font.pFontInfo->font_bitmap_array[smesh + j];
            ch = console_font_4x6[smesh + j];
            for (i = 0; i < sPaint.font.width; i++) 
            { // i = y
                if (ch & 0x40)
                {
                    //paintBuf[i] = sPaint.fColor;
                    paint_pixel_set (x + i, y + j);
                } else {
                    //paintBuf[i] = sPaint.bColor;
                    paint_pixel_clr (x + i, y + j);
                }
                ch = ch << 1; // @todo �������� ���������� ����� ���������
            }
            //halLCD_fillBlock (x + j, y,
            //    1, sPaint.font.height,
            //    &paintBuf[0]);
        }
    }
    
    return x + sPaint.font.vSpacing;// + sPaint.font.pFontInfo->hSpacing;
#endif
#if PAINT_FONT_Generic_8pt
    current_char_width = 0;
    if (PAINT_FONT_Generic_8pt == sPaint.font.numFont)
    {
        ch = (U8)c;

        if (192 <= (U8)ch) // ������������� '�'
            ch = (U8)(ch - (96 - 6)); //(6 - �������������)
        else
            ch = (U8)ch - 32;

        smesh = (U32)ch * sPaint.font.offset;

        current_char_width = sPaint.font.width;
        for (j = 0; j < current_char_width; j++)
        { // every column of the character
            ch = sPaint.font.pFontInfo->font_bitmap_array[smesh + j];
            for (i = 0; i < sPaint.font.height; i++)
            { // i = y
                if (ch & 0x01)
                {
                    paintBuf[i] = sPaint.fColor;
                } else {
                    paintBuf[i] = sPaint.bColor;
                }
                ch = ch >> 1; // @todo �������� ���������� ����� ���������
            }
            hal_paint_fillBlock (x + j, y,
                1, sPaint.font.height,
                &paintBuf[0]);
        }
    }
    else
    {
        // Get height in bytes
        font_height_bytes = (sPaint.font.height) / 8;
        if (((sPaint.font.height) % 8))
            font_height_bytes++;
        current_char_width = 0;
        if (' ' != (U8)c)
        {
            if (192 <= (U8)c) // ������������� '�' - ������� (U8)'�'
                c = (U8)(c - (98));//98
            else
                c = (U8)c - sPaint.font.pFontInfo->start_char;

            // Get width of current character width
            current_char_width = sPaint.font.pFontInfo->descr_array[c].char_width;
            
            // Get pointer to data
            bitmap_ptr = (const U8 *)&(sPaint.font.pFontInfo->font_bitmap_array[(sPaint.font.pFontInfo->descr_array[c].char_offset)]);
            __bitmap_ptr = bitmap_ptr; // Copy to temporary ptr
            for (k = 0; k < current_char_width; k++)
            { // Width of character
                for (i = 0; i < font_height_bytes; i++)
                { // Height of character
                    tmp = *__bitmap_ptr; // Get data to "shift"
                    __bitmap_ptr += current_char_width;
                    for (j = 0; j < 8; j++) { // Push byte
                        if ((tmp & 0x01))
                        {
                        //    if (vram_put_point(__p, mode) == GRAPH_ERROR) err = GRAPH_ERROR;};
                            paintBuf[i * 8 + j] = sPaint.fColor;
                        } else {
                            paintBuf[i * 8 + j] = sPaint.bColor;
                        }
                        tmp >>= 1;
                    }
                }
                hal_paint_fillBlock (x + k, y,
                    1, sPaint.font.height,
                    &paintBuf[0]);
                __bitmap_ptr = ++bitmap_ptr;
            }
        }
    }
    if (PAINT_FONT_MS == sPaint.font.type) // return coord for next symbol
        return (x + sPaint.font.width + sPaint.font.pFontInfo->hSpacing);
    else
        return (x + current_char_width + sPaint.font.pFontInfo->hSpacing);
#endif
}


COORD paint_putStrXY (COORD x, COORD y, const char *str)
{
    while ('\0' != *str)
    {
        x = paint_putCharXY (x, y, *str++);
    }
    return x;
}
