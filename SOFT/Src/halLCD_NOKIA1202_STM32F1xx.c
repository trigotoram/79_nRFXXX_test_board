
// https://github.com/adafruit/Adafruit-PCD8544-Nokia-5110-LCD-library/blob/master/Adafruit_PCD8544.h

#include "halPaint.h"
#include "board.h"
#include "modPaint.h"


#include <string.h> // for memset


#if (LCD_NOKIA1202)

#define SCREEN_VIRTUAL_W                98
#define SCREEN_VIRTUAL_H                72 //


// ��������� ���������� �������
struct halLCD_lcdStruct_t {
	U8       orientation; // ����������
    COLOR       color;
} halLCD_lcdStruct;


// ��������������� �������� ��� ��������� ������������� LCD
void halLCD_delay (U32 delay)
{
    delay = delay * 1000UL;
    while (delay){ delay--; }
}


void halLCD_setColor (COLOR color)
{
    halLCD_lcdStruct.color = color;
}


// the memory buffer for the LCD
volatile U8 screen_buf [SCREEN_VIRTUAL_W * SCREEN_VIRTUAL_H / 8] = 
{
// bitmap 98x72 from the file: 
// cake.bmp
// created at 04.11.2017 15:55:06
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x60,0xe0,0xc0,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,

       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x80,0xc0,0xc0,0xe0,0xe0,0xe0,0x80,0x80,0xa0,0x10,0x0b,0x0f,
       0x06,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,

       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,0x80,0x80,0x80,0x20,0x78,0xfc,
       0x06,0x00,0x3c,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x00,
       0x00,0x18,0xb8,0xf8,0xe8,0x48,0x0c,0x04,0x04,0x04,0x04,0x84,0x84,0xc4,0x46,0x42,
       0x22,0x22,0x92,0x92,0xda,0xcb,0xed,0xe5,0xf5,0xf3,0xfb,0xf9,0xf9,0xf8,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,

       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x06,0x0c,0x08,0x10,0x30,0x20,0x21,
       0x63,0x40,0x40,0x46,0x4d,0x59,0x73,0xf3,0xdb,0xcb,0x43,0x41,0x41,0x41,0x20,0x20,
       0x94,0x92,0xdb,0xc9,0xe8,0xe4,0xf4,0xf2,0xf2,0xf9,0xf9,0xfd,0xfc,0xfe,0xfe,0xff,
       0xff,0xff,0x7f,0x7f,0x3f,0x3f,0x1f,0x1f,0x8f,0x8f,0xcf,0xc7,0xe7,0xe3,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,

       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xfe,0xfe,0xff,0xff,
       0x7f,0x7f,0x3f,0x3f,0x1f,0x1f,0x9f,0x8f,0xcf,0xc7,0xe7,0xe3,0xe3,0xf1,0xf1,0xf8,
       0xf8,0xfc,0xfc,0xfe,0xfe,0xff,0xff,0xff,0x7f,0x7f,0x3f,0x3f,0x1f,0x1f,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,

       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xf1,0xf9,0xf8,0xfc,
       0xfc,0xfc,0xfe,0xfe,0xff,0xff,0x7f,0x7f,0x3f,0x3f,0x3f,0x1f,0x9f,0x8f,0xcf,0xc7,
       0xe7,0xe3,0xf3,0xf1,0xf9,0xf9,0xf8,0xfc,0xfc,0xfe,0xfe,0xff,0xff,0x7f,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,

       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xcf,0xc7,0xe7,0xe3,
       0xf3,0xf3,0xf1,0xf9,0xf8,0xfc,0xfc,0xfe,0xfe,0xff,0xff,0xff,0x7f,0x7f,0x3f,0x3f,
       0x1f,0x1f,0x0f,0x0f,0x0f,0x07,0x07,0x03,0x03,0x01,0x01,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,

       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x03,0x06,0x0c,0x08,0x10,0x10,0x30,
       0x20,0x20,0x20,0x20,0x60,0x40,0x40,0x40,0x40,0x40,0x40,0x60,0x3f,0x3f,0x1f,0x1f,
       0x1f,0x0f,0x0f,0x07,0x07,0x03,0x03,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,

       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
       0x00,0x00,
};

    

//-------------------------------------------------
// --- PCD8544 COMMANDS ---
#define CMD_NOOP               0x00
#define CMD_FS                 0x20
#define CMD_SET_Y              0x40
#define CMD_SET_X              0x80
 


void _LCD_spi (U16 data)
{
    U16 bit;
    
    for (bit = 0x0100; bit; bit = bit >> 1)
    {
        LCD_CLK_L;
        __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); //hhalLCD_delay (1);
        if (0x0100 & data)
        {
            LCD_DAT_H;
        }
        else
        {
            LCD_DAT_L;
        }
        data = data << 1;
        __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();//hhalLCD_delay (1);
        LCD_CLK_H;
        __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();//hhalLCD_delay (1);
    }
}


void _LCD_spiWrite (U16 c)
{
	LCD_CS_L;
    __NOP(); __NOP(); __NOP(); __NOP(); //halLCD_delay (1);
	_LCD_spi (c);
    __NOP(); __NOP(); __NOP(); __NOP(); //halLCD_delay (1);
	LCD_CS_H;
}


void halLCD_sendData (U8 c)
{
    _LCD_spi (c | 0x0100);
}


/*
void _LCD_setContrast (U8 val)
{
    if (val > 0x7F)
    {
        val = 0x7F;
    }

}
*/

void hal_paint_init (U8 mode)
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRL &= ~(GPIO_CRL_CNF3 | GPIO_CRL_CNF4 | GPIO_CRL_CNF5 | GPIO_CRL_CNF6);
    GPIOB->CRL |= (GPIO_CRL_MODE3 | GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6);
    
    LCD_CLK_L;
    LCD_CS_H;
    LCD_RES_L;
    halLCD_delay (100);
    LCD_RES_H;
    
    _LCD_spiWrite (0x00E2); // Reset
    halLCD_delay (100);
    
    _LCD_spiWrite (0x00E3); // NOP
    halLCD_delay (100);
    
    _LCD_spiWrite (0x00E3); // NOP
    halLCD_delay (100);
    
    _LCD_spiWrite (0x00A4); // Power saver off
    halLCD_delay (100);
    
    _LCD_spiWrite (0x002F); // Power control set
    halLCD_delay (100);

    _LCD_spiWrite (0x00A0); //X - Segment driver direction select - normal
    halLCD_delay (100);
    
    _LCD_spiWrite (0x00C0); //Y - Common driver direction select - inverse
    halLCD_delay (100);
    
    //!!!
    _LCD_spiWrite (0x00A6); // color - normal Positive - A7, Negative - A6
    halLCD_delay (100);
    //!!!
    //lcd_write(COMMAND, 0x91); // Contrast 0x80...0x9F
    _LCD_spiWrite (0x0080 | 25); //0-31
    //lcd_write(COMMAND, 0xA6); // 
    
    _LCD_spiWrite (0x00AF);   // LCD display on
    halLCD_delay (100);

    hal_paint_update ();
    
    HAL_Delay (1000);
}


void     hal_paint_setPixelColor (COORD x, COORD y, COLOR color)
{
    if ((x >= SCREEN_VIRTUAL_W) || (y >= SCREEN_VIRTUAL_H))
        return;
    if (COLOR_WHITE != color) // x is which column
        screen_buf[x + (y / 8) * SCREEN_VIRTUAL_W] |=  (1 << (y % 8));  
    else
        screen_buf[x + (y / 8) * SCREEN_VIRTUAL_W] &= ~(1 << (y % 8)); 
}


void hal_paint_setPixel (COORD x, COORD y)
{
    if ((x >= SCREEN_VIRTUAL_W) || (y >= SCREEN_VIRTUAL_H))
        return;
    if (COLOR_WHITE != halLCD_lcdStruct.color) // x is which column
        screen_buf[x + (y / 8) * SCREEN_VIRTUAL_W] |=  (1 << (y % 8));  
    else
        screen_buf[x + (y / 8) * SCREEN_VIRTUAL_W] &= ~(1 << (y % 8)); 
}


COLOR hal_paint_getPixel (COORD x, COORD y)
{
    if ((x >= SCREEN_VIRTUAL_W) || (y >= SCREEN_VIRTUAL_H))
        return COLOR_WHITE;
    if (COLOR_WHITE != ((screen_buf[x + (y / 8) * SCREEN_VIRTUAL_W] >> (y % 8)) & 0x01))
        return COLOR_BLACK;
    else
        return COLOR_WHITE;    
}


void hal_paint_gotoxy (U8 x, U8 y)
{
	_LCD_spiWrite (0x00B0 | (y & 0x0F));
	_LCD_spiWrite (0x0010 | (x >> 0x04));
	_LCD_spiWrite (0x000F & x);
}


void hal_paint_update (void)
{
    U8 col, maxcol, p;
#if LCD_ENABLE_PARTIAL_UPDATE
    static U8 halLCD_cnt = 0;

    if (++halLCD_cnt >= (SCREEN_VIRTUAL_H / 8))
        halLCD_cnt = 0;
    p = halLCD_cnt;
#else
    for (p = 0; p < (SCREEN_VIRTUAL_H / 8); p++)
    {
#endif
        // start at the beginning of the row
        //TODO check this trash!
        col = 0;
        maxcol = SCREEN_VIRTUAL_W - 1;
        U8 buff = 0;
        _LCD_spiWrite (0x00B0 | p);
        buff = 0 >> 4;
        _LCD_spiWrite (0x0010 | buff);
        buff = 0 & 0x0F;
        _LCD_spiWrite (buff);
    
        LCD_CS_L;
        for (; col <= maxcol; col++)
        {
            halLCD_sendData (screen_buf [(SCREEN_VIRTUAL_W * p) + col]);
        }
        LCD_CS_H;
        
#if LCD_ENABLE_PARTIAL_UPDATE
#else
    }
#endif
}


// clear everything
void hal_paint_cls (COLOR color)
{
    U8 c;
    if (COLOR_WHITE == color)
        c = 0x00;
    else
        c = 0xFF;
    
    memset ((void *)&screen_buf[0], c, SCREEN_VIRTUAL_W * SCREEN_VIRTUAL_H / 8);
}


// ������ ������ ����������� ����� �������� � � ����� � ���
void     hal_paint_fillBlock (COORD x, COORD y, U16 w, U16 h, COLOR *buf)
{
    COORD xx, yy;
    
    for (xx = 0; xx < w; xx++)
        for (yy = 0; yy < h; yy++)
            hal_paint_setPixelColor (x + xx, y + yy, *buf++);
}


void     hal_paint_fillBlockColor (COORD x, COORD y, U16 w, U16 h, COLOR color)
{
    U8 c;
    if (COLOR_WHITE == color)
        c = 0x00;
    else
        c = 0xFF;
    memset ((void *)&screen_buf[0], c, SCREEN_VIRTUAL_W * SCREEN_VIRTUAL_H / 8);
}


void halLCD_setOrientation (U8 orient)
{

}


void hal_paint_draw_batt (COORD x, COORD y, U8 percent)
{
#define BAT_N_SEG                       6
    hal_paint_gotoxy (x,y);

    if(percent < 0) percent = 0;
    int active = 1 + (BAT_N_SEG * percent) / 100;

    for (U8 i =0; i < BAT_N_SEG; i++)
    {
        if (i < active)
        {
            _LCD_spiWrite (0x007F);
            _LCD_spiWrite (0x007F);
            _LCD_spiWrite (0x0000);
        }
        else
        {
            _LCD_spiWrite (0x0041);
            _LCD_spiWrite (0x0041);
            _LCD_spiWrite (0x0041);
        }
    }
    _LCD_spiWrite (0x007F); // tail
    _LCD_spiWrite (0x001C);
}

#endif
