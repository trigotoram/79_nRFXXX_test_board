/**
 * @file    drvBME280_H.h
 * @author  Ht3
 * @date    04.11.2017  14.12
 * @version V1.0.0
 * @brief 

S16 calibration [12];
S32 temperature;
S32 pressure; 
U8 humidity;

 */
 

#ifndef DRVBME280_H
#define	DRVBME280_H 20171104

#include "board.h"


#ifdef	__cplusplus
extern "C" {
#endif
    
MSG drvBME280_calibration (S16 *calibration);

MSG drvBME280_get (S16 *calibration, S32 *temperature, S32 *pressure, U8 *humidity);

S32 drvBME280_get_altitude (S32 pressure);


#ifdef	__cplusplus
}
#endif

#endif	/** DRVBME280_H */
