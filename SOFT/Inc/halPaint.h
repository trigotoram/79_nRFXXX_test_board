/*
 * @file
 * @author  Ht3h5793
 * @date    29.09.2014
 * @version V6.5.7
 * @brief
 * @todo
 */

#ifndef HALSCREEN_H // �����������, include guard
#define	HALSCREEN_H 20170205 /* Revision ID */

#include "board.h"
#include "colors.h"

// !!!WARNING!!! not all LCD!
enum LCD_TYPE_DRAW {
  GDI_ROP_COPY,
  GDI_ROP_XOR,
  GDI_ROP_AND,
  GDI_ROP_OR,
  
  GDI_INVERSE
};


#ifdef	__cplusplus
extern "C" {
#endif
//������������ �������
void    hal_paint_init (U8 mode);
void    hal_paint_deinit (U8 mode);

void    hal_paint_set_color (COLOR color);
COLOR   hal_paint_get_color (void);
void    hal_paint_cls (COLOR color);

void    hal_paint_setPixelColor (COORD x, COORD y, COLOR color);
COLOR   hal_paint_getPixel (COORD x , COORD y);

void    hal_paint_fill_block_color (COORD x, COORD y, U16 w, U16 h, COLOR color);

void    hal_paint_fill_block (COORD x, COORD y, U16 w, U16 h, COLOR *color);

void    hal_paint_fillScreen (COLOR color);

void    hal_paint_setOrientation (U8 orient);

U16     hal_paint_getWidth(void);
U16     hal_paint_getHeight(void);

/** ����� ������� ���� ����, ��������� ���� */
void    hal_paint_setLayer (U8 num);
void    hal_paint_paintLayer (U8 num);
void    hal_paint_update (void);

void    hal_paint_set_contrast (U8 );

void    hal_paint_setBackLight (U8 value);

//trash
void _paint_rect (COORD x1, COORD y1, U16 xw, U16 yh);
void        hal_paint_layer_copy (U8 layerA, U8 layerB);

void        hal_paint_WriteDMA (COORD x, COORD y, U16 w, U16 h);


void paint_pixel_set (COORD x, COORD y);
void paint_pixel_clr (COORD x, COORD y);


//U16 paint_RGB888_RGB16 (U8 r, U8 g, U8 b);

void hal_paint_inverase_mode (BOOL mode);
void hal_paint_picture (void);

void        halLCD_writeCommand (U8 com);
void        halLCD_writeData8bit (U8 data);
void        halLCD_writeData16bit (U16 data);
void hal_paint_gotoxy (U8 x, U8 y);

#ifdef	__cplusplus
}
#endif

#endif	/* HALLCD_H */
