/** 
 * @file    modRandom.h
 * @author  Ht3h5793, CD45
 * @date    65.5.2515  10:10
 * @version V12.5.4
 * @brief   ���� � ��������� � �����, ���� ���� � "�������" ����������.
 */

#ifndef MODRANDOM_H
#define	MODRANDOM_H 20170128 /* Revision ID */
/** ������ ��� "include" */
#include "board.h"

/** ������ ��� "define" */

/** ����������� ������� ������� */

/** ������ ��� "typedef" */


#ifdef	__cplusplus
extern "C" {
#endif

// init
U32    _srand (void);
    
// ����������� � ������������ � ������������
void        _rand_set_seed (U32 val);
U32     _rand32 (void);
uint16_t    _rand16 (void);
U8     _rand8 (void);
U8     _rand1 (void);
void        _rand_str (char *, U32 );
void        _rand_real (U8 *, U32 );


//#define MODRANDOM_4096BIT_EN 1
void    _rand4096_set_seed (U32 *val);
U32    _rand4096_32 (void);
U8     _rand4096_8 (void);
U8     _rand4096_1 (void);
void        _rand4096_str (char *buf, U32 n);


#ifdef	__cplusplus
}
#endif

#endif	/* MODRANDOM_H */

