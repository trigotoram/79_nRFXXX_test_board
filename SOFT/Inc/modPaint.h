/*
 * @file    LCD grafic
 * @author  Ht3h5793
 * @date    05.03.2014
 * @version V13.8.6
 * @brief
 *
 * ������������ TheDotFactory
 *
 *

 �������� ����������� ���������
// "!"#$%&'()*+,-./0123456789:;<=>?@[\]^_`"
// "The quick brown fox jumps over the lazy dog."
// "����� [��] ��� ���� ����� ����������� ����� �� ����� ���."

//     paint_setBackgroundColor (COLOR_BLUE);
//     paint_setFont (PAINT_FONT_Generic_8pt, PAINT_FONT_MS);
// //     paint_strClearRow (7);
// //     paint_strClearRow (8);
// //     paint_strClearRow (9);
// //     paint_strClearRow (10);
// //     paint_strClearRow (11);
// //     paint_strClearRow (12);
//     paint_putStrXY (0, 256, (char *)" !\"#$%&'()*+,-./0123456789");
//     paint_putStrXY (0, 288, (char *)":;<=>?@[\]^_`{|}~");
//     paint_putStrXY (0, 320, (char *)"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
//     paint_putStrXY (0, 352, (char *)"abcdefghijklmnopqrstuvwxyz");
//     paint_putStrXY (0, 384, (char *)"���v???��� �����������?���������");
//     paint_putStrXY (0, 416, (char *)"��������������������������������");

   /// ������� �� �������� ��� ������ http://c-site.h1.ru/infa/bmp_struct.htm
     // http://code.google.com/p/lcd-image-converter/

	 
	 
    paint_init (SCREEN_ORIENTATION_GLOBAL);
    paint_setBackgroundColor (COLOR_BLACK);
    paint_clearScreen ();
    paint_setColor (COLOR_WHITE);
    paint_setFont (PAINT_FONT_Generic_8pt, PAINT_FONT_MS);

#else
    gfxInit();
    gdispSetOrientation (GDISP_ROTATE_90);  // flip by 90 degrees
    gwinSetDefaultFont (gdispOpenFont("*"));
	gwinSetDefaultStyle (&WhiteWidgetStyle, FALSE);
    my_font = gdispOpenFont("*");
    gdispClear (COLOR_BACKGRAUND);
    
#endif
    paint_putStrColRow (0, 2, "I2C start!");
    halI2C_init ();
    
    xsprintf (_str, "ID0:0x%08X ", UID [0]);
    paint_putStrColRow (0, 7, _str); 
    xsprintf (_str, "ID1:0x%08X ", UID [1]);
    paint_putStrColRow (0, 8, _str); 
    xsprintf (_str, "ID2:0x%08X ", UID [2]);
    paint_putStrColRow (0, 9, _str); 
	
	
*/

#ifndef MODPAINT_H
#define	MODPAINT_H 20170226 /* Revision ID */

#include "board.h"
#include "colors.h"

/* Point mode type */
typedef enum {
    PAINT_PIXEL_CLR = 0,
    PAINT_PIXEL_SET = 1,
    PAINT_PIXEL_XOR = 2	// Note: PIXEL_XOR mode affects only to vram_put_point().
                    // Graph functions only transmit parameter to vram_put_point() and result of graph functions
                    // depends on it's implementation. However Line() and Bar() functions work well.
} MODPAINT_point_mode;

/** quarters (for circle)
  * II  | I
  * III | IV
  */
typedef enum  {
    PAINT_QUARTERS_I   = 0x01,
    PAINT_QUARTERS_II  = 0x02,
    PAINT_QUARTERS_III = 0x04,
    PAINT_QUARTERS_IV  = 0x08,
    PAINT_QUARTERS_ALL = 0x0F,

} MODPAINT_QUARTERS;

typedef enum {
    PAINT_FONT_MS = 0, //monospaced
    PAINT_FONT_PP = 1 //proportional
        
} PAINT_FONT_TYPE;

/* Font descriptor type */
typedef struct {
    U16 char_width;
    U16 char_offset;
} font_descriptor_t;

typedef struct {
    U16 width; // max width
    U16 height; // height
    U16 vSpacing; // vSpacing ���������� ����� ���������
    U16 hSpacing; // hSpacing
    U16 offset; // offset
    U16 rezerv_1; // rezerv
    U16 rezerv_2; // rezerv

    unsigned char start_char;
    unsigned char end_char;
    const font_descriptor_t *descr_array;
    const unsigned char *font_bitmap_array;
} font_info_t;



#define SEG_LENGHT       17 //������ ������������ ����������� ������


#ifdef	__cplusplus
extern "C" {
#endif

//------------------------ PAINT H-level -------------------------
#define paint_getWidth()                SCREEN_W
#define paint_getHeight()               SCREEN_H

MSG     paint_init (U8 mode);


void    paint_setColor (COLOR color);
COLOR   paint_getColor (void);
void    paint_setBackgroundColor (COLOR color);
COLOR   paint_getBackgroundColor (void);

void    paint_clearScreen (void);

void    paint_pixel (COORD x, COORD y);
void    paint_pixelColor (COORD x, COORD y, COLOR color);
void    paint_pixelBig (COORD x, COORD y, U16 size);

void        paint_lineX (COORD x0, COORD y0, U16 w);
void        paint_lineY (COORD x0, COORD y0, U16 h);
void        paint_line (COORD x1, COORD y1, COORD x2, COORD y2);
//void paint_lineWidth (COORD x1, COORD y1, COORD x2, COORD y2, U16 width);

void        paint_rect (COORD x1, COORD y1, U16 xw, U16 yh);
void        paint_rectFill (COORD x1, COORD y1, U16 xw, U16 yh);

void        paint_circ (COORD x, COORD y, U16 radius, MODPAINT_QUARTERS corner_type);
void        paint_circFill (COORD x, COORD y, U16 radius, MODPAINT_QUARTERS corner_type);

// Draw a rounded rectangle
void        paint_rectRound (COORD x, COORD y, U16 w, U16 h, U16 r);
// Fill a rounded rectangle
void        paint_rectFillRound (COORD x, COORD y, U16 w, U16 h, U16 r);

void        paint_bitmapMono (COORD x1, COORD y1, U16 w, U16 h, const U8 *buf);
void        setBitmapColor (COORD x1, COORD y1, U16 w, U16 h, COLOR  *buf);

void        paint_triangle (COORD x0, COORD y0, COORD x1, COORD y1, COORD x2, COORD y2);
void        paint_triangleFill (COORD x0, COORD y0, COORD x1, COORD y1, COORD x2, COORD y2);

void        paint_ellipse (COORD X, COORD Y, COORD A, COORD B);
void        paint_ellipsFill (COORD x, COORD y, U16 w, U16 h, U8 corner_type);

// Pass 8-bit (each) R,G,B, get back 16-bit packed color
U16    paint_toColor565 (U8 r, U8 g, U8 b);

COLOR     paint_RGB888_RGB16 (U8 r, U8 g, U8 b);
void        paint_RGB16_RGB888 (COLOR color, U8 *pColor);
COLOR     paint_blendColor (COLOR fg, COLOR bg, U8 alpha);


// ����� �� �����, ���������� ������ ������ ���� ������������ �����
void        paint_repaint (void);

//---------------------------- TEXT -------------------------------
void        paint_setFont (U8 num, PAINT_FONT_TYPE type);
void        paint_setTextSize (U8 s);

// console type
COORD     paint_getMaxCol (void); //x
COORD     paint_getMaxRow (void); //y
MSG       paint_gotoColRow (COORD col, COORD row);
void        paint_putChar (char c);
void        paint_putCharAuto (char ch);
void        paint_putCharColRow (COORD col, COORD row, char c);
void        paint_strClearRow (COORD row);
void        paint_putStr (const char *str);
void        paint_putStrColRow (COORD col, COORD row, const char *str);

// normal type
COORD     paint_putCharXY  (COORD x, COORD y, char c);
COORD     paint_putStrXY (COORD x, COORD y, const char *str);

COORD     paint_getCursorCol (void);
COORD     paint_getCursorRow (void);

//void _paint_putStrXY (COORD x, COORD y, char *str);
void        paint_draw7SegHEX (U8 type, COORD x, COORD y, char c);


#ifdef	__cplusplus
}
#endif

#endif	/* MODPAINT_H */
