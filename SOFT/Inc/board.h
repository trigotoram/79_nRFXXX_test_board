/* 
 * File:   board.h
 */

#ifndef BOARD_H
#define	BOARD_H                         20170712


#define BOARD_nRFXXXX_test              (1)//������� �����
#define STM32                           1//for debug.h


/**
 *  ������ ��� "include"
 */
#include "main.h"
#include "stm32f1xx_hal.h"
//#include "usb_device.h"
#include "usbd_cdc_if.h"
//#include "conv.h"
#include "math.h"


/**
 *  ������ ��� "define"
 */

#define _DEBUG                          1 //commit, if not need debug

//#define ERROR_ACTION(a)              //fERROR_ACTION(a,__MODULE__,__LINE__)

#define SYSTIME                         uint32_t
#define TIC_PERIOD                      (1000UL) //us
#define CPU_FRQ                         (72000000UL) //us


//#define NULL                            (0)
#define TRUE                            1//true
#define FALSE                           0//false

#ifndef TRUE
#define TRUE                            1
#endif
#ifndef FALSE
#define FALSE                           0
#endif

#define FUNCTION_RETURN_OK              1 //- �������� ���������
#define FUNCTION_RETURN_ERROR           0
#define MSG                             uint32_t
#define BOOL                            uint8_t
#define U8                              uint8_t
#define S8                              int8_t
#define VU8                             uint8_t
#define U16                             uint16_t
#define S16                             int16_t
#define U32                             uint32_t
#define S32                             int32_t
#define U64                             uint64_t
#define S64                             int64_t
#define FLOAT                           float
#define DOUBLE                          double


// GPIO ------------------------------------------
#define KEY_IN                          (GPIOC->IDR & GPIO_IDR_IDR13)


// USB ---------------------------------
#define PIN_USB_PULLUP_L                GPIOB->BSRR = GPIO_BSRR_BR9
#define PIN_USB_PULLUP_H                GPIOB->BSRR = GPIO_BSRR_BS9


#define TEST_PIN_L                      GPIOA->BSRR = GPIO_BSRR_BR1
#define TEST_PIN_H                      GPIOA->BSRR = GPIO_BSRR_BS1

#define LED_FULL_POWER                  0 //push-pull or pull-up?

#define TEST_LED_OFF                    GPIOB->BSRR = GPIO_BSRR_BR8
#define TEST_LED_ON                     GPIOB->BSRR = GPIO_BSRR_BS8  
#define TEST_LED_INV                    GPIOB->ODR ^= GPIO_ODR_ODR8  


#define NRF8001_GPIO_CSN_L              
#define NRF8001_GPIO_CSN_H              


#define halSPIFLASH_GPIO_SPI_init       halSPI1_GPIO_SPI_init
#define halSPIFLASH_xspi                halSPI1_xspi
#define halNRF24LC01P_SPI_init          halSPI1_GPIO_SPI_init
#define halSPI_NRF24LC01P_xput          halSPI1_xspi



//------------------------------- LCD ------------------------------------------
#define HT3_PAINT                       1
//#define LCD_LPH9157                     1
//#define LCD_LM15SGFN07                     1
#define LCD_NOKIA1202                   1

#define COLORS_1BIT                     1

enum { 
    SCREEN_ORIENTATION_0 = 0, 
    SCREEN_ORIENTATION_90,
    SCREEN_ORIENTATION_180,
    SCREEN_ORIENTATION_270
};

#ifndef COORD
typedef uint8_t                         COORD;
#endif


//#define PAINT_FONT_x3y5                 1
#define PAINT_FONT_Generic_8pt          1
// ;paint primitives
#define PAINT_NEED_LINE                 1
#define PAINT_NEED_RECT                 1
#define PAINT_NEED_CIRCLE               1
#define PAINT_NEED_CIRCLE               1


#if LCD_NOKIA1202

#define INVERSE 2

typedef uint8_t                         COLOR;

//#define LCD_ENABLE_PARTIAL_UPDATE   1
#define SCREEN_W	                    98
#define SCREEN_H	                    68

#define SCREEN_ORIENTATION_GLOBAL       SCREEN_ORIENTATION_0
#if (SCREEN_W > SCREEN_H)
    #define PAINT_BUF_SIZE              SCREEN_W
#else
    #define PAINT_BUF_SIZE              SCREEN_H
#endif
                     
#define LCD_CLK_L                       GPIOB->BSRR = GPIO_BSRR_BR3
#define LCD_CLK_H                       GPIOB->BSRR = GPIO_BSRR_BS3

#define LCD_DAT_L                       GPIOB->BSRR = GPIO_BSRR_BR4
#define LCD_DAT_H                       GPIOB->BSRR = GPIO_BSRR_BS4

#define LCD_RES_L                       GPIOB->BSRR = GPIO_BSRR_BR6
#define LCD_RES_H                       GPIOB->BSRR = GPIO_BSRR_BS6

#define LCD_CS_L                        GPIOB->BSRR = GPIO_BSRR_BR5
#define LCD_CS_H                        GPIOB->BSRR = GPIO_BSRR_BS5

#endif


// UART --------------------------------
#define HAL_USART1                      1
#define USART1_TX_HOOK                  do {Uart1TX_Ready = SET;} while (0)
#define USART1_RX_HOOK                  do {Uart1RX_Ready = SET; ; } while (0)
#define USART1_TX_ERROR_HOOK            ERROR_ACTION(ERROR_USART1_HAL)
#define USART1_BUF_TX_SIZE              (1024)
#define USART1_BUF_RX_SIZE              (1024)
#define USART1_LED_INV              //LED_BLUE_INV 


#define HAL_SPI1                        1


// for xprintf.h
#define _USE_XFUNC_OUT	                1 // 1: Use output functions
#define	_CR_CRLF		                0 // 1: Convert \n ==> \r\n in the output char
#define _USE_XFUNC_IN	                0 // 1: Use input function
#define	_LINE_ECHO		                0 // 1: Echo back input chars in xgets function

//for conv.h
#define STR_MAX_SIZE                    (65535 -1)



// vor _crc.h
//#define NEED_CRC32_CCITT        1 
#define NEED_CRC16                      1
//#define NEED_CRC8               1
//#define NEED_CRC8_DS            1


//#define __NOP()               
#define __DI()                          __disable_irq() // do { #asm("cli") } while(0) // Global interrupt disable
#define __EI()                          __enable_irq() //do { #asm("sei") } while(0) // Global interrupt enable
#define __CLRWDT()                      do { IWDG->KR = 0x0000AAAA; } while (0) // ������������ �������



#include "_fifo.h"

#define fifo_UART_RX_SIZE               (64UL)


#define MODRANDOM_4096BIT_EN            0


// I2C ---------------------------------
#define HAL_I2C1                        1

//#define I2C_LM75_ADRESS 0x9E
//#define I2C_BMP180                      1
//#define I2C_HMC5883L                    1
#define I2C_MPU6050                     1
//#define I2C_MAX30100                    1
#define I2C_BME280                      1

#define I2C_24Cxx_ADDRESS               0x50
#define I2C_MPU6050_ADDRESS             0x68
#define I2C_BME280_ADDRESS              0x76


#if (1 == HAL_I2C1)
    #define GPIO_I2C1                   GPIOB
    #define GPIO_PIN_SDA1               GPIO_PIN_11
    #define GPIO_PIN_SCL1               GPIO_PIN_10

    #define SCL1_L                      GPIOB->BSRR = GPIO_BSRR_BR10 //I2C_GPIO->ODR &= ~GPIO_Pin_SCL                                
    #define SCL1_H                      GPIOB->BSRR = GPIO_BSRR_BS10 //I2C_GPIO->ODR |=  GPIO_Pin_SCL

    #define SDA1_L                      GPIOB->BSRR = GPIO_BSRR_BR11
    #define SDA1_H                      GPIOB->BSRR = GPIO_BSRR_BS11

    #define SCL1_IN                     (GPIOB->IDR & GPIO_PIN_10)
    #define SDA1_IN                     (GPIOB->IDR & GPIO_PIN_11)

    // GPIO_Init(I2C_GPIO, (GPIO_Pin_TypeDef)GPIO_Pin_SDA, GPIO_MODE_OUT_OD_HIZ_FAST); \
    // GPIO_Init(I2C_GPIO, (GPIO_Pin_TypeDef)GPIO_Pin_SCL, GPIO_MODE_OUT_OD_HIZ_FAST); 
    #define I2C1_INIT    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN; \
        SCL1_H; \
        SDA1_H; \
        GPIOB->CRH &= ~(GPIO_CRH_CNF10 |  GPIO_CRH_CNF11); \
        GPIOB->CRH |= (GPIO_CRH_CNF10_0 |  GPIO_CRH_CNF11_0 | GPIO_CRH_MODE10 | GPIO_CRH_MODE11);
        
    #define I2C1_DEINIT
    #define I2C1_DELAY                  halI2C1_delay()
            
#define halI2C_init()                   soft_halI2C_init() 
#define halI2C_transmit(a,b,c,d)        soft_halI2C_transmit(a,b,c,d) 
#define halI2C_receive(a,b,c,d)         soft_halI2C_receive(a,b,c,d) 

#endif


// end I2C -----------------------------

            
            
#ifdef	__cplusplus
extern "C" {
#endif
 

#ifdef	__cplusplus
}
#endif


#endif	/* BOARD_H */
