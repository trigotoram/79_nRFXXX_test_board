<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="12" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="24" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="24" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="13" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="9" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Datasheet" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="15" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="12" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_ic">
<packages>
<package name="SOT89">
<description>&lt;b&gt;SOT98&lt;/b&gt; PK (R-PDSO-G3)&lt;p&gt;
Source: http://focus.ti.com/lit/ds/symlink/ua78l05.pdf</description>
<smd name="1" x="-1.5" y="-2" dx="1" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="3" x="1.5" y="-2" dx="1" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="2" x="0" y="-1.8" dx="1" dy="2" layer="1" stop="no" cream="no"/>
<smd name="2@1" x="0" y="0.94" dx="2.2" dy="3.7" layer="1" roundness="100" cream="no"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="4" size="1" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<rectangle x1="-0.4" y1="-2.68" x2="0.4" y2="-1.28" layer="31"/>
<rectangle x1="-2.025" y1="-2.775" x2="-0.975" y2="-1.2" layer="29"/>
<rectangle x1="0.975" y1="-2.775" x2="2.025" y2="-1.2" layer="29"/>
<rectangle x1="-0.525" y1="-2.775" x2="0.525" y2="-1.2" layer="29"/>
<rectangle x1="1.1" y1="-2.68" x2="1.9" y2="-1.28" layer="31"/>
<rectangle x1="-1.9" y1="-2.68" x2="-1.1" y2="-1.28" layer="31"/>
<wire x1="-2.25" y1="1.25" x2="2.25" y2="1.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="1.25" x2="2.25" y2="-1.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.25" x2="-2.25" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.25" x2="-2.25" y2="1.25" width="0.127" layer="51"/>
<wire x1="-2.3" y1="-3" x2="-2.3" y2="1.3" width="0.2" layer="21"/>
<wire x1="-2.3" y1="1.3" x2="-1.4" y2="1.3" width="0.2" layer="21"/>
<wire x1="1.3" y1="1.3" x2="2.3" y2="1.3" width="0.2" layer="21"/>
<wire x1="2.3" y1="1.3" x2="2.3" y2="-3" width="0.2" layer="21"/>
<wire x1="-2.3" y1="-3" x2="2.3" y2="-3" width="0.2" layer="21"/>
<text x="0" y="0" size="1" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-1.1" y1="1.3" x2="-1.1" y2="2.1" width="0.1" layer="51"/>
<wire x1="-1.1" y1="2.1" x2="-0.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="-0.5" y1="2.7" x2="0.5" y2="2.7" width="0.1" layer="51"/>
<wire x1="0.5" y1="2.7" x2="1" y2="2.2" width="0.1" layer="51"/>
<wire x1="1" y1="2.2" x2="1.1" y2="2.1" width="0.1" layer="51"/>
<wire x1="1.1" y1="2.1" x2="1.1" y2="1.3" width="0.1" layer="51"/>
<wire x1="-1.9" y1="-1.3" x2="-1.9" y2="-2.6" width="0.1" layer="51"/>
<wire x1="-1.9" y1="-2.6" x2="-1.1" y2="-2.6" width="0.1" layer="51"/>
<wire x1="-1.1" y1="-2.6" x2="-1.1" y2="-1.3" width="0.1" layer="51"/>
<wire x1="-0.4" y1="-1.3" x2="-0.4" y2="-2.6" width="0.1" layer="51"/>
<wire x1="-0.4" y1="-2.6" x2="0.4" y2="-2.6" width="0.1" layer="51"/>
<wire x1="0.4" y1="-2.6" x2="0.4" y2="-1.3" width="0.1" layer="51"/>
<wire x1="1.1" y1="-1.3" x2="1.1" y2="-2.6" width="0.1" layer="51"/>
<wire x1="1.1" y1="-2.6" x2="1.9" y2="-2.6" width="0.1" layer="51"/>
<wire x1="1.9" y1="-2.6" x2="1.9" y2="-1.3" width="0.1" layer="51"/>
</package>
<package name="SOT23-3">
<description>&lt;b&gt;Small Outline Transistor; 3 leads&lt;/b&gt; Reflow soldering&lt;p&gt;
INFINEON, www.infineon.com/cmc_upload/0/000/010/257/eh_db_5b.pdf</description>
<smd name="3" x="0" y="1.2" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="1" y="-1.2" dx="0.8" dy="0.9" layer="1"/>
<smd name="1" x="-1" y="-1.2" dx="0.8" dy="0.9" layer="1"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="0" size="1" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.7" x2="1.5" y2="0.7" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.7" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.7" x2="-1.5" y2="-0.7" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.7" width="0.127" layer="51"/>
<wire x1="-1.7" y1="-1.9" x2="-1.7" y2="0.8" width="0.2" layer="21"/>
<wire x1="-1.7" y1="0.8" x2="-0.7" y2="0.8" width="0.2" layer="21"/>
<wire x1="0.7" y1="0.8" x2="1.7" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-1.9" width="0.2" layer="21"/>
<wire x1="-0.7" y1="0.8" x2="-0.7" y2="1.9" width="0.2" layer="21"/>
<wire x1="-0.7" y1="1.9" x2="0.7" y2="1.9" width="0.2" layer="21"/>
<wire x1="0.7" y1="1.9" x2="0.7" y2="0.8" width="0.2" layer="21"/>
<wire x1="-1.7" y1="-1.9" x2="1.7" y2="-1.9" width="0.2" layer="21"/>
<rectangle x1="-0.3" y1="0.8" x2="0.3" y2="1.5" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.3" y2="-0.8" layer="51"/>
<rectangle x1="-1.3" y1="-1.5" x2="-0.7" y2="-0.8" layer="51"/>
</package>
<package name="QFN32_PAD">
<wire x1="-2.225" y1="2.7" x2="-2.3" y2="2.7" width="0.2" layer="21"/>
<wire x1="-2.3" y1="2.7" x2="-2.7" y2="2.7" width="0.2" layer="21"/>
<wire x1="-2.7" y1="2.7" x2="-2.7" y2="2.3" width="0.2" layer="21"/>
<wire x1="-2.7" y1="2.3" x2="-2.7" y2="2.2" width="0.2" layer="21"/>
<wire x1="-2.7" y1="-2.2" x2="-2.7" y2="-2.7" width="0.2" layer="21"/>
<wire x1="-2.7" y1="-2.7" x2="-2.2" y2="-2.7" width="0.2" layer="21"/>
<wire x1="2.2" y1="-2.7" x2="2.7" y2="-2.7" width="0.2" layer="21"/>
<wire x1="2.7" y1="-2.7" x2="2.7" y2="-2.2" width="0.2" layer="21"/>
<wire x1="2.7" y1="2.2" x2="2.7" y2="2.7" width="0.2" layer="21"/>
<wire x1="2.7" y1="2.7" x2="2.2" y2="2.7" width="0.2" layer="21"/>
<rectangle x1="0.7" y1="0.7" x2="1.6" y2="1.6" layer="31" rot="R270"/>
<rectangle x1="0.7" y1="-1.6" x2="1.6" y2="-0.7" layer="31" rot="R270"/>
<rectangle x1="0.85" y1="-0.45" x2="1.45" y2="0.45" layer="31" rot="R270"/>
<rectangle x1="-0.45" y1="0.85" x2="0.45" y2="1.45" layer="31" rot="R270"/>
<rectangle x1="-1.6" y1="0.7" x2="-0.7" y2="1.6" layer="31" rot="R270"/>
<rectangle x1="-1.45" y1="-0.45" x2="-0.85" y2="0.45" layer="31" rot="R270"/>
<rectangle x1="-1.6" y1="-1.6" x2="-0.7" y2="-0.7" layer="31" rot="R270"/>
<rectangle x1="-0.45" y1="-1.45" x2="0.45" y2="-0.85" layer="31" rot="R270"/>
<smd name="1" x="-2.45" y="1.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="2" x="-2.45" y="1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="3" x="-2.45" y="0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="4" x="-2.45" y="0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="5" x="-2.45" y="-0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="6" x="-2.45" y="-0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="7" x="-2.45" y="-1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="8" x="-2.45" y="-1.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="9" x="-1.75" y="-2.45" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="-1.25" y="-2.45" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="-0.75" y="-2.45" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="-0.25" y="-2.45" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="0.25" y="-2.45" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="0.75" y="-2.45" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="1.25" y="-2.45" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="1.75" y="-2.45" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="2.45" y="-1.75" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="18" x="2.45" y="-1.25" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="19" x="2.45" y="-0.75" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="20" x="2.45" y="-0.25" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="21" x="2.45" y="0.25" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="22" x="2.45" y="0.75" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="23" x="2.45" y="1.25" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="24" x="2.45" y="1.75" dx="0.8" dy="0.3" layer="1" rot="R180"/>
<smd name="25" x="1.75" y="2.45" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="26" x="1.25" y="2.45" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="27" x="0.75" y="2.45" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="28" x="0.25" y="2.45" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="29" x="-0.25" y="2.45" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="30" x="-0.75" y="2.45" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="31" x="-1.25" y="2.45" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<smd name="32" x="-1.75" y="2.45" dx="0.8" dy="0.3" layer="1" rot="R270"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-4" size="1" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<smd name="GND.1" x="0" y="0" dx="3.45" dy="3.45" layer="1"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="-2.3" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.3" y1="2.5" x2="-2.5" y2="2.3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="2.3" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2.3" y1="2.7" x2="-2.7" y2="2.3" width="0.2" layer="21"/>
<text x="0" y="0" size="1.5" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
<circle x="-1.7" y="1.6" radius="0.282840625" width="0.2" layer="51"/>
<rectangle x1="-1.7" y1="1.7" x2="1.7" y2="2" layer="41"/>
<rectangle x1="-1.7" y1="-2" x2="1.7" y2="-1.7" layer="41"/>
<rectangle x1="1.7" y1="-1.7" x2="2" y2="1.7" layer="41"/>
<rectangle x1="-2" y1="-1.7" x2="-1.7" y2="1.7" layer="41"/>
</package>
<package name="QFN24">
<wire x1="-2" y1="2" x2="2" y2="2" width="0.127" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.127" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.127" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.127" layer="51"/>
<smd name="3" x="-1.975" y="0.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="4" x="-1.975" y="-0.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="5" x="-1.975" y="-0.75" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="6" x="-1.975" y="-1.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="-1.975" y="0.75" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="1" x="-1.975" y="1.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="16" x="1.975" y="0.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="15" x="1.975" y="-0.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="14" x="1.975" y="-0.75" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="13" x="1.975" y="-1.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="17" x="1.975" y="0.75" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="18" x="1.975" y="1.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="22" x="-0.25" y="1.975" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<smd name="21" x="0.25" y="1.975" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<smd name="20" x="0.75" y="1.975" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<smd name="19" x="1.25" y="1.975" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="1.975" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<smd name="24" x="-1.25" y="1.975" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<smd name="10" x="0.25" y="-1.975" dx="0.3" dy="0.8" layer="1"/>
<smd name="9" x="-0.25" y="-1.975" dx="0.3" dy="0.8" layer="1"/>
<smd name="8" x="-0.75" y="-1.975" dx="0.3" dy="0.8" layer="1"/>
<smd name="7" x="-1.25" y="-1.975" dx="0.3" dy="0.8" layer="1"/>
<smd name="11" x="0.75" y="-1.975" dx="0.3" dy="0.8" layer="1"/>
<smd name="12" x="1.25" y="-1.975" dx="0.3" dy="0.8" layer="1"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-1.7" y1="2.1" x2="-2.1" y2="1.7" width="0.2" layer="21"/>
<wire x1="1.7" y1="2.1" x2="2.1" y2="2.1" width="0.2" layer="21"/>
<wire x1="2.1" y1="2.1" x2="2.1" y2="1.7" width="0.2" layer="21"/>
<wire x1="-2.1" y1="-1.7" x2="-2.1" y2="-2.1" width="0.2" layer="21"/>
<wire x1="-2.1" y1="-2.1" x2="-1.7" y2="-2.1" width="0.2" layer="21"/>
<wire x1="1.7" y1="-2.1" x2="2.1" y2="-2.1" width="0.2" layer="21"/>
<wire x1="2.1" y1="-2.1" x2="2.1" y2="-1.7" width="0.2" layer="21"/>
<smd name="PAD" x="0" y="0" dx="2.7" dy="2.7" layer="1"/>
<text x="0" y="-3.81" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="0.254" y1="-0.127" x2="0.508" y2="-0.127" width="0.0508" layer="51"/>
<wire x1="0.508" y1="-0.127" x2="0.381" y2="0" width="0.0508" layer="51"/>
<wire x1="0.508" y1="-0.127" x2="0.381" y2="-0.254" width="0.0508" layer="51"/>
<wire x1="-0.381" y1="0.127" x2="-0.381" y2="0.381" width="0.0508" layer="51"/>
<wire x1="-0.381" y1="0.381" x2="-0.508" y2="0.254" width="0.0508" layer="51"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.254" width="0.0508" layer="51"/>
<circle x="0.254" y="0.254" radius="0.127" width="0.0508" layer="51"/>
<circle x="0.254" y="0.254" radius="0.0254" width="0.0508" layer="51"/>
<text x="-0.127" y="-0.254" size="0.3048" layer="51" ratio="15">X</text>
<text x="-0.508" y="-0.254" size="0.3048" layer="51" ratio="15">Y</text>
<text x="-0.127" y="0.127" size="0.3048" layer="51" ratio="15">Z</text>
<text x="0" y="0" size="1" layer="51" font="vector" ratio="11" align="center">&gt;NAME</text>
<circle x="-1.4" y="1.4" radius="0.22360625" width="0.2" layer="51"/>
<rectangle x1="-1.4" y1="-1.6" x2="1.4" y2="-1.3" layer="41"/>
<rectangle x1="-1.6" y1="-1.4" x2="-1.3" y2="1.4" layer="41"/>
<rectangle x1="-1.4" y1="1.3" x2="1.4" y2="1.6" layer="41"/>
<rectangle x1="1.3" y1="-1.4" x2="1.6" y2="1.4" layer="41"/>
</package>
<package name="WSOF6I">
<smd name="2" x="0" y="-1.4" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="3" x="0.5" y="-1.4" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="1" x="-0.5" y="-1.4" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="6" x="-0.5" y="1.4" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="5" x="0" y="1.4" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="4" x="0.5" y="1.4" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="GND" x="0" y="0" dx="1.7" dy="1.5" layer="1" rot="R180"/>
<wire x1="-1.1" y1="1.5" x2="-1.1" y2="-1.7" width="0.2" layer="21"/>
<wire x1="1.1" y1="-1.5" x2="1.1" y2="1.5" width="0.2" layer="21"/>
<wire x1="-0.97" y1="-1.705" x2="-0.97" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-0.8" y1="1.3" x2="0.8" y2="1.3" width="0.1" layer="51"/>
<wire x1="0.8" y1="1.3" x2="0.8" y2="-1.3" width="0.1" layer="51"/>
<wire x1="0.8" y1="-1.3" x2="-0.8" y2="-1.3" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-1.3" x2="-0.8" y2="1.3" width="0.1" layer="51"/>
<circle x="-0.6" y="-1.1" radius="0.1" width="0.1" layer="51"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="51"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<rectangle x1="-0.9" y1="-1.1" x2="0.9" y2="-0.8" layer="41"/>
<rectangle x1="-0.9" y1="0.8" x2="0.9" y2="1.1" layer="41"/>
</package>
<package name="LGA-8(BME280)">
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-1.25" y1="1.25" x2="1.25" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.25" y1="1.25" x2="1.25" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.25" y1="-1.25" x2="-1.25" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.25" y1="-1.25" x2="-1.25" y2="1.25" width="0.127" layer="51"/>
<circle x="0.8" y="1" radius="0.1" width="0.127" layer="51"/>
<circle x="0" y="-0.7" radius="0.282840625" width="0.127" layer="51"/>
<smd name="1" x="1.1" y="0.975" dx="0.35" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="1.1" y="0.325" dx="0.35" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="1.1" y="-0.325" dx="0.35" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="1.1" y="-0.975" dx="0.35" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="-1.1" y="-0.975" dx="0.35" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="-1.1" y="-0.325" dx="0.35" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="-1.1" y="0.325" dx="0.35" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="-1.1" y="0.975" dx="0.35" dy="0.6" layer="1" rot="R90"/>
<wire x1="-1.7" y1="1.4" x2="1.7" y2="1.4" width="0.2" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-1.4" width="0.2" layer="21"/>
<wire x1="1.7" y1="-1.4" x2="-1.7" y2="-1.4" width="0.2" layer="21"/>
<wire x1="-1.7" y1="-1.4" x2="-1.7" y2="1.4" width="0.2" layer="21"/>
<text x="0" y="0" size="0.8" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
</package>
<package name="SOT23-5L">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
package type OT</description>
<smd name="1" x="-0.95" y="-1.3" dx="0.6" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.6" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.6" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.6" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.6" dy="1.2" layer="1"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="2" y="0" size="0.8" layer="27" font="vector" ratio="12" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.9" x2="-1.2" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.2" y1="0.9" x2="-0.7" y2="0.9" width="0.127" layer="51"/>
<wire x1="-0.7" y1="0.9" x2="0.7" y2="0.9" width="0.127" layer="51"/>
<wire x1="0.7" y1="0.9" x2="1.2" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.2" y1="0.9" x2="1.5" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.9" x2="1.5" y2="-0.4" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.4" x2="1.5" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.9" x2="1.2" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.2" y1="-0.9" x2="0.7" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.7" y1="-0.9" x2="0.25" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.25" y1="-0.9" x2="-0.25" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-0.25" y1="-0.9" x2="-0.7" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-0.7" y1="-0.9" x2="-1.2" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-0.9" x2="-1.5" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.9" x2="-1.5" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.4" x2="-1.5" y2="0.9" width="0.127" layer="51"/>
<wire x1="-0.4" y1="1" x2="0.4" y2="1" width="0.2" layer="21"/>
<wire x1="-1.5" y1="1" x2="-1.7" y2="1" width="0.2" layer="21"/>
<wire x1="-1.7" y1="1" x2="-1.7" y2="-0.4" width="0.2" layer="21"/>
<wire x1="-1.7" y1="-0.4" x2="-1.7" y2="-1.5" width="0.2" layer="21"/>
<wire x1="1.5" y1="-1" x2="1.7" y2="-1" width="0.2" layer="21"/>
<wire x1="1.7" y1="-1" x2="1.7" y2="-0.4" width="0.2" layer="21"/>
<wire x1="1.7" y1="-0.4" x2="1.7" y2="1" width="0.2" layer="21"/>
<wire x1="1.7" y1="1" x2="1.5" y2="1" width="0.2" layer="21"/>
<wire x1="-1.7" y1="-0.4" x2="1.7" y2="-0.4" width="0.2" layer="21"/>
<wire x1="-1.5" y1="-1.6" x2="-1.5" y2="-2.1" width="0.2" layer="21"/>
<wire x1="-1.5" y1="-2.1" x2="1.5" y2="-2.1" width="0.2" layer="21"/>
<wire x1="1.5" y1="-2.1" x2="1.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="1.5" y1="1.3" x2="1.5" y2="2.1" width="0.2" layer="21"/>
<wire x1="1.5" y1="2.1" x2="-1.5" y2="2.1" width="0.2" layer="21"/>
<wire x1="-1.5" y1="2.1" x2="-1.5" y2="1.3" width="0.2" layer="21"/>
<text x="0" y="0" size="0.8" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-1.5" y1="-0.4" x2="1.5" y2="-0.4" width="0.1" layer="51"/>
<wire x1="-1.2" y1="-0.9" x2="-1.2" y2="-1.8" width="0.1" layer="51"/>
<wire x1="-1.2" y1="-1.8" x2="-0.7" y2="-1.8" width="0.1" layer="51"/>
<wire x1="-0.7" y1="-1.8" x2="-0.7" y2="-0.9" width="0.1" layer="51"/>
<wire x1="-0.25" y1="-0.9" x2="-0.25" y2="-1.8" width="0.1" layer="51"/>
<wire x1="-0.25" y1="-1.8" x2="0.25" y2="-1.8" width="0.1" layer="51"/>
<wire x1="0.25" y1="-1.8" x2="0.25" y2="-0.9" width="0.1" layer="51"/>
<wire x1="0.7" y1="-0.9" x2="0.7" y2="-1.8" width="0.1" layer="51"/>
<wire x1="0.7" y1="-1.8" x2="1.2" y2="-1.8" width="0.1" layer="51"/>
<wire x1="1.2" y1="-1.8" x2="1.2" y2="-0.9" width="0.1" layer="51"/>
<wire x1="-0.7" y1="0.9" x2="-0.7" y2="1.8" width="0.1" layer="51"/>
<wire x1="-0.7" y1="1.8" x2="-1.2" y2="1.8" width="0.1" layer="51"/>
<wire x1="-1.2" y1="1.8" x2="-1.2" y2="0.9" width="0.1" layer="51"/>
<wire x1="0.7" y1="0.9" x2="0.7" y2="1.8" width="0.1" layer="51"/>
<wire x1="0.7" y1="1.8" x2="1.2" y2="1.8" width="0.1" layer="51"/>
<wire x1="1.2" y1="1.8" x2="1.2" y2="0.9" width="0.1" layer="51"/>
</package>
<package name="DIL08">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.254" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.254" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.254" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.254" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.254" layer="21" curve="-180"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" diameter="1.27" shape="square" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-1.27" y="-1.27" size="1.27" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SO08-150">
<description>&lt;B&gt;Small Outline Narrow Plastic Gull Wing&lt;/B&gt;&lt;p&gt;
150-mil body, package type SN</description>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="1.52" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="1.52" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="1.52" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="1.52" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="1.52" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="1.52" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="1.52" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="1.52" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="3" y="0" size="1" layer="27" font="vector" ratio="12" rot="R90" align="center">&gt;VALUE</text>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="35"/>
<wire x1="2.5" y1="-1.5" x2="2.5" y2="1.5" width="0.2" layer="21"/>
<wire x1="2.5" y1="1.5" x2="-2.5" y2="1.5" width="0.2" layer="21"/>
<wire x1="-2.5" y1="1.5" x2="-2.5" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-1.5" x2="-2.4" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-2.4" y1="-1.5" x2="2.5" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-2.5" y1="2" x2="2.5" y2="2" width="0.127" layer="51"/>
<wire x1="2.5" y1="2" x2="2.5" y2="-2" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2" x2="-2.5" y2="-2" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="2" width="0.127" layer="51"/>
<rectangle x1="-2.06375" y1="2.06375" x2="-1.74625" y2="2.8575" layer="51"/>
<rectangle x1="-0.79375" y1="2.06375" x2="-0.47625" y2="2.8575" layer="51"/>
<rectangle x1="0.47625" y1="2.06375" x2="0.79375" y2="2.8575" layer="51"/>
<rectangle x1="1.74625" y1="2.06375" x2="2.06375" y2="2.8575" layer="51"/>
<rectangle x1="0.47625" y1="-2.8575" x2="0.79375" y2="-2.06375" layer="51"/>
<rectangle x1="-0.79375" y1="-2.8575" x2="-0.47625" y2="-2.06375" layer="51"/>
<rectangle x1="-2.06375" y1="-2.8575" x2="-1.74625" y2="-2.06375" layer="51"/>
<rectangle x1="1.74625" y1="-2.8575" x2="2.06375" y2="-2.06375" layer="51"/>
<wire x1="-2.5" y1="-1.5" x2="-2.5" y2="-3.3" width="0.2" layer="21"/>
<wire x1="-2.38125" y1="-1.27" x2="2.38125" y2="-1.27" width="0.127" layer="51"/>
<text x="0" y="0" size="1" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
</package>
<package name="LQFP48">
<description>&lt;b&gt;48-pin plastic LQFP (FPT-48P-M26)&lt;/b&gt;&lt;p&gt;
www.fma.fujitsu.com/pdf/e713717.pdf</description>
<wire x1="-3.375" y1="3.1" x2="-3.1" y2="3.375" width="0.2" layer="21"/>
<wire x1="-3.1" y1="3.375" x2="3.1" y2="3.375" width="0.2" layer="21"/>
<wire x1="3.1" y1="3.375" x2="3.375" y2="3.1" width="0.2" layer="21"/>
<wire x1="3.375" y1="3.1" x2="3.375" y2="-3.1" width="0.2" layer="21"/>
<wire x1="3.375" y1="-3.1" x2="3.1" y2="-3.375" width="0.2" layer="21"/>
<wire x1="3.1" y1="-3.375" x2="-3.1" y2="-3.375" width="0.2" layer="21"/>
<wire x1="-3.1" y1="-3.375" x2="-3.375" y2="-3.1" width="0.2" layer="21"/>
<wire x1="-3.375" y1="-3.1" x2="-3.375" y2="3.1" width="0.2" layer="21"/>
<circle x="-2" y="-2" radius="0.6" width="0.2" layer="21"/>
<smd name="1" x="-2.75" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="2" x="-2.25" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="3" x="-1.75" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="4" x="-1.25" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="5" x="-0.75" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="6" x="-0.25" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="7" x="0.25" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="8" x="0.75" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="9" x="1.25" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="10" x="1.75" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="11" x="2.25" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="12" x="2.75" y="-4.25" dx="0.2" dy="1" layer="1"/>
<smd name="13" x="4.25" y="-2.75" dx="1" dy="0.2" layer="1"/>
<smd name="14" x="4.25" y="-2.25" dx="1" dy="0.2" layer="1"/>
<smd name="15" x="4.25" y="-1.75" dx="1" dy="0.2" layer="1"/>
<smd name="16" x="4.25" y="-1.25" dx="1" dy="0.2" layer="1"/>
<smd name="17" x="4.25" y="-0.75" dx="1" dy="0.2" layer="1"/>
<smd name="18" x="4.25" y="-0.25" dx="1" dy="0.2" layer="1"/>
<smd name="19" x="4.25" y="0.25" dx="1" dy="0.2" layer="1"/>
<smd name="20" x="4.25" y="0.75" dx="1" dy="0.2" layer="1"/>
<smd name="21" x="4.25" y="1.25" dx="1" dy="0.2" layer="1"/>
<smd name="22" x="4.25" y="1.75" dx="1" dy="0.2" layer="1"/>
<smd name="23" x="4.25" y="2.25" dx="1" dy="0.2" layer="1"/>
<smd name="24" x="4.25" y="2.75" dx="1" dy="0.2" layer="1"/>
<smd name="25" x="2.75" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="26" x="2.25" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="27" x="1.75" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="28" x="1.25" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="29" x="0.75" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="30" x="0.25" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="31" x="-0.25" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="32" x="-0.75" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="33" x="-1.25" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="34" x="-1.75" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="35" x="-2.25" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="36" x="-2.75" y="4.25" dx="0.2" dy="1" layer="1"/>
<smd name="37" x="-4.25" y="2.75" dx="1" dy="0.2" layer="1"/>
<smd name="38" x="-4.25" y="2.25" dx="1" dy="0.2" layer="1"/>
<smd name="39" x="-4.25" y="1.75" dx="1" dy="0.2" layer="1"/>
<smd name="40" x="-4.25" y="1.25" dx="1" dy="0.2" layer="1"/>
<smd name="41" x="-4.25" y="0.75" dx="1" dy="0.2" layer="1"/>
<smd name="42" x="-4.25" y="0.25" dx="1" dy="0.2" layer="1"/>
<smd name="43" x="-4.25" y="-0.25" dx="1" dy="0.2" layer="1"/>
<smd name="44" x="-4.25" y="-0.75" dx="1" dy="0.2" layer="1"/>
<smd name="45" x="-4.25" y="-1.25" dx="1" dy="0.2" layer="1"/>
<smd name="46" x="-4.25" y="-1.75" dx="1" dy="0.2" layer="1"/>
<smd name="47" x="-4.25" y="-2.25" dx="1" dy="0.2" layer="1"/>
<smd name="48" x="-4.25" y="-2.75" dx="1" dy="0.2" layer="1"/>
<text x="0" y="0" size="1.5" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="2" size="1" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<rectangle x1="-2.85" y1="-4.5" x2="-2.65" y2="-3.45" layer="51"/>
<rectangle x1="-2.35" y1="-4.5" x2="-2.15" y2="-3.45" layer="51"/>
<rectangle x1="-1.85" y1="-4.5" x2="-1.65" y2="-3.45" layer="51"/>
<rectangle x1="-1.35" y1="-4.5" x2="-1.15" y2="-3.45" layer="51"/>
<rectangle x1="-0.85" y1="-4.5" x2="-0.65" y2="-3.45" layer="51"/>
<rectangle x1="-0.35" y1="-4.5" x2="-0.15" y2="-3.45" layer="51"/>
<rectangle x1="0.15" y1="-4.5" x2="0.35" y2="-3.45" layer="51"/>
<rectangle x1="0.65" y1="-4.5" x2="0.85" y2="-3.45" layer="51"/>
<rectangle x1="1.15" y1="-4.5" x2="1.35" y2="-3.45" layer="51"/>
<rectangle x1="1.65" y1="-4.5" x2="1.85" y2="-3.45" layer="51"/>
<rectangle x1="2.15" y1="-4.5" x2="2.35" y2="-3.45" layer="51"/>
<rectangle x1="2.65" y1="-4.5" x2="2.85" y2="-3.45" layer="51"/>
<rectangle x1="3.45" y1="-2.85" x2="4.5" y2="-2.65" layer="51"/>
<rectangle x1="3.45" y1="-2.35" x2="4.5" y2="-2.15" layer="51"/>
<rectangle x1="3.45" y1="-1.85" x2="4.5" y2="-1.65" layer="51"/>
<rectangle x1="3.45" y1="-1.35" x2="4.5" y2="-1.15" layer="51"/>
<rectangle x1="3.45" y1="-0.85" x2="4.5" y2="-0.65" layer="51"/>
<rectangle x1="3.45" y1="-0.35" x2="4.5" y2="-0.15" layer="51"/>
<rectangle x1="3.45" y1="0.15" x2="4.5" y2="0.35" layer="51"/>
<rectangle x1="3.45" y1="0.65" x2="4.5" y2="0.85" layer="51"/>
<rectangle x1="3.45" y1="1.15" x2="4.5" y2="1.35" layer="51"/>
<rectangle x1="3.45" y1="1.65" x2="4.5" y2="1.85" layer="51"/>
<rectangle x1="3.45" y1="2.15" x2="4.5" y2="2.35" layer="51"/>
<rectangle x1="3.45" y1="2.65" x2="4.5" y2="2.85" layer="51"/>
<rectangle x1="2.65" y1="3.45" x2="2.85" y2="4.5" layer="51"/>
<rectangle x1="2.15" y1="3.45" x2="2.35" y2="4.5" layer="51"/>
<rectangle x1="1.65" y1="3.45" x2="1.85" y2="4.5" layer="51"/>
<rectangle x1="1.15" y1="3.45" x2="1.35" y2="4.5" layer="51"/>
<rectangle x1="0.65" y1="3.45" x2="0.85" y2="4.5" layer="51"/>
<rectangle x1="0.15" y1="3.45" x2="0.35" y2="4.5" layer="51"/>
<rectangle x1="-0.35" y1="3.45" x2="-0.15" y2="4.5" layer="51"/>
<rectangle x1="-0.85" y1="3.45" x2="-0.65" y2="4.5" layer="51"/>
<rectangle x1="-1.35" y1="3.45" x2="-1.15" y2="4.5" layer="51"/>
<rectangle x1="-1.85" y1="3.45" x2="-1.65" y2="4.5" layer="51"/>
<rectangle x1="-2.35" y1="3.45" x2="-2.15" y2="4.5" layer="51"/>
<rectangle x1="-2.85" y1="3.45" x2="-2.65" y2="4.5" layer="51"/>
<rectangle x1="-4.5" y1="2.65" x2="-3.45" y2="2.85" layer="51"/>
<rectangle x1="-4.5" y1="2.15" x2="-3.45" y2="2.35" layer="51"/>
<rectangle x1="-4.5" y1="1.65" x2="-3.45" y2="1.85" layer="51"/>
<rectangle x1="-4.5" y1="1.15" x2="-3.45" y2="1.35" layer="51"/>
<rectangle x1="-4.5" y1="0.65" x2="-3.45" y2="0.85" layer="51"/>
<rectangle x1="-4.5" y1="0.15" x2="-3.45" y2="0.35" layer="51"/>
<rectangle x1="-4.5" y1="-0.35" x2="-3.45" y2="-0.15" layer="51"/>
<rectangle x1="-4.5" y1="-0.85" x2="-3.45" y2="-0.65" layer="51"/>
<rectangle x1="-4.5" y1="-1.35" x2="-3.45" y2="-1.15" layer="51"/>
<rectangle x1="-4.5" y1="-1.85" x2="-3.45" y2="-1.65" layer="51"/>
<rectangle x1="-4.5" y1="-2.35" x2="-3.45" y2="-2.15" layer="51"/>
<rectangle x1="-4.5" y1="-2.85" x2="-3.45" y2="-2.65" layer="51"/>
<text x="0" y="0" size="1.5" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-3.4" y1="-3.1" x2="-3.1" y2="-3.4" width="0.1" layer="51"/>
<wire x1="-3.1" y1="-3.4" x2="3.1" y2="-3.4" width="0.1" layer="51"/>
<wire x1="3.1" y1="-3.4" x2="3.4" y2="-3.1" width="0.1" layer="51"/>
<wire x1="3.4" y1="-3.1" x2="3.4" y2="3.1" width="0.1" layer="51"/>
<wire x1="3.4" y1="3.1" x2="3.1" y2="3.4" width="0.1" layer="51"/>
<wire x1="3.1" y1="3.4" x2="-3.1" y2="3.4" width="0.1" layer="51"/>
<wire x1="-3.1" y1="3.4" x2="-3.4" y2="3.1" width="0.1" layer="51"/>
<wire x1="-3.4" y1="3.1" x2="-3.4" y2="-3.1" width="0.1" layer="51"/>
<circle x="-2" y="-2" radius="0.70710625" width="0.1" layer="51"/>
</package>
<package name="TSSOP08">
<description>&lt;b&gt;Plastic Small Outline&lt;/b&gt;</description>
<wire x1="-2.15" y1="-1.4" x2="2.15" y2="-1.4" width="0.1" layer="51"/>
<wire x1="2.15" y1="-1.4" x2="2.15" y2="1.4" width="0.1" layer="51"/>
<wire x1="2.15" y1="1.4" x2="-2.15" y2="1.4" width="0.1" layer="51"/>
<wire x1="-2.15" y1="1.4" x2="-2.15" y2="-1.4" width="0.1" layer="51"/>
<circle x="-1.425" y="0.85" radius="0.325" width="0" layer="21"/>
<smd name="1" x="-2.925" y="0.975" dx="1.2" dy="0.35" layer="1"/>
<smd name="2" x="-2.925" y="0.325" dx="1.2" dy="0.35" layer="1"/>
<smd name="3" x="-2.925" y="-0.325" dx="1.2" dy="0.35" layer="1"/>
<smd name="4" x="-2.925" y="-0.975" dx="1.2" dy="0.35" layer="1"/>
<smd name="5" x="2.925" y="-0.975" dx="1.2" dy="0.35" layer="1"/>
<smd name="6" x="2.925" y="-0.325" dx="1.2" dy="0.35" layer="1"/>
<smd name="7" x="2.925" y="0.325" dx="1.2" dy="0.35" layer="1"/>
<smd name="8" x="2.925" y="0.975" dx="1.2" dy="0.35" layer="1"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-3.25" y="-2.925" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2" y1="0.85" x2="-2.2" y2="1.1" layer="51"/>
<rectangle x1="-3.2" y1="0.2" x2="-2.2" y2="0.45" layer="51"/>
<rectangle x1="-3.2" y1="-0.45" x2="-2.2" y2="-0.2" layer="51"/>
<rectangle x1="-3.2" y1="-1.1" x2="-2.2" y2="-0.85" layer="51"/>
<rectangle x1="2.2" y1="-1.1" x2="3.2" y2="-0.85" layer="51"/>
<rectangle x1="2.2" y1="-0.45" x2="3.2" y2="-0.2" layer="51"/>
<rectangle x1="2.2" y1="0.2" x2="3.2" y2="0.45" layer="51"/>
<rectangle x1="2.2" y1="0.85" x2="3.2" y2="1.1" layer="51"/>
<wire x1="-2" y1="1.5" x2="2" y2="1.5" width="0.2" layer="21"/>
<wire x1="2" y1="1.5" x2="2" y2="-1.5" width="0.2" layer="21"/>
<wire x1="2" y1="-1.5" x2="-2" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-2" y1="-1.5" x2="-2" y2="1.5" width="0.2" layer="21"/>
<circle x="-1.425" y="0.85" radius="0.325" width="0" layer="51"/>
<wire x1="-2" y1="1.5" x2="-3.4" y2="1.5" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="STAB-LINEAR-3">
<wire x1="0" y1="0" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="1.27" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VIN" x="-5.08" y="-2.54" length="middle"/>
<pin name="GND" x="7.62" y="-17.78" length="middle" rot="R90"/>
<pin name="VOUT\" x="20.32" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="NRF8001">
<wire x1="0" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="38.1" y2="0" width="0.254" layer="94"/>
<wire x1="38.1" y1="0" x2="38.1" y2="-38.1" width="0.254" layer="94"/>
<wire x1="38.1" y1="-38.1" x2="0" y2="-38.1" width="0.254" layer="94"/>
<wire x1="0" y1="-38.1" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="38.1" y="-40.64" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SCK" x="15.24" y="-43.18" length="middle" rot="R90"/>
<pin name="MOSI" x="20.32" y="-43.18" length="middle" rot="R90"/>
<pin name="MISO" x="22.86" y="-43.18" length="middle" rot="R90"/>
<pin name="RXD" x="12.7" y="-43.18" length="middle" rot="R90"/>
<pin name="VSS@1" x="43.18" y="-27.94" length="middle" rot="R180"/>
<pin name="XC2" x="22.86" y="5.08" length="middle" rot="R270"/>
<pin name="XC1" x="20.32" y="5.08" length="middle" rot="R270"/>
<pin name="VDD_PA" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="ANT1" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="ANT2" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="VSS@3" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="AVDD@0" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="IREF" x="27.94" y="5.08" length="middle" rot="R270"/>
<pin name="VDD@1" x="-5.08" y="-10.16" length="middle"/>
<pin name="DCDC" x="10.16" y="5.08" length="middle" rot="R270"/>
<pin name="ACTIVE" x="-5.08" y="-22.86" length="middle"/>
<pin name="/REQN" x="17.78" y="-43.18" length="middle" rot="R90"/>
<pin name="NC" x="25.4" y="-43.18" length="middle" rot="R90"/>
<pin name="/RYDN" x="27.94" y="-43.18" length="middle" rot="R90"/>
<pin name="VSS@2" x="43.18" y="-25.4" length="middle" rot="R180"/>
<pin name="/RESET" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="AVDD@1" x="25.4" y="5.08" length="middle" rot="R270"/>
<pin name="AVDD@2" x="17.78" y="5.08" length="middle" rot="R270"/>
<pin name="VSS@4" x="15.24" y="5.08" length="middle" rot="R270"/>
<pin name="VSS@5" x="12.7" y="5.08" length="middle" rot="R270"/>
<pin name="VDD@2" x="10.16" y="-43.18" length="middle" rot="R90"/>
<pin name="VSS@7" x="-5.08" y="-27.94" length="middle"/>
<pin name="TXD" x="-5.08" y="-25.4" length="middle"/>
<pin name="XL1" x="-5.08" y="-20.32" length="middle"/>
<pin name="XL2" x="-5.08" y="-17.78" length="middle"/>
<pin name="DEC1" x="-5.08" y="-15.24" length="middle"/>
<pin name="DEC2" x="-5.08" y="-12.7" length="middle"/>
</symbol>
<symbol name="MPU-6050">
<pin name="CLKIN" x="-5.08" y="-12.7" length="middle"/>
<pin name="NC@1" x="-5.08" y="-15.24" length="middle"/>
<pin name="NC@2" x="-5.08" y="-17.78" length="middle"/>
<pin name="NC@3" x="-5.08" y="-20.32" length="middle"/>
<pin name="AUX_DA" x="-5.08" y="-25.4" length="middle"/>
<pin name="NC@4" x="-5.08" y="-22.86" length="middle"/>
<pin name="AUX_CL" x="12.7" y="-43.18" length="middle" rot="R90"/>
<pin name="VLOGIC" x="15.24" y="-43.18" length="middle" rot="R90"/>
<pin name="AD0" x="17.78" y="-43.18" length="middle" rot="R90"/>
<pin name="FSYNC" x="22.86" y="-43.18" length="middle" rot="R90"/>
<pin name="REGOUT" x="20.32" y="-43.18" length="middle" rot="R90"/>
<pin name="INT" x="25.4" y="-43.18" length="middle" rot="R90"/>
<pin name="VDD" x="43.18" y="-25.4" length="middle" rot="R180"/>
<pin name="NC@5" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="NC@6" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="NC@7" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="NC@8" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="GND" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="RESV1" x="25.4" y="5.08" length="middle" rot="R270"/>
<pin name="CPOUT" x="22.86" y="5.08" length="middle" rot="R270"/>
<pin name="RESV2" x="20.32" y="5.08" length="middle" rot="R270"/>
<pin name="RESV3" x="17.78" y="5.08" length="middle" rot="R270"/>
<pin name="SCL" x="15.24" y="5.08" length="middle" rot="R270"/>
<pin name="SDA" x="12.7" y="5.08" length="middle" rot="R270"/>
<text x="0" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="38.1" y="-40.64" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="0" y1="0" x2="0" y2="-38.1" width="0.254" layer="94"/>
<wire x1="0" y1="-38.1" x2="38.1" y2="-38.1" width="0.254" layer="94"/>
<wire x1="38.1" y1="-38.1" x2="38.1" y2="0" width="0.254" layer="94"/>
<wire x1="38.1" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="BH1750">
<pin name="VCC" x="-5.08" y="-2.54" length="middle"/>
<pin name="ADDR" x="-5.08" y="-5.08" length="middle"/>
<pin name="GND" x="-5.08" y="-7.62" length="middle"/>
<pin name="SCL" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="DVI" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="SDA" x="22.86" y="-7.62" length="middle" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-12.7" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="BME280">
<wire x1="0" y1="0" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="22.86" y2="-15.24" width="0.254" layer="94"/>
<wire x1="22.86" y1="-15.24" x2="22.86" y2="0" width="0.254" layer="94"/>
<wire x1="22.86" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.9304" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-17.78" size="1.9304" layer="96" font="vector">&gt;VALUE</text>
<pin name="CS" x="27.94" y="-10.16" length="middle" rot="R180"/>
<pin name="VDD" x="-5.08" y="-2.54" length="middle"/>
<pin name="VDDIO" x="-5.08" y="-5.08" length="middle"/>
<pin name="SDI/SDA" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="SCK/SCL" x="27.94" y="-5.08" length="middle" rot="R180"/>
<pin name="SDO/ADR" x="27.94" y="-7.62" length="middle" rot="R180"/>
<pin name="GND@2" x="-5.08" y="-12.7" length="middle"/>
<pin name="GND@1" x="-5.08" y="-10.16" length="middle"/>
</symbol>
<symbol name="LTC4054">
<wire x1="0" y1="0" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="-12.7" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="0" y="1.27" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="CHRG" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="GND" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="BAT" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PROG" x="-5.08" y="-7.62" length="middle"/>
<pin name="VCC" x="-5.08" y="-2.54" length="middle"/>
</symbol>
<symbol name="STM32F10XC">
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="53.34" y2="0" width="0.254" layer="94"/>
<wire x1="53.34" y1="0" x2="53.34" y2="-53.34" width="0.254" layer="94"/>
<wire x1="53.34" y1="-53.34" x2="0" y2="-53.34" width="0.254" layer="94"/>
<wire x1="0" y1="-53.34" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="17.78" y="-27.94" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="25.4" y="-25.4" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="VBAT" x="-5.08" y="-12.7" length="middle"/>
<pin name="PC13-TAMPER" x="-5.08" y="-15.24" length="middle"/>
<pin name="OSC32-IN" x="-5.08" y="-17.78" length="middle"/>
<pin name="OSC32-OUT" x="-5.08" y="-20.32" length="middle"/>
<pin name="OSC-IN" x="-5.08" y="-22.86" length="middle"/>
<pin name="OSC-OUT" x="-5.08" y="-25.4" length="middle"/>
<pin name="NRST" x="-5.08" y="-27.94" length="middle"/>
<pin name="VSSA" x="-5.08" y="-30.48" length="middle"/>
<pin name="VDDA" x="-5.08" y="-33.02" length="middle"/>
<pin name="PA0" x="-5.08" y="-35.56" length="middle"/>
<pin name="PA1" x="-5.08" y="-38.1" length="middle"/>
<pin name="PA2" x="-5.08" y="-40.64" length="middle"/>
<pin name="PA3" x="12.7" y="-58.42" length="middle" rot="R90"/>
<pin name="PA4" x="15.24" y="-58.42" length="middle" rot="R90"/>
<pin name="PA5" x="17.78" y="-58.42" length="middle" rot="R90"/>
<pin name="PA6" x="20.32" y="-58.42" length="middle" rot="R90"/>
<pin name="PA7" x="22.86" y="-58.42" length="middle" rot="R90"/>
<pin name="PB0" x="25.4" y="-58.42" length="middle" rot="R90"/>
<pin name="PB1" x="27.94" y="-58.42" length="middle" rot="R90"/>
<pin name="PB2/BOOT1" x="30.48" y="-58.42" length="middle" rot="R90"/>
<pin name="PB10" x="33.02" y="-58.42" length="middle" rot="R90"/>
<pin name="PB11" x="35.56" y="-58.42" length="middle" rot="R90"/>
<pin name="VSS@1" x="38.1" y="-58.42" length="middle" rot="R90"/>
<pin name="VDD@1" x="40.64" y="-58.42" length="middle" rot="R90"/>
<pin name="PB12" x="58.42" y="-40.64" length="middle" rot="R180"/>
<pin name="PB13" x="58.42" y="-38.1" length="middle" rot="R180"/>
<pin name="PB14" x="58.42" y="-35.56" length="middle" rot="R180"/>
<pin name="PB15" x="58.42" y="-33.02" length="middle" rot="R180"/>
<pin name="PA8" x="58.42" y="-30.48" length="middle" rot="R180"/>
<pin name="PA9" x="58.42" y="-27.94" length="middle" rot="R180"/>
<pin name="PA10" x="58.42" y="-25.4" length="middle" rot="R180"/>
<pin name="PA11" x="58.42" y="-22.86" length="middle" rot="R180"/>
<pin name="PA12" x="58.42" y="-20.32" length="middle" rot="R180"/>
<pin name="PA13" x="58.42" y="-17.78" length="middle" rot="R180"/>
<pin name="VSS@2" x="58.42" y="-15.24" length="middle" rot="R180"/>
<pin name="VDD@2" x="58.42" y="-12.7" length="middle" rot="R180"/>
<pin name="PA14" x="40.64" y="5.08" length="middle" rot="R270"/>
<pin name="PA15" x="38.1" y="5.08" length="middle" rot="R270"/>
<pin name="PB3" x="35.56" y="5.08" length="middle" rot="R270"/>
<pin name="PB4" x="33.02" y="5.08" length="middle" rot="R270"/>
<pin name="PB5" x="30.48" y="5.08" length="middle" rot="R270"/>
<pin name="PB6" x="27.94" y="5.08" length="middle" rot="R270"/>
<pin name="PB7" x="25.4" y="5.08" length="middle" rot="R270"/>
<pin name="BOOT0" x="22.86" y="5.08" length="middle" rot="R270"/>
<pin name="PB8" x="20.32" y="5.08" length="middle" rot="R270"/>
<pin name="PB9" x="17.78" y="5.08" length="middle" rot="R270"/>
<pin name="VSS@3" x="15.24" y="5.08" length="middle" rot="R270"/>
<pin name="VDD@3" x="12.7" y="5.08" length="middle" rot="R270"/>
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="AT24C02">
<wire x1="0" y1="0" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.6764" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-15.24" size="1.6764" layer="96" font="vector">&gt;VALUE</text>
<pin name="A0" x="-5.08" y="-2.54" length="middle"/>
<pin name="A1" x="-5.08" y="-5.08" length="middle"/>
<pin name="A2" x="-5.08" y="-7.62" length="middle"/>
<pin name="VSS" x="-5.08" y="-10.16" length="middle"/>
<pin name="VCC" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="WP" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="SCL" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="SDA" x="20.32" y="-10.16" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCP170X" prefix="U" uservalue="yes">
<description>http://ww1.microchip.com/downloads/en/DeviceDoc/20001826C.pdf
Analog - XC6206</description>
<gates>
<gate name="G$1" symbol="STAB-LINEAR-3" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT\" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT89" package="SOT89">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="2"/>
<connect gate="G$1" pin="VOUT\" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NRF8001" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="NRF8001" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN32_PAD">
<connects>
<connect gate="G$1" pin="/REQN" pad="12"/>
<connect gate="G$1" pin="/RESET" pad="19"/>
<connect gate="G$1" pin="/RYDN" pad="16"/>
<connect gate="G$1" pin="ACTIVE" pad="6"/>
<connect gate="G$1" pin="ANT1" pad="21"/>
<connect gate="G$1" pin="ANT2" pad="22"/>
<connect gate="G$1" pin="AVDD@0" pad="24"/>
<connect gate="G$1" pin="AVDD@1" pad="26"/>
<connect gate="G$1" pin="AVDD@2" pad="29"/>
<connect gate="G$1" pin="DCDC" pad="32"/>
<connect gate="G$1" pin="DEC1" pad="3"/>
<connect gate="G$1" pin="DEC2" pad="2"/>
<connect gate="G$1" pin="IREF" pad="25"/>
<connect gate="G$1" pin="MISO" pad="14"/>
<connect gate="G$1" pin="MOSI" pad="13"/>
<connect gate="G$1" pin="NC" pad="15"/>
<connect gate="G$1" pin="RXD" pad="10"/>
<connect gate="G$1" pin="SCK" pad="11"/>
<connect gate="G$1" pin="TXD" pad="7"/>
<connect gate="G$1" pin="VDD@1" pad="1"/>
<connect gate="G$1" pin="VDD@2" pad="9"/>
<connect gate="G$1" pin="VDD_PA" pad="20"/>
<connect gate="G$1" pin="VSS@1" pad="17 GND.1"/>
<connect gate="G$1" pin="VSS@2" pad="18"/>
<connect gate="G$1" pin="VSS@3" pad="23"/>
<connect gate="G$1" pin="VSS@4" pad="30"/>
<connect gate="G$1" pin="VSS@5" pad="31"/>
<connect gate="G$1" pin="VSS@7" pad="8"/>
<connect gate="G$1" pin="XC1" pad="28"/>
<connect gate="G$1" pin="XC2" pad="27"/>
<connect gate="G$1" pin="XL1" pad="5"/>
<connect gate="G$1" pin="XL2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MPU-6050" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="MPU-6050" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN24">
<connects>
<connect gate="G$1" pin="AD0" pad="9"/>
<connect gate="G$1" pin="AUX_CL" pad="7"/>
<connect gate="G$1" pin="AUX_DA" pad="6"/>
<connect gate="G$1" pin="CLKIN" pad="1"/>
<connect gate="G$1" pin="CPOUT" pad="20"/>
<connect gate="G$1" pin="FSYNC" pad="11"/>
<connect gate="G$1" pin="GND" pad="18 PAD"/>
<connect gate="G$1" pin="INT" pad="12"/>
<connect gate="G$1" pin="NC@1" pad="2"/>
<connect gate="G$1" pin="NC@2" pad="3"/>
<connect gate="G$1" pin="NC@3" pad="4"/>
<connect gate="G$1" pin="NC@4" pad="5"/>
<connect gate="G$1" pin="NC@5" pad="14"/>
<connect gate="G$1" pin="NC@6" pad="15"/>
<connect gate="G$1" pin="NC@7" pad="16"/>
<connect gate="G$1" pin="NC@8" pad="17"/>
<connect gate="G$1" pin="REGOUT" pad="10"/>
<connect gate="G$1" pin="RESV1" pad="19"/>
<connect gate="G$1" pin="RESV2" pad="21"/>
<connect gate="G$1" pin="RESV3" pad="22"/>
<connect gate="G$1" pin="SCL" pad="23"/>
<connect gate="G$1" pin="SDA" pad="24"/>
<connect gate="G$1" pin="VDD" pad="13"/>
<connect gate="G$1" pin="VLOGIC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BH1750" prefix="U">
<description>http://rohmfs.rohm.com/en/products/databook/datasheet/ic/sensor/light/bh1750fvi-e.pdf</description>
<gates>
<gate name="G$1" symbol="BH1750" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WSOF6I">
<connects>
<connect gate="G$1" pin="ADDR" pad="2"/>
<connect gate="G$1" pin="DVI" pad="5"/>
<connect gate="G$1" pin="GND" pad="3 GND"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BME280" prefix="U">
<description>PRESSURE</description>
<gates>
<gate name="G$1" symbol="BME280" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LGA-8(BME280)">
<connects>
<connect gate="G$1" pin="CS" pad="2"/>
<connect gate="G$1" pin="GND@1" pad="7"/>
<connect gate="G$1" pin="GND@2" pad="1"/>
<connect gate="G$1" pin="SCK/SCL" pad="4"/>
<connect gate="G$1" pin="SDI/SDA" pad="3"/>
<connect gate="G$1" pin="SDO/ADR" pad="5"/>
<connect gate="G$1" pin="VDD" pad="8"/>
<connect gate="G$1" pin="VDDIO" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC4054" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="LTC4054" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="BAT" pad="3"/>
<connect gate="G$1" pin="CHRG" pad="1"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="PROG" pad="5"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F10XC" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="STM32F10XC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LQFP48">
<connects>
<connect gate="G$1" pin="BOOT0" pad="44"/>
<connect gate="G$1" pin="NRST" pad="7"/>
<connect gate="G$1" pin="OSC-IN" pad="5"/>
<connect gate="G$1" pin="OSC-OUT" pad="6"/>
<connect gate="G$1" pin="OSC32-IN" pad="3"/>
<connect gate="G$1" pin="OSC32-OUT" pad="4"/>
<connect gate="G$1" pin="PA0" pad="10"/>
<connect gate="G$1" pin="PA1" pad="11"/>
<connect gate="G$1" pin="PA10" pad="31"/>
<connect gate="G$1" pin="PA11" pad="32"/>
<connect gate="G$1" pin="PA12" pad="33"/>
<connect gate="G$1" pin="PA13" pad="34"/>
<connect gate="G$1" pin="PA14" pad="37"/>
<connect gate="G$1" pin="PA15" pad="38"/>
<connect gate="G$1" pin="PA2" pad="12"/>
<connect gate="G$1" pin="PA3" pad="13"/>
<connect gate="G$1" pin="PA4" pad="14"/>
<connect gate="G$1" pin="PA5" pad="15"/>
<connect gate="G$1" pin="PA6" pad="16"/>
<connect gate="G$1" pin="PA7" pad="17"/>
<connect gate="G$1" pin="PA8" pad="29"/>
<connect gate="G$1" pin="PA9" pad="30"/>
<connect gate="G$1" pin="PB0" pad="18"/>
<connect gate="G$1" pin="PB1" pad="19"/>
<connect gate="G$1" pin="PB10" pad="21"/>
<connect gate="G$1" pin="PB11" pad="22"/>
<connect gate="G$1" pin="PB12" pad="25"/>
<connect gate="G$1" pin="PB13" pad="26"/>
<connect gate="G$1" pin="PB14" pad="27"/>
<connect gate="G$1" pin="PB15" pad="28"/>
<connect gate="G$1" pin="PB2/BOOT1" pad="20"/>
<connect gate="G$1" pin="PB3" pad="39"/>
<connect gate="G$1" pin="PB4" pad="40"/>
<connect gate="G$1" pin="PB5" pad="41"/>
<connect gate="G$1" pin="PB6" pad="42"/>
<connect gate="G$1" pin="PB7" pad="43"/>
<connect gate="G$1" pin="PB8" pad="45"/>
<connect gate="G$1" pin="PB9" pad="46"/>
<connect gate="G$1" pin="PC13-TAMPER" pad="2"/>
<connect gate="G$1" pin="VBAT" pad="1"/>
<connect gate="G$1" pin="VDD@1" pad="24"/>
<connect gate="G$1" pin="VDD@2" pad="36"/>
<connect gate="G$1" pin="VDD@3" pad="48"/>
<connect gate="G$1" pin="VDDA" pad="9"/>
<connect gate="G$1" pin="VSS@1" pad="23"/>
<connect gate="G$1" pin="VSS@2" pad="35"/>
<connect gate="G$1" pin="VSS@3" pad="47"/>
<connect gate="G$1" pin="VSSA" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="24C0X" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="AT24C02" x="0" y="0"/>
</gates>
<devices>
<device name="-TSSOP8" package="TSSOP08">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-DIP8" package="DIL08">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SO8" package="SO08-150">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_passive">
<packages>
<package name="C0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.9" layer="1"/>
<text x="0" y="0" size="0.7" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="0.4" x2="-1.5" y2="-0.4" width="0.2" layer="21"/>
<wire x1="1.5" y1="-0.4" x2="1.5" y2="0.4" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-0.7" x2="-0.5" y2="-0.7" width="0.2" layer="21"/>
<wire x1="1.2" y1="-0.7" x2="0.5" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-1.2" y1="0.7" x2="-0.5" y2="0.7" width="0.2" layer="21"/>
<wire x1="0.5" y1="0.7" x2="1.2" y2="0.7" width="0.2" layer="21"/>
<wire x1="-1.2" y1="0.7" x2="-1.5" y2="0.4" width="0.2" layer="21" curve="90"/>
<wire x1="-1.2" y1="-0.7" x2="-1.5" y2="-0.4" width="0.2" layer="21" curve="-90"/>
<wire x1="1.2" y1="-0.7" x2="1.5" y2="-0.4" width="0.2" layer="21" curve="90"/>
<wire x1="1.2" y1="0.7" x2="1.5" y2="0.4" width="0.2" layer="21" curve="-90"/>
<text x="0" y="-1" size="0.7" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<text x="0" y="0" size="0.5" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.9" layer="1"/>
<text x="0" y="0" size="0.7" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="-1.5" y1="0.7" x2="-1.5" y2="-0.7" width="0.2" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="1.5" y2="0.7" width="0.2" layer="21"/>
<wire x1="-1.5" y1="-0.7" x2="-0.5" y2="-0.7" width="0.2" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="0.5" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-1.5" y1="0.7" x2="-0.5" y2="0.7" width="0.2" layer="21"/>
<wire x1="0.5" y1="0.7" x2="1.5" y2="0.7" width="0.2" layer="21"/>
<text x="0" y="-1" size="0.7" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<text x="0" y="0" size="0.6" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="0" y="0" size="0.4" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="-0.275" y2="0.25" width="0.127" layer="51"/>
<wire x1="-0.275" y1="0.25" x2="0.275" y2="0.25" width="0.127" layer="51"/>
<wire x1="0.275" y1="0.25" x2="0.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.127" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="0.275" y2="-0.25" width="0.127" layer="51"/>
<wire x1="0.275" y1="-0.25" x2="-0.275" y2="-0.25" width="0.127" layer="51"/>
<wire x1="-0.275" y1="-0.25" x2="-0.5" y2="-0.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="0" y1="-0.3" x2="0" y2="0.3" width="0.15" layer="21"/>
<text x="0" y="0" size="0.3" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="1" x="-2.5" y="0" dx="1.4" dy="2.8" layer="1"/>
<smd name="2" x="2.5" y="0" dx="1.4" dy="2.8" layer="1"/>
<text x="0" y="0" size="1.5" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="-2.5" y1="1.25" x2="2.5" y2="1.25" width="0.127" layer="51"/>
<wire x1="2.5" y1="1.25" x2="2.5" y2="-1.25" width="0.127" layer="51"/>
<wire x1="2.5" y1="-1.25" x2="-2.5" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-1.25" x2="-2.5" y2="1.25" width="0.127" layer="51"/>
<wire x1="-1" y1="1.7" x2="-3.5" y2="1.7" width="0.2" layer="21"/>
<wire x1="-3.5" y1="1.7" x2="-3.5" y2="-1.7" width="0.2" layer="21"/>
<wire x1="-3.5" y1="-1.7" x2="-1" y2="-1.7" width="0.2" layer="21"/>
<wire x1="1" y1="-1.7" x2="3.5" y2="-1.7" width="0.2" layer="21"/>
<wire x1="3.5" y1="-1.7" x2="3.5" y2="1.7" width="0.2" layer="21"/>
<wire x1="3.5" y1="1.7" x2="1" y2="1.7" width="0.2" layer="21"/>
</package>
<package name="R0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="0" y="0" size="0.4" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="-0.275" y2="0.25" width="0.127" layer="51"/>
<wire x1="-0.275" y1="0.25" x2="0.275" y2="0.25" width="0.127" layer="51"/>
<wire x1="0.275" y1="0.25" x2="0.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.127" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="0.275" y2="-0.25" width="0.127" layer="51"/>
<wire x1="0.275" y1="-0.25" x2="-0.275" y2="-0.25" width="0.127" layer="51"/>
<wire x1="-0.275" y1="-0.25" x2="-0.5" y2="-0.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="0" y1="-0.3" x2="0" y2="0.3" width="0.15" layer="21"/>
</package>
<package name="R0201">
<smd name="1" x="-0.3" y="0" dx="0.3" dy="0.4" layer="1"/>
<smd name="2" x="0.3" y="0" dx="0.3" dy="0.4" layer="1"/>
<text x="0" y="0" size="0.2" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="0" y1="0.2" x2="0" y2="-0.2" width="0.1" layer="21"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-12.03" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="12.03" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-12" y="0" drill="1.3" diameter="3" shape="octagon"/>
<pad name="2" x="12" y="0" drill="1.2" diameter="3" shape="octagon"/>
<text x="0" y="0" size="2" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="HS50">
<description>ARCOL Power Resistor 50W&lt;p&gt;
Distrib. RS Component</description>
<wire x1="24.95" y1="7" x2="24.95" y2="14.55" width="0.2032" layer="21"/>
<wire x1="24.95" y1="14.55" x2="14.55" y2="14.55" width="0.2032" layer="21"/>
<wire x1="-24.95" y1="-7" x2="24.95" y2="-7" width="0.2032" layer="21"/>
<wire x1="24.95" y1="-7" x2="24.95" y2="7" width="0.2032" layer="21"/>
<wire x1="24.95" y1="7" x2="14.55" y2="7" width="0.2032" layer="21"/>
<wire x1="14.55" y1="7" x2="-24.95" y2="7" width="0.2032" layer="21"/>
<wire x1="-24.95" y1="7" x2="-24.95" y2="-7" width="0.2032" layer="21"/>
<wire x1="14.55" y1="7" x2="14.55" y2="14.55" width="0.2032" layer="21"/>
<wire x1="-24.95" y1="-7" x2="-24.95" y2="-14.55" width="0.2032" layer="21"/>
<wire x1="-24.95" y1="-14.55" x2="-14.55" y2="-14.55" width="0.2032" layer="21"/>
<wire x1="-14.55" y1="-7" x2="-14.55" y2="-14.55" width="0.2032" layer="21"/>
<wire x1="-25" y1="0.5" x2="-36" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-36" y1="0.5" x2="-36" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="-36" y1="-0.5" x2="-25.1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="25" y1="-0.5" x2="36" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="36" y1="-0.5" x2="36" y2="0.5" width="0.2032" layer="51"/>
<wire x1="36" y1="0.5" x2="25.1" y2="0.5" width="0.2032" layer="51"/>
<pad name="1" x="-31.47" y="0" drill="1.3" shape="octagon"/>
<pad name="2" x="31.47" y="0" drill="1.3" shape="octagon"/>
<text x="-6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.35" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-25.5" y1="-7.5" x2="25.5" y2="7.5" layer="43"/>
<rectangle x1="14" y1="7.5" x2="25.5" y2="15" layer="43"/>
<rectangle x1="-25.5" y1="-15" x2="-14" y2="-7.5" layer="43"/>
<hole x="19.85" y="10.7" drill="3.2"/>
<hole x="-19.85" y="-10.7" drill="3.2"/>
</package>
<package name="C0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-1" y="0" dx="1.3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1" layer="1" rot="R90"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.6" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1.5" y1="0.9" x2="-0.4" y2="0.9" width="0.2" layer="21"/>
<wire x1="0.4" y1="0.9" x2="1.5" y2="0.9" width="0.2" layer="21"/>
<wire x1="1.8" y1="0.6" x2="1.8" y2="-0.6" width="0.2" layer="21"/>
<wire x1="1.5" y1="-0.9" x2="0.4" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.5" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-0.6" x2="-1.8" y2="0.6" width="0.2" layer="21"/>
<wire x1="-1.5" y1="0.9" x2="-1.8" y2="0.6" width="0.2" layer="21" curve="90"/>
<wire x1="1.5" y1="0.9" x2="1.8" y2="0.6" width="0.2" layer="21" curve="-90"/>
<wire x1="1.5" y1="-0.9" x2="1.8" y2="-0.6" width="0.2" layer="21" curve="90"/>
<wire x1="-1.5" y1="-0.9" x2="-1.8" y2="-0.6" width="0.2" layer="21" curve="-90"/>
</package>
<package name="C1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="2" x="1.5" y="0" dx="1" dy="1.6" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="1" dy="1.6" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-1.8" size="1" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.127" layer="51"/>
<wire x1="-0.9" y1="1.1" x2="-1.9" y2="1.1" width="0.2" layer="21"/>
<wire x1="-2.3" y1="0.7" x2="-2.3" y2="0.4" width="0.2" layer="21"/>
<wire x1="-2.3" y1="-0.4" x2="-2.3" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-1.9" y1="-1.1" x2="-0.9" y2="-1.1" width="0.2" layer="21"/>
<wire x1="0.9" y1="-1.1" x2="1.9" y2="-1.1" width="0.2" layer="21"/>
<wire x1="2.3" y1="-0.7" x2="2.3" y2="-0.4" width="0.2" layer="21"/>
<wire x1="2.3" y1="0.4" x2="2.3" y2="0.7" width="0.2" layer="21"/>
<wire x1="1.9" y1="1.1" x2="0.9" y2="1.1" width="0.2" layer="21"/>
<wire x1="1.9" y1="-1.1" x2="2.3" y2="-0.7" width="0.2" layer="21" curve="90"/>
<wire x1="1.9" y1="1.1" x2="2.3" y2="0.7" width="0.2" layer="21" curve="-90"/>
<wire x1="-1.9" y1="1.1" x2="-2.3" y2="0.7" width="0.2" layer="21" curve="90"/>
<wire x1="-1.9" y1="-1.1" x2="-2.3" y2="-0.7" width="0.2" layer="21" curve="-90"/>
<text x="0" y="0" size="0.8" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
</package>
<package name="C2.5/5-4">
<description>&lt;B&gt;MKS2&lt;/B&gt;, 5 - 7.5 x 4 mm, grid 2.54 + 5.08 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-1.27" y="2.032" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="2" x="1.5" y="0" dx="1" dy="1.6" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="1" dy="1.6" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-1.7" size="0.8" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.127" layer="51"/>
<wire x1="-0.9" y1="1.1" x2="-2.2" y2="1.1" width="0.2" layer="21"/>
<wire x1="-2.2" y1="1.1" x2="-2.2" y2="0.4" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-0.4" x2="-2.2" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-1.1" x2="-0.9" y2="-1.1" width="0.2" layer="21"/>
<wire x1="0.9" y1="-1.1" x2="2.2" y2="-1.1" width="0.2" layer="21"/>
<wire x1="2.2" y1="-1.1" x2="2.2" y2="-0.4" width="0.2" layer="21"/>
<wire x1="2.2" y1="0.4" x2="2.2" y2="1.1" width="0.2" layer="21"/>
<wire x1="2.2" y1="1.1" x2="0.9" y2="1.1" width="0.2" layer="21"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-1" y="0" dx="1.3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1" layer="1" rot="R90"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-2" size="0.6" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1.8" y1="0.9" x2="-0.4" y2="0.9" width="0.2" layer="21"/>
<wire x1="0.4" y1="0.9" x2="1.8" y2="0.9" width="0.2" layer="21"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.2" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="0.4" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.8" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.2" layer="21"/>
</package>
<package name="2W-0922-22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="12.93" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-13.03" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.2" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.2" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.2" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.2" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.2" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.2" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.2" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.2" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-13" y="0" drill="1.1" diameter="1.6" shape="long"/>
<pad name="2" x="13" y="0" drill="1.1" diameter="1.6" shape="long"/>
<text x="0" y="0" size="2" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-2" size="1.5" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
<wire x1="-9.5" y1="4.6" x2="-8.9" y2="4.6" width="0.2" layer="51"/>
<wire x1="-8.9" y1="4.6" x2="-8.6" y2="4.3" width="0.2" layer="51"/>
<wire x1="-8.6" y1="4.3" x2="8.6" y2="4.3" width="0.2" layer="51"/>
<wire x1="8.6" y1="4.3" x2="8.9" y2="4.6" width="0.2" layer="51"/>
<wire x1="8.9" y1="4.6" x2="9.8" y2="4.6" width="0.2" layer="51"/>
<wire x1="9.8" y1="4.6" x2="10.2" y2="4.2" width="0.2" layer="51" curve="-90"/>
<wire x1="10.2" y1="4.2" x2="10.2" y2="-4.1" width="0.2" layer="51"/>
<wire x1="10.2" y1="-4.1" x2="9.7" y2="-4.6" width="0.2" layer="51" curve="-90"/>
<wire x1="9.7" y1="-4.6" x2="8.9" y2="-4.6" width="0.2" layer="51"/>
<wire x1="8.9" y1="-4.6" x2="8.6" y2="-4.3" width="0.2" layer="51"/>
<wire x1="8.6" y1="-4.3" x2="-8.6" y2="-4.3" width="0.2" layer="51"/>
<wire x1="-8.6" y1="-4.3" x2="-8.9" y2="-4.6" width="0.2" layer="51"/>
<wire x1="-8.9" y1="-4.6" x2="-9.7" y2="-4.6" width="0.2" layer="51"/>
<wire x1="-9.7" y1="-4.6" x2="-10.2" y2="-4.1" width="0.2" layer="51" curve="-90"/>
<wire x1="-10.2" y1="-4.1" x2="-10.2" y2="4" width="0.2" layer="51"/>
<wire x1="-10.2" y1="4" x2="-9.6" y2="4.6" width="0.2" layer="51" curve="-90"/>
</package>
<package name="0805-THM-7">
<wire x1="4.318" y1="0" x2="3.302" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="0" x2="-3.302" y2="0" width="0.6096" layer="51"/>
<wire x1="-2.413" y1="0.889" x2="-2.159" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="-0.889" x2="-2.159" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.143" x2="2.413" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="1.143" x2="2.413" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="-0.889" x2="-2.413" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.143" x2="-1.778" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="1.016" x2="-1.778" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-1.143" x2="-1.778" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-1.016" x2="-1.778" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.016" x2="1.778" y2="1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.016" x2="-1.651" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.016" x2="1.778" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.016" x2="-1.651" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.143" x2="1.778" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.143" x2="1.778" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.889" x2="2.413" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-3.738" y1="0" x2="-1.1" y2="0" width="0.3048" layer="1"/>
<wire x1="1" y1="0" x2="4.338" y2="0" width="0.3048" layer="1"/>
<pad name="1" x="-4.318" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="4.318" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-3.1306" y="-2.775" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.413" y1="-0.3048" x2="3.2766" y2="0.3048" layer="21"/>
<rectangle x1="-3.2766" y1="-0.3048" x2="-2.413" y2="0.3048" layer="21"/>
<rectangle x1="-1.6254" y1="-0.7" x2="-0.4254" y2="0.7" layer="1"/>
<rectangle x1="0.4254" y1="-0.7" x2="1.6254" y2="0.7" layer="1"/>
<rectangle x1="-1.7254" y1="-0.8" x2="-0.3254" y2="0.8" layer="29"/>
<rectangle x1="0.3254" y1="-0.8" x2="1.7254" y2="0.8" layer="29"/>
<rectangle x1="-1.64" y1="-0.7" x2="-0.41" y2="0.71" layer="31"/>
<rectangle x1="0.4" y1="-0.7" x2="1.64" y2="0.7" layer="31"/>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95" font="vector" align="center-left">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96" font="vector" align="center-left">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="C2.5/5-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-EU" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-THM-7" package="0805-THM-7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2W-0922/22" package="2W-0922-22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HS50" package="HS50">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_connectors">
<packages>
<package name="PLS-2.54-2">
<wire x1="0" y1="1.016" x2="0.254" y2="1.27" width="0.2" layer="21"/>
<wire x1="0" y1="1.016" x2="-0.254" y2="1.27" width="0.2" layer="21"/>
<wire x1="0" y1="-1.016" x2="0.254" y2="-1.27" width="0.2" layer="21"/>
<wire x1="0" y1="-1.016" x2="-0.254" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="-2.54" y2="-1.27" width="0.2" layer="21"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.2" layer="21"/>
<wire x1="2.286" y1="-1.27" x2="0.254" y2="-1.27" width="0.2" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.2" layer="21"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.2" layer="21"/>
<wire x1="2.286" y1="1.27" x2="0.254" y2="1.27" width="0.2" layer="21"/>
<wire x1="-0.254" y1="1.27" x2="-2.54" y2="1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.2" layer="21"/>
<rectangle x1="0.9652" y1="-0.3048" x2="1.5748" y2="0.3048" layer="51" rot="R270"/>
<rectangle x1="-1.5748" y1="-0.3048" x2="-0.9652" y2="0.3048" layer="51" rot="R270"/>
<pad name="1" x="-1.27" y="0" drill="1" diameter="1.778" shape="square" rot="R270"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.778" rot="R270"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="0" y1="1.016" x2="0.254" y2="1.27" width="0.2" layer="51"/>
<wire x1="0" y1="1.016" x2="-0.254" y2="1.27" width="0.2" layer="51"/>
<wire x1="0" y1="-1.016" x2="0.254" y2="-1.27" width="0.2" layer="51"/>
<wire x1="0" y1="-1.016" x2="-0.254" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-2.54" y2="-1.27" width="0.2" layer="51"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.2" layer="51"/>
<wire x1="2.286" y1="-1.27" x2="0.254" y2="-1.27" width="0.2" layer="51"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.2" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.2" layer="51"/>
<wire x1="2.286" y1="1.27" x2="0.254" y2="1.27" width="0.2" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-2.54" y2="1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.2" layer="51"/>
</package>
<package name="JP1R">
<wire x1="2.54" y1="1.778" x2="2.54" y2="3.81" width="0.254" layer="21"/>
<wire x1="0" y1="1.778" x2="0" y2="3.81" width="0.254" layer="21"/>
<wire x1="0" y1="1.778" x2="0.254" y2="1.524" width="0.254" layer="21"/>
<wire x1="-0.254" y1="1.524" x2="0" y2="1.778" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.778" x2="-2.286" y2="1.524" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.778" x2="-2.54" y2="3.81" width="0.254" layer="21"/>
<wire x1="2.286" y1="1.524" x2="0.254" y2="1.524" width="0.254" layer="21"/>
<wire x1="2.286" y1="1.524" x2="2.54" y2="1.778" width="0.254" layer="21"/>
<wire x1="-0.254" y1="1.524" x2="-2.286" y2="1.524" width="0.254" layer="21"/>
<wire x1="2.286" y1="4.064" x2="0.254" y2="4.064" width="0.254" layer="21"/>
<wire x1="-0.254" y1="4.064" x2="-2.286" y2="4.064" width="0.254" layer="21"/>
<wire x1="0" y1="3.81" x2="0.254" y2="4.064" width="0.254" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="-2.286" y2="4.064" width="0.254" layer="21"/>
<wire x1="2.286" y1="4.064" x2="2.54" y2="3.81" width="0.254" layer="21"/>
<wire x1="-0.254" y1="4.064" x2="0" y2="3.81" width="0.254" layer="21"/>
<wire x1="1.27" y1="4.572" x2="1.27" y2="9.525" width="0.6604" layer="21"/>
<wire x1="-1.27" y1="4.572" x2="-1.27" y2="9.525" width="0.6604" layer="21"/>
<rectangle x1="0.9652" y1="1.016" x2="1.5748" y2="1.524" layer="21"/>
<rectangle x1="-1.5748" y1="1.016" x2="-0.9652" y2="1.524" layer="21"/>
<rectangle x1="0.9652" y1="-0.254" x2="1.5748" y2="1.016" layer="51"/>
<rectangle x1="-1.5748" y1="-0.254" x2="-0.9652" y2="1.016" layer="51"/>
<rectangle x1="0.9652" y1="4.064" x2="1.5748" y2="4.572" layer="21"/>
<rectangle x1="-1.5748" y1="4.064" x2="-0.9652" y2="4.572" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1" diameter="1.778" shape="square" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.778" rot="R90"/>
<text x="-3.175" y="-0.635" size="1.27" layer="25" ratio="20" rot="R90">&gt;NAME</text>
<text x="-1.524" y="2.54" size="1.016" layer="21" ratio="20">1</text>
<text x="0.889" y="2.54" size="1.016" layer="21" ratio="20">2</text>
<text x="4.445" y="-0.635" size="1.27" layer="27" ratio="20" rot="R90">&gt;VALUE</text>
</package>
<package name="ISP-TYPEB-4">
<smd name="1" x="0" y="0" dx="1.4" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="0" dx="1.4" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="0" dx="1.4" dy="1.2" layer="1" rot="R90"/>
<smd name="4" x="7.62" y="0" dx="1.4" dy="1.2" layer="1" rot="R90"/>
<wire x1="-1.27" y1="1.27" x2="8.89" y2="1.27" width="0.254" layer="21"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="-1.27" width="0.254" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="-0.635" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="1.27" width="0.254" layer="21"/>
<text x="-1.27" y="2.54" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
</package>
<package name="PLS-2.54-4">
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="-1.016" x2="-2.54" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-5.08" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="0" y2="-1.016" width="0.2" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="-2.286" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-0.254" y1="1.27" x2="0" y2="1.016" width="0.2" layer="21"/>
<wire x1="-0.254" y1="1.27" x2="-2.286" y2="1.27" width="0.2" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-5.08" y2="1.27" width="0.2" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.2" layer="21"/>
<rectangle x1="-1.5748" y1="-0.3048" x2="-0.9652" y2="0.3048" layer="51" rot="R270"/>
<rectangle x1="-4.1148" y1="-0.3048" x2="-3.5052" y2="0.3048" layer="51" rot="R270"/>
<pad name="1" x="-3.81" y="0" drill="1" diameter="1.778" shape="square" rot="R270"/>
<pad name="2" x="-1.27" y="0" drill="1" diameter="1.778" rot="R270"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="0" y1="1.016" x2="0.254" y2="1.27" width="0.2" layer="21"/>
<wire x1="0" y1="-1.016" x2="0.254" y2="-1.27" width="0.2" layer="21"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.2" layer="21"/>
<wire x1="2.286" y1="-1.27" x2="0.254" y2="-1.27" width="0.2" layer="21"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.2" layer="21"/>
<wire x1="2.286" y1="1.27" x2="0.254" y2="1.27" width="0.2" layer="21"/>
<rectangle x1="0.9652" y1="-0.3048" x2="1.5748" y2="0.3048" layer="51" rot="R270"/>
<pad name="3" x="1.27" y="0" drill="1" diameter="1.778" rot="R270"/>
<wire x1="2.54" y1="1.016" x2="2.794" y2="1.27" width="0.2" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.794" y2="-1.27" width="0.2" layer="21"/>
<wire x1="4.826" y1="-1.27" x2="5.08" y2="-1.016" width="0.2" layer="21"/>
<wire x1="4.826" y1="-1.27" x2="2.794" y2="-1.27" width="0.2" layer="21"/>
<wire x1="5.08" y1="-1.016" x2="5.08" y2="1.016" width="0.2" layer="21"/>
<wire x1="4.826" y1="1.27" x2="5.08" y2="1.016" width="0.2" layer="21"/>
<wire x1="4.826" y1="1.27" x2="2.794" y2="1.27" width="0.2" layer="21"/>
<rectangle x1="3.5052" y1="-0.3048" x2="4.1148" y2="0.3048" layer="51" rot="R270"/>
<pad name="4" x="3.81" y="0" drill="1" diameter="1.778" rot="R270"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.2" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="-2.54" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="-1.27" x2="-5.08" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="0" y2="-1.016" width="0.2" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-2.286" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="0" y2="1.016" width="0.2" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-2.286" y2="1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="1.27" x2="-5.08" y2="1.27" width="0.2" layer="51"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.2" layer="51"/>
<wire x1="0" y1="1.016" x2="0.254" y2="1.27" width="0.2" layer="51"/>
<wire x1="0" y1="-1.016" x2="0.254" y2="-1.27" width="0.2" layer="51"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.2" layer="51"/>
<wire x1="2.286" y1="-1.27" x2="0.254" y2="-1.27" width="0.2" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.2" layer="51"/>
<wire x1="2.286" y1="1.27" x2="0.254" y2="1.27" width="0.2" layer="51"/>
<wire x1="2.54" y1="1.016" x2="2.794" y2="1.27" width="0.2" layer="51"/>
<wire x1="2.54" y1="-1.016" x2="2.794" y2="-1.27" width="0.2" layer="51"/>
<wire x1="4.826" y1="-1.27" x2="5.08" y2="-1.016" width="0.2" layer="51"/>
<wire x1="4.826" y1="-1.27" x2="2.794" y2="-1.27" width="0.2" layer="51"/>
<wire x1="5.08" y1="-1.016" x2="5.08" y2="1.016" width="0.2" layer="51"/>
<wire x1="4.826" y1="1.27" x2="5.08" y2="1.016" width="0.2" layer="51"/>
<wire x1="4.826" y1="1.27" x2="2.794" y2="1.27" width="0.2" layer="51"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.2" layer="51"/>
</package>
<package name="1X02_LOCK">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="JP1_SMALL">
<wire x1="-0.762" y1="0" x2="-1.016" y2="0.254" width="0.2" layer="21"/>
<wire x1="-0.762" y1="0" x2="-1.016" y2="-0.254" width="0.2" layer="21"/>
<wire x1="0.762" y1="0" x2="1.016" y2="0.254" width="0.2" layer="21"/>
<wire x1="0.762" y1="0" x2="1.016" y2="-0.254" width="0.2" layer="21"/>
<wire x1="1.016" y1="-0.254" x2="1.016" y2="-2.286" width="0.2" layer="21"/>
<wire x1="0.762" y1="-2.54" x2="1.016" y2="-2.286" width="0.2" layer="21"/>
<wire x1="1.016" y1="2.286" x2="0.762" y2="2.54" width="0.2" layer="21"/>
<wire x1="1.016" y1="2.286" x2="1.016" y2="0.254" width="0.2" layer="21"/>
<wire x1="0.762" y1="2.54" x2="-0.762" y2="2.54" width="0.2" layer="21"/>
<wire x1="-1.016" y1="2.286" x2="-0.762" y2="2.54" width="0.2" layer="21"/>
<wire x1="-1.016" y1="2.286" x2="-1.016" y2="0.254" width="0.2" layer="21"/>
<wire x1="-1.016" y1="-0.254" x2="-1.016" y2="-2.286" width="0.2" layer="21"/>
<wire x1="-0.762" y1="-2.54" x2="-1.016" y2="-2.286" width="0.2" layer="21"/>
<wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.2" layer="21"/>
<rectangle x1="-0.3048" y1="0.9652" x2="0.3048" y2="1.5748" layer="51"/>
<rectangle x1="-0.3048" y1="-1.5748" x2="0.3048" y2="-0.9652" layer="51"/>
<pad name="1" x="0" y="-1.27" drill="1" diameter="1.5" shape="square"/>
<pad name="2" x="0" y="1.27" drill="1" diameter="1.5"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="2.921" y="-2.54" size="1.27" layer="27" ratio="20" rot="R90">&gt;VALUE</text>
</package>
<package name="TERMINAL_BLOCK_2P_5">
<description>wire to board terminal block 2 pins 5mm pitch &lt;br&gt;
Mfr no: 1776244-2 &lt;br&gt;
&lt;a href="http://www.ebay.com/itm/10pcs-2-Pin-Screw-Terminal-Block-Connector-5mm-Pitch-B-/300625175136?pt=LH_DefaultDomain_0&amp;hash=item45fea82260#ht_1330wt_1396"&gt; Ebay &lt;/a&gt;
&lt;br&gt;
&lt;a href="http://www.digikey.com/product-detail/en/1776244-2/A97996-ND/1826859"&gt; Digikey &lt;/a&gt;</description>
<wire x1="-5" y1="4.2" x2="5" y2="4.2" width="0.2" layer="21"/>
<wire x1="5" y1="4.2" x2="5" y2="-3.3" width="0.2" layer="21"/>
<wire x1="5" y1="-3.3" x2="-5" y2="-3.3" width="0.2" layer="21"/>
<wire x1="-5" y1="-3.3" x2="-5" y2="2.47" width="0.2" layer="21"/>
<wire x1="-5" y1="2.47" x2="-5" y2="2.605" width="0.1524" layer="21"/>
<wire x1="-5" y1="2.605" x2="-5" y2="3.195" width="0.2" layer="21"/>
<wire x1="-5" y1="3.195" x2="-5" y2="4.2" width="0.2" layer="21"/>
<wire x1="-5" y1="3.195" x2="-5.58" y2="3.35" width="0.2" layer="21"/>
<wire x1="-5.58" y1="3.35" x2="-5.58" y2="2.45" width="0.2" layer="21"/>
<wire x1="-5.58" y1="2.45" x2="-5" y2="2.605" width="0.2" layer="21"/>
<wire x1="-1.417" y1="1.375" x2="-2.158" y2="0.092" width="0.0762" layer="51"/>
<wire x1="-2.158" y1="0.092" x2="-0.875" y2="-0.65" width="0.0762" layer="51"/>
<wire x1="-1.125" y1="-1.083" x2="-2.408" y2="-0.342" width="0.0762" layer="51"/>
<wire x1="-2.408" y1="-0.342" x2="-3.15" y2="-1.625" width="0.0762" layer="51"/>
<wire x1="-3.583" y1="-1.375" x2="-2.842" y2="-0.092" width="0.0762" layer="51"/>
<wire x1="-2.842" y1="-0.092" x2="-4.125" y2="0.65" width="0.0762" layer="51"/>
<wire x1="-3.875" y1="1.083" x2="-2.592" y2="0.342" width="0.0762" layer="51"/>
<wire x1="-2.592" y1="0.342" x2="-1.85" y2="1.625" width="0.0762" layer="51"/>
<wire x1="3.583" y1="1.375" x2="2.842" y2="0.092" width="0.0762" layer="51"/>
<wire x1="2.842" y1="0.092" x2="4.125" y2="-0.65" width="0.0762" layer="51"/>
<wire x1="3.875" y1="-1.083" x2="2.592" y2="-0.342" width="0.0762" layer="51"/>
<wire x1="2.592" y1="-0.342" x2="1.85" y2="-1.625" width="0.0762" layer="51"/>
<wire x1="1.417" y1="-1.375" x2="2.158" y2="-0.092" width="0.0762" layer="51"/>
<wire x1="2.158" y1="-0.092" x2="0.875" y2="0.65" width="0.0762" layer="51"/>
<wire x1="1.125" y1="1.083" x2="2.408" y2="0.342" width="0.0762" layer="51"/>
<wire x1="2.408" y1="0.342" x2="3.15" y2="1.625" width="0.0762" layer="51"/>
<circle x="-2.5" y="0" radius="1.75" width="0.0762" layer="51"/>
<circle x="2.5" y="0" radius="1.75" width="0.0762" layer="51"/>
<pad name="1" x="2.5" y="0" drill="1.4" diameter="2.38" shape="long" rot="R90"/>
<pad name="2" x="-2.5" y="0" drill="1.4" diameter="2.38" shape="long" rot="R90"/>
<text x="2" y="-5" size="1.27" layer="21" font="vector" ratio="10">1</text>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="-5" y1="4.2" x2="5" y2="4.2" width="0.2" layer="51"/>
<wire x1="5" y1="4.2" x2="5" y2="2.47" width="0.2" layer="51"/>
<wire x1="5" y1="2.47" x2="5" y2="-2.56" width="0.2" layer="51"/>
<wire x1="5" y1="-2.56" x2="5" y2="-3.3" width="0.2" layer="51"/>
<wire x1="5" y1="-3.3" x2="-5" y2="-3.3" width="0.2" layer="51"/>
<wire x1="-5" y1="-3.3" x2="-5" y2="-2.56" width="0.2" layer="51"/>
<wire x1="-5" y1="-2.56" x2="-5" y2="2.47" width="0.2" layer="51"/>
<wire x1="-5" y1="2.47" x2="-5" y2="2.605" width="0.1524" layer="51"/>
<wire x1="-5" y1="2.605" x2="-5" y2="3.195" width="0.2" layer="51"/>
<wire x1="-5" y1="3.195" x2="-5" y2="4.2" width="0.2" layer="51"/>
<wire x1="-5" y1="3.195" x2="-5.58" y2="3.35" width="0.2" layer="51"/>
<wire x1="-5.58" y1="3.35" x2="-5.58" y2="2.45" width="0.2" layer="51"/>
<wire x1="-5.58" y1="2.45" x2="-5" y2="2.605" width="0.2" layer="51"/>
<wire x1="-5" y1="2.47" x2="5" y2="2.47" width="0.1" layer="51"/>
<wire x1="-5" y1="-2.56" x2="5" y2="-2.56" width="0.1" layer="51"/>
<circle x="-2.5" y="2.9" radius="0.45" width="0.0762" layer="51"/>
<circle x="2.5" y="2.9" radius="0.45" width="0.0762" layer="51"/>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<wire x1="-1.1" y1="0" x2="-1.3" y2="0" width="0.2" layer="21"/>
<wire x1="-1.3" y1="0" x2="-1.3" y2="1" width="0.2" layer="21"/>
<wire x1="-1.3" y1="1" x2="-1" y2="1.3" width="0.2" layer="21"/>
<wire x1="-1.3" y1="0" x2="-1.3" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-1.3" y1="-1.1" x2="-1" y2="-1.4" width="0.2" layer="21"/>
<wire x1="3.9" y1="0" x2="3.7" y2="0" width="0.2" layer="21"/>
<wire x1="3.9" y1="0" x2="3.9" y2="0.9" width="0.2" layer="21"/>
<wire x1="3.9" y1="0.9" x2="3.6" y2="1.2" width="0.2" layer="21"/>
<wire x1="3.9" y1="0" x2="3.9" y2="-1.1" width="0.2" layer="21"/>
<wire x1="3.9" y1="-1.1" x2="3.6" y2="-1.4" width="0.2" layer="21"/>
<wire x1="0.9" y1="0" x2="1.6" y2="0" width="0.2" layer="21"/>
</package>
<package name="7395-04">
<description>&lt;b&gt;MOLEX 2.54mm KK RA CONNECTOR&lt;/b&gt;
&lt;br&gt;Fixed Orientation</description>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-1.905" width="0.254" layer="51"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-1.905" width="0.254" layer="51"/>
<wire x1="4.445" y1="-1.905" x2="4.445" y2="-2.54" width="0.254" layer="51"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.254" layer="51"/>
<wire x1="3.175" y1="-2.54" x2="3.175" y2="-1.905" width="0.254" layer="51"/>
<wire x1="3.175" y1="-1.905" x2="1.905" y2="-1.905" width="0.254" layer="51"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="-2.54" width="0.254" layer="51"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.254" layer="51"/>
<wire x1="0.635" y1="-2.54" x2="0.635" y2="-1.905" width="0.254" layer="51"/>
<wire x1="0.635" y1="-1.905" x2="-0.635" y2="-1.905" width="0.254" layer="51"/>
<wire x1="-0.635" y1="-1.905" x2="-0.635" y2="-2.54" width="0.254" layer="51"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.254" layer="51"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="-1.905" width="0.254" layer="51"/>
<wire x1="-1.905" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="51"/>
<wire x1="-3.175" y1="-1.905" x2="-3.175" y2="-2.54" width="0.254" layer="51"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.254" layer="51"/>
<wire x1="-4.445" y1="-2.54" x2="-4.445" y2="-1.905" width="0.254" layer="51"/>
<wire x1="-4.445" y1="-1.905" x2="-5.08" y2="-1.905" width="0.254" layer="51"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="-5.08" width="0.254" layer="51"/>
<wire x1="-5.08" y1="-5.08" x2="-4.191" y2="-5.08" width="0.254" layer="51"/>
<wire x1="-4.191" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="51"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="51"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-13.081" width="0.254" layer="51"/>
<wire x1="3.81" y1="-13.081" x2="3.556" y2="-13.589" width="0.254" layer="51"/>
<wire x1="3.556" y1="-13.589" x2="-0.0508" y2="-13.589" width="0.254" layer="51"/>
<wire x1="-3.81" y1="-5.08" x2="-3.81" y2="-13.081" width="0.254" layer="51"/>
<wire x1="-3.81" y1="-13.081" x2="-3.556" y2="-13.589" width="0.254" layer="51"/>
<wire x1="-3.556" y1="-13.589" x2="0.0508" y2="-13.589" width="0.254" layer="51"/>
<wire x1="1.27" y1="-5.08" x2="0.889" y2="-5.08" width="0.254" layer="51"/>
<wire x1="-4.191" y1="-5.08" x2="-4.191" y2="-11.303" width="0.254" layer="51"/>
<wire x1="-4.191" y1="-11.303" x2="-3.8608" y2="-11.6586" width="0.254" layer="51"/>
<wire x1="4.191" y1="-5.08" x2="4.191" y2="-11.303" width="0.254" layer="51"/>
<wire x1="4.191" y1="-11.303" x2="3.8608" y2="-11.6586" width="0.254" layer="51"/>
<pad name="1" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="3.3259" y="-3.4021" size="1.27" layer="51" ratio="14" rot="R270">1</text>
<text x="-4.2433" y="-3.2751" size="1.27" layer="51" ratio="14" rot="R270">4</text>
<text x="5.9421" y="-2.4399" size="0.8128" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-2.5146" x2="4.064" y2="-0.2794" layer="51"/>
<rectangle x1="1.016" y1="-2.5146" x2="1.524" y2="-0.2794" layer="51"/>
<rectangle x1="-1.524" y1="-2.5146" x2="-1.016" y2="-0.2794" layer="51"/>
<rectangle x1="-4.064" y1="-2.5146" x2="-3.556" y2="-0.2794" layer="51"/>
</package>
<package name="2C_SMD">
<rectangle x1="-0.3048" y1="0.9652" x2="0.3048" y2="1.5748" layer="51"/>
<rectangle x1="-0.3048" y1="-1.5748" x2="0.3048" y2="-0.9652" layer="51"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="2.921" y="-2.54" size="1.27" layer="27" ratio="20" rot="R90">&gt;VALUE</text>
<smd name="2" x="-1.27" y="1.27" dx="4" dy="2" layer="1"/>
<smd name="1" x="-1.27" y="-1.27" dx="4" dy="2" layer="1"/>
<wire x1="-3" y1="0" x2="1" y2="0" width="0.2" layer="21"/>
<wire x1="1" y1="0" x2="1" y2="2.4" width="0.2" layer="21"/>
<wire x1="1" y1="0" x2="1" y2="-2.4" width="0.2" layer="21"/>
</package>
<package name="PLS2-2">
<pad name="1" x="0" y="0" drill="0.8" diameter="1.4" shape="square"/>
<pad name="2" x="2" y="0" drill="0.8" diameter="1.4"/>
<wire x1="-1" y1="1" x2="1" y2="1" width="0.2" layer="21"/>
<wire x1="1" y1="1" x2="3" y2="1" width="0.2" layer="21"/>
<wire x1="3" y1="1" x2="3" y2="-1" width="0.2" layer="21"/>
<wire x1="3" y1="-1" x2="1" y2="-1" width="0.2" layer="21"/>
<wire x1="1" y1="-1" x2="-1" y2="-1" width="0.2" layer="21"/>
<wire x1="-1" y1="-1" x2="-1" y2="1" width="0.2" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.2" layer="21"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<wire x1="-1" y1="0.7" x2="-0.7" y2="1" width="0.1" layer="51"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1" layer="51"/>
<wire x1="0.7" y1="1" x2="1" y2="0.7" width="0.1" layer="51"/>
<wire x1="1" y1="0.7" x2="1.3" y2="1" width="0.1" layer="51"/>
<wire x1="1.3" y1="1" x2="2.7" y2="1" width="0.1" layer="51"/>
<wire x1="2.7" y1="1" x2="3" y2="0.7" width="0.1" layer="51"/>
<wire x1="3" y1="0.7" x2="3" y2="-0.7" width="0.1" layer="51"/>
<wire x1="3" y1="-0.7" x2="2.7" y2="-1" width="0.1" layer="51"/>
<wire x1="2.7" y1="-1" x2="1.3" y2="-1" width="0.1" layer="51"/>
<wire x1="1.3" y1="-1" x2="1" y2="-0.7" width="0.1" layer="51"/>
<wire x1="1" y1="-0.7" x2="0.7" y2="-1" width="0.1" layer="51"/>
<wire x1="0.7" y1="-1" x2="-0.7" y2="-1" width="0.1" layer="51"/>
<wire x1="-0.7" y1="-1" x2="-1" y2="-0.7" width="0.1" layer="51"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.1" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
</package>
<package name="PLS2-4">
<pad name="1" x="0" y="0" drill="0.8" diameter="1.4" shape="square"/>
<pad name="2" x="2" y="0" drill="0.8" diameter="1.4"/>
<pad name="3" x="4" y="0" drill="0.8" diameter="1.4"/>
<pad name="4" x="6" y="0" drill="0.8" diameter="1.4"/>
<wire x1="-1" y1="1" x2="0.7" y2="1" width="0.2" layer="21"/>
<wire x1="1.3" y1="1" x2="2.7" y2="1" width="0.2" layer="21"/>
<wire x1="3.3" y1="1" x2="4.7" y2="1" width="0.2" layer="21"/>
<wire x1="5.3" y1="1" x2="6.7" y2="1" width="0.2" layer="21"/>
<wire x1="7" y1="0.7" x2="7" y2="-0.7" width="0.2" layer="21"/>
<wire x1="6.7" y1="-1" x2="5.3" y2="-1" width="0.2" layer="21"/>
<wire x1="4.7" y1="-1" x2="3.3" y2="-1" width="0.2" layer="21"/>
<wire x1="2.7" y1="-1" x2="1.3" y2="-1" width="0.2" layer="21"/>
<wire x1="0.7" y1="-1" x2="-1" y2="-1" width="0.2" layer="21"/>
<wire x1="-1" y1="-1" x2="-1" y2="1" width="0.2" layer="21"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<wire x1="1" y1="0.7" x2="1" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-1" y1="0.7" x2="-0.7" y2="1" width="0.1" layer="51"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1" layer="51"/>
<wire x1="0.7" y1="1" x2="1" y2="0.7" width="0.1" layer="51"/>
<wire x1="1" y1="0.7" x2="1.3" y2="1" width="0.1" layer="51"/>
<wire x1="1.3" y1="1" x2="2.7" y2="1" width="0.1" layer="51"/>
<wire x1="2.7" y1="1" x2="3" y2="0.7" width="0.1" layer="51"/>
<wire x1="3" y1="0.7" x2="3.3" y2="1" width="0.1" layer="51"/>
<wire x1="3.3" y1="1" x2="4.7" y2="1" width="0.1" layer="51"/>
<wire x1="4.7" y1="1" x2="5" y2="0.7" width="0.1" layer="51"/>
<wire x1="5" y1="0.7" x2="5.3" y2="1" width="0.1" layer="51"/>
<wire x1="5.3" y1="1" x2="6.7" y2="1" width="0.1" layer="51"/>
<wire x1="6.7" y1="1" x2="7" y2="0.7" width="0.1" layer="51"/>
<wire x1="7" y1="0.7" x2="7" y2="-0.7" width="0.1" layer="51"/>
<wire x1="7" y1="-0.7" x2="6.7" y2="-1" width="0.1" layer="51"/>
<wire x1="6.7" y1="-1" x2="5.3" y2="-1" width="0.1" layer="51"/>
<wire x1="5.3" y1="-1" x2="5" y2="-0.7" width="0.1" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.7" y2="-1" width="0.1" layer="51"/>
<wire x1="4.7" y1="-1" x2="3.3" y2="-1" width="0.1" layer="51"/>
<wire x1="3.3" y1="-1" x2="3" y2="-0.7" width="0.1" layer="51"/>
<wire x1="3" y1="-0.7" x2="2.7" y2="-1" width="0.1" layer="51"/>
<wire x1="2.7" y1="-1" x2="1.3" y2="-1" width="0.1" layer="51"/>
<wire x1="1.3" y1="-1" x2="1" y2="-0.7" width="0.1" layer="51"/>
<wire x1="1" y1="-0.7" x2="0.7" y2="-1" width="0.1" layer="51"/>
<wire x1="0.7" y1="-1" x2="-0.7" y2="-1" width="0.1" layer="51"/>
<wire x1="-0.7" y1="-1" x2="-1" y2="-0.7" width="0.1" layer="51"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.1" layer="51"/>
<wire x1="3" y1="0.7" x2="3.3" y2="1" width="0.2" layer="21"/>
<wire x1="3" y1="0.7" x2="2.7" y2="1" width="0.2" layer="21"/>
<wire x1="0.7" y1="1" x2="1" y2="0.7" width="0.2" layer="21"/>
<wire x1="1" y1="0.7" x2="1.3" y2="1" width="0.2" layer="21"/>
<wire x1="6.7" y1="1" x2="7" y2="0.7" width="0.2" layer="21"/>
<wire x1="7" y1="-0.7" x2="6.7" y2="-1" width="0.2" layer="21"/>
<wire x1="5.3" y1="-1" x2="5" y2="-0.7" width="0.2" layer="21"/>
<wire x1="5" y1="-0.7" x2="4.7" y2="-1" width="0.2" layer="21"/>
<wire x1="3.3" y1="-1" x2="3" y2="-0.7" width="0.2" layer="21"/>
<wire x1="3" y1="-0.7" x2="2.7" y2="-1" width="0.2" layer="21"/>
<wire x1="4.7" y1="1" x2="5" y2="0.7" width="0.2" layer="21"/>
<wire x1="5" y1="0.7" x2="5.3" y2="1" width="0.2" layer="21"/>
<wire x1="0.7" y1="-1" x2="1" y2="-0.7" width="0.2" layer="21"/>
<wire x1="1" y1="-0.7" x2="1.3" y2="-1" width="0.2" layer="21"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51"/>
<rectangle x1="5.75" y1="-0.25" x2="6.25" y2="0.25" layer="51"/>
</package>
<package name="USB-B-MICRO-SMD_V03">
<wire x1="-4" y1="2.6" x2="-3.3" y2="2.3" width="0.08" layer="51"/>
<wire x1="-3.3" y1="2.3" x2="-3.4" y2="2.5" width="0.08" layer="51"/>
<wire x1="-3.3" y1="2.3" x2="-3.5" y2="2.2" width="0.08" layer="51"/>
<wire x1="3.9" y1="2.15" x2="-3.9" y2="2.15" width="0.127" layer="49"/>
<wire x1="3.9" y1="-2.85" x2="-3.9" y2="-2.85" width="0.127" layer="49"/>
<wire x1="3.9" y1="-2.85" x2="3.9" y2="2.15" width="0.127" layer="49"/>
<wire x1="-3.9" y1="-2.85" x2="-3.9" y2="2.15" width="0.127" layer="49"/>
<wire x1="3.99288125" y1="-2.981959375" x2="3.99288125" y2="-1.365" width="0.2" layer="21"/>
<wire x1="4" y1="-3" x2="3" y2="-3" width="0.2" layer="21"/>
<wire x1="-4" y1="-1.365" x2="-4" y2="-3" width="0.2" layer="21"/>
<wire x1="-4" y1="-3" x2="-3" y2="-3" width="0.2" layer="21"/>
<wire x1="4" y1="1" x2="4" y2="2" width="0.2" layer="21"/>
<wire x1="-4" y1="1" x2="-4" y2="2" width="0.2" layer="21"/>
<wire x1="-4" y1="2" x2="4" y2="2" width="0" layer="51"/>
<rectangle x1="-4.434" y1="-0.9" x2="-2.934" y2="0.9" layer="31" rot="R90"/>
<rectangle x1="-0.35" y1="-0.75" x2="0.35" y2="0.75" layer="31" rot="R90"/>
<rectangle x1="2.934" y1="-0.9" x2="4.434" y2="0.9" layer="31" rot="R90"/>
<rectangle x1="2.834" y1="-1" x2="4.534" y2="1" layer="29" rot="R270"/>
<rectangle x1="-4.534" y1="-1" x2="-2.834" y2="1" layer="29" rot="R270"/>
<rectangle x1="-0.85" y1="-1.35" x2="0.85" y2="1.35" layer="29" rot="R270"/>
<smd name="D+1" x="0" y="-2.725" dx="0.4" dy="1.45" layer="1"/>
<smd name="D-1" x="0.65" y="-2.725" dx="0.4" dy="1.45" layer="1"/>
<smd name="GND1" x="-1.3" y="-2.725" dx="0.4" dy="1.45" layer="1"/>
<smd name="ID1" x="-0.65" y="-2.725" dx="0.4" dy="1.45" layer="1"/>
<smd name="SHIELD@1" x="3.685" y="0" dx="1.8" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="SHIELD@2" x="-3.685" y="0" dx="1.8" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="SHIELD@3" x="0" y="0" dx="2.5" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="VBUS1" x="1.3" y="-2.725" dx="0.4" dy="1.45" layer="1"/>
<text x="-5.66" y="2.96" size="0.3048" layer="51">PCB Front</text>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" rot="R180" align="center">&gt;NAME</text>
<text x="5.461" y="1.27" size="0.4064" layer="27" rot="R270">&gt;VALUE</text>
<hole x="1.9" y="-2.2" drill="0.85"/>
<hole x="-1.9" y="-2.2" drill="0.85"/>
<pad name="SHIELD@4" x="-3.3" y="0" drill="1.2" diameter="1.6"/>
<pad name="SHIELD@5" x="3.3" y="0" drill="1.2" diameter="1.6"/>
</package>
<package name="PLS2-5">
<pad name="1" x="0" y="0" drill="0.8" diameter="1.4" shape="square"/>
<pad name="2" x="2" y="0" drill="0.8" diameter="1.4"/>
<wire x1="-1" y1="1" x2="1" y2="1" width="0.2" layer="21"/>
<wire x1="1" y1="1" x2="3" y2="1" width="0.2" layer="21"/>
<wire x1="3" y1="-1" x2="1" y2="-1" width="0.2" layer="21"/>
<wire x1="1" y1="-1" x2="-1" y2="-1" width="0.2" layer="21"/>
<wire x1="-1" y1="-1" x2="-1" y2="1" width="0.2" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.2" layer="21"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<wire x1="-1" y1="0.7" x2="-0.7" y2="1" width="0.1" layer="51"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1" layer="51"/>
<wire x1="0.7" y1="1" x2="1" y2="0.7" width="0.1" layer="51"/>
<wire x1="1" y1="0.7" x2="1.3" y2="1" width="0.1" layer="51"/>
<wire x1="1.3" y1="1" x2="2.7" y2="1" width="0.1" layer="51"/>
<wire x1="2.7" y1="1" x2="3" y2="0.7" width="0.1" layer="51"/>
<wire x1="3" y1="-0.7" x2="2.7" y2="-1" width="0.1" layer="51"/>
<wire x1="2.7" y1="-1" x2="1.3" y2="-1" width="0.1" layer="51"/>
<wire x1="1.3" y1="-1" x2="1" y2="-0.7" width="0.1" layer="51"/>
<wire x1="1" y1="-0.7" x2="0.7" y2="-1" width="0.1" layer="51"/>
<wire x1="0.7" y1="-1" x2="-0.7" y2="-1" width="0.1" layer="51"/>
<wire x1="-0.7" y1="-1" x2="-1" y2="-0.7" width="0.1" layer="51"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.1" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<pad name="3" x="4" y="0" drill="0.8" diameter="1.4"/>
<wire x1="3" y1="1" x2="5" y2="1" width="0.2" layer="21"/>
<wire x1="5" y1="-1" x2="3" y2="-1" width="0.2" layer="21"/>
<wire x1="3" y1="0.7" x2="3.3" y2="1" width="0.1" layer="51"/>
<wire x1="3.3" y1="1" x2="4.7" y2="1" width="0.1" layer="51"/>
<wire x1="4.7" y1="1" x2="5" y2="0.7" width="0.1" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.7" y2="-1" width="0.1" layer="51"/>
<wire x1="4.7" y1="-1" x2="3.3" y2="-1" width="0.1" layer="51"/>
<wire x1="3.3" y1="-1" x2="3" y2="-0.7" width="0.1" layer="51"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51"/>
<pad name="4" x="6" y="0" drill="0.8" diameter="1.4"/>
<wire x1="5" y1="1" x2="7" y2="1" width="0.2" layer="21"/>
<wire x1="7" y1="-1" x2="5" y2="-1" width="0.2" layer="21"/>
<wire x1="5" y1="0.7" x2="5.3" y2="1" width="0.1" layer="51"/>
<wire x1="5.3" y1="1" x2="6.7" y2="1" width="0.1" layer="51"/>
<wire x1="6.7" y1="1" x2="7" y2="0.7" width="0.1" layer="51"/>
<wire x1="7" y1="-0.7" x2="6.7" y2="-1" width="0.1" layer="51"/>
<wire x1="6.7" y1="-1" x2="5.3" y2="-1" width="0.1" layer="51"/>
<wire x1="5.3" y1="-1" x2="5" y2="-0.7" width="0.1" layer="51"/>
<rectangle x1="5.75" y1="-0.25" x2="6.25" y2="0.25" layer="51"/>
<pad name="5" x="8" y="0" drill="0.8" diameter="1.4"/>
<wire x1="7" y1="1" x2="9" y2="1" width="0.2" layer="21"/>
<wire x1="9" y1="1" x2="9" y2="-1" width="0.2" layer="21"/>
<wire x1="9" y1="-1" x2="7" y2="-1" width="0.2" layer="21"/>
<wire x1="7" y1="0.7" x2="7.3" y2="1" width="0.1" layer="51"/>
<wire x1="7.3" y1="1" x2="8.7" y2="1" width="0.1" layer="51"/>
<wire x1="8.7" y1="1" x2="9" y2="0.7" width="0.1" layer="51"/>
<wire x1="9" y1="0.7" x2="9" y2="-0.7" width="0.1" layer="51"/>
<wire x1="9" y1="-0.7" x2="8.7" y2="-1" width="0.1" layer="51"/>
<wire x1="8.7" y1="-1" x2="7.3" y2="-1" width="0.1" layer="51"/>
<wire x1="7.3" y1="-1" x2="7" y2="-0.7" width="0.1" layer="51"/>
<rectangle x1="7.75" y1="-0.25" x2="8.25" y2="0.25" layer="51"/>
</package>
<package name="PLS-2.54-5">
<wire x1="-3.81" y1="1.016" x2="-3.556" y2="1.27" width="0.2" layer="21"/>
<wire x1="-3.81" y1="1.016" x2="-3.81" y2="1.27" width="0.2" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.556" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-6.35" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-1.524" y1="-1.27" x2="-1.27" y2="-1.016" width="0.2" layer="21"/>
<wire x1="-1.524" y1="-1.27" x2="-3.556" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-1.524" y1="1.27" x2="-1.27" y2="1.016" width="0.2" layer="21"/>
<wire x1="-1.524" y1="1.27" x2="-3.556" y2="1.27" width="0.2" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-6.35" y2="1.27" width="0.2" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="-1.27" width="0.2" layer="21"/>
<rectangle x1="-2.8448" y1="-0.3048" x2="-2.2352" y2="0.3048" layer="51" rot="R270"/>
<rectangle x1="-5.3848" y1="-0.3048" x2="-4.7752" y2="0.3048" layer="51" rot="R270"/>
<pad name="1" x="-5.08" y="0" drill="1" diameter="1.778" shape="square" rot="R270"/>
<pad name="2" x="-2.54" y="0" drill="1" diameter="1.778" rot="R270"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="-1.27" y1="1.016" x2="-1.016" y2="1.27" width="0.2" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.016" y2="-1.27" width="0.2" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="1.27" y2="-1.016" width="0.2" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="-1.016" y2="-1.27" width="0.2" layer="21"/>
<wire x1="1.016" y1="1.27" x2="1.27" y2="1.016" width="0.2" layer="21"/>
<wire x1="1.016" y1="1.27" x2="-1.016" y2="1.27" width="0.2" layer="21"/>
<rectangle x1="-0.3048" y1="-0.3048" x2="0.3048" y2="0.3048" layer="51" rot="R270"/>
<pad name="3" x="0" y="0" drill="1" diameter="1.778" rot="R270"/>
<wire x1="1.27" y1="1.016" x2="1.524" y2="1.27" width="0.2" layer="21"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-1.27" width="0.2" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.016" width="0.2" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="1.524" y2="-1.27" width="0.2" layer="21"/>
<wire x1="3.556" y1="1.27" x2="3.81" y2="1.016" width="0.2" layer="21"/>
<wire x1="3.556" y1="1.27" x2="1.524" y2="1.27" width="0.2" layer="21"/>
<rectangle x1="2.2352" y1="-0.3048" x2="2.8448" y2="0.3048" layer="51" rot="R270"/>
<pad name="4" x="2.54" y="0" drill="1" diameter="1.778" rot="R270"/>
<wire x1="-3.81" y1="1.016" x2="-3.81" y2="-1.016" width="0.2" layer="21"/>
<wire x1="3.81" y1="1.016" x2="4.064" y2="1.27" width="0.2" layer="21"/>
<wire x1="3.81" y1="-1.016" x2="4.064" y2="-1.27" width="0.2" layer="21"/>
<wire x1="6.096" y1="-1.27" x2="6.35" y2="-1.016" width="0.2" layer="21"/>
<wire x1="6.096" y1="-1.27" x2="4.064" y2="-1.27" width="0.2" layer="21"/>
<wire x1="6.35" y1="-1.016" x2="6.35" y2="1.016" width="0.2" layer="21"/>
<wire x1="6.096" y1="1.27" x2="6.35" y2="1.016" width="0.2" layer="21"/>
<wire x1="6.096" y1="1.27" x2="4.064" y2="1.27" width="0.2" layer="21"/>
<rectangle x1="4.7752" y1="-0.3048" x2="5.3848" y2="0.3048" layer="51" rot="R270"/>
<pad name="5" x="5.08" y="0" drill="1" diameter="1.778" rot="R270"/>
<wire x1="-3.81" y1="1.016" x2="-3.556" y2="1.27" width="0.2" layer="51"/>
<wire x1="-3.81" y1="1.016" x2="-3.81" y2="1.27" width="0.2" layer="51"/>
<wire x1="-3.81" y1="-1.016" x2="-3.556" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-3.81" y1="-1.016" x2="-3.81" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-3.81" y1="-1.27" x2="-6.35" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-1.27" y2="-1.016" width="0.2" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-3.556" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-1.524" y1="1.27" x2="-1.27" y2="1.016" width="0.2" layer="51"/>
<wire x1="-1.524" y1="1.27" x2="-3.556" y2="1.27" width="0.2" layer="51"/>
<wire x1="-3.81" y1="1.27" x2="-6.35" y2="1.27" width="0.2" layer="51"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-1.27" y1="1.016" x2="-1.016" y2="1.27" width="0.2" layer="51"/>
<wire x1="-1.27" y1="-1.016" x2="-1.016" y2="-1.27" width="0.2" layer="51"/>
<wire x1="1.016" y1="-1.27" x2="1.27" y2="-1.016" width="0.2" layer="51"/>
<wire x1="1.016" y1="-1.27" x2="-1.016" y2="-1.27" width="0.2" layer="51"/>
<wire x1="1.016" y1="1.27" x2="1.27" y2="1.016" width="0.2" layer="51"/>
<wire x1="1.016" y1="1.27" x2="-1.016" y2="1.27" width="0.2" layer="51"/>
<wire x1="1.27" y1="1.016" x2="1.524" y2="1.27" width="0.2" layer="51"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-1.27" width="0.2" layer="51"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.016" width="0.2" layer="51"/>
<wire x1="3.556" y1="-1.27" x2="1.524" y2="-1.27" width="0.2" layer="51"/>
<wire x1="3.556" y1="1.27" x2="3.81" y2="1.016" width="0.2" layer="51"/>
<wire x1="3.556" y1="1.27" x2="1.524" y2="1.27" width="0.2" layer="51"/>
<wire x1="-3.81" y1="1.016" x2="-3.81" y2="-1.016" width="0.2" layer="51"/>
<wire x1="3.81" y1="1.016" x2="4.064" y2="1.27" width="0.2" layer="51"/>
<wire x1="3.81" y1="-1.016" x2="4.064" y2="-1.27" width="0.2" layer="51"/>
<wire x1="6.096" y1="-1.27" x2="6.35" y2="-1.016" width="0.2" layer="51"/>
<wire x1="6.096" y1="-1.27" x2="4.064" y2="-1.27" width="0.2" layer="51"/>
<wire x1="6.35" y1="-1.016" x2="6.35" y2="1.016" width="0.2" layer="51"/>
<wire x1="6.096" y1="1.27" x2="6.35" y2="1.016" width="0.2" layer="51"/>
<wire x1="6.096" y1="1.27" x2="4.064" y2="1.27" width="0.2" layer="51"/>
</package>
<package name="ISP-TYPEA-4">
<pad name="1" x="0" y="0" drill="0.8" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.8"/>
<pad name="3" x="5.08" y="0" drill="0.8"/>
<pad name="4" x="7.62" y="0" drill="0.8"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.27" x2="8.89" y2="1.27" width="0.254" layer="21"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="-1.27" width="0.254" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="1.27" y2="-1.27" width="0.254" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.254" layer="21"/>
<text x="-1.27" y="2.54" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
</package>
<package name="ISP-TYPEC-4">
<smd name="1" x="0" y="0" dx="2.5" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="0" dx="2.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="0" dx="2.5" dy="1" layer="1" rot="R90"/>
<smd name="4" x="7.62" y="0" dx="2.5" dy="1" layer="1" rot="R90"/>
<wire x1="-0.9525" y1="1.5875" x2="8.5725" y2="1.5875" width="0.254" layer="21"/>
<wire x1="8.5725" y1="1.5875" x2="8.5725" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-0.9525" y1="-1.27" x2="-0.9525" y2="1.5875" width="0.254" layer="21"/>
<text x="0" y="0" size="1.2" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-3.81" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="USB-MINIB">
<description>Surface Mount USB Mini-B Connector</description>
<wire x1="3.9" y1="-1.24" x2="3.9" y2="2.86" width="0.127" layer="51"/>
<wire x1="-2.9591" y1="-0.5471" x2="-2.7514" y2="-3.2985" width="0.1016" layer="51"/>
<wire x1="-2.7514" y1="-3.2985" x2="-2.5438" y2="-3.558" width="0.1016" layer="51" curve="68.629849"/>
<wire x1="-2.5438" y1="-3.558" x2="-1.9727" y2="-3.558" width="0.1016" layer="51" curve="34.099487"/>
<wire x1="-1.9727" y1="-3.558" x2="-1.7651" y2="-3.2985" width="0.1016" layer="51" curve="68.629849"/>
<wire x1="-1.7651" y1="-3.2985" x2="-1.5055" y2="-0.5471" width="0.1016" layer="51"/>
<wire x1="-1.5055" y1="-0.5471" x2="-1.7132" y2="-0.5471" width="0.1016" layer="51"/>
<wire x1="-1.7132" y1="-0.5471" x2="-1.9727" y2="-2.9351" width="0.1016" layer="51"/>
<wire x1="-1.9727" y1="-2.9351" x2="-2.4919" y2="-2.9351" width="0.1016" layer="51"/>
<wire x1="-2.4919" y1="-2.9351" x2="-2.7514" y2="-0.5471" width="0.1016" layer="51"/>
<wire x1="-2.7514" y1="-0.5471" x2="-2.9591" y2="-0.5471" width="0.1016" layer="51"/>
<wire x1="-1.2459" y1="-3.2984" x2="-1.0383" y2="0.7508" width="0.1016" layer="51"/>
<wire x1="-1.0383" y1="0.7508" x2="-0.8306" y2="0.9584" width="0.1016" layer="51" curve="-83.771817"/>
<wire x1="-1.2459" y1="-3.2985" x2="-1.0383" y2="-3.5061" width="0.1016" layer="51" curve="90"/>
<wire x1="-1.0382" y1="-3.5061" x2="-0.8306" y2="-3.2985" width="0.1016" layer="51" curve="90"/>
<wire x1="-0.8306" y1="-3.2985" x2="-0.623" y2="0.1278" width="0.1016" layer="51"/>
<wire x1="2.9589" y1="-0.5471" x2="2.7512" y2="-3.2985" width="0.1016" layer="51"/>
<wire x1="2.7512" y1="-3.2985" x2="2.5436" y2="-3.558" width="0.1016" layer="51" curve="-68.629849"/>
<wire x1="2.5436" y1="-3.558" x2="1.9725" y2="-3.558" width="0.1016" layer="51" curve="-34.099487"/>
<wire x1="1.9725" y1="-3.558" x2="1.7649" y2="-3.2985" width="0.1016" layer="51" curve="-68.629849"/>
<wire x1="1.7649" y1="-3.2985" x2="1.5053" y2="-0.5471" width="0.1016" layer="51"/>
<wire x1="1.5053" y1="-0.5471" x2="1.713" y2="-0.5471" width="0.1016" layer="51"/>
<wire x1="1.713" y1="-0.5471" x2="1.9725" y2="-2.9351" width="0.1016" layer="51"/>
<wire x1="1.9725" y1="-2.9351" x2="2.4917" y2="-2.9351" width="0.1016" layer="51"/>
<wire x1="2.4917" y1="-2.9351" x2="2.7512" y2="-0.5471" width="0.1016" layer="51"/>
<wire x1="2.7512" y1="-0.5471" x2="2.9589" y2="-0.5471" width="0.1016" layer="51"/>
<wire x1="1.2457" y1="-3.2984" x2="1.0381" y2="0.7508" width="0.1016" layer="51"/>
<wire x1="1.0381" y1="0.7508" x2="0.8304" y2="0.9584" width="0.1016" layer="51" curve="83.722654"/>
<wire x1="0.8304" y1="0.9584" x2="-0.8307" y2="0.9584" width="0.1016" layer="51"/>
<wire x1="1.2457" y1="-3.2985" x2="1.0381" y2="-3.5061" width="0.1016" layer="51" curve="-90"/>
<wire x1="1.038" y1="-3.5061" x2="0.8304" y2="-3.2985" width="0.1016" layer="51" curve="-90"/>
<wire x1="0.8304" y1="-3.2985" x2="0.6228" y2="0.1278" width="0.1016" layer="51"/>
<wire x1="0.6228" y1="0.1278" x2="-0.6232" y2="0.1278" width="0.1016" layer="51"/>
<wire x1="3.88" y1="4.2594" x2="5.03" y2="4.2594" width="0.1016" layer="51"/>
<wire x1="5.03" y1="2.8808" x2="3.88" y2="2.8808" width="0.1016" layer="51"/>
<wire x1="5.05" y1="3.3" x2="5.05" y2="3.9" width="0.1016" layer="51" curve="-180"/>
<wire x1="5.05" y1="4.25" x2="5.05" y2="3.9" width="0.1016" layer="51"/>
<wire x1="5.05" y1="2.9" x2="5.05" y2="3.3" width="0.1016" layer="51"/>
<wire x1="3.88" y1="-1.2806" x2="5.03" y2="-1.2806" width="0.1016" layer="51"/>
<wire x1="5.03" y1="-2.6592" x2="3.88" y2="-2.6592" width="0.1016" layer="51"/>
<wire x1="5.05" y1="-2.24" x2="5.05" y2="-1.64" width="0.1016" layer="51" curve="-180"/>
<wire x1="5.05" y1="-1.29" x2="5.05" y2="-1.64" width="0.1016" layer="51"/>
<wire x1="5.05" y1="-2.64" x2="5.05" y2="-2.24" width="0.1016" layer="51"/>
<wire x1="-3.91" y1="2.8606" x2="-5.06" y2="2.8606" width="0.1016" layer="51"/>
<wire x1="-5.06" y1="4.2392" x2="-3.91" y2="4.2392" width="0.1016" layer="51"/>
<wire x1="-5.08" y1="3.82" x2="-5.08" y2="3.22" width="0.1016" layer="51" curve="-180"/>
<wire x1="-5.08" y1="2.87" x2="-5.08" y2="3.22" width="0.1016" layer="51"/>
<wire x1="-5.08" y1="4.22" x2="-5.08" y2="3.82" width="0.1016" layer="51"/>
<wire x1="-3.91" y1="-2.6794" x2="-5.06" y2="-2.6794" width="0.1016" layer="51"/>
<wire x1="-5.06" y1="-1.3008" x2="-3.91" y2="-1.3008" width="0.1016" layer="51"/>
<wire x1="-5.08" y1="-1.72" x2="-5.08" y2="-2.32" width="0.1016" layer="51" curve="-180"/>
<wire x1="-5.08" y1="-2.67" x2="-5.08" y2="-2.32" width="0.1016" layer="51"/>
<wire x1="-5.08" y1="-1.32" x2="-5.08" y2="-1.72" width="0.1016" layer="51"/>
<wire x1="-3.9" y1="-1.29" x2="-3.9" y2="2.81" width="0.127" layer="51"/>
<wire x1="-3.9" y1="-4.6" x2="3.9" y2="-4.6" width="0.127" layer="51"/>
<wire x1="-2.75" y1="4.5" x2="-2.75" y2="3.75" width="0.1016" layer="51"/>
<wire x1="-2.75" y1="3.75" x2="2.75" y2="3.75" width="0.1016" layer="51"/>
<wire x1="2.75" y1="3.75" x2="2.75" y2="4.5" width="0.1016" layer="51"/>
<wire x1="-2.25" y1="3.5" x2="-2.25" y2="2.75" width="0.1016" layer="51"/>
<wire x1="-2.25" y1="2.75" x2="-3" y2="2.75" width="0.1016" layer="51"/>
<wire x1="-3" y1="2.75" x2="-3" y2="3.5" width="0.1016" layer="51"/>
<wire x1="-3" y1="3.5" x2="-2.25" y2="3.5" width="0.1016" layer="51"/>
<wire x1="3" y1="3.5" x2="2.25" y2="3.5" width="0.1016" layer="51"/>
<wire x1="2.25" y1="3.5" x2="2.25" y2="2.75" width="0.1016" layer="51"/>
<wire x1="2.25" y1="2.75" x2="3" y2="2.75" width="0.1016" layer="51"/>
<wire x1="3" y1="2.75" x2="3" y2="3.5" width="0.1016" layer="51"/>
<wire x1="-3.9" y1="-0.6" x2="-3.9" y2="2.1" width="0.2" layer="21"/>
<wire x1="3.9" y1="2.1" x2="3.9" y2="-0.6" width="0.2" layer="21"/>
<wire x1="-3" y1="4.6" x2="-2.2" y2="4.6" width="0.2" layer="21"/>
<wire x1="2.2" y1="4.6" x2="3.1" y2="4.6" width="0.2" layer="21"/>
<wire x1="-3.9" y1="-4.6" x2="-3.9" y2="-2.7" width="0.127" layer="51"/>
<wire x1="3.9" y1="-4.6" x2="3.9" y2="-2.7" width="0.127" layer="51"/>
<wire x1="-3.9" y1="4.6" x2="-3.9" y2="4.25" width="0.127" layer="51"/>
<wire x1="3.9" y1="4.6" x2="3.9" y2="4.3" width="0.127" layer="51"/>
<smd name="SHIELD@4" x="-4.4" y="3.5" dx="2" dy="2.4" layer="1"/>
<smd name="VBUS" x="-1.6" y="4.064" dx="0.5" dy="2.308" layer="1" rot="R180"/>
<smd name="D-" x="-0.8" y="4.064" dx="0.5" dy="2.308" layer="1" rot="R180"/>
<smd name="D+" x="0" y="4.064" dx="0.5" dy="2.308" layer="1" rot="R180"/>
<smd name="ID" x="0.8" y="4.064" dx="0.5" dy="2.308" layer="1" rot="R180"/>
<smd name="GND" x="1.6" y="4.064" dx="0.5" dy="2.308" layer="1" rot="R180"/>
<smd name="SHIELD@1" x="-4.4" y="-2" dx="2" dy="2.4" layer="1"/>
<smd name="SHIELD@3" x="4.4" y="3.5" dx="2" dy="2.4" layer="1"/>
<smd name="SHIELD@2" x="4.4" y="-2" dx="2" dy="2.4" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<hole x="-2.2" y="1" drill="1"/>
<hole x="2.2" y="1" drill="1"/>
</package>
<package name="DG306-5.0-02P-12-00AH">
<wire x1="-5" y1="5" x2="5" y2="5" width="0.2" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="-4" width="0.2" layer="21"/>
<wire x1="5" y1="-4" x2="-5" y2="-4" width="0.2" layer="21"/>
<wire x1="-5" y1="-4" x2="-5" y2="3" width="0.2" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="1.5" diameter="3" shape="long" rot="R90"/>
<pad name="2" x="2.5" y="0" drill="1.5" diameter="3" shape="long" rot="R90"/>
<text x="0" y="0" size="1.5" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-5" y1="3" x2="-5" y2="4" width="0.2" layer="21"/>
<wire x1="-5" y1="4" x2="-5" y2="5" width="0.2" layer="21"/>
<wire x1="-5" y1="4" x2="-5.6" y2="4" width="0.2" layer="21"/>
<wire x1="-5.6" y1="4" x2="-5.6" y2="3" width="0.2" layer="21"/>
<wire x1="-5.6" y1="3" x2="-5" y2="3" width="0.2" layer="21"/>
<wire x1="-5" y1="5" x2="-5" y2="4" width="0.2" layer="51"/>
<wire x1="-5" y1="4" x2="-5" y2="3" width="0.2" layer="51"/>
<wire x1="-5" y1="3" x2="-5" y2="-4" width="0.2" layer="51"/>
<wire x1="-5" y1="-4" x2="-4" y2="-4" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-1" y2="-4" width="0.2" layer="51"/>
<wire x1="-1" y1="-4" x2="1" y2="-4" width="0.2" layer="51"/>
<wire x1="1" y1="-4" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="4" y1="-4" x2="5" y2="-4" width="0.2" layer="51"/>
<wire x1="5" y1="-4" x2="5" y2="5" width="0.2" layer="51"/>
<wire x1="5" y1="5" x2="-5" y2="5" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="-5" width="0.2" layer="51"/>
<wire x1="-4" y1="-5" x2="-1" y2="-5" width="0.2" layer="51"/>
<wire x1="-1" y1="-5" x2="-1" y2="-4" width="0.2" layer="51"/>
<wire x1="1" y1="-4" x2="1" y2="-5" width="0.2" layer="51"/>
<wire x1="1" y1="-5" x2="4" y2="-5" width="0.2" layer="51"/>
<wire x1="4" y1="-5" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="-5" y1="3" x2="-5.6" y2="3" width="0.2" layer="51"/>
<wire x1="-5.6" y1="3" x2="-5.6" y2="4" width="0.2" layer="51"/>
<wire x1="-5.6" y1="4" x2="-5" y2="4" width="0.2" layer="51"/>
</package>
<package name="JST-PH-2-SMT">
<wire x1="-4" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="51"/>
<wire x1="4" y1="2.5" x2="4" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="2.5" width="0.2032" layer="51"/>
<wire x1="4" y1="-2.5" x2="-4" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="2.5" x2="2.25" y2="2.5" width="0.2" layer="21"/>
<wire x1="4" y1="-2" x2="4" y2="-2.5" width="0.2" layer="21"/>
<wire x1="4" y1="-2.5" x2="1.75" y2="-2.5" width="0.2" layer="21"/>
<wire x1="-1.75" y1="-2.5" x2="-4" y2="-2.5" width="0.2" layer="21"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="-2" width="0.2" layer="21"/>
<smd name="1" x="-1" y="-1.8" dx="1" dy="5.5" layer="1"/>
<smd name="2" x="1" y="-1.8" dx="1" dy="5.5" layer="1"/>
<smd name="NC1" x="-3.4" y="0" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="0" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="0" y="0" size="1.5" layer="25" font="vector" ratio="12" align="center">&gt;Name</text>
<text x="-2.54" y="-7.62" size="1.27" layer="27" font="vector">&gt;Value</text>
<text x="-3.2" y="-4" size="1.4224" layer="21" ratio="12">+</text>
<text x="1.9" y="-4" size="1.4224" layer="21" ratio="12">-</text>
</package>
</packages>
<symbols>
<symbol name="CONN-2PIN-R">
<pin name="1" x="0" y="-2.54" visible="pad" length="point" rot="R180"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="point" rot="R180"/>
<wire x1="0" y1="-7.62" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-10.16" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="CONN-4PIN-L">
<pin name="1" x="0" y="-2.54" visible="pad" length="point"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="point"/>
<pin name="3" x="0" y="-7.62" visible="pad" length="point"/>
<wire x1="0" y1="0" x2="0" y2="-12.7" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-15.24" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="4" x="0" y="-10.16" visible="pad" length="point"/>
</symbol>
<symbol name="USB-5S">
<wire x1="-0.04" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="-17.78" width="0.254" layer="94"/>
<wire x1="8.84" y1="-6.02" x2="6.69" y2="-6.02" width="0.254" layer="94"/>
<wire x1="6.69" y1="-6.02" x2="5.94" y2="-6.02" width="0.254" layer="94"/>
<wire x1="4.64" y1="-4.42" x2="5.64" y2="-4.42" width="0.254" layer="94"/>
<wire x1="3.89" y1="-7.47" x2="5.09" y2="-7.47" width="0.254" layer="94"/>
<wire x1="5.09" y1="-7.47" x2="5.94" y2="-6.02" width="0.254" layer="94"/>
<wire x1="5.64" y1="-4.42" x2="6.69" y2="-6.02" width="0.254" layer="94"/>
<wire x1="5.94" y1="-6.02" x2="2.24" y2="-6.02" width="0.254" layer="94"/>
<wire x1="2.24" y1="-6.02" x2="2.24" y2="-6.82" width="0.254" layer="94"/>
<wire x1="2.24" y1="-6.82" x2="2.04" y2="-6.82" width="0.254" layer="94"/>
<wire x1="2.04" y1="-6.82" x2="1.24" y2="-6.02" width="0.254" layer="94"/>
<wire x1="1.24" y1="-6.02" x2="2.04" y2="-5.22" width="0.254" layer="94"/>
<wire x1="2.04" y1="-6.62" x2="2.04" y2="-5.22" width="0.254" layer="94"/>
<wire x1="2.04" y1="-5.22" x2="2.24" y2="-5.22" width="0.254" layer="94"/>
<wire x1="2.24" y1="-5.22" x2="2.24" y2="-6.02" width="0.254" layer="94"/>
<wire x1="1.64" y1="-6.02" x2="1.64" y2="-6.22" width="0.254" layer="94"/>
<wire x1="1.64" y1="-6.22" x2="1.84" y2="-6.42" width="0.254" layer="94"/>
<wire x1="1.84" y1="-6.42" x2="1.84" y2="-5.62" width="0.254" layer="94"/>
<wire x1="1.84" y1="-5.62" x2="1.44" y2="-6.02" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-17.78" width="0.254" layer="94"/>
<wire x1="0" y1="-17.78" x2="17.78" y2="-17.78" width="0.254" layer="94"/>
<circle x="8.84" y="-6.02" radius="0.5" width="1" layer="94"/>
<circle x="4.24" y="-4.42" radius="0.2" width="1" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="3.04" y1="-8.07" x2="4.24" y2="-6.87" layer="94"/>
<pin name="D+" x="20.32" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="D-" x="20.32" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="VBUS" x="20.32" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="20.32" y="-12.7" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="ID" x="20.32" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="SHIELD" x="20.32" y="-15.24" visible="pin" length="short" rot="R180"/>
</symbol>
<symbol name="SWD-5">
<wire x1="0" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-17.78" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="SWCLK" x="-5.08" y="-7.62" length="middle"/>
<pin name="GND" x="-5.08" y="-10.16" length="middle"/>
<pin name="SWDAT" x="-5.08" y="-12.7" length="middle"/>
<pin name="VDD" x="-5.08" y="-2.54" length="middle"/>
<pin name="RESET" x="-5.08" y="-5.08" length="middle"/>
</symbol>
<symbol name="CONN-PLS-I2C">
<pin name="VCC" x="-5.08" y="-2.54" length="middle"/>
<pin name="SDA" x="-5.08" y="-5.08" length="middle"/>
<pin name="SCL" x="-5.08" y="-7.62" length="middle"/>
<pin name="GND" x="-5.08" y="-10.16" length="middle"/>
<wire x1="0" y1="0" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="0" y="-15.24" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CON-2P" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="CONN-2PIN-R" x="0" y="0"/>
</gates>
<devices>
<device name="-1" package="PLS-2.54-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2" package="DG306-5.0-02P-12-00AH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3" package="JP1R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JP1_SMALL" package="JP1_SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TERMINAL_BLOCK_2P_5" package="TERMINAL_BLOCK_2P_5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-28" package="JST-PH-2-SMT">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="2C_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PLS2-2" package="PLS2-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CON-4P" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="CONN-4PIN-L" x="0" y="0"/>
</gates>
<devices>
<device name="-2" package="ISP-TYPEB-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1" package="PLS-2.54-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7395-04" package="7395-04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PLS2-4" package="PLS2-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB-5S" prefix="X" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;USB Connectors&lt;/b&gt;&lt;/p&gt;
&lt;b&gt;USBMINIB&lt;/b&gt; - Surface Mount Female Mini-B USB Connector 4UConnector: 06564&lt;/p&gt;
&lt;p&gt;By microbuilder.eu&lt;/p&gt;
Thru-hole RA Female Mini-B USB Connector 4UConnector: 18732&lt;/p&gt;
&lt;p&gt;By ladyada.net&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="USB-5S" x="0" y="0"/>
</gates>
<devices>
<device name="-MINI" package="USB-MINIB">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ID" pad="ID"/>
<connect gate="G$1" pin="SHIELD" pad="SHIELD@1 SHIELD@2 SHIELD@3 SHIELD@4"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICRO" package="USB-B-MICRO-SMD_V03">
<connects>
<connect gate="G$1" pin="D+" pad="D+1"/>
<connect gate="G$1" pin="D-" pad="D-1"/>
<connect gate="G$1" pin="GND" pad="GND1"/>
<connect gate="G$1" pin="ID" pad="ID1"/>
<connect gate="G$1" pin="SHIELD" pad="SHIELD@1 SHIELD@2 SHIELD@3 SHIELD@4 SHIELD@5"/>
<connect gate="G$1" pin="VBUS" pad="VBUS1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWD-5" prefix="X">
<gates>
<gate name="G$1" symbol="SWD-5" x="0" y="0"/>
</gates>
<devices>
<device name="-PLS-5" package="PLS-2.54-5">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="RESET" pad="2"/>
<connect gate="G$1" pin="SWCLK" pad="3"/>
<connect gate="G$1" pin="SWDAT" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PLS2-5" package="PLS2-5">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="RESET" pad="2"/>
<connect gate="G$1" pin="SWCLK" pad="3"/>
<connect gate="G$1" pin="SWDAT" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="I2C" prefix="X">
<gates>
<gate name="G$1" symbol="CONN-PLS-I2C" x="0" y="0"/>
</gates>
<devices>
<device name="-1" package="ISP-TYPEA-4">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="2"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2" package="ISP-TYPEC-4">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="2"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="PLS-2.54-4">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="2"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PLS2-4" package="PLS2-4">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="2"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_transistor">
<packages>
<package name="SOT23-3">
<description>&lt;b&gt;Small Outline Transistor; 3 leads&lt;/b&gt; Reflow soldering&lt;p&gt;
INFINEON, www.infineon.com/cmc_upload/0/000/010/257/eh_db_5b.pdf</description>
<smd name="3" x="0" y="1.2" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="1" y="-1.2" dx="0.8" dy="0.9" layer="1"/>
<smd name="1" x="-1" y="-1.2" dx="0.8" dy="0.9" layer="1"/>
<wire x1="-1.5" y1="0.7" x2="1.5" y2="0.7" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.7" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.7" x2="-1.5" y2="-0.7" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.7" width="0.127" layer="51"/>
<wire x1="-1.7" y1="-1.9" x2="-1.7" y2="0.8" width="0.2" layer="21"/>
<wire x1="-1.7" y1="0.8" x2="-0.7" y2="0.8" width="0.2" layer="21"/>
<wire x1="0.7" y1="0.8" x2="1.7" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-1.9" width="0.2" layer="21"/>
<wire x1="-0.7" y1="0.8" x2="-0.7" y2="1.9" width="0.2" layer="21"/>
<wire x1="-0.7" y1="1.9" x2="0.7" y2="1.9" width="0.2" layer="21"/>
<wire x1="0.7" y1="1.9" x2="0.7" y2="0.8" width="0.2" layer="21"/>
<wire x1="-1.7" y1="-1.9" x2="1.7" y2="-1.9" width="0.2" layer="21"/>
<rectangle x1="-1.3" y1="-1.5" x2="-0.7" y2="-0.8" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.3" y2="-0.8" layer="51"/>
<rectangle x1="-0.3" y1="0.8" x2="0.3" y2="1.5" layer="51"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-2.5" size="0.6" layer="27" font="vector" align="center">&gt;VALUE</text>
<wire x1="-0.4" y1="-0.8" x2="0.4" y2="-0.8" width="0.2" layer="21"/>
<text x="0" y="0" size="0.8" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
</package>
<package name="TO220BH">
<description>&lt;b&gt;Molded Package&lt;/b&gt;&lt;p&gt;
grid 2.54 mm</description>
<wire x1="-5.207" y1="-1.27" x2="5.207" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.207" y1="14.605" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-1.27" x2="5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="5.207" y1="11.176" x2="4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="4.318" y1="11.176" x2="4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="4.318" y1="12.7" x2="5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="5.207" y1="12.7" x2="5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-1.27" x2="-5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="11.176" x2="-4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="11.176" x2="-4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="12.7" x2="-5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="12.7" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<circle x="0" y="11.176" radius="1.8034" width="0.1524" layer="21"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="42"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="43"/>
<pad name="1" x="-2.54" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<pad name="2" x="0" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<text x="0" y="0" size="1.5" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-3.937" y="2.54" size="1.778" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-4.445" y="7.874" size="1.016" layer="21" ratio="10">A17,5mm</text>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-4.064" layer="21"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-4.064" layer="21"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-4.064" layer="21"/>
<rectangle x1="-3.175" y1="-4.064" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="-0.635" y1="-4.064" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-4.064" x2="3.175" y2="-1.27" layer="21"/>
<rectangle x1="-2.921" y1="-6.604" x2="-2.159" y2="-4.699" layer="51"/>
<rectangle x1="-0.381" y1="-6.604" x2="0.381" y2="-4.699" layer="51"/>
<rectangle x1="2.159" y1="-6.604" x2="2.921" y2="-4.699" layer="51"/>
<hole x="0" y="11.176" drill="3.302"/>
<smd name="2_" x="0" y="6.985" dx="11" dy="20" layer="1"/>
</package>
<package name="TO252">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
TS-003</description>
<wire x1="3.2766" y1="3.8354" x2="3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-2.159" x2="-3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.159" x2="-3.2766" y2="3.8354" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="3.835" x2="3.2774" y2="3.8346" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="5.983" x2="3.973" y2="5.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-5.983" x2="-3.973" y2="-5.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-5.983" x2="-3.973" y2="5.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="5.983" x2="3.973" y2="-5.983" width="0.0508" layer="39"/>
<wire x1="-2.5654" y1="3.937" x2="-2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.6482" x2="-2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.1054" x2="2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.1054" x2="2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.6482" x2="2.5654" y2="3.937" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.937" x2="-2.5654" y2="3.937" width="0.2032" layer="51"/>
<smd name="3" x="0" y="2.5" dx="5.4" dy="6.2" layer="1"/>
<smd name="1" x="-2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<smd name="2" x="2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.0226" x2="0.4318" y2="-2.2606" layer="21"/>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="3.937"/>
<vertex x="-2.5654" y="4.6482"/>
<vertex x="-2.1082" y="5.1054"/>
<vertex x="2.1082" y="5.1054"/>
<vertex x="2.5654" y="4.6482"/>
<vertex x="2.5654" y="3.937"/>
</polygon>
</package>
<package name="MICRO3">
<description>&lt;b&gt;Micro3 TM Package Outline&lt;/b&gt;&lt;p&gt;
www.irf.com / irlml5203.pdf</description>
<wire x1="-1.45" y1="0.65" x2="-0.6" y2="0.65" width="0.1016" layer="21"/>
<wire x1="-0.6" y1="0.65" x2="0.6" y2="0.65" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.65" x2="1.45" y2="0.65" width="0.1016" layer="21"/>
<wire x1="1.45" y1="0.65" x2="1.45" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="1.45" y1="-0.65" x2="0.35" y2="-0.65" width="0.1016" layer="51"/>
<wire x1="0.35" y1="-0.65" x2="-0.35" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.65" x2="-1.45" y2="-0.65" width="0.1016" layer="51"/>
<wire x1="-1.45" y1="-0.65" x2="-1.45" y2="0.65" width="0.1016" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="0" y="1" dx="0.8" dy="0.9" layer="1"/>
<text x="-1.6" y="1.6" size="1.778" layer="25">&gt;NAME</text>
<text x="-1.5" y="-3.4" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-0.27" y1="0.66" x2="0.27" y2="1.25" layer="51"/>
<rectangle x1="-1.22" y1="-1.25" x2="-0.68" y2="-0.65" layer="51"/>
<rectangle x1="0.68" y1="-1.25" x2="1.22" y2="-0.64" layer="51"/>
</package>
<package name="SOT223">
<description>&lt;b&gt;Smal Outline Transistor&lt;/b&gt;</description>
<smd name="1" x="-2.3" y="-3.3" dx="1.4" dy="1.7" layer="1"/>
<smd name="2" x="0" y="-3.3" dx="1.4" dy="1.7" layer="1"/>
<smd name="3" x="2.3" y="-3.3" dx="1.4" dy="1.7" layer="1"/>
<smd name="4" x="0" y="3" dx="4" dy="2" layer="1"/>
<text x="0" y="0" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="1.016" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-3.3" y1="1.8" x2="3.3" y2="1.8" width="0.127" layer="51"/>
<wire x1="3.3" y1="1.8" x2="3.3" y2="-1.8" width="0.127" layer="51"/>
<wire x1="3.3" y1="-1.8" x2="-3.3" y2="-1.8" width="0.127" layer="51"/>
<wire x1="-3.3" y1="-1.8" x2="-3.3" y2="1.8" width="0.127" layer="51"/>
<wire x1="-3.3" y1="1.8" x2="3.3" y2="1.8" width="0.2" layer="21"/>
<wire x1="3.3" y1="1.8" x2="3.3" y2="-2.2" width="0.2" layer="21"/>
<wire x1="3.3" y1="-2.2" x2="-3.3" y2="-2.2" width="0.2" layer="21"/>
<wire x1="-3.3" y1="-2.2" x2="-3.3" y2="1.8" width="0.2" layer="21"/>
</package>
<package name="TO220V">
<description>&lt;b&gt;TO 220 vertical&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.127" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="0" y="0" size="1.5" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
<package name="DPAK">
<wire x1="3.3" y1="2.5" x2="3.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="3.3" y1="-3.5" x2="-3.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-3.3" y1="-3.5" x2="-3.3" y2="2.5" width="0.2" layer="51"/>
<wire x1="-3.3" y1="2.5" x2="3.3" y2="2.5" width="0.2" layer="51"/>
<wire x1="-2.5654" y1="2.567" x2="-2.5654" y2="3.2782" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="3.2782" x2="-2.1082" y2="3.7354" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="3.7354" x2="2.1082" y2="3.7354" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="3.7354" x2="2.5654" y2="3.2782" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.2782" x2="2.5654" y2="2.567" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="2.567" x2="-2.5654" y2="2.567" width="0.2032" layer="51"/>
<smd name="1" x="-2.28" y="-5.31" dx="1.6" dy="3" layer="1"/>
<smd name="3" x="2.28" y="-5.31" dx="1.6" dy="3" layer="1"/>
<smd name="4" x="0" y="1.6" dx="6.5" dy="5.6" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2" size="0.4" layer="27" font="vector" rot="R180" align="center">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-6.7262" x2="-1.8542" y2="-3.8306" layer="51"/>
<rectangle x1="1.8542" y1="-6.7262" x2="2.7178" y2="-3.8306" layer="51"/>
<rectangle x1="-0.4318" y1="-4.5926" x2="0.4318" y2="-3.8306" layer="51"/>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="2.567"/>
<vertex x="-2.5654" y="3.2782"/>
<vertex x="-2.1082" y="3.7354"/>
<vertex x="2.1082" y="3.7354"/>
<vertex x="2.5654" y="3.2782"/>
<vertex x="2.5654" y="2.567"/>
</polygon>
<wire x1="-1.2" y1="-4" x2="-1.2" y2="-7.2" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-7.2" x2="-3.4" y2="-7.2" width="0.2" layer="21"/>
<wire x1="-3.4" y1="-7.2" x2="-3.4" y2="-3.8" width="0.2" layer="21"/>
<wire x1="-3.4" y1="-3.8" x2="-3.6" y2="-3.8" width="0.2" layer="21"/>
<wire x1="-3.6" y1="-3.8" x2="-3.6" y2="4.8" width="0.2" layer="21"/>
<wire x1="-3.6" y1="4.8" x2="3.6" y2="4.8" width="0.2" layer="21"/>
<wire x1="3.6" y1="4.8" x2="3.6" y2="-3.8" width="0.2" layer="21"/>
<wire x1="3.6" y1="-3.8" x2="3.4" y2="-3.8" width="0.2" layer="21"/>
<wire x1="3.4" y1="-3.8" x2="3.4" y2="-7.2" width="0.2" layer="21"/>
<wire x1="3.4" y1="-7.2" x2="1.2" y2="-7.2" width="0.2" layer="21"/>
<wire x1="1.2" y1="-7.2" x2="1.2" y2="-4" width="0.2" layer="21"/>
<wire x1="1.2" y1="-4" x2="0.6" y2="-4" width="0.2" layer="21"/>
<wire x1="0.6" y1="-4" x2="0.6" y2="-4.8" width="0.2" layer="21"/>
<wire x1="0.6" y1="-4.8" x2="-0.6" y2="-4.8" width="0.2" layer="21"/>
<wire x1="-0.6" y1="-4.8" x2="-0.6" y2="-4" width="0.2" layer="21"/>
<wire x1="-0.6" y1="-4" x2="-1.2" y2="-4" width="0.2" layer="21"/>
</package>
<package name="TO247BH">
<description>&lt;b&gt;Molded Package&lt;/b&gt;&lt;p&gt;
grid 5.6 mm</description>
<wire x1="-7.874" y1="-6.35" x2="7.874" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-6.096" x2="7.62" y2="-6.096" width="0.0508" layer="21"/>
<wire x1="7.874" y1="5.461" x2="7.874" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="7.874" y1="14.605" x2="-7.874" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-6.35" x2="-7.874" y2="5.461" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="5.461" x2="-7.874" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="9.271" x2="-7.874" y2="14.605" width="0.1524" layer="21"/>
<wire x1="7.874" y1="9.271" x2="7.874" y2="5.461" width="0.1524" layer="21" curve="180"/>
<wire x1="7.62" y1="-6.096" x2="7.62" y2="4.953" width="0.0508" layer="21"/>
<wire x1="-7.874" y1="9.271" x2="-7.874" y2="5.461" width="0.1524" layer="21" curve="-180"/>
<wire x1="-7.62" y1="-6.096" x2="-7.62" y2="4.953" width="0.0508" layer="21"/>
<wire x1="-7.62" y1="9.779" x2="-7.62" y2="13.97" width="0.0508" layer="21"/>
<wire x1="7.874" y1="14.605" x2="7.62" y2="13.97" width="0.0508" layer="21"/>
<wire x1="7.62" y1="13.97" x2="-7.62" y2="13.97" width="0.0508" layer="21"/>
<wire x1="-7.62" y1="13.97" x2="-7.874" y2="14.605" width="0.0508" layer="21"/>
<wire x1="-7.62" y1="4.953" x2="-7.874" y2="5.461" width="0.0508" layer="21"/>
<wire x1="7.874" y1="5.461" x2="7.62" y2="4.953" width="0.0508" layer="21"/>
<wire x1="-7.62" y1="-6.096" x2="-7.874" y2="-6.35" width="0.0508" layer="21"/>
<wire x1="7.874" y1="-6.35" x2="7.62" y2="-6.096" width="0.0508" layer="21"/>
<wire x1="7.62" y1="9.779" x2="7.62" y2="4.953" width="0.0508" layer="21" curve="167.981988"/>
<wire x1="7.874" y1="14.605" x2="7.874" y2="9.271" width="0.1524" layer="21"/>
<wire x1="7.874" y1="9.271" x2="7.874" y2="5.461" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.953" x2="-7.62" y2="9.779" width="0.0508" layer="21" curve="167.981988"/>
<wire x1="7.874" y1="9.271" x2="7.62" y2="9.779" width="0.0508" layer="21"/>
<wire x1="7.62" y1="9.779" x2="7.62" y2="13.97" width="0.0508" layer="21"/>
<wire x1="-7.62" y1="9.779" x2="-7.874" y2="9.271" width="0.0508" layer="21"/>
<circle x="0" y="9.144" radius="5.08" width="0" layer="42"/>
<circle x="0" y="9.144" radius="5.08" width="0" layer="43"/>
<circle x="0" y="9.144" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.588" y="-10.16" drill="1.4986" shape="long" rot="R90"/>
<pad name="2" x="0" y="-10.16" drill="1.4986" shape="long" rot="R90"/>
<pad name="3" x="5.588" y="-10.16" drill="1.4986" shape="long" rot="R90"/>
<text x="-5.08" y="0.635" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.572" y1="-8.001" x2="6.223" y2="-6.35" layer="21"/>
<rectangle x1="-1.524" y1="-8.001" x2="1.524" y2="-6.35" layer="21"/>
<rectangle x1="-6.223" y1="-8.001" x2="-4.572" y2="-6.35" layer="21"/>
<rectangle x1="-6.223" y1="-10.033" x2="-4.572" y2="-8.001" layer="51"/>
<rectangle x1="-6.223" y1="-10.541" x2="-4.953" y2="-10.033" layer="51"/>
<rectangle x1="-1.524" y1="-10.033" x2="1.524" y2="-8.001" layer="51"/>
<rectangle x1="-0.635" y1="-10.541" x2="0.635" y2="-10.033" layer="51"/>
<rectangle x1="4.572" y1="-10.033" x2="6.223" y2="-8.001" layer="51"/>
<rectangle x1="4.953" y1="-10.541" x2="6.223" y2="-10.033" layer="51"/>
<hole x="0" y="9.144" drill="4.1148"/>
</package>
<package name="TO247BV">
<description>&lt;b&gt;Molded Package&lt;/b&gt;&lt;p&gt;
grid 5.6 mm</description>
<wire x1="-6.985" y1="-5.08" x2="6.985" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-5.08" x2="7.366" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-5.08" x2="-7.366" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="7.366" y1="-4.699" x2="7.874" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-7.366" y1="-4.699" x2="-7.874" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-1.524" x2="7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.874" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-1.524" x2="-6.731" y2="-1.524" width="0.0508" layer="21"/>
<wire x1="-6.731" y1="-1.524" x2="-4.445" y2="-1.524" width="0.0508" layer="51"/>
<wire x1="-4.445" y1="-1.524" x2="-1.27" y2="-1.524" width="0.0508" layer="21"/>
<wire x1="-1.27" y1="-1.524" x2="1.143" y2="-1.524" width="0.0508" layer="51"/>
<wire x1="1.143" y1="-1.524" x2="4.445" y2="-1.524" width="0.0508" layer="21"/>
<wire x1="4.445" y1="-1.524" x2="6.731" y2="-1.524" width="0.0508" layer="51"/>
<wire x1="6.731" y1="-1.524" x2="7.874" y2="-1.524" width="0.0508" layer="21"/>
<pad name="G" x="-5.588" y="-2.921" drill="1.4986" shape="long" rot="R90"/>
<pad name="D" x="0" y="-2.921" drill="1.4986" shape="long" rot="R90"/>
<pad name="S" x="5.588" y="-2.921" drill="1.4986" shape="long" rot="R90"/>
<text x="-7.62" y="-6.858" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-6.858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MFPS">
<wire x1="-1.016" y1="-2.54" x2="-1.016" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.016" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.5334" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.2352" y1="0" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.286" y1="0" x2="1.016" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0.508" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="1.143" y1="0.254" x2="2.032" y2="0" width="0.3048" layer="94"/>
<wire x1="2.032" y1="0" x2="1.143" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.143" y1="-0.254" x2="1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="1.143" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.08" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.715" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-0.635" x2="4.445" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.635" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0.762" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="4.445" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="4.191" y2="1.016" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0.762" x2="5.969" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="7.62" y="0.762" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="7.62" y="-1.778" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="1.524" y="-3.302" size="0.8128" layer="93">D</text>
<text x="1.524" y="2.54" size="0.8128" layer="93">S</text>
<text x="-2.286" y="1.27" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-2.54" y="2.54" visible="off" length="point" direction="pas"/>
<pin name="D" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="MFNS">
<wire x1="-1.1176" y1="2.413" x2="-1.1176" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.1176" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.5334" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="5.08" y2="1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.08" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="4.445" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.635" x2="5.715" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-0.635" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.715" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0.762" x2="5.969" y2="1.016" width="0.1524" layer="94"/>
<wire x1="4.445" y1="0.762" x2="4.191" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0" x2="1.778" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0.254" x2="0.762" y2="0" width="0.3048" layer="94"/>
<wire x1="0.762" y1="0" x2="1.651" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.651" y1="-0.254" x2="1.651" y2="0" width="0.3048" layer="94"/>
<wire x1="1.651" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="7.62" y="0.508" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="7.62" y="-2.032" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="1.27" y="2.54" size="0.8128" layer="93">D</text>
<text x="1.27" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FET_P" prefix="VT" uservalue="yes">
<gates>
<gate name="G$1" symbol="MFPS" x="0" y="0"/>
</gates>
<devices>
<device name="-TO220" package="TO220BH">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="TO252">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FET_N" prefix="VT" uservalue="yes">
<gates>
<gate name="G$1" symbol="MFNS" x="0" y="0"/>
</gates>
<devices>
<device name="-MICRO3" package="MICRO3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO220BH" package="TO220BH">
<connects>
<connect gate="G$1" pin="D" pad="2 2_"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT223" package="SOT223">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO220V" package="TO220V">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DPAK" package="DPAK">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO247BH" package="TO247BH">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO247BV" package="TO247BV">
<connects>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_misc">
<packages>
<package name="HC18U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.461" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="-5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.461" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="10.16" x2="-4.445" y2="10.16" width="0.1524" layer="21"/>
<wire x1="4.445" y1="10.16" x2="5.08" y2="9.525" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.525" x2="-4.445" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.54" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.668" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.889" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-5.08" x2="6.35" y2="10.795" layer="43"/>
</package>
<package name="MM39SL">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.683" y1="-1.651" x2="3.683" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="-3.683" y1="-2.286" x2="3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="1.651" x2="-3.683" y2="1.651" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-4.826" y2="0.762" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-3.683" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.055" x2="-6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.715" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="0.762" x2="-4.826" y2="-0.762" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="-0.762" x2="-4.826" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-5.715" y2="-2.055" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="2.286" x2="-3.683" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-3.683" y1="1.651" x2="-4.826" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-6.223" y1="2.286" x2="-5.715" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.055" x2="-6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-5.715" y2="2.055" width="0.0508" layer="51"/>
<wire x1="6.223" y1="-2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="5.842" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="5.715" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="5.715" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.651" x2="5.842" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="3.683" y1="-1.651" x2="5.715" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="5.842" y1="1.651" x2="6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="5.842" y1="1.651" x2="5.715" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.715" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="2.286" x2="5.715" y2="2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.651" x2="3.683" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-3.81" y1="-0.254" x2="-2.54" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.254" x2="-2.54" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.254" x2="-3.81" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.254" x2="-3.81" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="1.016" width="0.1016" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-3.175" y2="-1.016" width="0.1016" layer="21"/>
<circle x="5.08" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="2" x="4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="3" x="4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="4" x="-4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CRYSTAL-SMD-3225">
<description>KX-7T 16.0 MHz</description>
<smd name="1" x="-1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="3" x="1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="4" x="-1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.8" x2="2.1" y2="-1.8" width="0.2" layer="21"/>
<wire x1="2.1" y1="-1.8" x2="-1.9" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-2.1" y1="-1.6" x2="-2.1" y2="1.8" width="0.2" layer="21"/>
<wire x1="-2.1" y1="1.8" x2="2.1" y2="1.8" width="0.2" layer="21"/>
<wire x1="-2.1" y1="-1.6" x2="-1.9" y2="-1.8" width="0.2" layer="21"/>
<text x="0" y="0" size="0.8" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
<circle x="-1.3" y="-0.9" radius="0.14141875" width="0.1" layer="51"/>
</package>
<package name="SN2032">
<description>&lt;b&gt;LI BATTERY&lt;/b&gt; Varta</description>
<pad name="+" x="0" y="0" drill="1.2" diameter="2.5" shape="square"/>
<pad name="-" x="0" y="20.3" drill="1.2" diameter="2.5"/>
<text x="0" y="8" size="1" layer="25" font="vector" ratio="12" rot="R180" align="center">&gt;NAME</text>
<circle x="0" y="7.62" radius="11" width="0.1" layer="51"/>
<wire x1="-6.604" y1="21.844" x2="-6.604" y2="16.51" width="0.1" layer="51"/>
<wire x1="6.604" y1="21.844" x2="-6.604" y2="21.844" width="0.1" layer="51"/>
<wire x1="6.604" y1="16.51" x2="6.604" y2="21.844" width="0.1" layer="51"/>
</package>
<package name="86SMX">
<description>&lt;b&gt;CRYSTAL RESONATOR&lt;/b&gt;</description>
<wire x1="-3.81" y1="1.905" x2="2.413" y2="1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="2.286" x2="2.413" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="2.413" y2="-1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="-2.286" x2="2.413" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.334" y1="1.905" x2="-5.334" y2="1.016" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-3.81" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="-2.286" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.286" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-2.286" x2="-3.81" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.54" x2="-4.191" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-2.2098" x2="-6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.35" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-2.54" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.016" x2="-5.334" y2="-1.016" width="0.0508" layer="21"/>
<wire x1="-5.334" y1="-1.016" x2="-5.334" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-6.35" y2="-2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="-2.54" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.54" x2="-4.191" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-3.81" y2="1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="2.286" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.286" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="2.286" x2="-3.81" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-6.604" y1="2.286" x2="-6.35" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="2.2098" x2="-6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-5.969" y1="2.54" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-6.35" y2="2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="2.54" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="6.604" y1="2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.223" y2="1.905" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.54" x2="5.842" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-2.286" x2="2.794" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.905" x2="6.223" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="6.223" y1="1.905" x2="6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="2.286" x2="6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.54" x2="5.842" y2="2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="2.286" x2="2.794" y2="2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="2.54" x2="5.842" y2="2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="1.905" x2="6.223" y2="1.905" width="0.0508" layer="51"/>
<wire x1="2.413" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-3.651" y1="-0.254" x2="-2.381" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.381" y1="-0.254" x2="-2.381" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.381" y1="0.254" x2="-3.651" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-3.651" y1="0.254" x2="-3.651" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.651" y1="0.635" x2="-3.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.016" y1="0.635" x2="-2.381" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.016" y1="0.635" x2="-3.016" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-3.651" y1="-0.635" x2="-3.016" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.016" y1="-0.635" x2="-2.381" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.016" y1="-0.635" x2="-3.016" y2="-1.016" width="0.0508" layer="21"/>
<smd name="2" x="4.318" y="-2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="3" x="4.318" y="2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="1" x="-5.08" y="-2.286" dx="2.286" dy="2.1844" layer="1"/>
<smd name="4" x="-5.08" y="2.286" dx="2.286" dy="2.1844" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-3.683" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM20SS">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.032" y1="-1.27" x2="2.032" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-1.778" x2="2.032" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.032" y1="1.27" x2="-2.032" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.778" x2="2.032" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="0.381" x2="-2.921" y2="-0.381" width="0.0508" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-3.556" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.552" x2="-4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-2.921" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="1.27" x2="-2.921" y2="0.381" width="0.0508" layer="51"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-0.381" x2="-2.921" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-2.032" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-3.556" y1="-1.778" x2="-2.032" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-3.556" y2="-1.552" width="0.0508" layer="51"/>
<wire x1="-4.064" y1="1.778" x2="-3.556" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.552" x2="-4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="-2.921" y1="1.27" x2="-3.556" y2="1.552" width="0.0508" layer="51"/>
<wire x1="-3.048" y1="1.778" x2="-3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="1.905" x2="-2.54" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="1.778" x2="-2.032" y2="1.778" width="0.1524" layer="51"/>
<wire x1="4.064" y1="-1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.778" x2="4.064" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.556" y2="1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.27" x2="2.032" y2="1.27" width="0.0508" layer="51"/>
<wire x1="3.048" y1="-1.905" x2="3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.778" x2="2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="3.048" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-1.27" x2="3.556" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="2.032" y1="-1.778" x2="3.556" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="3.81" y1="1.27" x2="4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="3.048" y1="1.778" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="1.905" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="1.778" x2="3.556" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-0.254" x2="-0.508" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-0.508" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-1.778" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.254" x2="-1.778" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.635" x2="-1.143" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-1.143" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="-0.635" x2="-1.143" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="-1.016" width="0.0508" layer="21"/>
<circle x="3.048" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="2" x="2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="3" x="2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="4" x="-2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SJK-7M">
<smd name="1" x="-3.15" y="-0.45" dx="1.2" dy="0.6" layer="1" rot="R180"/>
<smd name="4" x="-3.15" y="0.45" dx="1.2" dy="0.6" layer="1" rot="R180"/>
<smd name="2" x="3.15" y="-0.45" dx="1.2" dy="0.6" layer="1" rot="R180"/>
<smd name="3" x="3.15" y="0.45" dx="1.2" dy="0.6" layer="1" rot="R180"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-3.7" y1="1" x2="-2.2" y2="1" width="0.2" layer="21"/>
<wire x1="-2.2" y1="1" x2="3.5" y2="1" width="0.2" layer="21"/>
<wire x1="3.5" y1="-1" x2="-2.2" y2="-1" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-1" x2="-3.7" y2="-1" width="0.2" layer="21"/>
<wire x1="-3.5" y1="0.7" x2="3.5" y2="0.7" width="0.127" layer="51"/>
<wire x1="3.5" y1="0.7" x2="3.5" y2="-0.7" width="0.127" layer="51"/>
<wire x1="3.5" y1="-0.7" x2="-3.5" y2="-0.7" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-0.7" x2="-3.5" y2="0.7" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1" x2="-2.2" y2="-1" width="0.2" layer="21"/>
<text x="0" y="0" size="1" layer="51" font="vector" ratio="12" align="center">&gt;NAME</text>
</package>
<package name="CTS406">
<description>&lt;b&gt;Model 406 6.0x3.5mm Low Cost Surface Mount Crystal&lt;/b&gt;&lt;p&gt;
Source: 008-0260-0_E.pdf</description>
<wire x1="-1.05" y1="1.85" x2="1.05" y2="1.85" width="0.2032" layer="21"/>
<wire x1="-2.475" y1="1.65" x2="2.475" y2="1.65" width="0.2032" layer="51"/>
<wire x1="3.1" y1="0.3" x2="3.1" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="2.9" y1="1.225" x2="2.9" y2="-1.225" width="0.2032" layer="51"/>
<wire x1="1.05" y1="-1.85" x2="-1.05" y2="-1.85" width="0.2032" layer="21"/>
<wire x1="2.475" y1="-1.65" x2="-2.475" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-1.225" x2="-2.9" y2="1.225" width="0.2032" layer="51"/>
<wire x1="-3.1" y1="-0.3" x2="-3.1" y2="0.3" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="1.225" x2="-2.475" y2="1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.475" y1="1.65" x2="2.9" y2="1.225" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.9" y1="-1.225" x2="2.475" y2="-1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="-2.475" y1="-1.65" x2="-2.9" y2="-1.225" width="0.2032" layer="51" curve="89.516721"/>
<circle x="-2.35" y="-1.1" radius="0.182" width="0" layer="51"/>
<smd name="1" x="-2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="2" x="2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="3" x="2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="4" x="-2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-1" size="1.2" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
</package>
<package name="RN-ANT">
<wire x1="-4.3942" y1="-0.2032" x2="-4.3942" y2="2.159" width="0.508" layer="1"/>
<wire x1="-4.3942" y1="4.3434" x2="-1.7526" y2="4.3434" width="0.508" layer="1"/>
<wire x1="-1.7526" y1="4.3434" x2="-1.7526" y2="6.858" width="0.508" layer="1"/>
<wire x1="-1.7526" y1="6.858" x2="-4.3942" y2="6.858" width="0.508" layer="1"/>
<wire x1="-4.3942" y1="6.858" x2="-4.3942" y2="9.0678" width="0.508" layer="1"/>
<wire x1="-4.3942" y1="9.0678" x2="-1.7526" y2="9.0678" width="0.508" layer="1"/>
<wire x1="-1.7526" y1="9.0678" x2="-1.7526" y2="11.5824" width="0.508" layer="1"/>
<wire x1="-1.7526" y1="11.5824" x2="-4.3942" y2="11.5824" width="0.508" layer="1"/>
<wire x1="-4.3942" y1="11.5824" x2="-4.3942" y2="13.7922" width="0.508" layer="1"/>
<wire x1="-4.3942" y1="13.7922" x2="-2.7432" y2="13.7922" width="0.508" layer="1"/>
<wire x1="0" y1="2.159" x2="-4.3942" y2="2.159" width="0.508" layer="1"/>
<wire x1="-4.3942" y1="2.159" x2="-4.3942" y2="4.3434" width="0.508" layer="1"/>
<wire x1="-4.191" y1="0" x2="-1.27" y2="0" width="0.889" layer="1"/>
<smd name="GND" x="-1.143" y="0" dx="2.54" dy="0.889" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="FEED" x="0" y="2.159" dx="0.0254" dy="0.0254" layer="1" stop="no" thermals="no" cream="no"/>
</package>
<package name="ANTENNA-2.4GHZ-IFA">
<description>Inverted F Antenna
http://focus.ti.com/lit/an/swru120b/swru120b.pdf</description>
<wire x1="-12" y1="8" x2="16" y2="8" width="0.2" layer="21"/>
<wire x1="-12" y1="8" x2="-12" y2="0" width="0.2" layer="21"/>
<wire x1="16" y1="8" x2="16" y2="0" width="0.2" layer="21"/>
<wire x1="-12" y1="0" x2="16" y2="0" width="0.2" layer="21"/>
<smd name="A" x="0" y="0.05" dx="0.25" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="G" x="-3.5" y="0" dx="0.25" dy="0.25" layer="1" stop="no" thermals="no" cream="no"/>
<polygon width="0.1" layer="1">
<vertex x="-3.905" y="-0.08"/>
<vertex x="-3.905" y="0.98"/>
<vertex x="-10.735" y="0.98"/>
<vertex x="-10.735" y="6.91"/>
<vertex x="15.235" y="6.91"/>
<vertex x="15.235" y="5.92"/>
<vertex x="-1.765" y="5.92"/>
<vertex x="-1.765" y="3.91"/>
<vertex x="-0.765" y="3.91"/>
<vertex x="-0.765" y="2.92"/>
<vertex x="-3.755" y="2.92"/>
<vertex x="-3.755" y="3.91"/>
<vertex x="-2.755" y="3.91"/>
<vertex x="-2.755" y="4.92"/>
<vertex x="-8.765" y="4.92"/>
<vertex x="-8.765" y="2.05"/>
<vertex x="-0.765" y="2.05"/>
<vertex x="-0.765" y="-0.08"/>
<vertex x="-1.755" y="-0.08"/>
<vertex x="-1.755" y="0.92"/>
<vertex x="-2.765" y="0.92"/>
<vertex x="-2.765" y="-0.08"/>
</polygon>
<polygon width="0.1" layer="1">
<vertex x="-0.125" y="1.96"/>
<vertex x="-0.125" y="2.18"/>
<vertex x="-0.935" y="2.99"/>
<vertex x="-0.935" y="3.28"/>
<vertex x="-0.875" y="3.28"/>
<vertex x="0.125" y="2.28"/>
<vertex x="0.125" y="1.96"/>
</polygon>
<polygon width="0.1" layer="1">
<vertex x="-0.125" y="1.99"/>
<vertex x="0.125" y="1.99"/>
<vertex x="0.125" y="-0.08"/>
<vertex x="-0.125" y="-0.08"/>
</polygon>
<rectangle x1="-12" y1="0" x2="16" y2="8" layer="29"/>
<rectangle x1="-11" y1="-1" x2="17" y2="7" layer="39"/>
</package>
<package name="ANTENNA-2.4GHZ-MEANTRED">
<description>Small Size 2.4HGz PCB anetenna
http://focus.ti.com/lit/an/swra117d/swra117d.pdf</description>
<wire x1="0.5" y1="0" x2="14" y2="0" width="0.5" layer="1"/>
<pad name="G" x="0.5" y="0" drill="0.5" diameter="0.8128"/>
<smd name="A" x="2.54" y="0" dx="0.5" dy="0.5" layer="16" rot="R90"/>
<rectangle x1="0" y1="0" x2="0.9" y2="5" layer="16"/>
<rectangle x1="0" y1="4.9" x2="5" y2="5.4" layer="16"/>
<rectangle x1="2.3" y1="0" x2="2.8" y2="5" layer="16"/>
<rectangle x1="4.5" y1="2.26" x2="5" y2="5" layer="16"/>
<rectangle x1="4.9" y1="2.26" x2="7.1" y2="2.76" layer="16"/>
<rectangle x1="7" y1="2.26" x2="7.5" y2="5" layer="16"/>
<rectangle x1="7" y1="4.9" x2="9.7" y2="5.4" layer="16"/>
<rectangle x1="9.2" y1="2.26" x2="9.7" y2="5" layer="16"/>
<rectangle x1="9.6" y1="2.26" x2="11.8" y2="2.76" layer="16"/>
<rectangle x1="11.7" y1="2.26" x2="12.2" y2="5" layer="16"/>
<rectangle x1="11.7" y1="4.9" x2="14.4" y2="5.4" layer="16"/>
<rectangle x1="13.9" y1="0.96" x2="14.4" y2="5" layer="16"/>
</package>
<package name="OLIMEX_ANT_ESP8266_ANT">
<wire x1="-5.334" y1="-2.667" x2="1.651" y2="-2.667" width="0.635" layer="1"/>
<wire x1="-5.334" y1="-2.667" x2="-5.334" y2="3.429" width="0.635" layer="1"/>
<wire x1="-5.334" y1="3.429" x2="-4.191" y2="3.429" width="0.635" layer="1"/>
<wire x1="-4.191" y1="3.429" x2="-4.191" y2="-0.508" width="0.635" layer="1"/>
<wire x1="-4.191" y1="-0.508" x2="-3.048" y2="-0.508" width="0.635" layer="1"/>
<wire x1="-3.048" y1="-0.508" x2="-3.048" y2="3.429" width="0.635" layer="1"/>
<wire x1="-3.048" y1="3.429" x2="-1.905" y2="3.429" width="0.635" layer="1"/>
<wire x1="-1.905" y1="3.429" x2="-1.905" y2="-0.508" width="0.635" layer="1"/>
<wire x1="-1.905" y1="-0.508" x2="-0.762" y2="-0.508" width="0.635" layer="1"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="3.429" width="0.635" layer="1"/>
<wire x1="-0.762" y1="3.429" x2="0.381" y2="3.429" width="0.635" layer="1"/>
<wire x1="0.381" y1="3.429" x2="0.381" y2="-0.508" width="0.635" layer="1"/>
<wire x1="0.381" y1="-0.508" x2="1.524" y2="-0.508" width="0.635" layer="1"/>
<wire x1="1.524" y1="-0.508" x2="1.524" y2="3.429" width="0.635" layer="1"/>
<wire x1="1.524" y1="3.429" x2="5.461" y2="3.429" width="0.635" layer="1"/>
<wire x1="-4.572" y1="-4.191" x2="-4.572" y2="-3.429" width="0.254" layer="41"/>
<wire x1="-4.572" y1="-3.429" x2="6.223" y2="-3.429" width="0.254" layer="41"/>
<wire x1="6.223" y1="-3.429" x2="6.223" y2="4.191" width="0.254" layer="41"/>
<wire x1="6.223" y1="4.191" x2="-6.096" y2="4.191" width="0.254" layer="41"/>
<wire x1="-6.096" y1="4.191" x2="-6.096" y2="-4.191" width="0.254" layer="41"/>
<wire x1="-6.096" y1="-1.524" x2="6.223" y2="-1.524" width="0.254" layer="42"/>
<wire x1="6.223" y1="-1.524" x2="6.223" y2="4.191" width="0.254" layer="42"/>
<wire x1="6.223" y1="4.191" x2="-6.096" y2="4.191" width="0.254" layer="42"/>
<wire x1="-6.096" y1="4.191" x2="-6.096" y2="-1.524" width="0.254" layer="42"/>
<wire x1="-5.334" y1="-2.667" x2="1.651" y2="-2.667" width="0.762" layer="29"/>
<wire x1="-5.334" y1="-2.667" x2="-5.334" y2="3.429" width="0.762" layer="29"/>
<wire x1="-5.334" y1="3.429" x2="-4.191" y2="3.429" width="0.762" layer="29"/>
<wire x1="-4.191" y1="3.429" x2="-4.191" y2="-0.508" width="0.762" layer="29"/>
<wire x1="-4.191" y1="-0.508" x2="-3.048" y2="-0.508" width="0.762" layer="29"/>
<wire x1="-3.048" y1="-0.508" x2="-3.048" y2="3.429" width="0.762" layer="29"/>
<wire x1="-3.048" y1="3.429" x2="-1.905" y2="3.429" width="0.762" layer="29"/>
<wire x1="-1.905" y1="3.429" x2="-1.905" y2="-0.508" width="0.762" layer="29"/>
<wire x1="-1.905" y1="-0.508" x2="-0.762" y2="-0.508" width="0.762" layer="29"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="3.429" width="0.762" layer="29"/>
<wire x1="-0.762" y1="3.429" x2="0.381" y2="3.429" width="0.762" layer="29"/>
<wire x1="0.381" y1="3.429" x2="0.381" y2="-0.508" width="0.762" layer="29"/>
<wire x1="0.381" y1="-0.508" x2="1.524" y2="-0.508" width="0.762" layer="29"/>
<wire x1="1.524" y1="-0.508" x2="1.524" y2="3.429" width="0.762" layer="29"/>
<wire x1="1.524" y1="3.429" x2="5.461" y2="3.429" width="0.762" layer="29"/>
<smd name="ANT" x="-5.334" y="-3.302" dx="1.905" dy="0.635" layer="1" rot="R90"/>
<pad name="GND" x="1.651" y="-2.667" drill="0.4" diameter="0.635"/>
<text x="0.889" y="-4.318" size="1.016" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<text x="-4.191" y="-4.318" size="1.016" layer="25" font="vector" ratio="20">&gt;NAME</text>
</package>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
</package>
<package name="ANT2-SMD-9.5X2X1.2MM">
<smd name="1" x="-4.5" y="0" dx="1.5" dy="1.8" layer="1"/>
<smd name="2" x="4.5" y="0" dx="1.5" dy="1.8" layer="1"/>
<wire x1="-3.9" y1="1" x2="3.9" y2="1" width="0.127" layer="21"/>
<wire x1="4.75" y1="1" x2="4.75" y2="-1" width="0.127" layer="51"/>
<wire x1="3.9" y1="-1" x2="-3.9" y2="-1" width="0.127" layer="21"/>
<wire x1="-4.75" y1="-1" x2="-4.75" y2="1" width="0.127" layer="51"/>
<text x="-1.905" y="1.27" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<rectangle x1="-4.75" y1="-1.05" x2="4.75" y2="1.05" layer="39"/>
<circle x="-5.715" y="0" radius="0.381" width="0" layer="21"/>
</package>
<package name="LOGO_NOMAD_V1">
<rectangle x1="4.064" y1="0.508" x2="4.318" y2="0.762" layer="16"/>
<rectangle x1="4.318" y1="0.508" x2="4.572" y2="0.762" layer="16"/>
<rectangle x1="4.572" y1="0.508" x2="4.826" y2="0.762" layer="16"/>
<rectangle x1="5.08" y1="0.508" x2="5.334" y2="0.762" layer="16"/>
<rectangle x1="5.588" y1="0.508" x2="5.842" y2="0.762" layer="16"/>
<rectangle x1="5.842" y1="0.508" x2="6.096" y2="0.762" layer="16"/>
<rectangle x1="6.096" y1="0.508" x2="6.35" y2="0.762" layer="16"/>
<rectangle x1="6.604" y1="0.508" x2="6.858" y2="0.762" layer="16"/>
<rectangle x1="7.112" y1="0.508" x2="7.366" y2="0.762" layer="16"/>
<rectangle x1="7.366" y1="0.508" x2="7.62" y2="0.762" layer="16"/>
<rectangle x1="7.62" y1="0.508" x2="7.874" y2="0.762" layer="16"/>
<rectangle x1="8.382" y1="0.508" x2="8.636" y2="0.762" layer="16"/>
<rectangle x1="9.906" y1="0.508" x2="10.16" y2="0.762" layer="16"/>
<rectangle x1="10.16" y1="0.508" x2="10.414" y2="0.762" layer="16"/>
<rectangle x1="11.43" y1="0.508" x2="11.684" y2="0.762" layer="16"/>
<rectangle x1="11.684" y1="0.508" x2="11.938" y2="0.762" layer="16"/>
<rectangle x1="12.7" y1="0.508" x2="12.954" y2="0.762" layer="16"/>
<rectangle x1="12.954" y1="0.508" x2="13.208" y2="0.762" layer="16"/>
<rectangle x1="13.97" y1="0.508" x2="14.224" y2="0.762" layer="16"/>
<rectangle x1="14.224" y1="0.508" x2="14.478" y2="0.762" layer="16"/>
<rectangle x1="15.494" y1="0.508" x2="15.748" y2="0.762" layer="16"/>
<rectangle x1="15.748" y1="0.508" x2="16.002" y2="0.762" layer="16"/>
<rectangle x1="16.256" y1="0.508" x2="16.51" y2="0.762" layer="16"/>
<rectangle x1="16.51" y1="0.508" x2="16.764" y2="0.762" layer="16"/>
<rectangle x1="16.764" y1="0.508" x2="17.018" y2="0.762" layer="16"/>
<rectangle x1="17.018" y1="0.508" x2="17.272" y2="0.762" layer="16"/>
<rectangle x1="17.272" y1="0.508" x2="17.526" y2="0.762" layer="16"/>
<rectangle x1="17.526" y1="0.508" x2="17.78" y2="0.762" layer="16"/>
<rectangle x1="19.304" y1="0.508" x2="19.558" y2="0.762" layer="16"/>
<rectangle x1="19.558" y1="0.508" x2="19.812" y2="0.762" layer="16"/>
<rectangle x1="4.572" y1="0.762" x2="4.826" y2="1.016" layer="16"/>
<rectangle x1="5.588" y1="0.762" x2="5.842" y2="1.016" layer="16"/>
<rectangle x1="6.096" y1="0.762" x2="6.35" y2="1.016" layer="16"/>
<rectangle x1="7.366" y1="0.762" x2="7.62" y2="1.016" layer="16"/>
<rectangle x1="8.128" y1="0.762" x2="8.382" y2="1.016" layer="16"/>
<rectangle x1="8.382" y1="0.762" x2="8.636" y2="1.016" layer="16"/>
<rectangle x1="8.636" y1="0.762" x2="8.89" y2="1.016" layer="16"/>
<rectangle x1="9.906" y1="0.762" x2="10.16" y2="1.016" layer="16"/>
<rectangle x1="10.16" y1="0.762" x2="10.414" y2="1.016" layer="16"/>
<rectangle x1="11.43" y1="0.762" x2="11.684" y2="1.016" layer="16"/>
<rectangle x1="11.684" y1="0.762" x2="11.938" y2="1.016" layer="16"/>
<rectangle x1="12.7" y1="0.762" x2="12.954" y2="1.016" layer="16"/>
<rectangle x1="12.954" y1="0.762" x2="13.208" y2="1.016" layer="16"/>
<rectangle x1="13.97" y1="0.762" x2="14.224" y2="1.016" layer="16"/>
<rectangle x1="14.224" y1="0.762" x2="14.478" y2="1.016" layer="16"/>
<rectangle x1="15.494" y1="0.762" x2="15.748" y2="1.016" layer="16"/>
<rectangle x1="15.748" y1="0.762" x2="16.002" y2="1.016" layer="16"/>
<rectangle x1="16.256" y1="0.762" x2="16.51" y2="1.016" layer="16"/>
<rectangle x1="16.51" y1="0.762" x2="16.764" y2="1.016" layer="16"/>
<rectangle x1="16.764" y1="0.762" x2="17.018" y2="1.016" layer="16"/>
<rectangle x1="17.018" y1="0.762" x2="17.272" y2="1.016" layer="16"/>
<rectangle x1="17.272" y1="0.762" x2="17.526" y2="1.016" layer="16"/>
<rectangle x1="17.526" y1="0.762" x2="17.78" y2="1.016" layer="16"/>
<rectangle x1="19.304" y1="0.762" x2="19.558" y2="1.016" layer="16"/>
<rectangle x1="19.558" y1="0.762" x2="19.812" y2="1.016" layer="16"/>
<rectangle x1="4.064" y1="1.016" x2="4.318" y2="1.27" layer="16"/>
<rectangle x1="4.318" y1="1.016" x2="4.572" y2="1.27" layer="16"/>
<rectangle x1="4.572" y1="1.016" x2="4.826" y2="1.27" layer="16"/>
<rectangle x1="5.588" y1="1.016" x2="5.842" y2="1.27" layer="16"/>
<rectangle x1="5.842" y1="1.016" x2="6.096" y2="1.27" layer="16"/>
<rectangle x1="6.096" y1="1.016" x2="6.35" y2="1.27" layer="16"/>
<rectangle x1="7.366" y1="1.016" x2="7.62" y2="1.27" layer="16"/>
<rectangle x1="8.128" y1="1.016" x2="8.382" y2="1.27" layer="16"/>
<rectangle x1="8.636" y1="1.016" x2="8.89" y2="1.27" layer="16"/>
<rectangle x1="9.906" y1="1.016" x2="10.16" y2="1.27" layer="16"/>
<rectangle x1="10.16" y1="1.016" x2="10.414" y2="1.27" layer="16"/>
<rectangle x1="10.414" y1="1.016" x2="10.668" y2="1.27" layer="16"/>
<rectangle x1="10.668" y1="1.016" x2="10.922" y2="1.27" layer="16"/>
<rectangle x1="10.922" y1="1.016" x2="11.176" y2="1.27" layer="16"/>
<rectangle x1="11.176" y1="1.016" x2="11.43" y2="1.27" layer="16"/>
<rectangle x1="11.43" y1="1.016" x2="11.684" y2="1.27" layer="16"/>
<rectangle x1="11.684" y1="1.016" x2="11.938" y2="1.27" layer="16"/>
<rectangle x1="12.7" y1="1.016" x2="12.954" y2="1.27" layer="16"/>
<rectangle x1="12.954" y1="1.016" x2="13.208" y2="1.27" layer="16"/>
<rectangle x1="13.97" y1="1.016" x2="14.224" y2="1.27" layer="16"/>
<rectangle x1="14.224" y1="1.016" x2="14.478" y2="1.27" layer="16"/>
<rectangle x1="14.478" y1="1.016" x2="14.732" y2="1.27" layer="16"/>
<rectangle x1="14.732" y1="1.016" x2="14.986" y2="1.27" layer="16"/>
<rectangle x1="14.986" y1="1.016" x2="15.24" y2="1.27" layer="16"/>
<rectangle x1="15.24" y1="1.016" x2="15.494" y2="1.27" layer="16"/>
<rectangle x1="15.494" y1="1.016" x2="15.748" y2="1.27" layer="16"/>
<rectangle x1="15.748" y1="1.016" x2="16.002" y2="1.27" layer="16"/>
<rectangle x1="17.272" y1="1.016" x2="17.526" y2="1.27" layer="16"/>
<rectangle x1="17.526" y1="1.016" x2="17.78" y2="1.27" layer="16"/>
<rectangle x1="19.304" y1="1.016" x2="19.558" y2="1.27" layer="16"/>
<rectangle x1="19.558" y1="1.016" x2="19.812" y2="1.27" layer="16"/>
<rectangle x1="4.064" y1="1.27" x2="4.318" y2="1.524" layer="16"/>
<rectangle x1="6.096" y1="1.27" x2="6.35" y2="1.524" layer="16"/>
<rectangle x1="7.366" y1="1.27" x2="7.62" y2="1.524" layer="16"/>
<rectangle x1="7.62" y1="1.27" x2="7.874" y2="1.524" layer="16"/>
<rectangle x1="8.128" y1="1.27" x2="8.382" y2="1.524" layer="16"/>
<rectangle x1="8.636" y1="1.27" x2="8.89" y2="1.524" layer="16"/>
<rectangle x1="10.16" y1="1.27" x2="10.414" y2="1.524" layer="16"/>
<rectangle x1="10.414" y1="1.27" x2="10.668" y2="1.524" layer="16"/>
<rectangle x1="10.668" y1="1.27" x2="10.922" y2="1.524" layer="16"/>
<rectangle x1="10.922" y1="1.27" x2="11.176" y2="1.524" layer="16"/>
<rectangle x1="11.176" y1="1.27" x2="11.43" y2="1.524" layer="16"/>
<rectangle x1="11.43" y1="1.27" x2="11.684" y2="1.524" layer="16"/>
<rectangle x1="12.7" y1="1.27" x2="12.954" y2="1.524" layer="16"/>
<rectangle x1="12.954" y1="1.27" x2="13.208" y2="1.524" layer="16"/>
<rectangle x1="14.224" y1="1.27" x2="14.478" y2="1.524" layer="16"/>
<rectangle x1="14.478" y1="1.27" x2="14.732" y2="1.524" layer="16"/>
<rectangle x1="14.732" y1="1.27" x2="14.986" y2="1.524" layer="16"/>
<rectangle x1="14.986" y1="1.27" x2="15.24" y2="1.524" layer="16"/>
<rectangle x1="15.24" y1="1.27" x2="15.494" y2="1.524" layer="16"/>
<rectangle x1="15.494" y1="1.27" x2="15.748" y2="1.524" layer="16"/>
<rectangle x1="17.272" y1="1.27" x2="17.526" y2="1.524" layer="16"/>
<rectangle x1="17.526" y1="1.27" x2="17.78" y2="1.524" layer="16"/>
<rectangle x1="19.304" y1="1.27" x2="19.558" y2="1.524" layer="16"/>
<rectangle x1="19.558" y1="1.27" x2="19.812" y2="1.524" layer="16"/>
<rectangle x1="4.064" y1="1.524" x2="4.318" y2="1.778" layer="16"/>
<rectangle x1="4.318" y1="1.524" x2="4.572" y2="1.778" layer="16"/>
<rectangle x1="4.572" y1="1.524" x2="4.826" y2="1.778" layer="16"/>
<rectangle x1="5.588" y1="1.524" x2="5.842" y2="1.778" layer="16"/>
<rectangle x1="5.842" y1="1.524" x2="6.096" y2="1.778" layer="16"/>
<rectangle x1="6.096" y1="1.524" x2="6.35" y2="1.778" layer="16"/>
<rectangle x1="7.366" y1="1.524" x2="7.62" y2="1.778" layer="16"/>
<rectangle x1="10.16" y1="1.524" x2="10.414" y2="1.778" layer="16"/>
<rectangle x1="10.414" y1="1.524" x2="10.668" y2="1.778" layer="16"/>
<rectangle x1="11.176" y1="1.524" x2="11.43" y2="1.778" layer="16"/>
<rectangle x1="11.43" y1="1.524" x2="11.684" y2="1.778" layer="16"/>
<rectangle x1="12.7" y1="1.524" x2="12.954" y2="1.778" layer="16"/>
<rectangle x1="12.954" y1="1.524" x2="13.208" y2="1.778" layer="16"/>
<rectangle x1="14.224" y1="1.524" x2="14.478" y2="1.778" layer="16"/>
<rectangle x1="14.478" y1="1.524" x2="14.732" y2="1.778" layer="16"/>
<rectangle x1="15.24" y1="1.524" x2="15.494" y2="1.778" layer="16"/>
<rectangle x1="15.494" y1="1.524" x2="15.748" y2="1.778" layer="16"/>
<rectangle x1="17.272" y1="1.524" x2="17.526" y2="1.778" layer="16"/>
<rectangle x1="17.526" y1="1.524" x2="17.78" y2="1.778" layer="16"/>
<rectangle x1="19.304" y1="1.524" x2="19.558" y2="1.778" layer="16"/>
<rectangle x1="19.558" y1="1.524" x2="19.812" y2="1.778" layer="16"/>
<rectangle x1="10.16" y1="1.778" x2="10.414" y2="2.032" layer="16"/>
<rectangle x1="10.414" y1="1.778" x2="10.668" y2="2.032" layer="16"/>
<rectangle x1="11.176" y1="1.778" x2="11.43" y2="2.032" layer="16"/>
<rectangle x1="11.43" y1="1.778" x2="11.684" y2="2.032" layer="16"/>
<rectangle x1="12.7" y1="1.778" x2="12.954" y2="2.032" layer="16"/>
<rectangle x1="12.954" y1="1.778" x2="13.208" y2="2.032" layer="16"/>
<rectangle x1="14.224" y1="1.778" x2="14.478" y2="2.032" layer="16"/>
<rectangle x1="14.478" y1="1.778" x2="14.732" y2="2.032" layer="16"/>
<rectangle x1="15.24" y1="1.778" x2="15.494" y2="2.032" layer="16"/>
<rectangle x1="15.494" y1="1.778" x2="15.748" y2="2.032" layer="16"/>
<rectangle x1="17.272" y1="1.778" x2="17.526" y2="2.032" layer="16"/>
<rectangle x1="17.526" y1="1.778" x2="17.78" y2="2.032" layer="16"/>
<rectangle x1="18.034" y1="1.778" x2="18.288" y2="2.032" layer="16"/>
<rectangle x1="18.288" y1="1.778" x2="18.542" y2="2.032" layer="16"/>
<rectangle x1="18.542" y1="1.778" x2="18.796" y2="2.032" layer="16"/>
<rectangle x1="18.796" y1="1.778" x2="19.05" y2="2.032" layer="16"/>
<rectangle x1="19.05" y1="1.778" x2="19.304" y2="2.032" layer="16"/>
<rectangle x1="19.304" y1="1.778" x2="19.558" y2="2.032" layer="16"/>
<rectangle x1="19.558" y1="1.778" x2="19.812" y2="2.032" layer="16"/>
<rectangle x1="10.16" y1="2.032" x2="10.414" y2="2.286" layer="16"/>
<rectangle x1="10.414" y1="2.032" x2="10.668" y2="2.286" layer="16"/>
<rectangle x1="11.176" y1="2.032" x2="11.43" y2="2.286" layer="16"/>
<rectangle x1="11.43" y1="2.032" x2="11.684" y2="2.286" layer="16"/>
<rectangle x1="12.7" y1="2.032" x2="12.954" y2="2.286" layer="16"/>
<rectangle x1="12.954" y1="2.032" x2="13.208" y2="2.286" layer="16"/>
<rectangle x1="14.224" y1="2.032" x2="14.478" y2="2.286" layer="16"/>
<rectangle x1="14.478" y1="2.032" x2="14.732" y2="2.286" layer="16"/>
<rectangle x1="15.24" y1="2.032" x2="15.494" y2="2.286" layer="16"/>
<rectangle x1="15.494" y1="2.032" x2="15.748" y2="2.286" layer="16"/>
<rectangle x1="17.272" y1="2.032" x2="17.526" y2="2.286" layer="16"/>
<rectangle x1="17.526" y1="2.032" x2="17.78" y2="2.286" layer="16"/>
<rectangle x1="18.034" y1="2.032" x2="18.288" y2="2.286" layer="16"/>
<rectangle x1="18.288" y1="2.032" x2="18.542" y2="2.286" layer="16"/>
<rectangle x1="18.542" y1="2.032" x2="18.796" y2="2.286" layer="16"/>
<rectangle x1="18.796" y1="2.032" x2="19.05" y2="2.286" layer="16"/>
<rectangle x1="19.05" y1="2.032" x2="19.304" y2="2.286" layer="16"/>
<rectangle x1="19.304" y1="2.032" x2="19.558" y2="2.286" layer="16"/>
<rectangle x1="19.558" y1="2.032" x2="19.812" y2="2.286" layer="16"/>
<rectangle x1="10.414" y1="2.286" x2="10.668" y2="2.54" layer="16"/>
<rectangle x1="10.668" y1="2.286" x2="10.922" y2="2.54" layer="16"/>
<rectangle x1="10.922" y1="2.286" x2="11.176" y2="2.54" layer="16"/>
<rectangle x1="11.176" y1="2.286" x2="11.43" y2="2.54" layer="16"/>
<rectangle x1="12.7" y1="2.286" x2="12.954" y2="2.54" layer="16"/>
<rectangle x1="12.954" y1="2.286" x2="13.208" y2="2.54" layer="16"/>
<rectangle x1="14.478" y1="2.286" x2="14.732" y2="2.54" layer="16"/>
<rectangle x1="14.732" y1="2.286" x2="14.986" y2="2.54" layer="16"/>
<rectangle x1="14.986" y1="2.286" x2="15.24" y2="2.54" layer="16"/>
<rectangle x1="15.24" y1="2.286" x2="15.494" y2="2.54" layer="16"/>
<rectangle x1="17.272" y1="2.286" x2="17.526" y2="2.54" layer="16"/>
<rectangle x1="17.526" y1="2.286" x2="17.78" y2="2.54" layer="16"/>
<rectangle x1="18.034" y1="2.286" x2="18.288" y2="2.54" layer="16"/>
<rectangle x1="18.288" y1="2.286" x2="18.542" y2="2.54" layer="16"/>
<rectangle x1="19.304" y1="2.286" x2="19.558" y2="2.54" layer="16"/>
<rectangle x1="19.558" y1="2.286" x2="19.812" y2="2.54" layer="16"/>
<rectangle x1="10.414" y1="2.54" x2="10.668" y2="2.794" layer="16"/>
<rectangle x1="10.668" y1="2.54" x2="10.922" y2="2.794" layer="16"/>
<rectangle x1="10.922" y1="2.54" x2="11.176" y2="2.794" layer="16"/>
<rectangle x1="11.176" y1="2.54" x2="11.43" y2="2.794" layer="16"/>
<rectangle x1="12.7" y1="2.54" x2="12.954" y2="2.794" layer="16"/>
<rectangle x1="12.954" y1="2.54" x2="13.208" y2="2.794" layer="16"/>
<rectangle x1="14.478" y1="2.54" x2="14.732" y2="2.794" layer="16"/>
<rectangle x1="14.732" y1="2.54" x2="14.986" y2="2.794" layer="16"/>
<rectangle x1="14.986" y1="2.54" x2="15.24" y2="2.794" layer="16"/>
<rectangle x1="15.24" y1="2.54" x2="15.494" y2="2.794" layer="16"/>
<rectangle x1="17.272" y1="2.54" x2="17.526" y2="2.794" layer="16"/>
<rectangle x1="17.526" y1="2.54" x2="17.78" y2="2.794" layer="16"/>
<rectangle x1="18.034" y1="2.54" x2="18.288" y2="2.794" layer="16"/>
<rectangle x1="18.288" y1="2.54" x2="18.542" y2="2.794" layer="16"/>
<rectangle x1="19.304" y1="2.54" x2="19.558" y2="2.794" layer="16"/>
<rectangle x1="19.558" y1="2.54" x2="19.812" y2="2.794" layer="16"/>
<rectangle x1="10.414" y1="2.794" x2="10.668" y2="3.048" layer="16"/>
<rectangle x1="10.668" y1="2.794" x2="10.922" y2="3.048" layer="16"/>
<rectangle x1="10.922" y1="2.794" x2="11.176" y2="3.048" layer="16"/>
<rectangle x1="11.176" y1="2.794" x2="11.43" y2="3.048" layer="16"/>
<rectangle x1="12.7" y1="2.794" x2="12.954" y2="3.048" layer="16"/>
<rectangle x1="12.954" y1="2.794" x2="13.208" y2="3.048" layer="16"/>
<rectangle x1="14.478" y1="2.794" x2="14.732" y2="3.048" layer="16"/>
<rectangle x1="14.732" y1="2.794" x2="14.986" y2="3.048" layer="16"/>
<rectangle x1="14.986" y1="2.794" x2="15.24" y2="3.048" layer="16"/>
<rectangle x1="15.24" y1="2.794" x2="15.494" y2="3.048" layer="16"/>
<rectangle x1="17.272" y1="2.794" x2="17.526" y2="3.048" layer="16"/>
<rectangle x1="17.526" y1="2.794" x2="17.78" y2="3.048" layer="16"/>
<rectangle x1="18.034" y1="2.794" x2="18.288" y2="3.048" layer="16"/>
<rectangle x1="18.288" y1="2.794" x2="18.542" y2="3.048" layer="16"/>
<rectangle x1="19.304" y1="2.794" x2="19.558" y2="3.048" layer="16"/>
<rectangle x1="19.558" y1="2.794" x2="19.812" y2="3.048" layer="16"/>
<rectangle x1="10.668" y1="3.048" x2="10.922" y2="3.302" layer="16"/>
<rectangle x1="10.922" y1="3.048" x2="11.176" y2="3.302" layer="16"/>
<rectangle x1="11.938" y1="3.048" x2="12.192" y2="3.302" layer="16"/>
<rectangle x1="12.192" y1="3.048" x2="12.446" y2="3.302" layer="16"/>
<rectangle x1="12.446" y1="3.048" x2="12.7" y2="3.302" layer="16"/>
<rectangle x1="12.7" y1="3.048" x2="12.954" y2="3.302" layer="16"/>
<rectangle x1="12.954" y1="3.048" x2="13.208" y2="3.302" layer="16"/>
<rectangle x1="13.208" y1="3.048" x2="13.462" y2="3.302" layer="16"/>
<rectangle x1="13.462" y1="3.048" x2="13.716" y2="3.302" layer="16"/>
<rectangle x1="13.716" y1="3.048" x2="13.97" y2="3.302" layer="16"/>
<rectangle x1="14.732" y1="3.048" x2="14.986" y2="3.302" layer="16"/>
<rectangle x1="14.986" y1="3.048" x2="15.24" y2="3.302" layer="16"/>
<rectangle x1="17.272" y1="3.048" x2="17.526" y2="3.302" layer="16"/>
<rectangle x1="17.526" y1="3.048" x2="17.78" y2="3.302" layer="16"/>
<rectangle x1="18.034" y1="3.048" x2="18.288" y2="3.302" layer="16"/>
<rectangle x1="18.288" y1="3.048" x2="18.542" y2="3.302" layer="16"/>
<rectangle x1="18.542" y1="3.048" x2="18.796" y2="3.302" layer="16"/>
<rectangle x1="18.796" y1="3.048" x2="19.05" y2="3.302" layer="16"/>
<rectangle x1="19.05" y1="3.048" x2="19.304" y2="3.302" layer="16"/>
<rectangle x1="19.304" y1="3.048" x2="19.558" y2="3.302" layer="16"/>
<rectangle x1="19.558" y1="3.048" x2="19.812" y2="3.302" layer="16"/>
<rectangle x1="10.668" y1="3.302" x2="10.922" y2="3.556" layer="16"/>
<rectangle x1="10.922" y1="3.302" x2="11.176" y2="3.556" layer="16"/>
<rectangle x1="11.938" y1="3.302" x2="12.192" y2="3.556" layer="16"/>
<rectangle x1="12.192" y1="3.302" x2="12.446" y2="3.556" layer="16"/>
<rectangle x1="12.446" y1="3.302" x2="12.7" y2="3.556" layer="16"/>
<rectangle x1="12.7" y1="3.302" x2="12.954" y2="3.556" layer="16"/>
<rectangle x1="12.954" y1="3.302" x2="13.208" y2="3.556" layer="16"/>
<rectangle x1="13.208" y1="3.302" x2="13.462" y2="3.556" layer="16"/>
<rectangle x1="13.462" y1="3.302" x2="13.716" y2="3.556" layer="16"/>
<rectangle x1="13.716" y1="3.302" x2="13.97" y2="3.556" layer="16"/>
<rectangle x1="14.732" y1="3.302" x2="14.986" y2="3.556" layer="16"/>
<rectangle x1="14.986" y1="3.302" x2="15.24" y2="3.556" layer="16"/>
<rectangle x1="17.272" y1="3.302" x2="17.526" y2="3.556" layer="16"/>
<rectangle x1="17.526" y1="3.302" x2="17.78" y2="3.556" layer="16"/>
<rectangle x1="18.034" y1="3.302" x2="18.288" y2="3.556" layer="16"/>
<rectangle x1="18.288" y1="3.302" x2="18.542" y2="3.556" layer="16"/>
<rectangle x1="18.542" y1="3.302" x2="18.796" y2="3.556" layer="16"/>
<rectangle x1="18.796" y1="3.302" x2="19.05" y2="3.556" layer="16"/>
<rectangle x1="19.05" y1="3.302" x2="19.304" y2="3.556" layer="16"/>
<rectangle x1="19.304" y1="3.302" x2="19.558" y2="3.556" layer="16"/>
<rectangle x1="19.558" y1="3.302" x2="19.812" y2="3.556" layer="16"/>
<rectangle x1="12.7" y1="4.318" x2="12.954" y2="4.572" layer="16"/>
<rectangle x1="12.954" y1="4.318" x2="13.208" y2="4.572" layer="16"/>
<rectangle x1="13.208" y1="4.318" x2="13.462" y2="4.572" layer="16"/>
<rectangle x1="13.462" y1="4.318" x2="13.716" y2="4.572" layer="16"/>
<rectangle x1="13.716" y1="4.318" x2="13.97" y2="4.572" layer="16"/>
<rectangle x1="12.192" y1="4.572" x2="12.446" y2="4.826" layer="16"/>
<rectangle x1="12.446" y1="4.572" x2="12.7" y2="4.826" layer="16"/>
<rectangle x1="12.7" y1="4.572" x2="12.954" y2="4.826" layer="16"/>
<rectangle x1="13.462" y1="4.572" x2="13.716" y2="4.826" layer="16"/>
<rectangle x1="13.716" y1="4.572" x2="13.97" y2="4.826" layer="16"/>
<rectangle x1="13.97" y1="4.572" x2="14.224" y2="4.826" layer="16"/>
<rectangle x1="14.224" y1="4.572" x2="14.478" y2="4.826" layer="16"/>
<rectangle x1="14.478" y1="4.572" x2="14.732" y2="4.826" layer="16"/>
<rectangle x1="11.684" y1="4.826" x2="11.938" y2="5.08" layer="16"/>
<rectangle x1="11.938" y1="4.826" x2="12.192" y2="5.08" layer="16"/>
<rectangle x1="12.192" y1="4.826" x2="12.446" y2="5.08" layer="16"/>
<rectangle x1="14.478" y1="4.826" x2="14.732" y2="5.08" layer="16"/>
<rectangle x1="14.732" y1="4.826" x2="14.986" y2="5.08" layer="16"/>
<rectangle x1="14.986" y1="4.826" x2="15.24" y2="5.08" layer="16"/>
<rectangle x1="11.43" y1="5.08" x2="11.684" y2="5.334" layer="16"/>
<rectangle x1="11.684" y1="5.08" x2="11.938" y2="5.334" layer="16"/>
<rectangle x1="11.938" y1="5.08" x2="12.192" y2="5.334" layer="16"/>
<rectangle x1="14.986" y1="5.08" x2="15.24" y2="5.334" layer="16"/>
<rectangle x1="15.24" y1="5.08" x2="15.494" y2="5.334" layer="16"/>
<rectangle x1="11.176" y1="5.334" x2="11.43" y2="5.588" layer="16"/>
<rectangle x1="11.43" y1="5.334" x2="11.684" y2="5.588" layer="16"/>
<rectangle x1="15.24" y1="5.334" x2="15.494" y2="5.588" layer="16"/>
<rectangle x1="15.494" y1="5.334" x2="15.748" y2="5.588" layer="16"/>
<rectangle x1="10.922" y1="5.588" x2="11.176" y2="5.842" layer="16"/>
<rectangle x1="11.176" y1="5.588" x2="11.43" y2="5.842" layer="16"/>
<rectangle x1="15.494" y1="5.588" x2="15.748" y2="5.842" layer="16"/>
<rectangle x1="15.748" y1="5.588" x2="16.002" y2="5.842" layer="16"/>
<rectangle x1="10.668" y1="5.842" x2="10.922" y2="6.096" layer="16"/>
<rectangle x1="10.922" y1="5.842" x2="11.176" y2="6.096" layer="16"/>
<rectangle x1="15.748" y1="5.842" x2="16.002" y2="6.096" layer="16"/>
<rectangle x1="10.668" y1="6.096" x2="10.922" y2="6.35" layer="16"/>
<rectangle x1="10.922" y1="6.096" x2="11.176" y2="6.35" layer="16"/>
<rectangle x1="15.748" y1="6.096" x2="16.002" y2="6.35" layer="16"/>
<rectangle x1="16.002" y1="6.096" x2="16.256" y2="6.35" layer="16"/>
<rectangle x1="10.414" y1="6.35" x2="10.668" y2="6.604" layer="16"/>
<rectangle x1="10.668" y1="6.35" x2="10.922" y2="6.604" layer="16"/>
<rectangle x1="16.002" y1="6.35" x2="16.256" y2="6.604" layer="16"/>
<rectangle x1="10.16" y1="6.604" x2="10.414" y2="6.858" layer="16"/>
<rectangle x1="10.414" y1="6.604" x2="10.668" y2="6.858" layer="16"/>
<rectangle x1="10.668" y1="6.604" x2="10.922" y2="6.858" layer="16"/>
<rectangle x1="16.002" y1="6.604" x2="16.256" y2="6.858" layer="16"/>
<rectangle x1="10.16" y1="6.858" x2="10.414" y2="7.112" layer="16"/>
<rectangle x1="16.002" y1="6.858" x2="16.256" y2="7.112" layer="16"/>
<rectangle x1="16.256" y1="6.858" x2="16.51" y2="7.112" layer="16"/>
<rectangle x1="10.16" y1="7.112" x2="10.414" y2="7.366" layer="16"/>
<rectangle x1="12.446" y1="7.112" x2="12.7" y2="7.366" layer="16"/>
<rectangle x1="12.7" y1="7.112" x2="12.954" y2="7.366" layer="16"/>
<rectangle x1="12.954" y1="7.112" x2="13.208" y2="7.366" layer="16"/>
<rectangle x1="13.208" y1="7.112" x2="13.462" y2="7.366" layer="16"/>
<rectangle x1="13.462" y1="7.112" x2="13.716" y2="7.366" layer="16"/>
<rectangle x1="13.716" y1="7.112" x2="13.97" y2="7.366" layer="16"/>
<rectangle x1="13.97" y1="7.112" x2="14.224" y2="7.366" layer="16"/>
<rectangle x1="14.224" y1="7.112" x2="14.478" y2="7.366" layer="16"/>
<rectangle x1="16.256" y1="7.112" x2="16.51" y2="7.366" layer="16"/>
<rectangle x1="9.906" y1="7.366" x2="10.16" y2="7.62" layer="16"/>
<rectangle x1="10.16" y1="7.366" x2="10.414" y2="7.62" layer="16"/>
<rectangle x1="10.414" y1="7.366" x2="10.668" y2="7.62" layer="16"/>
<rectangle x1="16.256" y1="7.366" x2="16.51" y2="7.62" layer="16"/>
<rectangle x1="9.906" y1="7.62" x2="10.16" y2="7.874" layer="16"/>
<rectangle x1="10.16" y1="7.62" x2="10.414" y2="7.874" layer="16"/>
<rectangle x1="10.414" y1="7.62" x2="10.668" y2="7.874" layer="16"/>
<rectangle x1="16.256" y1="7.62" x2="16.51" y2="7.874" layer="16"/>
<rectangle x1="16.51" y1="7.62" x2="16.764" y2="7.874" layer="16"/>
<rectangle x1="9.652" y1="7.874" x2="9.906" y2="8.128" layer="16"/>
<rectangle x1="9.906" y1="7.874" x2="10.16" y2="8.128" layer="16"/>
<rectangle x1="10.16" y1="7.874" x2="10.414" y2="8.128" layer="16"/>
<rectangle x1="10.414" y1="7.874" x2="10.668" y2="8.128" layer="16"/>
<rectangle x1="16.256" y1="7.874" x2="16.51" y2="8.128" layer="16"/>
<rectangle x1="16.51" y1="7.874" x2="16.764" y2="8.128" layer="16"/>
<rectangle x1="16.764" y1="7.874" x2="17.018" y2="8.128" layer="16"/>
<rectangle x1="9.398" y1="8.128" x2="9.652" y2="8.382" layer="16"/>
<rectangle x1="9.906" y1="8.128" x2="10.16" y2="8.382" layer="16"/>
<rectangle x1="10.16" y1="8.128" x2="10.414" y2="8.382" layer="16"/>
<rectangle x1="10.414" y1="8.128" x2="10.668" y2="8.382" layer="16"/>
<rectangle x1="13.208" y1="8.128" x2="13.462" y2="8.382" layer="16"/>
<rectangle x1="13.462" y1="8.128" x2="13.716" y2="8.382" layer="16"/>
<rectangle x1="13.716" y1="8.128" x2="13.97" y2="8.382" layer="16"/>
<rectangle x1="13.97" y1="8.128" x2="14.224" y2="8.382" layer="16"/>
<rectangle x1="16.256" y1="8.128" x2="16.51" y2="8.382" layer="16"/>
<rectangle x1="16.51" y1="8.128" x2="16.764" y2="8.382" layer="16"/>
<rectangle x1="9.144" y1="8.382" x2="9.398" y2="8.636" layer="16"/>
<rectangle x1="9.906" y1="8.382" x2="10.16" y2="8.636" layer="16"/>
<rectangle x1="10.16" y1="8.382" x2="10.414" y2="8.636" layer="16"/>
<rectangle x1="10.414" y1="8.382" x2="10.668" y2="8.636" layer="16"/>
<rectangle x1="12.7" y1="8.382" x2="12.954" y2="8.636" layer="16"/>
<rectangle x1="12.954" y1="8.382" x2="13.208" y2="8.636" layer="16"/>
<rectangle x1="13.208" y1="8.382" x2="13.462" y2="8.636" layer="16"/>
<rectangle x1="13.462" y1="8.382" x2="13.716" y2="8.636" layer="16"/>
<rectangle x1="13.716" y1="8.382" x2="13.97" y2="8.636" layer="16"/>
<rectangle x1="13.97" y1="8.382" x2="14.224" y2="8.636" layer="16"/>
<rectangle x1="14.224" y1="8.382" x2="14.478" y2="8.636" layer="16"/>
<rectangle x1="16.256" y1="8.382" x2="16.51" y2="8.636" layer="16"/>
<rectangle x1="16.51" y1="8.382" x2="16.764" y2="8.636" layer="16"/>
<rectangle x1="17.018" y1="8.382" x2="17.272" y2="8.636" layer="16"/>
<rectangle x1="8.89" y1="8.636" x2="9.144" y2="8.89" layer="16"/>
<rectangle x1="9.652" y1="8.636" x2="9.906" y2="8.89" layer="16"/>
<rectangle x1="9.906" y1="8.636" x2="10.16" y2="8.89" layer="16"/>
<rectangle x1="10.16" y1="8.636" x2="10.414" y2="8.89" layer="16"/>
<rectangle x1="10.414" y1="8.636" x2="10.668" y2="8.89" layer="16"/>
<rectangle x1="12.954" y1="8.636" x2="13.208" y2="8.89" layer="16"/>
<rectangle x1="13.208" y1="8.636" x2="13.462" y2="8.89" layer="16"/>
<rectangle x1="14.224" y1="8.636" x2="14.478" y2="8.89" layer="16"/>
<rectangle x1="16.256" y1="8.636" x2="16.51" y2="8.89" layer="16"/>
<rectangle x1="16.51" y1="8.636" x2="16.764" y2="8.89" layer="16"/>
<rectangle x1="8.89" y1="8.89" x2="9.144" y2="9.144" layer="16"/>
<rectangle x1="9.652" y1="8.89" x2="9.906" y2="9.144" layer="16"/>
<rectangle x1="9.906" y1="8.89" x2="10.16" y2="9.144" layer="16"/>
<rectangle x1="10.16" y1="8.89" x2="10.414" y2="9.144" layer="16"/>
<rectangle x1="10.414" y1="8.89" x2="10.668" y2="9.144" layer="16"/>
<rectangle x1="12.7" y1="8.89" x2="12.954" y2="9.144" layer="16"/>
<rectangle x1="12.954" y1="8.89" x2="13.208" y2="9.144" layer="16"/>
<rectangle x1="13.208" y1="8.89" x2="13.462" y2="9.144" layer="16"/>
<rectangle x1="14.224" y1="8.89" x2="14.478" y2="9.144" layer="16"/>
<rectangle x1="16.51" y1="8.89" x2="16.764" y2="9.144" layer="16"/>
<rectangle x1="8.89" y1="9.144" x2="9.144" y2="9.398" layer="16"/>
<rectangle x1="9.652" y1="9.144" x2="9.906" y2="9.398" layer="16"/>
<rectangle x1="9.906" y1="9.144" x2="10.16" y2="9.398" layer="16"/>
<rectangle x1="10.16" y1="9.144" x2="10.414" y2="9.398" layer="16"/>
<rectangle x1="10.414" y1="9.144" x2="10.668" y2="9.398" layer="16"/>
<rectangle x1="12.954" y1="9.144" x2="13.208" y2="9.398" layer="16"/>
<rectangle x1="13.208" y1="9.144" x2="13.462" y2="9.398" layer="16"/>
<rectangle x1="14.224" y1="9.144" x2="14.478" y2="9.398" layer="16"/>
<rectangle x1="16.51" y1="9.144" x2="16.764" y2="9.398" layer="16"/>
<rectangle x1="17.272" y1="9.144" x2="17.526" y2="9.398" layer="16"/>
<rectangle x1="9.652" y1="9.398" x2="9.906" y2="9.652" layer="16"/>
<rectangle x1="9.906" y1="9.398" x2="10.16" y2="9.652" layer="16"/>
<rectangle x1="10.16" y1="9.398" x2="10.414" y2="9.652" layer="16"/>
<rectangle x1="12.954" y1="9.398" x2="13.208" y2="9.652" layer="16"/>
<rectangle x1="13.208" y1="9.398" x2="13.462" y2="9.652" layer="16"/>
<rectangle x1="14.224" y1="9.398" x2="14.478" y2="9.652" layer="16"/>
<rectangle x1="16.51" y1="9.398" x2="16.764" y2="9.652" layer="16"/>
<rectangle x1="16.764" y1="9.398" x2="17.018" y2="9.652" layer="16"/>
<rectangle x1="8.89" y1="9.652" x2="9.144" y2="9.906" layer="16"/>
<rectangle x1="9.652" y1="9.652" x2="9.906" y2="9.906" layer="16"/>
<rectangle x1="9.906" y1="9.652" x2="10.16" y2="9.906" layer="16"/>
<rectangle x1="10.16" y1="9.652" x2="10.414" y2="9.906" layer="16"/>
<rectangle x1="12.954" y1="9.652" x2="13.208" y2="9.906" layer="16"/>
<rectangle x1="13.208" y1="9.652" x2="13.462" y2="9.906" layer="16"/>
<rectangle x1="13.97" y1="9.652" x2="14.224" y2="9.906" layer="16"/>
<rectangle x1="16.51" y1="9.652" x2="16.764" y2="9.906" layer="16"/>
<rectangle x1="16.764" y1="9.652" x2="17.018" y2="9.906" layer="16"/>
<rectangle x1="17.272" y1="9.652" x2="17.526" y2="9.906" layer="16"/>
<rectangle x1="8.89" y1="9.906" x2="9.144" y2="10.16" layer="16"/>
<rectangle x1="9.652" y1="9.906" x2="9.906" y2="10.16" layer="16"/>
<rectangle x1="9.906" y1="9.906" x2="10.16" y2="10.16" layer="16"/>
<rectangle x1="10.16" y1="9.906" x2="10.414" y2="10.16" layer="16"/>
<rectangle x1="13.208" y1="9.906" x2="13.462" y2="10.16" layer="16"/>
<rectangle x1="15.748" y1="9.906" x2="16.002" y2="10.16" layer="16"/>
<rectangle x1="16.002" y1="9.906" x2="16.256" y2="10.16" layer="16"/>
<rectangle x1="16.51" y1="9.906" x2="16.764" y2="10.16" layer="16"/>
<rectangle x1="16.764" y1="9.906" x2="17.018" y2="10.16" layer="16"/>
<rectangle x1="17.272" y1="9.906" x2="17.526" y2="10.16" layer="16"/>
<rectangle x1="8.89" y1="10.16" x2="9.144" y2="10.414" layer="16"/>
<rectangle x1="9.652" y1="10.16" x2="9.906" y2="10.414" layer="16"/>
<rectangle x1="9.906" y1="10.16" x2="10.16" y2="10.414" layer="16"/>
<rectangle x1="10.16" y1="10.16" x2="10.414" y2="10.414" layer="16"/>
<rectangle x1="11.176" y1="10.16" x2="11.43" y2="10.414" layer="16"/>
<rectangle x1="15.24" y1="10.16" x2="15.494" y2="10.414" layer="16"/>
<rectangle x1="15.748" y1="10.16" x2="16.002" y2="10.414" layer="16"/>
<rectangle x1="16.002" y1="10.16" x2="16.256" y2="10.414" layer="16"/>
<rectangle x1="16.256" y1="10.16" x2="16.51" y2="10.414" layer="16"/>
<rectangle x1="16.764" y1="10.16" x2="17.018" y2="10.414" layer="16"/>
<rectangle x1="17.272" y1="10.16" x2="17.526" y2="10.414" layer="16"/>
<rectangle x1="8.89" y1="10.414" x2="9.144" y2="10.668" layer="16"/>
<rectangle x1="9.652" y1="10.414" x2="9.906" y2="10.668" layer="16"/>
<rectangle x1="9.906" y1="10.414" x2="10.16" y2="10.668" layer="16"/>
<rectangle x1="10.16" y1="10.414" x2="10.414" y2="10.668" layer="16"/>
<rectangle x1="10.922" y1="10.414" x2="11.176" y2="10.668" layer="16"/>
<rectangle x1="11.176" y1="10.414" x2="11.43" y2="10.668" layer="16"/>
<rectangle x1="11.43" y1="10.414" x2="11.684" y2="10.668" layer="16"/>
<rectangle x1="11.684" y1="10.414" x2="11.938" y2="10.668" layer="16"/>
<rectangle x1="13.208" y1="10.414" x2="13.462" y2="10.668" layer="16"/>
<rectangle x1="14.986" y1="10.414" x2="15.24" y2="10.668" layer="16"/>
<rectangle x1="15.24" y1="10.414" x2="15.494" y2="10.668" layer="16"/>
<rectangle x1="15.494" y1="10.414" x2="15.748" y2="10.668" layer="16"/>
<rectangle x1="15.748" y1="10.414" x2="16.002" y2="10.668" layer="16"/>
<rectangle x1="16.002" y1="10.414" x2="16.256" y2="10.668" layer="16"/>
<rectangle x1="16.256" y1="10.414" x2="16.51" y2="10.668" layer="16"/>
<rectangle x1="16.51" y1="10.414" x2="16.764" y2="10.668" layer="16"/>
<rectangle x1="16.764" y1="10.414" x2="17.018" y2="10.668" layer="16"/>
<rectangle x1="17.018" y1="10.414" x2="17.272" y2="10.668" layer="16"/>
<rectangle x1="8.89" y1="10.668" x2="9.144" y2="10.922" layer="16"/>
<rectangle x1="9.144" y1="10.668" x2="9.398" y2="10.922" layer="16"/>
<rectangle x1="9.398" y1="10.668" x2="9.652" y2="10.922" layer="16"/>
<rectangle x1="9.652" y1="10.668" x2="9.906" y2="10.922" layer="16"/>
<rectangle x1="9.906" y1="10.668" x2="10.16" y2="10.922" layer="16"/>
<rectangle x1="10.16" y1="10.668" x2="10.414" y2="10.922" layer="16"/>
<rectangle x1="10.414" y1="10.668" x2="10.668" y2="10.922" layer="16"/>
<rectangle x1="10.922" y1="10.668" x2="11.176" y2="10.922" layer="16"/>
<rectangle x1="11.176" y1="10.668" x2="11.43" y2="10.922" layer="16"/>
<rectangle x1="11.43" y1="10.668" x2="11.684" y2="10.922" layer="16"/>
<rectangle x1="11.684" y1="10.668" x2="11.938" y2="10.922" layer="16"/>
<rectangle x1="11.938" y1="10.668" x2="12.192" y2="10.922" layer="16"/>
<rectangle x1="13.208" y1="10.668" x2="13.462" y2="10.922" layer="16"/>
<rectangle x1="14.224" y1="10.668" x2="14.478" y2="10.922" layer="16"/>
<rectangle x1="14.478" y1="10.668" x2="14.732" y2="10.922" layer="16"/>
<rectangle x1="14.986" y1="10.668" x2="15.24" y2="10.922" layer="16"/>
<rectangle x1="15.24" y1="10.668" x2="15.494" y2="10.922" layer="16"/>
<rectangle x1="15.494" y1="10.668" x2="15.748" y2="10.922" layer="16"/>
<rectangle x1="15.748" y1="10.668" x2="16.002" y2="10.922" layer="16"/>
<rectangle x1="16.002" y1="10.668" x2="16.256" y2="10.922" layer="16"/>
<rectangle x1="16.256" y1="10.668" x2="16.51" y2="10.922" layer="16"/>
<rectangle x1="16.51" y1="10.668" x2="16.764" y2="10.922" layer="16"/>
<rectangle x1="16.764" y1="10.668" x2="17.018" y2="10.922" layer="16"/>
<rectangle x1="9.144" y1="10.922" x2="9.398" y2="11.176" layer="16"/>
<rectangle x1="9.398" y1="10.922" x2="9.652" y2="11.176" layer="16"/>
<rectangle x1="9.906" y1="10.922" x2="10.16" y2="11.176" layer="16"/>
<rectangle x1="10.16" y1="10.922" x2="10.414" y2="11.176" layer="16"/>
<rectangle x1="10.414" y1="10.922" x2="10.668" y2="11.176" layer="16"/>
<rectangle x1="10.922" y1="10.922" x2="11.176" y2="11.176" layer="16"/>
<rectangle x1="11.176" y1="10.922" x2="11.43" y2="11.176" layer="16"/>
<rectangle x1="11.43" y1="10.922" x2="11.684" y2="11.176" layer="16"/>
<rectangle x1="11.684" y1="10.922" x2="11.938" y2="11.176" layer="16"/>
<rectangle x1="11.938" y1="10.922" x2="12.192" y2="11.176" layer="16"/>
<rectangle x1="12.7" y1="10.922" x2="12.954" y2="11.176" layer="16"/>
<rectangle x1="12.954" y1="10.922" x2="13.208" y2="11.176" layer="16"/>
<rectangle x1="14.224" y1="10.922" x2="14.478" y2="11.176" layer="16"/>
<rectangle x1="14.478" y1="10.922" x2="14.732" y2="11.176" layer="16"/>
<rectangle x1="14.732" y1="10.922" x2="14.986" y2="11.176" layer="16"/>
<rectangle x1="14.986" y1="10.922" x2="15.24" y2="11.176" layer="16"/>
<rectangle x1="15.24" y1="10.922" x2="15.494" y2="11.176" layer="16"/>
<rectangle x1="16.002" y1="10.922" x2="16.256" y2="11.176" layer="16"/>
<rectangle x1="16.256" y1="10.922" x2="16.51" y2="11.176" layer="16"/>
<rectangle x1="16.51" y1="10.922" x2="16.764" y2="11.176" layer="16"/>
<rectangle x1="16.764" y1="10.922" x2="17.018" y2="11.176" layer="16"/>
<rectangle x1="9.398" y1="11.176" x2="9.652" y2="11.43" layer="16"/>
<rectangle x1="9.652" y1="11.176" x2="9.906" y2="11.43" layer="16"/>
<rectangle x1="9.906" y1="11.176" x2="10.16" y2="11.43" layer="16"/>
<rectangle x1="10.16" y1="11.176" x2="10.414" y2="11.43" layer="16"/>
<rectangle x1="10.414" y1="11.176" x2="10.668" y2="11.43" layer="16"/>
<rectangle x1="10.922" y1="11.176" x2="11.176" y2="11.43" layer="16"/>
<rectangle x1="11.938" y1="11.176" x2="12.192" y2="11.43" layer="16"/>
<rectangle x1="12.192" y1="11.176" x2="12.446" y2="11.43" layer="16"/>
<rectangle x1="12.446" y1="11.176" x2="12.7" y2="11.43" layer="16"/>
<rectangle x1="12.7" y1="11.176" x2="12.954" y2="11.43" layer="16"/>
<rectangle x1="14.986" y1="11.176" x2="15.24" y2="11.43" layer="16"/>
<rectangle x1="15.24" y1="11.176" x2="15.494" y2="11.43" layer="16"/>
<rectangle x1="15.494" y1="11.176" x2="15.748" y2="11.43" layer="16"/>
<rectangle x1="16.002" y1="11.176" x2="16.256" y2="11.43" layer="16"/>
<rectangle x1="16.256" y1="11.176" x2="16.51" y2="11.43" layer="16"/>
<rectangle x1="16.51" y1="11.176" x2="16.764" y2="11.43" layer="16"/>
<rectangle x1="16.764" y1="11.176" x2="17.018" y2="11.43" layer="16"/>
<rectangle x1="9.398" y1="11.43" x2="9.652" y2="11.684" layer="16"/>
<rectangle x1="9.652" y1="11.43" x2="9.906" y2="11.684" layer="16"/>
<rectangle x1="9.906" y1="11.43" x2="10.16" y2="11.684" layer="16"/>
<rectangle x1="10.16" y1="11.43" x2="10.414" y2="11.684" layer="16"/>
<rectangle x1="10.414" y1="11.43" x2="10.668" y2="11.684" layer="16"/>
<rectangle x1="16.256" y1="11.43" x2="16.51" y2="11.684" layer="16"/>
<rectangle x1="16.51" y1="11.43" x2="16.764" y2="11.684" layer="16"/>
<rectangle x1="16.764" y1="11.43" x2="17.018" y2="11.684" layer="16"/>
<rectangle x1="9.398" y1="11.684" x2="9.652" y2="11.938" layer="16"/>
<rectangle x1="9.652" y1="11.684" x2="9.906" y2="11.938" layer="16"/>
<rectangle x1="9.906" y1="11.684" x2="10.16" y2="11.938" layer="16"/>
<rectangle x1="10.16" y1="11.684" x2="10.414" y2="11.938" layer="16"/>
<rectangle x1="10.414" y1="11.684" x2="10.668" y2="11.938" layer="16"/>
<rectangle x1="16.51" y1="11.684" x2="16.764" y2="11.938" layer="16"/>
<rectangle x1="16.764" y1="11.684" x2="17.018" y2="11.938" layer="16"/>
<rectangle x1="9.398" y1="11.938" x2="9.652" y2="12.192" layer="16"/>
<rectangle x1="9.652" y1="11.938" x2="9.906" y2="12.192" layer="16"/>
<rectangle x1="9.906" y1="11.938" x2="10.16" y2="12.192" layer="16"/>
<rectangle x1="10.16" y1="11.938" x2="10.414" y2="12.192" layer="16"/>
<rectangle x1="10.414" y1="11.938" x2="10.668" y2="12.192" layer="16"/>
<rectangle x1="10.668" y1="11.938" x2="10.922" y2="12.192" layer="16"/>
<rectangle x1="16.51" y1="11.938" x2="16.764" y2="12.192" layer="16"/>
<rectangle x1="16.764" y1="11.938" x2="17.018" y2="12.192" layer="16"/>
<rectangle x1="9.398" y1="12.192" x2="9.652" y2="12.446" layer="16"/>
<rectangle x1="9.652" y1="12.192" x2="9.906" y2="12.446" layer="16"/>
<rectangle x1="9.906" y1="12.192" x2="10.16" y2="12.446" layer="16"/>
<rectangle x1="10.16" y1="12.192" x2="10.414" y2="12.446" layer="16"/>
<rectangle x1="10.414" y1="12.192" x2="10.668" y2="12.446" layer="16"/>
<rectangle x1="10.668" y1="12.192" x2="10.922" y2="12.446" layer="16"/>
<rectangle x1="16.51" y1="12.192" x2="16.764" y2="12.446" layer="16"/>
<rectangle x1="16.764" y1="12.192" x2="17.018" y2="12.446" layer="16"/>
<rectangle x1="9.398" y1="12.446" x2="9.652" y2="12.7" layer="16"/>
<rectangle x1="9.652" y1="12.446" x2="9.906" y2="12.7" layer="16"/>
<rectangle x1="9.906" y1="12.446" x2="10.16" y2="12.7" layer="16"/>
<rectangle x1="10.16" y1="12.446" x2="10.414" y2="12.7" layer="16"/>
<rectangle x1="10.414" y1="12.446" x2="10.668" y2="12.7" layer="16"/>
<rectangle x1="10.668" y1="12.446" x2="10.922" y2="12.7" layer="16"/>
<rectangle x1="16.51" y1="12.446" x2="16.764" y2="12.7" layer="16"/>
<rectangle x1="16.764" y1="12.446" x2="17.018" y2="12.7" layer="16"/>
<rectangle x1="17.018" y1="12.446" x2="17.272" y2="12.7" layer="16"/>
<rectangle x1="9.398" y1="12.7" x2="9.652" y2="12.954" layer="16"/>
<rectangle x1="9.652" y1="12.7" x2="9.906" y2="12.954" layer="16"/>
<rectangle x1="9.906" y1="12.7" x2="10.16" y2="12.954" layer="16"/>
<rectangle x1="10.16" y1="12.7" x2="10.414" y2="12.954" layer="16"/>
<rectangle x1="10.414" y1="12.7" x2="10.668" y2="12.954" layer="16"/>
<rectangle x1="10.668" y1="12.7" x2="10.922" y2="12.954" layer="16"/>
<rectangle x1="16.51" y1="12.7" x2="16.764" y2="12.954" layer="16"/>
<rectangle x1="16.764" y1="12.7" x2="17.018" y2="12.954" layer="16"/>
<rectangle x1="17.018" y1="12.7" x2="17.272" y2="12.954" layer="16"/>
<rectangle x1="9.398" y1="12.954" x2="9.652" y2="13.208" layer="16"/>
<rectangle x1="9.652" y1="12.954" x2="9.906" y2="13.208" layer="16"/>
<rectangle x1="9.906" y1="12.954" x2="10.16" y2="13.208" layer="16"/>
<rectangle x1="10.16" y1="12.954" x2="10.414" y2="13.208" layer="16"/>
<rectangle x1="10.414" y1="12.954" x2="10.668" y2="13.208" layer="16"/>
<rectangle x1="10.668" y1="12.954" x2="10.922" y2="13.208" layer="16"/>
<rectangle x1="16.256" y1="12.954" x2="16.51" y2="13.208" layer="16"/>
<rectangle x1="16.51" y1="12.954" x2="16.764" y2="13.208" layer="16"/>
<rectangle x1="16.764" y1="12.954" x2="17.018" y2="13.208" layer="16"/>
<rectangle x1="17.018" y1="12.954" x2="17.272" y2="13.208" layer="16"/>
<rectangle x1="9.398" y1="13.208" x2="9.652" y2="13.462" layer="16"/>
<rectangle x1="9.652" y1="13.208" x2="9.906" y2="13.462" layer="16"/>
<rectangle x1="9.906" y1="13.208" x2="10.16" y2="13.462" layer="16"/>
<rectangle x1="10.16" y1="13.208" x2="10.414" y2="13.462" layer="16"/>
<rectangle x1="10.414" y1="13.208" x2="10.668" y2="13.462" layer="16"/>
<rectangle x1="10.668" y1="13.208" x2="10.922" y2="13.462" layer="16"/>
<rectangle x1="16.256" y1="13.208" x2="16.51" y2="13.462" layer="16"/>
<rectangle x1="16.51" y1="13.208" x2="16.764" y2="13.462" layer="16"/>
<rectangle x1="16.764" y1="13.208" x2="17.018" y2="13.462" layer="16"/>
<rectangle x1="17.018" y1="13.208" x2="17.272" y2="13.462" layer="16"/>
<rectangle x1="9.398" y1="13.462" x2="9.652" y2="13.716" layer="16"/>
<rectangle x1="9.652" y1="13.462" x2="9.906" y2="13.716" layer="16"/>
<rectangle x1="9.906" y1="13.462" x2="10.16" y2="13.716" layer="16"/>
<rectangle x1="10.16" y1="13.462" x2="10.414" y2="13.716" layer="16"/>
<rectangle x1="10.414" y1="13.462" x2="10.668" y2="13.716" layer="16"/>
<rectangle x1="10.668" y1="13.462" x2="10.922" y2="13.716" layer="16"/>
<rectangle x1="10.922" y1="13.462" x2="11.176" y2="13.716" layer="16"/>
<rectangle x1="16.256" y1="13.462" x2="16.51" y2="13.716" layer="16"/>
<rectangle x1="16.51" y1="13.462" x2="16.764" y2="13.716" layer="16"/>
<rectangle x1="16.764" y1="13.462" x2="17.018" y2="13.716" layer="16"/>
<rectangle x1="17.018" y1="13.462" x2="17.272" y2="13.716" layer="16"/>
<rectangle x1="9.652" y1="13.716" x2="9.906" y2="13.97" layer="16"/>
<rectangle x1="9.906" y1="13.716" x2="10.16" y2="13.97" layer="16"/>
<rectangle x1="10.16" y1="13.716" x2="10.414" y2="13.97" layer="16"/>
<rectangle x1="10.414" y1="13.716" x2="10.668" y2="13.97" layer="16"/>
<rectangle x1="10.668" y1="13.716" x2="10.922" y2="13.97" layer="16"/>
<rectangle x1="10.922" y1="13.716" x2="11.176" y2="13.97" layer="16"/>
<rectangle x1="15.748" y1="13.716" x2="16.002" y2="13.97" layer="16"/>
<rectangle x1="16.002" y1="13.716" x2="16.256" y2="13.97" layer="16"/>
<rectangle x1="16.256" y1="13.716" x2="16.51" y2="13.97" layer="16"/>
<rectangle x1="16.51" y1="13.716" x2="16.764" y2="13.97" layer="16"/>
<rectangle x1="16.764" y1="13.716" x2="17.018" y2="13.97" layer="16"/>
<rectangle x1="17.018" y1="13.716" x2="17.272" y2="13.97" layer="16"/>
<rectangle x1="9.652" y1="13.97" x2="9.906" y2="14.224" layer="16"/>
<rectangle x1="9.906" y1="13.97" x2="10.16" y2="14.224" layer="16"/>
<rectangle x1="10.16" y1="13.97" x2="10.414" y2="14.224" layer="16"/>
<rectangle x1="10.414" y1="13.97" x2="10.668" y2="14.224" layer="16"/>
<rectangle x1="10.668" y1="13.97" x2="10.922" y2="14.224" layer="16"/>
<rectangle x1="10.922" y1="13.97" x2="11.176" y2="14.224" layer="16"/>
<rectangle x1="11.176" y1="13.97" x2="11.43" y2="14.224" layer="16"/>
<rectangle x1="11.43" y1="13.97" x2="11.684" y2="14.224" layer="16"/>
<rectangle x1="12.954" y1="13.97" x2="13.208" y2="14.224" layer="16"/>
<rectangle x1="13.208" y1="13.97" x2="13.462" y2="14.224" layer="16"/>
<rectangle x1="13.462" y1="13.97" x2="13.716" y2="14.224" layer="16"/>
<rectangle x1="14.224" y1="13.97" x2="14.478" y2="14.224" layer="16"/>
<rectangle x1="14.478" y1="13.97" x2="14.732" y2="14.224" layer="16"/>
<rectangle x1="14.732" y1="13.97" x2="14.986" y2="14.224" layer="16"/>
<rectangle x1="14.986" y1="13.97" x2="15.24" y2="14.224" layer="16"/>
<rectangle x1="15.24" y1="13.97" x2="15.494" y2="14.224" layer="16"/>
<rectangle x1="15.494" y1="13.97" x2="15.748" y2="14.224" layer="16"/>
<rectangle x1="15.748" y1="13.97" x2="16.002" y2="14.224" layer="16"/>
<rectangle x1="16.002" y1="13.97" x2="16.256" y2="14.224" layer="16"/>
<rectangle x1="16.256" y1="13.97" x2="16.51" y2="14.224" layer="16"/>
<rectangle x1="16.51" y1="13.97" x2="16.764" y2="14.224" layer="16"/>
<rectangle x1="16.764" y1="13.97" x2="17.018" y2="14.224" layer="16"/>
<rectangle x1="9.906" y1="14.224" x2="10.16" y2="14.478" layer="16"/>
<rectangle x1="10.16" y1="14.224" x2="10.414" y2="14.478" layer="16"/>
<rectangle x1="10.414" y1="14.224" x2="10.668" y2="14.478" layer="16"/>
<rectangle x1="10.668" y1="14.224" x2="10.922" y2="14.478" layer="16"/>
<rectangle x1="10.922" y1="14.224" x2="11.176" y2="14.478" layer="16"/>
<rectangle x1="11.176" y1="14.224" x2="11.43" y2="14.478" layer="16"/>
<rectangle x1="11.43" y1="14.224" x2="11.684" y2="14.478" layer="16"/>
<rectangle x1="11.684" y1="14.224" x2="11.938" y2="14.478" layer="16"/>
<rectangle x1="11.938" y1="14.224" x2="12.192" y2="14.478" layer="16"/>
<rectangle x1="12.192" y1="14.224" x2="12.446" y2="14.478" layer="16"/>
<rectangle x1="12.446" y1="14.224" x2="12.7" y2="14.478" layer="16"/>
<rectangle x1="12.7" y1="14.224" x2="12.954" y2="14.478" layer="16"/>
<rectangle x1="12.954" y1="14.224" x2="13.208" y2="14.478" layer="16"/>
<rectangle x1="13.208" y1="14.224" x2="13.462" y2="14.478" layer="16"/>
<rectangle x1="13.462" y1="14.224" x2="13.716" y2="14.478" layer="16"/>
<rectangle x1="13.716" y1="14.224" x2="13.97" y2="14.478" layer="16"/>
<rectangle x1="13.97" y1="14.224" x2="14.224" y2="14.478" layer="16"/>
<rectangle x1="14.224" y1="14.224" x2="14.478" y2="14.478" layer="16"/>
<rectangle x1="14.478" y1="14.224" x2="14.732" y2="14.478" layer="16"/>
<rectangle x1="14.732" y1="14.224" x2="14.986" y2="14.478" layer="16"/>
<rectangle x1="14.986" y1="14.224" x2="15.24" y2="14.478" layer="16"/>
<rectangle x1="15.24" y1="14.224" x2="15.494" y2="14.478" layer="16"/>
<rectangle x1="15.494" y1="14.224" x2="15.748" y2="14.478" layer="16"/>
<rectangle x1="15.748" y1="14.224" x2="16.002" y2="14.478" layer="16"/>
<rectangle x1="16.002" y1="14.224" x2="16.256" y2="14.478" layer="16"/>
<rectangle x1="16.256" y1="14.224" x2="16.51" y2="14.478" layer="16"/>
<rectangle x1="16.51" y1="14.224" x2="16.764" y2="14.478" layer="16"/>
<rectangle x1="16.764" y1="14.224" x2="17.018" y2="14.478" layer="16"/>
<rectangle x1="10.16" y1="14.478" x2="10.414" y2="14.732" layer="16"/>
<rectangle x1="10.414" y1="14.478" x2="10.668" y2="14.732" layer="16"/>
<rectangle x1="10.668" y1="14.478" x2="10.922" y2="14.732" layer="16"/>
<rectangle x1="10.922" y1="14.478" x2="11.176" y2="14.732" layer="16"/>
<rectangle x1="11.176" y1="14.478" x2="11.43" y2="14.732" layer="16"/>
<rectangle x1="11.43" y1="14.478" x2="11.684" y2="14.732" layer="16"/>
<rectangle x1="11.684" y1="14.478" x2="11.938" y2="14.732" layer="16"/>
<rectangle x1="11.938" y1="14.478" x2="12.192" y2="14.732" layer="16"/>
<rectangle x1="12.192" y1="14.478" x2="12.446" y2="14.732" layer="16"/>
<rectangle x1="12.446" y1="14.478" x2="12.7" y2="14.732" layer="16"/>
<rectangle x1="12.7" y1="14.478" x2="12.954" y2="14.732" layer="16"/>
<rectangle x1="12.954" y1="14.478" x2="13.208" y2="14.732" layer="16"/>
<rectangle x1="13.208" y1="14.478" x2="13.462" y2="14.732" layer="16"/>
<rectangle x1="13.462" y1="14.478" x2="13.716" y2="14.732" layer="16"/>
<rectangle x1="13.716" y1="14.478" x2="13.97" y2="14.732" layer="16"/>
<rectangle x1="13.97" y1="14.478" x2="14.224" y2="14.732" layer="16"/>
<rectangle x1="14.224" y1="14.478" x2="14.478" y2="14.732" layer="16"/>
<rectangle x1="14.478" y1="14.478" x2="14.732" y2="14.732" layer="16"/>
<rectangle x1="14.732" y1="14.478" x2="14.986" y2="14.732" layer="16"/>
<rectangle x1="14.986" y1="14.478" x2="15.24" y2="14.732" layer="16"/>
<rectangle x1="15.24" y1="14.478" x2="15.494" y2="14.732" layer="16"/>
<rectangle x1="15.494" y1="14.478" x2="15.748" y2="14.732" layer="16"/>
<rectangle x1="15.748" y1="14.478" x2="16.002" y2="14.732" layer="16"/>
<rectangle x1="16.002" y1="14.478" x2="16.256" y2="14.732" layer="16"/>
<rectangle x1="16.256" y1="14.478" x2="16.51" y2="14.732" layer="16"/>
<rectangle x1="16.51" y1="14.478" x2="16.764" y2="14.732" layer="16"/>
<rectangle x1="10.414" y1="14.732" x2="10.668" y2="14.986" layer="16"/>
<rectangle x1="10.668" y1="14.732" x2="10.922" y2="14.986" layer="16"/>
<rectangle x1="10.922" y1="14.732" x2="11.176" y2="14.986" layer="16"/>
<rectangle x1="11.176" y1="14.732" x2="11.43" y2="14.986" layer="16"/>
<rectangle x1="11.43" y1="14.732" x2="11.684" y2="14.986" layer="16"/>
<rectangle x1="11.684" y1="14.732" x2="11.938" y2="14.986" layer="16"/>
<rectangle x1="11.938" y1="14.732" x2="12.192" y2="14.986" layer="16"/>
<rectangle x1="12.192" y1="14.732" x2="12.446" y2="14.986" layer="16"/>
<rectangle x1="12.446" y1="14.732" x2="12.7" y2="14.986" layer="16"/>
<rectangle x1="12.7" y1="14.732" x2="12.954" y2="14.986" layer="16"/>
<rectangle x1="12.954" y1="14.732" x2="13.208" y2="14.986" layer="16"/>
<rectangle x1="13.208" y1="14.732" x2="13.462" y2="14.986" layer="16"/>
<rectangle x1="13.462" y1="14.732" x2="13.716" y2="14.986" layer="16"/>
<rectangle x1="13.716" y1="14.732" x2="13.97" y2="14.986" layer="16"/>
<rectangle x1="13.97" y1="14.732" x2="14.224" y2="14.986" layer="16"/>
<rectangle x1="14.224" y1="14.732" x2="14.478" y2="14.986" layer="16"/>
<rectangle x1="14.478" y1="14.732" x2="14.732" y2="14.986" layer="16"/>
<rectangle x1="14.732" y1="14.732" x2="14.986" y2="14.986" layer="16"/>
<rectangle x1="14.986" y1="14.732" x2="15.24" y2="14.986" layer="16"/>
<rectangle x1="15.24" y1="14.732" x2="15.494" y2="14.986" layer="16"/>
<rectangle x1="15.494" y1="14.732" x2="15.748" y2="14.986" layer="16"/>
<rectangle x1="15.748" y1="14.732" x2="16.002" y2="14.986" layer="16"/>
<rectangle x1="16.002" y1="14.732" x2="16.256" y2="14.986" layer="16"/>
<rectangle x1="16.256" y1="14.732" x2="16.51" y2="14.986" layer="16"/>
<rectangle x1="10.668" y1="14.986" x2="10.922" y2="15.24" layer="16"/>
<rectangle x1="10.922" y1="14.986" x2="11.176" y2="15.24" layer="16"/>
<rectangle x1="11.176" y1="14.986" x2="11.43" y2="15.24" layer="16"/>
<rectangle x1="11.43" y1="14.986" x2="11.684" y2="15.24" layer="16"/>
<rectangle x1="11.684" y1="14.986" x2="11.938" y2="15.24" layer="16"/>
<rectangle x1="11.938" y1="14.986" x2="12.192" y2="15.24" layer="16"/>
<rectangle x1="12.192" y1="14.986" x2="12.446" y2="15.24" layer="16"/>
<rectangle x1="12.446" y1="14.986" x2="12.7" y2="15.24" layer="16"/>
<rectangle x1="12.7" y1="14.986" x2="12.954" y2="15.24" layer="16"/>
<rectangle x1="12.954" y1="14.986" x2="13.208" y2="15.24" layer="16"/>
<rectangle x1="13.208" y1="14.986" x2="13.462" y2="15.24" layer="16"/>
<rectangle x1="13.462" y1="14.986" x2="13.716" y2="15.24" layer="16"/>
<rectangle x1="13.716" y1="14.986" x2="13.97" y2="15.24" layer="16"/>
<rectangle x1="13.97" y1="14.986" x2="14.224" y2="15.24" layer="16"/>
<rectangle x1="14.224" y1="14.986" x2="14.478" y2="15.24" layer="16"/>
<rectangle x1="14.478" y1="14.986" x2="14.732" y2="15.24" layer="16"/>
<rectangle x1="14.732" y1="14.986" x2="14.986" y2="15.24" layer="16"/>
<rectangle x1="14.986" y1="14.986" x2="15.24" y2="15.24" layer="16"/>
<rectangle x1="15.24" y1="14.986" x2="15.494" y2="15.24" layer="16"/>
<rectangle x1="15.494" y1="14.986" x2="15.748" y2="15.24" layer="16"/>
<rectangle x1="15.748" y1="14.986" x2="16.002" y2="15.24" layer="16"/>
<rectangle x1="11.176" y1="15.24" x2="11.43" y2="15.494" layer="16"/>
<rectangle x1="11.43" y1="15.24" x2="11.684" y2="15.494" layer="16"/>
<rectangle x1="11.684" y1="15.24" x2="11.938" y2="15.494" layer="16"/>
<rectangle x1="11.938" y1="15.24" x2="12.192" y2="15.494" layer="16"/>
<rectangle x1="12.192" y1="15.24" x2="12.446" y2="15.494" layer="16"/>
<rectangle x1="12.446" y1="15.24" x2="12.7" y2="15.494" layer="16"/>
<rectangle x1="12.7" y1="15.24" x2="12.954" y2="15.494" layer="16"/>
<rectangle x1="12.954" y1="15.24" x2="13.208" y2="15.494" layer="16"/>
<rectangle x1="13.208" y1="15.24" x2="13.462" y2="15.494" layer="16"/>
<rectangle x1="13.462" y1="15.24" x2="13.716" y2="15.494" layer="16"/>
<rectangle x1="13.716" y1="15.24" x2="13.97" y2="15.494" layer="16"/>
<rectangle x1="13.97" y1="15.24" x2="14.224" y2="15.494" layer="16"/>
<rectangle x1="14.224" y1="15.24" x2="14.478" y2="15.494" layer="16"/>
<rectangle x1="14.478" y1="15.24" x2="14.732" y2="15.494" layer="16"/>
<rectangle x1="14.732" y1="15.24" x2="14.986" y2="15.494" layer="16"/>
<rectangle x1="14.986" y1="15.24" x2="15.24" y2="15.494" layer="16"/>
<rectangle x1="15.24" y1="15.24" x2="15.494" y2="15.494" layer="16"/>
<rectangle x1="15.494" y1="15.24" x2="15.748" y2="15.494" layer="16"/>
<rectangle x1="11.43" y1="15.494" x2="11.684" y2="15.748" layer="16"/>
<rectangle x1="11.684" y1="15.494" x2="11.938" y2="15.748" layer="16"/>
<rectangle x1="11.938" y1="15.494" x2="12.192" y2="15.748" layer="16"/>
<rectangle x1="12.192" y1="15.494" x2="12.446" y2="15.748" layer="16"/>
<rectangle x1="12.446" y1="15.494" x2="12.7" y2="15.748" layer="16"/>
<rectangle x1="12.7" y1="15.494" x2="12.954" y2="15.748" layer="16"/>
<rectangle x1="12.954" y1="15.494" x2="13.208" y2="15.748" layer="16"/>
<rectangle x1="13.208" y1="15.494" x2="13.462" y2="15.748" layer="16"/>
<rectangle x1="13.462" y1="15.494" x2="13.716" y2="15.748" layer="16"/>
<rectangle x1="13.716" y1="15.494" x2="13.97" y2="15.748" layer="16"/>
<rectangle x1="13.97" y1="15.494" x2="14.224" y2="15.748" layer="16"/>
<rectangle x1="14.224" y1="15.494" x2="14.478" y2="15.748" layer="16"/>
<rectangle x1="14.478" y1="15.494" x2="14.732" y2="15.748" layer="16"/>
<rectangle x1="14.732" y1="15.494" x2="14.986" y2="15.748" layer="16"/>
<rectangle x1="14.986" y1="15.494" x2="15.24" y2="15.748" layer="16"/>
<rectangle x1="11.938" y1="15.748" x2="12.192" y2="16.002" layer="16"/>
<rectangle x1="12.192" y1="15.748" x2="12.446" y2="16.002" layer="16"/>
<rectangle x1="12.446" y1="15.748" x2="12.7" y2="16.002" layer="16"/>
<rectangle x1="12.7" y1="15.748" x2="12.954" y2="16.002" layer="16"/>
<rectangle x1="12.954" y1="15.748" x2="13.208" y2="16.002" layer="16"/>
<rectangle x1="13.208" y1="15.748" x2="13.462" y2="16.002" layer="16"/>
<rectangle x1="13.462" y1="15.748" x2="13.716" y2="16.002" layer="16"/>
<rectangle x1="13.716" y1="15.748" x2="13.97" y2="16.002" layer="16"/>
<rectangle x1="13.97" y1="15.748" x2="14.224" y2="16.002" layer="16"/>
<rectangle x1="14.224" y1="15.748" x2="14.478" y2="16.002" layer="16"/>
<rectangle x1="14.478" y1="15.748" x2="14.732" y2="16.002" layer="16"/>
</package>
<package name="LOGO_TRIFORS_V1">
<rectangle x1="8.3312" y1="1.016" x2="8.5344" y2="1.2192" layer="1"/>
<rectangle x1="8.5344" y1="1.016" x2="8.7376" y2="1.2192" layer="1"/>
<rectangle x1="8.7376" y1="1.016" x2="8.9408" y2="1.2192" layer="1"/>
<rectangle x1="8.9408" y1="1.016" x2="9.144" y2="1.2192" layer="1"/>
<rectangle x1="9.144" y1="1.016" x2="9.3472" y2="1.2192" layer="1"/>
<rectangle x1="9.3472" y1="1.016" x2="9.5504" y2="1.2192" layer="1"/>
<rectangle x1="9.5504" y1="1.016" x2="9.7536" y2="1.2192" layer="1"/>
<rectangle x1="9.7536" y1="1.016" x2="9.9568" y2="1.2192" layer="1"/>
<rectangle x1="9.9568" y1="1.016" x2="10.16" y2="1.2192" layer="1"/>
<rectangle x1="10.16" y1="1.016" x2="10.3632" y2="1.2192" layer="1"/>
<rectangle x1="10.3632" y1="1.016" x2="10.5664" y2="1.2192" layer="1"/>
<rectangle x1="10.5664" y1="1.016" x2="10.7696" y2="1.2192" layer="1"/>
<rectangle x1="10.7696" y1="1.016" x2="10.9728" y2="1.2192" layer="1"/>
<rectangle x1="10.9728" y1="1.016" x2="11.176" y2="1.2192" layer="1"/>
<rectangle x1="11.176" y1="1.016" x2="11.3792" y2="1.2192" layer="1"/>
<rectangle x1="11.3792" y1="1.016" x2="11.5824" y2="1.2192" layer="1"/>
<rectangle x1="11.5824" y1="1.016" x2="11.7856" y2="1.2192" layer="1"/>
<rectangle x1="11.7856" y1="1.016" x2="11.9888" y2="1.2192" layer="1"/>
<rectangle x1="11.9888" y1="1.016" x2="12.192" y2="1.2192" layer="1"/>
<rectangle x1="12.192" y1="1.016" x2="12.3952" y2="1.2192" layer="1"/>
<rectangle x1="12.3952" y1="1.016" x2="12.5984" y2="1.2192" layer="1"/>
<rectangle x1="13.6144" y1="1.016" x2="13.8176" y2="1.2192" layer="1"/>
<rectangle x1="13.8176" y1="1.016" x2="14.0208" y2="1.2192" layer="1"/>
<rectangle x1="14.0208" y1="1.016" x2="14.224" y2="1.2192" layer="1"/>
<rectangle x1="14.224" y1="1.016" x2="14.4272" y2="1.2192" layer="1"/>
<rectangle x1="14.4272" y1="1.016" x2="14.6304" y2="1.2192" layer="1"/>
<rectangle x1="14.6304" y1="1.016" x2="14.8336" y2="1.2192" layer="1"/>
<rectangle x1="14.8336" y1="1.016" x2="15.0368" y2="1.2192" layer="1"/>
<rectangle x1="15.0368" y1="1.016" x2="15.24" y2="1.2192" layer="1"/>
<rectangle x1="15.24" y1="1.016" x2="15.4432" y2="1.2192" layer="1"/>
<rectangle x1="15.4432" y1="1.016" x2="15.6464" y2="1.2192" layer="1"/>
<rectangle x1="15.6464" y1="1.016" x2="15.8496" y2="1.2192" layer="1"/>
<rectangle x1="15.8496" y1="1.016" x2="16.0528" y2="1.2192" layer="1"/>
<rectangle x1="16.0528" y1="1.016" x2="16.256" y2="1.2192" layer="1"/>
<rectangle x1="16.256" y1="1.016" x2="16.4592" y2="1.2192" layer="1"/>
<rectangle x1="16.4592" y1="1.016" x2="16.6624" y2="1.2192" layer="1"/>
<rectangle x1="16.6624" y1="1.016" x2="16.8656" y2="1.2192" layer="1"/>
<rectangle x1="16.8656" y1="1.016" x2="17.0688" y2="1.2192" layer="1"/>
<rectangle x1="17.0688" y1="1.016" x2="17.272" y2="1.2192" layer="1"/>
<rectangle x1="17.272" y1="1.016" x2="17.4752" y2="1.2192" layer="1"/>
<rectangle x1="17.4752" y1="1.016" x2="17.6784" y2="1.2192" layer="1"/>
<rectangle x1="17.6784" y1="1.016" x2="17.8816" y2="1.2192" layer="1"/>
<rectangle x1="8.3312" y1="1.2192" x2="8.5344" y2="1.4224" layer="1"/>
<rectangle x1="8.5344" y1="1.2192" x2="8.7376" y2="1.4224" layer="1"/>
<rectangle x1="8.7376" y1="1.2192" x2="8.9408" y2="1.4224" layer="1"/>
<rectangle x1="8.9408" y1="1.2192" x2="9.144" y2="1.4224" layer="1"/>
<rectangle x1="9.144" y1="1.2192" x2="9.3472" y2="1.4224" layer="1"/>
<rectangle x1="9.3472" y1="1.2192" x2="9.5504" y2="1.4224" layer="1"/>
<rectangle x1="9.5504" y1="1.2192" x2="9.7536" y2="1.4224" layer="1"/>
<rectangle x1="9.7536" y1="1.2192" x2="9.9568" y2="1.4224" layer="1"/>
<rectangle x1="9.9568" y1="1.2192" x2="10.16" y2="1.4224" layer="1"/>
<rectangle x1="10.16" y1="1.2192" x2="10.3632" y2="1.4224" layer="1"/>
<rectangle x1="10.3632" y1="1.2192" x2="10.5664" y2="1.4224" layer="1"/>
<rectangle x1="10.5664" y1="1.2192" x2="10.7696" y2="1.4224" layer="1"/>
<rectangle x1="10.7696" y1="1.2192" x2="10.9728" y2="1.4224" layer="1"/>
<rectangle x1="10.9728" y1="1.2192" x2="11.176" y2="1.4224" layer="1"/>
<rectangle x1="11.176" y1="1.2192" x2="11.3792" y2="1.4224" layer="1"/>
<rectangle x1="11.3792" y1="1.2192" x2="11.5824" y2="1.4224" layer="1"/>
<rectangle x1="11.5824" y1="1.2192" x2="11.7856" y2="1.4224" layer="1"/>
<rectangle x1="11.7856" y1="1.2192" x2="11.9888" y2="1.4224" layer="1"/>
<rectangle x1="11.9888" y1="1.2192" x2="12.192" y2="1.4224" layer="1"/>
<rectangle x1="12.192" y1="1.2192" x2="12.3952" y2="1.4224" layer="1"/>
<rectangle x1="12.3952" y1="1.2192" x2="12.5984" y2="1.4224" layer="1"/>
<rectangle x1="13.6144" y1="1.2192" x2="13.8176" y2="1.4224" layer="1"/>
<rectangle x1="13.8176" y1="1.2192" x2="14.0208" y2="1.4224" layer="1"/>
<rectangle x1="14.0208" y1="1.2192" x2="14.224" y2="1.4224" layer="1"/>
<rectangle x1="14.224" y1="1.2192" x2="14.4272" y2="1.4224" layer="1"/>
<rectangle x1="14.4272" y1="1.2192" x2="14.6304" y2="1.4224" layer="1"/>
<rectangle x1="14.6304" y1="1.2192" x2="14.8336" y2="1.4224" layer="1"/>
<rectangle x1="14.8336" y1="1.2192" x2="15.0368" y2="1.4224" layer="1"/>
<rectangle x1="15.0368" y1="1.2192" x2="15.24" y2="1.4224" layer="1"/>
<rectangle x1="15.24" y1="1.2192" x2="15.4432" y2="1.4224" layer="1"/>
<rectangle x1="15.4432" y1="1.2192" x2="15.6464" y2="1.4224" layer="1"/>
<rectangle x1="15.6464" y1="1.2192" x2="15.8496" y2="1.4224" layer="1"/>
<rectangle x1="15.8496" y1="1.2192" x2="16.0528" y2="1.4224" layer="1"/>
<rectangle x1="16.0528" y1="1.2192" x2="16.256" y2="1.4224" layer="1"/>
<rectangle x1="16.256" y1="1.2192" x2="16.4592" y2="1.4224" layer="1"/>
<rectangle x1="16.4592" y1="1.2192" x2="16.6624" y2="1.4224" layer="1"/>
<rectangle x1="16.6624" y1="1.2192" x2="16.8656" y2="1.4224" layer="1"/>
<rectangle x1="16.8656" y1="1.2192" x2="17.0688" y2="1.4224" layer="1"/>
<rectangle x1="17.0688" y1="1.2192" x2="17.272" y2="1.4224" layer="1"/>
<rectangle x1="17.272" y1="1.2192" x2="17.4752" y2="1.4224" layer="1"/>
<rectangle x1="17.4752" y1="1.2192" x2="17.6784" y2="1.4224" layer="1"/>
<rectangle x1="17.6784" y1="1.2192" x2="17.8816" y2="1.4224" layer="1"/>
<rectangle x1="8.5344" y1="1.4224" x2="8.7376" y2="1.6256" layer="1"/>
<rectangle x1="8.7376" y1="1.4224" x2="8.9408" y2="1.6256" layer="1"/>
<rectangle x1="8.9408" y1="1.4224" x2="9.144" y2="1.6256" layer="1"/>
<rectangle x1="9.144" y1="1.4224" x2="9.3472" y2="1.6256" layer="1"/>
<rectangle x1="9.3472" y1="1.4224" x2="9.5504" y2="1.6256" layer="1"/>
<rectangle x1="9.5504" y1="1.4224" x2="9.7536" y2="1.6256" layer="1"/>
<rectangle x1="9.7536" y1="1.4224" x2="9.9568" y2="1.6256" layer="1"/>
<rectangle x1="9.9568" y1="1.4224" x2="10.16" y2="1.6256" layer="1"/>
<rectangle x1="10.16" y1="1.4224" x2="10.3632" y2="1.6256" layer="1"/>
<rectangle x1="10.3632" y1="1.4224" x2="10.5664" y2="1.6256" layer="1"/>
<rectangle x1="10.5664" y1="1.4224" x2="10.7696" y2="1.6256" layer="1"/>
<rectangle x1="10.7696" y1="1.4224" x2="10.9728" y2="1.6256" layer="1"/>
<rectangle x1="10.9728" y1="1.4224" x2="11.176" y2="1.6256" layer="1"/>
<rectangle x1="11.176" y1="1.4224" x2="11.3792" y2="1.6256" layer="1"/>
<rectangle x1="11.3792" y1="1.4224" x2="11.5824" y2="1.6256" layer="1"/>
<rectangle x1="11.5824" y1="1.4224" x2="11.7856" y2="1.6256" layer="1"/>
<rectangle x1="11.7856" y1="1.4224" x2="11.9888" y2="1.6256" layer="1"/>
<rectangle x1="11.9888" y1="1.4224" x2="12.192" y2="1.6256" layer="1"/>
<rectangle x1="12.192" y1="1.4224" x2="12.3952" y2="1.6256" layer="1"/>
<rectangle x1="13.8176" y1="1.4224" x2="14.0208" y2="1.6256" layer="1"/>
<rectangle x1="14.0208" y1="1.4224" x2="14.224" y2="1.6256" layer="1"/>
<rectangle x1="14.224" y1="1.4224" x2="14.4272" y2="1.6256" layer="1"/>
<rectangle x1="14.4272" y1="1.4224" x2="14.6304" y2="1.6256" layer="1"/>
<rectangle x1="14.6304" y1="1.4224" x2="14.8336" y2="1.6256" layer="1"/>
<rectangle x1="14.8336" y1="1.4224" x2="15.0368" y2="1.6256" layer="1"/>
<rectangle x1="15.0368" y1="1.4224" x2="15.24" y2="1.6256" layer="1"/>
<rectangle x1="15.24" y1="1.4224" x2="15.4432" y2="1.6256" layer="1"/>
<rectangle x1="15.4432" y1="1.4224" x2="15.6464" y2="1.6256" layer="1"/>
<rectangle x1="15.6464" y1="1.4224" x2="15.8496" y2="1.6256" layer="1"/>
<rectangle x1="15.8496" y1="1.4224" x2="16.0528" y2="1.6256" layer="1"/>
<rectangle x1="16.0528" y1="1.4224" x2="16.256" y2="1.6256" layer="1"/>
<rectangle x1="16.256" y1="1.4224" x2="16.4592" y2="1.6256" layer="1"/>
<rectangle x1="16.4592" y1="1.4224" x2="16.6624" y2="1.6256" layer="1"/>
<rectangle x1="16.6624" y1="1.4224" x2="16.8656" y2="1.6256" layer="1"/>
<rectangle x1="16.8656" y1="1.4224" x2="17.0688" y2="1.6256" layer="1"/>
<rectangle x1="17.0688" y1="1.4224" x2="17.272" y2="1.6256" layer="1"/>
<rectangle x1="17.272" y1="1.4224" x2="17.4752" y2="1.6256" layer="1"/>
<rectangle x1="17.4752" y1="1.4224" x2="17.6784" y2="1.6256" layer="1"/>
<rectangle x1="8.5344" y1="1.6256" x2="8.7376" y2="1.8288" layer="1"/>
<rectangle x1="8.7376" y1="1.6256" x2="8.9408" y2="1.8288" layer="1"/>
<rectangle x1="8.9408" y1="1.6256" x2="9.144" y2="1.8288" layer="1"/>
<rectangle x1="9.144" y1="1.6256" x2="9.3472" y2="1.8288" layer="1"/>
<rectangle x1="9.3472" y1="1.6256" x2="9.5504" y2="1.8288" layer="1"/>
<rectangle x1="9.5504" y1="1.6256" x2="9.7536" y2="1.8288" layer="1"/>
<rectangle x1="9.7536" y1="1.6256" x2="9.9568" y2="1.8288" layer="1"/>
<rectangle x1="9.9568" y1="1.6256" x2="10.16" y2="1.8288" layer="1"/>
<rectangle x1="10.16" y1="1.6256" x2="10.3632" y2="1.8288" layer="1"/>
<rectangle x1="10.3632" y1="1.6256" x2="10.5664" y2="1.8288" layer="1"/>
<rectangle x1="10.5664" y1="1.6256" x2="10.7696" y2="1.8288" layer="1"/>
<rectangle x1="10.7696" y1="1.6256" x2="10.9728" y2="1.8288" layer="1"/>
<rectangle x1="10.9728" y1="1.6256" x2="11.176" y2="1.8288" layer="1"/>
<rectangle x1="11.176" y1="1.6256" x2="11.3792" y2="1.8288" layer="1"/>
<rectangle x1="11.3792" y1="1.6256" x2="11.5824" y2="1.8288" layer="1"/>
<rectangle x1="11.5824" y1="1.6256" x2="11.7856" y2="1.8288" layer="1"/>
<rectangle x1="11.7856" y1="1.6256" x2="11.9888" y2="1.8288" layer="1"/>
<rectangle x1="11.9888" y1="1.6256" x2="12.192" y2="1.8288" layer="1"/>
<rectangle x1="12.192" y1="1.6256" x2="12.3952" y2="1.8288" layer="1"/>
<rectangle x1="13.8176" y1="1.6256" x2="14.0208" y2="1.8288" layer="1"/>
<rectangle x1="14.0208" y1="1.6256" x2="14.224" y2="1.8288" layer="1"/>
<rectangle x1="14.224" y1="1.6256" x2="14.4272" y2="1.8288" layer="1"/>
<rectangle x1="14.4272" y1="1.6256" x2="14.6304" y2="1.8288" layer="1"/>
<rectangle x1="14.6304" y1="1.6256" x2="14.8336" y2="1.8288" layer="1"/>
<rectangle x1="14.8336" y1="1.6256" x2="15.0368" y2="1.8288" layer="1"/>
<rectangle x1="15.0368" y1="1.6256" x2="15.24" y2="1.8288" layer="1"/>
<rectangle x1="15.24" y1="1.6256" x2="15.4432" y2="1.8288" layer="1"/>
<rectangle x1="15.4432" y1="1.6256" x2="15.6464" y2="1.8288" layer="1"/>
<rectangle x1="15.6464" y1="1.6256" x2="15.8496" y2="1.8288" layer="1"/>
<rectangle x1="15.8496" y1="1.6256" x2="16.0528" y2="1.8288" layer="1"/>
<rectangle x1="16.0528" y1="1.6256" x2="16.256" y2="1.8288" layer="1"/>
<rectangle x1="16.256" y1="1.6256" x2="16.4592" y2="1.8288" layer="1"/>
<rectangle x1="16.4592" y1="1.6256" x2="16.6624" y2="1.8288" layer="1"/>
<rectangle x1="16.6624" y1="1.6256" x2="16.8656" y2="1.8288" layer="1"/>
<rectangle x1="16.8656" y1="1.6256" x2="17.0688" y2="1.8288" layer="1"/>
<rectangle x1="17.0688" y1="1.6256" x2="17.272" y2="1.8288" layer="1"/>
<rectangle x1="17.272" y1="1.6256" x2="17.4752" y2="1.8288" layer="1"/>
<rectangle x1="17.4752" y1="1.6256" x2="17.6784" y2="1.8288" layer="1"/>
<rectangle x1="8.7376" y1="1.8288" x2="8.9408" y2="2.032" layer="1"/>
<rectangle x1="8.9408" y1="1.8288" x2="9.144" y2="2.032" layer="1"/>
<rectangle x1="9.144" y1="1.8288" x2="9.3472" y2="2.032" layer="1"/>
<rectangle x1="9.3472" y1="1.8288" x2="9.5504" y2="2.032" layer="1"/>
<rectangle x1="9.5504" y1="1.8288" x2="9.7536" y2="2.032" layer="1"/>
<rectangle x1="9.7536" y1="1.8288" x2="9.9568" y2="2.032" layer="1"/>
<rectangle x1="9.9568" y1="1.8288" x2="10.16" y2="2.032" layer="1"/>
<rectangle x1="10.16" y1="1.8288" x2="10.3632" y2="2.032" layer="1"/>
<rectangle x1="10.3632" y1="1.8288" x2="10.5664" y2="2.032" layer="1"/>
<rectangle x1="10.5664" y1="1.8288" x2="10.7696" y2="2.032" layer="1"/>
<rectangle x1="10.7696" y1="1.8288" x2="10.9728" y2="2.032" layer="1"/>
<rectangle x1="10.9728" y1="1.8288" x2="11.176" y2="2.032" layer="1"/>
<rectangle x1="11.176" y1="1.8288" x2="11.3792" y2="2.032" layer="1"/>
<rectangle x1="11.3792" y1="1.8288" x2="11.5824" y2="2.032" layer="1"/>
<rectangle x1="11.5824" y1="1.8288" x2="11.7856" y2="2.032" layer="1"/>
<rectangle x1="11.7856" y1="1.8288" x2="11.9888" y2="2.032" layer="1"/>
<rectangle x1="11.9888" y1="1.8288" x2="12.192" y2="2.032" layer="1"/>
<rectangle x1="14.0208" y1="1.8288" x2="14.224" y2="2.032" layer="1"/>
<rectangle x1="14.224" y1="1.8288" x2="14.4272" y2="2.032" layer="1"/>
<rectangle x1="14.4272" y1="1.8288" x2="14.6304" y2="2.032" layer="1"/>
<rectangle x1="14.6304" y1="1.8288" x2="14.8336" y2="2.032" layer="1"/>
<rectangle x1="14.8336" y1="1.8288" x2="15.0368" y2="2.032" layer="1"/>
<rectangle x1="15.0368" y1="1.8288" x2="15.24" y2="2.032" layer="1"/>
<rectangle x1="15.24" y1="1.8288" x2="15.4432" y2="2.032" layer="1"/>
<rectangle x1="15.4432" y1="1.8288" x2="15.6464" y2="2.032" layer="1"/>
<rectangle x1="15.6464" y1="1.8288" x2="15.8496" y2="2.032" layer="1"/>
<rectangle x1="15.8496" y1="1.8288" x2="16.0528" y2="2.032" layer="1"/>
<rectangle x1="16.0528" y1="1.8288" x2="16.256" y2="2.032" layer="1"/>
<rectangle x1="16.256" y1="1.8288" x2="16.4592" y2="2.032" layer="1"/>
<rectangle x1="16.4592" y1="1.8288" x2="16.6624" y2="2.032" layer="1"/>
<rectangle x1="16.6624" y1="1.8288" x2="16.8656" y2="2.032" layer="1"/>
<rectangle x1="16.8656" y1="1.8288" x2="17.0688" y2="2.032" layer="1"/>
<rectangle x1="17.0688" y1="1.8288" x2="17.272" y2="2.032" layer="1"/>
<rectangle x1="17.272" y1="1.8288" x2="17.4752" y2="2.032" layer="1"/>
<rectangle x1="8.7376" y1="2.032" x2="8.9408" y2="2.2352" layer="1"/>
<rectangle x1="8.9408" y1="2.032" x2="9.144" y2="2.2352" layer="1"/>
<rectangle x1="9.144" y1="2.032" x2="9.3472" y2="2.2352" layer="1"/>
<rectangle x1="9.3472" y1="2.032" x2="9.5504" y2="2.2352" layer="1"/>
<rectangle x1="9.5504" y1="2.032" x2="9.7536" y2="2.2352" layer="1"/>
<rectangle x1="9.7536" y1="2.032" x2="9.9568" y2="2.2352" layer="1"/>
<rectangle x1="9.9568" y1="2.032" x2="10.16" y2="2.2352" layer="1"/>
<rectangle x1="10.16" y1="2.032" x2="10.3632" y2="2.2352" layer="1"/>
<rectangle x1="10.3632" y1="2.032" x2="10.5664" y2="2.2352" layer="1"/>
<rectangle x1="10.5664" y1="2.032" x2="10.7696" y2="2.2352" layer="1"/>
<rectangle x1="10.7696" y1="2.032" x2="10.9728" y2="2.2352" layer="1"/>
<rectangle x1="10.9728" y1="2.032" x2="11.176" y2="2.2352" layer="1"/>
<rectangle x1="11.176" y1="2.032" x2="11.3792" y2="2.2352" layer="1"/>
<rectangle x1="11.3792" y1="2.032" x2="11.5824" y2="2.2352" layer="1"/>
<rectangle x1="11.5824" y1="2.032" x2="11.7856" y2="2.2352" layer="1"/>
<rectangle x1="11.7856" y1="2.032" x2="11.9888" y2="2.2352" layer="1"/>
<rectangle x1="11.9888" y1="2.032" x2="12.192" y2="2.2352" layer="1"/>
<rectangle x1="14.0208" y1="2.032" x2="14.224" y2="2.2352" layer="1"/>
<rectangle x1="14.224" y1="2.032" x2="14.4272" y2="2.2352" layer="1"/>
<rectangle x1="14.4272" y1="2.032" x2="14.6304" y2="2.2352" layer="1"/>
<rectangle x1="14.6304" y1="2.032" x2="14.8336" y2="2.2352" layer="1"/>
<rectangle x1="14.8336" y1="2.032" x2="15.0368" y2="2.2352" layer="1"/>
<rectangle x1="15.0368" y1="2.032" x2="15.24" y2="2.2352" layer="1"/>
<rectangle x1="15.24" y1="2.032" x2="15.4432" y2="2.2352" layer="1"/>
<rectangle x1="15.4432" y1="2.032" x2="15.6464" y2="2.2352" layer="1"/>
<rectangle x1="15.6464" y1="2.032" x2="15.8496" y2="2.2352" layer="1"/>
<rectangle x1="15.8496" y1="2.032" x2="16.0528" y2="2.2352" layer="1"/>
<rectangle x1="16.0528" y1="2.032" x2="16.256" y2="2.2352" layer="1"/>
<rectangle x1="16.256" y1="2.032" x2="16.4592" y2="2.2352" layer="1"/>
<rectangle x1="16.4592" y1="2.032" x2="16.6624" y2="2.2352" layer="1"/>
<rectangle x1="16.6624" y1="2.032" x2="16.8656" y2="2.2352" layer="1"/>
<rectangle x1="16.8656" y1="2.032" x2="17.0688" y2="2.2352" layer="1"/>
<rectangle x1="17.0688" y1="2.032" x2="17.272" y2="2.2352" layer="1"/>
<rectangle x1="17.272" y1="2.032" x2="17.4752" y2="2.2352" layer="1"/>
<rectangle x1="8.9408" y1="2.2352" x2="9.144" y2="2.4384" layer="1"/>
<rectangle x1="9.144" y1="2.2352" x2="9.3472" y2="2.4384" layer="1"/>
<rectangle x1="9.3472" y1="2.2352" x2="9.5504" y2="2.4384" layer="1"/>
<rectangle x1="9.5504" y1="2.2352" x2="9.7536" y2="2.4384" layer="1"/>
<rectangle x1="9.7536" y1="2.2352" x2="9.9568" y2="2.4384" layer="1"/>
<rectangle x1="9.9568" y1="2.2352" x2="10.16" y2="2.4384" layer="1"/>
<rectangle x1="10.16" y1="2.2352" x2="10.3632" y2="2.4384" layer="1"/>
<rectangle x1="10.3632" y1="2.2352" x2="10.5664" y2="2.4384" layer="1"/>
<rectangle x1="10.5664" y1="2.2352" x2="10.7696" y2="2.4384" layer="1"/>
<rectangle x1="10.7696" y1="2.2352" x2="10.9728" y2="2.4384" layer="1"/>
<rectangle x1="10.9728" y1="2.2352" x2="11.176" y2="2.4384" layer="1"/>
<rectangle x1="11.176" y1="2.2352" x2="11.3792" y2="2.4384" layer="1"/>
<rectangle x1="11.3792" y1="2.2352" x2="11.5824" y2="2.4384" layer="1"/>
<rectangle x1="11.5824" y1="2.2352" x2="11.7856" y2="2.4384" layer="1"/>
<rectangle x1="11.7856" y1="2.2352" x2="11.9888" y2="2.4384" layer="1"/>
<rectangle x1="14.224" y1="2.2352" x2="14.4272" y2="2.4384" layer="1"/>
<rectangle x1="14.4272" y1="2.2352" x2="14.6304" y2="2.4384" layer="1"/>
<rectangle x1="14.6304" y1="2.2352" x2="14.8336" y2="2.4384" layer="1"/>
<rectangle x1="14.8336" y1="2.2352" x2="15.0368" y2="2.4384" layer="1"/>
<rectangle x1="15.0368" y1="2.2352" x2="15.24" y2="2.4384" layer="1"/>
<rectangle x1="15.24" y1="2.2352" x2="15.4432" y2="2.4384" layer="1"/>
<rectangle x1="15.4432" y1="2.2352" x2="15.6464" y2="2.4384" layer="1"/>
<rectangle x1="15.6464" y1="2.2352" x2="15.8496" y2="2.4384" layer="1"/>
<rectangle x1="15.8496" y1="2.2352" x2="16.0528" y2="2.4384" layer="1"/>
<rectangle x1="16.0528" y1="2.2352" x2="16.256" y2="2.4384" layer="1"/>
<rectangle x1="16.256" y1="2.2352" x2="16.4592" y2="2.4384" layer="1"/>
<rectangle x1="16.4592" y1="2.2352" x2="16.6624" y2="2.4384" layer="1"/>
<rectangle x1="16.6624" y1="2.2352" x2="16.8656" y2="2.4384" layer="1"/>
<rectangle x1="16.8656" y1="2.2352" x2="17.0688" y2="2.4384" layer="1"/>
<rectangle x1="17.0688" y1="2.2352" x2="17.272" y2="2.4384" layer="1"/>
<rectangle x1="8.9408" y1="2.4384" x2="9.144" y2="2.6416" layer="1"/>
<rectangle x1="9.144" y1="2.4384" x2="9.3472" y2="2.6416" layer="1"/>
<rectangle x1="9.3472" y1="2.4384" x2="9.5504" y2="2.6416" layer="1"/>
<rectangle x1="9.5504" y1="2.4384" x2="9.7536" y2="2.6416" layer="1"/>
<rectangle x1="9.7536" y1="2.4384" x2="9.9568" y2="2.6416" layer="1"/>
<rectangle x1="9.9568" y1="2.4384" x2="10.16" y2="2.6416" layer="1"/>
<rectangle x1="10.16" y1="2.4384" x2="10.3632" y2="2.6416" layer="1"/>
<rectangle x1="10.3632" y1="2.4384" x2="10.5664" y2="2.6416" layer="1"/>
<rectangle x1="10.5664" y1="2.4384" x2="10.7696" y2="2.6416" layer="1"/>
<rectangle x1="10.7696" y1="2.4384" x2="10.9728" y2="2.6416" layer="1"/>
<rectangle x1="10.9728" y1="2.4384" x2="11.176" y2="2.6416" layer="1"/>
<rectangle x1="11.176" y1="2.4384" x2="11.3792" y2="2.6416" layer="1"/>
<rectangle x1="11.3792" y1="2.4384" x2="11.5824" y2="2.6416" layer="1"/>
<rectangle x1="11.5824" y1="2.4384" x2="11.7856" y2="2.6416" layer="1"/>
<rectangle x1="11.7856" y1="2.4384" x2="11.9888" y2="2.6416" layer="1"/>
<rectangle x1="14.224" y1="2.4384" x2="14.4272" y2="2.6416" layer="1"/>
<rectangle x1="14.4272" y1="2.4384" x2="14.6304" y2="2.6416" layer="1"/>
<rectangle x1="14.6304" y1="2.4384" x2="14.8336" y2="2.6416" layer="1"/>
<rectangle x1="14.8336" y1="2.4384" x2="15.0368" y2="2.6416" layer="1"/>
<rectangle x1="15.0368" y1="2.4384" x2="15.24" y2="2.6416" layer="1"/>
<rectangle x1="15.24" y1="2.4384" x2="15.4432" y2="2.6416" layer="1"/>
<rectangle x1="15.4432" y1="2.4384" x2="15.6464" y2="2.6416" layer="1"/>
<rectangle x1="15.6464" y1="2.4384" x2="15.8496" y2="2.6416" layer="1"/>
<rectangle x1="15.8496" y1="2.4384" x2="16.0528" y2="2.6416" layer="1"/>
<rectangle x1="16.0528" y1="2.4384" x2="16.256" y2="2.6416" layer="1"/>
<rectangle x1="16.256" y1="2.4384" x2="16.4592" y2="2.6416" layer="1"/>
<rectangle x1="16.4592" y1="2.4384" x2="16.6624" y2="2.6416" layer="1"/>
<rectangle x1="16.6624" y1="2.4384" x2="16.8656" y2="2.6416" layer="1"/>
<rectangle x1="16.8656" y1="2.4384" x2="17.0688" y2="2.6416" layer="1"/>
<rectangle x1="17.0688" y1="2.4384" x2="17.272" y2="2.6416" layer="1"/>
<rectangle x1="9.144" y1="2.6416" x2="9.3472" y2="2.8448" layer="1"/>
<rectangle x1="9.3472" y1="2.6416" x2="9.5504" y2="2.8448" layer="1"/>
<rectangle x1="9.5504" y1="2.6416" x2="9.7536" y2="2.8448" layer="1"/>
<rectangle x1="9.7536" y1="2.6416" x2="9.9568" y2="2.8448" layer="1"/>
<rectangle x1="9.9568" y1="2.6416" x2="10.16" y2="2.8448" layer="1"/>
<rectangle x1="10.16" y1="2.6416" x2="10.3632" y2="2.8448" layer="1"/>
<rectangle x1="10.3632" y1="2.6416" x2="10.5664" y2="2.8448" layer="1"/>
<rectangle x1="10.5664" y1="2.6416" x2="10.7696" y2="2.8448" layer="1"/>
<rectangle x1="10.7696" y1="2.6416" x2="10.9728" y2="2.8448" layer="1"/>
<rectangle x1="10.9728" y1="2.6416" x2="11.176" y2="2.8448" layer="1"/>
<rectangle x1="11.176" y1="2.6416" x2="11.3792" y2="2.8448" layer="1"/>
<rectangle x1="11.3792" y1="2.6416" x2="11.5824" y2="2.8448" layer="1"/>
<rectangle x1="11.5824" y1="2.6416" x2="11.7856" y2="2.8448" layer="1"/>
<rectangle x1="14.4272" y1="2.6416" x2="14.6304" y2="2.8448" layer="1"/>
<rectangle x1="14.6304" y1="2.6416" x2="14.8336" y2="2.8448" layer="1"/>
<rectangle x1="14.8336" y1="2.6416" x2="15.0368" y2="2.8448" layer="1"/>
<rectangle x1="15.0368" y1="2.6416" x2="15.24" y2="2.8448" layer="1"/>
<rectangle x1="15.24" y1="2.6416" x2="15.4432" y2="2.8448" layer="1"/>
<rectangle x1="15.4432" y1="2.6416" x2="15.6464" y2="2.8448" layer="1"/>
<rectangle x1="15.6464" y1="2.6416" x2="15.8496" y2="2.8448" layer="1"/>
<rectangle x1="15.8496" y1="2.6416" x2="16.0528" y2="2.8448" layer="1"/>
<rectangle x1="16.0528" y1="2.6416" x2="16.256" y2="2.8448" layer="1"/>
<rectangle x1="16.256" y1="2.6416" x2="16.4592" y2="2.8448" layer="1"/>
<rectangle x1="16.4592" y1="2.6416" x2="16.6624" y2="2.8448" layer="1"/>
<rectangle x1="16.6624" y1="2.6416" x2="16.8656" y2="2.8448" layer="1"/>
<rectangle x1="16.8656" y1="2.6416" x2="17.0688" y2="2.8448" layer="1"/>
<rectangle x1="9.144" y1="2.8448" x2="9.3472" y2="3.048" layer="1"/>
<rectangle x1="9.3472" y1="2.8448" x2="9.5504" y2="3.048" layer="1"/>
<rectangle x1="9.5504" y1="2.8448" x2="9.7536" y2="3.048" layer="1"/>
<rectangle x1="9.7536" y1="2.8448" x2="9.9568" y2="3.048" layer="1"/>
<rectangle x1="9.9568" y1="2.8448" x2="10.16" y2="3.048" layer="1"/>
<rectangle x1="10.16" y1="2.8448" x2="10.3632" y2="3.048" layer="1"/>
<rectangle x1="10.3632" y1="2.8448" x2="10.5664" y2="3.048" layer="1"/>
<rectangle x1="10.5664" y1="2.8448" x2="10.7696" y2="3.048" layer="1"/>
<rectangle x1="10.7696" y1="2.8448" x2="10.9728" y2="3.048" layer="1"/>
<rectangle x1="10.9728" y1="2.8448" x2="11.176" y2="3.048" layer="1"/>
<rectangle x1="11.176" y1="2.8448" x2="11.3792" y2="3.048" layer="1"/>
<rectangle x1="11.3792" y1="2.8448" x2="11.5824" y2="3.048" layer="1"/>
<rectangle x1="11.5824" y1="2.8448" x2="11.7856" y2="3.048" layer="1"/>
<rectangle x1="14.4272" y1="2.8448" x2="14.6304" y2="3.048" layer="1"/>
<rectangle x1="14.6304" y1="2.8448" x2="14.8336" y2="3.048" layer="1"/>
<rectangle x1="14.8336" y1="2.8448" x2="15.0368" y2="3.048" layer="1"/>
<rectangle x1="15.0368" y1="2.8448" x2="15.24" y2="3.048" layer="1"/>
<rectangle x1="15.24" y1="2.8448" x2="15.4432" y2="3.048" layer="1"/>
<rectangle x1="15.4432" y1="2.8448" x2="15.6464" y2="3.048" layer="1"/>
<rectangle x1="15.6464" y1="2.8448" x2="15.8496" y2="3.048" layer="1"/>
<rectangle x1="15.8496" y1="2.8448" x2="16.0528" y2="3.048" layer="1"/>
<rectangle x1="16.0528" y1="2.8448" x2="16.256" y2="3.048" layer="1"/>
<rectangle x1="16.256" y1="2.8448" x2="16.4592" y2="3.048" layer="1"/>
<rectangle x1="16.4592" y1="2.8448" x2="16.6624" y2="3.048" layer="1"/>
<rectangle x1="16.6624" y1="2.8448" x2="16.8656" y2="3.048" layer="1"/>
<rectangle x1="16.8656" y1="2.8448" x2="17.0688" y2="3.048" layer="1"/>
<rectangle x1="9.3472" y1="3.048" x2="9.5504" y2="3.2512" layer="1"/>
<rectangle x1="9.5504" y1="3.048" x2="9.7536" y2="3.2512" layer="1"/>
<rectangle x1="9.7536" y1="3.048" x2="9.9568" y2="3.2512" layer="1"/>
<rectangle x1="9.9568" y1="3.048" x2="10.16" y2="3.2512" layer="1"/>
<rectangle x1="10.16" y1="3.048" x2="10.3632" y2="3.2512" layer="1"/>
<rectangle x1="10.3632" y1="3.048" x2="10.5664" y2="3.2512" layer="1"/>
<rectangle x1="10.5664" y1="3.048" x2="10.7696" y2="3.2512" layer="1"/>
<rectangle x1="10.7696" y1="3.048" x2="10.9728" y2="3.2512" layer="1"/>
<rectangle x1="10.9728" y1="3.048" x2="11.176" y2="3.2512" layer="1"/>
<rectangle x1="11.176" y1="3.048" x2="11.3792" y2="3.2512" layer="1"/>
<rectangle x1="11.3792" y1="3.048" x2="11.5824" y2="3.2512" layer="1"/>
<rectangle x1="14.6304" y1="3.048" x2="14.8336" y2="3.2512" layer="1"/>
<rectangle x1="14.8336" y1="3.048" x2="15.0368" y2="3.2512" layer="1"/>
<rectangle x1="15.0368" y1="3.048" x2="15.24" y2="3.2512" layer="1"/>
<rectangle x1="15.24" y1="3.048" x2="15.4432" y2="3.2512" layer="1"/>
<rectangle x1="15.4432" y1="3.048" x2="15.6464" y2="3.2512" layer="1"/>
<rectangle x1="15.6464" y1="3.048" x2="15.8496" y2="3.2512" layer="1"/>
<rectangle x1="15.8496" y1="3.048" x2="16.0528" y2="3.2512" layer="1"/>
<rectangle x1="16.0528" y1="3.048" x2="16.256" y2="3.2512" layer="1"/>
<rectangle x1="16.256" y1="3.048" x2="16.4592" y2="3.2512" layer="1"/>
<rectangle x1="16.4592" y1="3.048" x2="16.6624" y2="3.2512" layer="1"/>
<rectangle x1="16.6624" y1="3.048" x2="16.8656" y2="3.2512" layer="1"/>
<rectangle x1="9.3472" y1="3.2512" x2="9.5504" y2="3.4544" layer="1"/>
<rectangle x1="9.5504" y1="3.2512" x2="9.7536" y2="3.4544" layer="1"/>
<rectangle x1="9.7536" y1="3.2512" x2="9.9568" y2="3.4544" layer="1"/>
<rectangle x1="9.9568" y1="3.2512" x2="10.16" y2="3.4544" layer="1"/>
<rectangle x1="10.16" y1="3.2512" x2="10.3632" y2="3.4544" layer="1"/>
<rectangle x1="10.3632" y1="3.2512" x2="10.5664" y2="3.4544" layer="1"/>
<rectangle x1="10.5664" y1="3.2512" x2="10.7696" y2="3.4544" layer="1"/>
<rectangle x1="10.7696" y1="3.2512" x2="10.9728" y2="3.4544" layer="1"/>
<rectangle x1="10.9728" y1="3.2512" x2="11.176" y2="3.4544" layer="1"/>
<rectangle x1="11.176" y1="3.2512" x2="11.3792" y2="3.4544" layer="1"/>
<rectangle x1="11.3792" y1="3.2512" x2="11.5824" y2="3.4544" layer="1"/>
<rectangle x1="14.6304" y1="3.2512" x2="14.8336" y2="3.4544" layer="1"/>
<rectangle x1="14.8336" y1="3.2512" x2="15.0368" y2="3.4544" layer="1"/>
<rectangle x1="15.0368" y1="3.2512" x2="15.24" y2="3.4544" layer="1"/>
<rectangle x1="15.24" y1="3.2512" x2="15.4432" y2="3.4544" layer="1"/>
<rectangle x1="15.4432" y1="3.2512" x2="15.6464" y2="3.4544" layer="1"/>
<rectangle x1="15.6464" y1="3.2512" x2="15.8496" y2="3.4544" layer="1"/>
<rectangle x1="15.8496" y1="3.2512" x2="16.0528" y2="3.4544" layer="1"/>
<rectangle x1="16.0528" y1="3.2512" x2="16.256" y2="3.4544" layer="1"/>
<rectangle x1="16.256" y1="3.2512" x2="16.4592" y2="3.4544" layer="1"/>
<rectangle x1="16.4592" y1="3.2512" x2="16.6624" y2="3.4544" layer="1"/>
<rectangle x1="16.6624" y1="3.2512" x2="16.8656" y2="3.4544" layer="1"/>
<rectangle x1="9.5504" y1="3.4544" x2="9.7536" y2="3.6576" layer="1"/>
<rectangle x1="9.7536" y1="3.4544" x2="9.9568" y2="3.6576" layer="1"/>
<rectangle x1="9.9568" y1="3.4544" x2="10.16" y2="3.6576" layer="1"/>
<rectangle x1="10.16" y1="3.4544" x2="10.3632" y2="3.6576" layer="1"/>
<rectangle x1="10.3632" y1="3.4544" x2="10.5664" y2="3.6576" layer="1"/>
<rectangle x1="10.5664" y1="3.4544" x2="10.7696" y2="3.6576" layer="1"/>
<rectangle x1="10.7696" y1="3.4544" x2="10.9728" y2="3.6576" layer="1"/>
<rectangle x1="10.9728" y1="3.4544" x2="11.176" y2="3.6576" layer="1"/>
<rectangle x1="11.176" y1="3.4544" x2="11.3792" y2="3.6576" layer="1"/>
<rectangle x1="14.8336" y1="3.4544" x2="15.0368" y2="3.6576" layer="1"/>
<rectangle x1="15.0368" y1="3.4544" x2="15.24" y2="3.6576" layer="1"/>
<rectangle x1="15.24" y1="3.4544" x2="15.4432" y2="3.6576" layer="1"/>
<rectangle x1="15.4432" y1="3.4544" x2="15.6464" y2="3.6576" layer="1"/>
<rectangle x1="15.6464" y1="3.4544" x2="15.8496" y2="3.6576" layer="1"/>
<rectangle x1="15.8496" y1="3.4544" x2="16.0528" y2="3.6576" layer="1"/>
<rectangle x1="16.0528" y1="3.4544" x2="16.256" y2="3.6576" layer="1"/>
<rectangle x1="16.256" y1="3.4544" x2="16.4592" y2="3.6576" layer="1"/>
<rectangle x1="16.4592" y1="3.4544" x2="16.6624" y2="3.6576" layer="1"/>
<rectangle x1="9.5504" y1="3.6576" x2="9.7536" y2="3.8608" layer="1"/>
<rectangle x1="9.7536" y1="3.6576" x2="9.9568" y2="3.8608" layer="1"/>
<rectangle x1="9.9568" y1="3.6576" x2="10.16" y2="3.8608" layer="1"/>
<rectangle x1="10.16" y1="3.6576" x2="10.3632" y2="3.8608" layer="1"/>
<rectangle x1="10.3632" y1="3.6576" x2="10.5664" y2="3.8608" layer="1"/>
<rectangle x1="10.5664" y1="3.6576" x2="10.7696" y2="3.8608" layer="1"/>
<rectangle x1="10.7696" y1="3.6576" x2="10.9728" y2="3.8608" layer="1"/>
<rectangle x1="10.9728" y1="3.6576" x2="11.176" y2="3.8608" layer="1"/>
<rectangle x1="11.176" y1="3.6576" x2="11.3792" y2="3.8608" layer="1"/>
<rectangle x1="14.8336" y1="3.6576" x2="15.0368" y2="3.8608" layer="1"/>
<rectangle x1="15.0368" y1="3.6576" x2="15.24" y2="3.8608" layer="1"/>
<rectangle x1="15.24" y1="3.6576" x2="15.4432" y2="3.8608" layer="1"/>
<rectangle x1="15.4432" y1="3.6576" x2="15.6464" y2="3.8608" layer="1"/>
<rectangle x1="15.6464" y1="3.6576" x2="15.8496" y2="3.8608" layer="1"/>
<rectangle x1="15.8496" y1="3.6576" x2="16.0528" y2="3.8608" layer="1"/>
<rectangle x1="16.0528" y1="3.6576" x2="16.256" y2="3.8608" layer="1"/>
<rectangle x1="16.256" y1="3.6576" x2="16.4592" y2="3.8608" layer="1"/>
<rectangle x1="16.4592" y1="3.6576" x2="16.6624" y2="3.8608" layer="1"/>
<rectangle x1="9.7536" y1="3.8608" x2="9.9568" y2="4.064" layer="1"/>
<rectangle x1="9.9568" y1="3.8608" x2="10.16" y2="4.064" layer="1"/>
<rectangle x1="10.16" y1="3.8608" x2="10.3632" y2="4.064" layer="1"/>
<rectangle x1="10.3632" y1="3.8608" x2="10.5664" y2="4.064" layer="1"/>
<rectangle x1="10.5664" y1="3.8608" x2="10.7696" y2="4.064" layer="1"/>
<rectangle x1="10.7696" y1="3.8608" x2="10.9728" y2="4.064" layer="1"/>
<rectangle x1="10.9728" y1="3.8608" x2="11.176" y2="4.064" layer="1"/>
<rectangle x1="15.0368" y1="3.8608" x2="15.24" y2="4.064" layer="1"/>
<rectangle x1="15.24" y1="3.8608" x2="15.4432" y2="4.064" layer="1"/>
<rectangle x1="15.4432" y1="3.8608" x2="15.6464" y2="4.064" layer="1"/>
<rectangle x1="15.6464" y1="3.8608" x2="15.8496" y2="4.064" layer="1"/>
<rectangle x1="15.8496" y1="3.8608" x2="16.0528" y2="4.064" layer="1"/>
<rectangle x1="16.0528" y1="3.8608" x2="16.256" y2="4.064" layer="1"/>
<rectangle x1="16.256" y1="3.8608" x2="16.4592" y2="4.064" layer="1"/>
<rectangle x1="9.7536" y1="4.064" x2="9.9568" y2="4.2672" layer="1"/>
<rectangle x1="9.9568" y1="4.064" x2="10.16" y2="4.2672" layer="1"/>
<rectangle x1="10.16" y1="4.064" x2="10.3632" y2="4.2672" layer="1"/>
<rectangle x1="10.3632" y1="4.064" x2="10.5664" y2="4.2672" layer="1"/>
<rectangle x1="10.5664" y1="4.064" x2="10.7696" y2="4.2672" layer="1"/>
<rectangle x1="10.7696" y1="4.064" x2="10.9728" y2="4.2672" layer="1"/>
<rectangle x1="10.9728" y1="4.064" x2="11.176" y2="4.2672" layer="1"/>
<rectangle x1="15.0368" y1="4.064" x2="15.24" y2="4.2672" layer="1"/>
<rectangle x1="15.24" y1="4.064" x2="15.4432" y2="4.2672" layer="1"/>
<rectangle x1="15.4432" y1="4.064" x2="15.6464" y2="4.2672" layer="1"/>
<rectangle x1="15.6464" y1="4.064" x2="15.8496" y2="4.2672" layer="1"/>
<rectangle x1="15.8496" y1="4.064" x2="16.0528" y2="4.2672" layer="1"/>
<rectangle x1="16.0528" y1="4.064" x2="16.256" y2="4.2672" layer="1"/>
<rectangle x1="16.256" y1="4.064" x2="16.4592" y2="4.2672" layer="1"/>
<rectangle x1="9.9568" y1="4.2672" x2="10.16" y2="4.4704" layer="1"/>
<rectangle x1="10.16" y1="4.2672" x2="10.3632" y2="4.4704" layer="1"/>
<rectangle x1="10.3632" y1="4.2672" x2="10.5664" y2="4.4704" layer="1"/>
<rectangle x1="10.5664" y1="4.2672" x2="10.7696" y2="4.4704" layer="1"/>
<rectangle x1="10.7696" y1="4.2672" x2="10.9728" y2="4.4704" layer="1"/>
<rectangle x1="15.24" y1="4.2672" x2="15.4432" y2="4.4704" layer="1"/>
<rectangle x1="15.4432" y1="4.2672" x2="15.6464" y2="4.4704" layer="1"/>
<rectangle x1="15.6464" y1="4.2672" x2="15.8496" y2="4.4704" layer="1"/>
<rectangle x1="15.8496" y1="4.2672" x2="16.0528" y2="4.4704" layer="1"/>
<rectangle x1="16.0528" y1="4.2672" x2="16.256" y2="4.4704" layer="1"/>
<rectangle x1="9.9568" y1="4.4704" x2="10.16" y2="4.6736" layer="1"/>
<rectangle x1="10.16" y1="4.4704" x2="10.3632" y2="4.6736" layer="1"/>
<rectangle x1="10.3632" y1="4.4704" x2="10.5664" y2="4.6736" layer="1"/>
<rectangle x1="10.5664" y1="4.4704" x2="10.7696" y2="4.6736" layer="1"/>
<rectangle x1="10.7696" y1="4.4704" x2="10.9728" y2="4.6736" layer="1"/>
<rectangle x1="15.24" y1="4.4704" x2="15.4432" y2="4.6736" layer="1"/>
<rectangle x1="15.4432" y1="4.4704" x2="15.6464" y2="4.6736" layer="1"/>
<rectangle x1="15.6464" y1="4.4704" x2="15.8496" y2="4.6736" layer="1"/>
<rectangle x1="15.8496" y1="4.4704" x2="16.0528" y2="4.6736" layer="1"/>
<rectangle x1="16.0528" y1="4.4704" x2="16.256" y2="4.6736" layer="1"/>
<rectangle x1="10.16" y1="4.6736" x2="10.3632" y2="4.8768" layer="1"/>
<rectangle x1="10.3632" y1="4.6736" x2="10.5664" y2="4.8768" layer="1"/>
<rectangle x1="10.5664" y1="4.6736" x2="10.7696" y2="4.8768" layer="1"/>
<rectangle x1="15.4432" y1="4.6736" x2="15.6464" y2="4.8768" layer="1"/>
<rectangle x1="15.6464" y1="4.6736" x2="15.8496" y2="4.8768" layer="1"/>
<rectangle x1="15.8496" y1="4.6736" x2="16.0528" y2="4.8768" layer="1"/>
<rectangle x1="10.16" y1="4.8768" x2="10.3632" y2="5.08" layer="1"/>
<rectangle x1="10.3632" y1="4.8768" x2="10.5664" y2="5.08" layer="1"/>
<rectangle x1="10.5664" y1="4.8768" x2="10.7696" y2="5.08" layer="1"/>
<rectangle x1="15.4432" y1="4.8768" x2="15.6464" y2="5.08" layer="1"/>
<rectangle x1="15.6464" y1="4.8768" x2="15.8496" y2="5.08" layer="1"/>
<rectangle x1="15.8496" y1="4.8768" x2="16.0528" y2="5.08" layer="1"/>
<rectangle x1="10.3632" y1="5.08" x2="10.5664" y2="5.2832" layer="1"/>
<rectangle x1="15.6464" y1="5.08" x2="15.8496" y2="5.2832" layer="1"/>
<rectangle x1="10.3632" y1="5.2832" x2="10.5664" y2="5.4864" layer="1"/>
<rectangle x1="15.6464" y1="5.2832" x2="15.8496" y2="5.4864" layer="1"/>
<rectangle x1="8.3312" y1="6.2992" x2="8.5344" y2="6.5024" layer="1"/>
<rectangle x1="8.5344" y1="6.2992" x2="8.7376" y2="6.5024" layer="1"/>
<rectangle x1="8.7376" y1="6.2992" x2="8.9408" y2="6.5024" layer="1"/>
<rectangle x1="8.9408" y1="6.2992" x2="9.144" y2="6.5024" layer="1"/>
<rectangle x1="9.144" y1="6.2992" x2="9.3472" y2="6.5024" layer="1"/>
<rectangle x1="9.3472" y1="6.2992" x2="9.5504" y2="6.5024" layer="1"/>
<rectangle x1="9.5504" y1="6.2992" x2="9.7536" y2="6.5024" layer="1"/>
<rectangle x1="9.7536" y1="6.2992" x2="9.9568" y2="6.5024" layer="1"/>
<rectangle x1="9.9568" y1="6.2992" x2="10.16" y2="6.5024" layer="1"/>
<rectangle x1="10.16" y1="6.2992" x2="10.3632" y2="6.5024" layer="1"/>
<rectangle x1="10.3632" y1="6.2992" x2="10.5664" y2="6.5024" layer="1"/>
<rectangle x1="10.5664" y1="6.2992" x2="10.7696" y2="6.5024" layer="1"/>
<rectangle x1="10.7696" y1="6.2992" x2="10.9728" y2="6.5024" layer="1"/>
<rectangle x1="10.9728" y1="6.2992" x2="11.176" y2="6.5024" layer="1"/>
<rectangle x1="11.176" y1="6.2992" x2="11.3792" y2="6.5024" layer="1"/>
<rectangle x1="11.3792" y1="6.2992" x2="11.5824" y2="6.5024" layer="1"/>
<rectangle x1="11.5824" y1="6.2992" x2="11.7856" y2="6.5024" layer="1"/>
<rectangle x1="11.7856" y1="6.2992" x2="11.9888" y2="6.5024" layer="1"/>
<rectangle x1="11.9888" y1="6.2992" x2="12.192" y2="6.5024" layer="1"/>
<rectangle x1="12.192" y1="6.2992" x2="12.3952" y2="6.5024" layer="1"/>
<rectangle x1="12.3952" y1="6.2992" x2="12.5984" y2="6.5024" layer="1"/>
<rectangle x1="14.0208" y1="6.2992" x2="14.224" y2="6.5024" layer="1"/>
<rectangle x1="14.224" y1="6.2992" x2="14.4272" y2="6.5024" layer="1"/>
<rectangle x1="14.4272" y1="6.2992" x2="14.6304" y2="6.5024" layer="1"/>
<rectangle x1="14.8336" y1="6.2992" x2="15.0368" y2="6.5024" layer="1"/>
<rectangle x1="15.24" y1="6.2992" x2="15.4432" y2="6.5024" layer="1"/>
<rectangle x1="15.4432" y1="6.2992" x2="15.6464" y2="6.5024" layer="1"/>
<rectangle x1="15.6464" y1="6.2992" x2="15.8496" y2="6.5024" layer="1"/>
<rectangle x1="16.0528" y1="6.2992" x2="16.256" y2="6.5024" layer="1"/>
<rectangle x1="16.4592" y1="6.2992" x2="16.6624" y2="6.5024" layer="1"/>
<rectangle x1="16.6624" y1="6.2992" x2="16.8656" y2="6.5024" layer="1"/>
<rectangle x1="16.8656" y1="6.2992" x2="17.0688" y2="6.5024" layer="1"/>
<rectangle x1="17.272" y1="6.2992" x2="17.4752" y2="6.5024" layer="1"/>
<rectangle x1="8.3312" y1="6.5024" x2="8.5344" y2="6.7056" layer="1"/>
<rectangle x1="8.5344" y1="6.5024" x2="8.7376" y2="6.7056" layer="1"/>
<rectangle x1="8.7376" y1="6.5024" x2="8.9408" y2="6.7056" layer="1"/>
<rectangle x1="8.9408" y1="6.5024" x2="9.144" y2="6.7056" layer="1"/>
<rectangle x1="9.144" y1="6.5024" x2="9.3472" y2="6.7056" layer="1"/>
<rectangle x1="9.3472" y1="6.5024" x2="9.5504" y2="6.7056" layer="1"/>
<rectangle x1="9.5504" y1="6.5024" x2="9.7536" y2="6.7056" layer="1"/>
<rectangle x1="9.7536" y1="6.5024" x2="9.9568" y2="6.7056" layer="1"/>
<rectangle x1="9.9568" y1="6.5024" x2="10.16" y2="6.7056" layer="1"/>
<rectangle x1="10.16" y1="6.5024" x2="10.3632" y2="6.7056" layer="1"/>
<rectangle x1="10.3632" y1="6.5024" x2="10.5664" y2="6.7056" layer="1"/>
<rectangle x1="10.5664" y1="6.5024" x2="10.7696" y2="6.7056" layer="1"/>
<rectangle x1="10.7696" y1="6.5024" x2="10.9728" y2="6.7056" layer="1"/>
<rectangle x1="10.9728" y1="6.5024" x2="11.176" y2="6.7056" layer="1"/>
<rectangle x1="11.176" y1="6.5024" x2="11.3792" y2="6.7056" layer="1"/>
<rectangle x1="11.3792" y1="6.5024" x2="11.5824" y2="6.7056" layer="1"/>
<rectangle x1="11.5824" y1="6.5024" x2="11.7856" y2="6.7056" layer="1"/>
<rectangle x1="11.7856" y1="6.5024" x2="11.9888" y2="6.7056" layer="1"/>
<rectangle x1="11.9888" y1="6.5024" x2="12.192" y2="6.7056" layer="1"/>
<rectangle x1="12.192" y1="6.5024" x2="12.3952" y2="6.7056" layer="1"/>
<rectangle x1="12.3952" y1="6.5024" x2="12.5984" y2="6.7056" layer="1"/>
<rectangle x1="14.224" y1="6.5024" x2="14.4272" y2="6.7056" layer="1"/>
<rectangle x1="15.24" y1="6.5024" x2="15.4432" y2="6.7056" layer="1"/>
<rectangle x1="16.4592" y1="6.5024" x2="16.6624" y2="6.7056" layer="1"/>
<rectangle x1="16.8656" y1="6.5024" x2="17.0688" y2="6.7056" layer="1"/>
<rectangle x1="8.5344" y1="6.7056" x2="8.7376" y2="6.9088" layer="1"/>
<rectangle x1="8.7376" y1="6.7056" x2="8.9408" y2="6.9088" layer="1"/>
<rectangle x1="8.9408" y1="6.7056" x2="9.144" y2="6.9088" layer="1"/>
<rectangle x1="9.144" y1="6.7056" x2="9.3472" y2="6.9088" layer="1"/>
<rectangle x1="9.3472" y1="6.7056" x2="9.5504" y2="6.9088" layer="1"/>
<rectangle x1="9.5504" y1="6.7056" x2="9.7536" y2="6.9088" layer="1"/>
<rectangle x1="9.7536" y1="6.7056" x2="9.9568" y2="6.9088" layer="1"/>
<rectangle x1="9.9568" y1="6.7056" x2="10.16" y2="6.9088" layer="1"/>
<rectangle x1="10.16" y1="6.7056" x2="10.3632" y2="6.9088" layer="1"/>
<rectangle x1="10.3632" y1="6.7056" x2="10.5664" y2="6.9088" layer="1"/>
<rectangle x1="10.5664" y1="6.7056" x2="10.7696" y2="6.9088" layer="1"/>
<rectangle x1="10.7696" y1="6.7056" x2="10.9728" y2="6.9088" layer="1"/>
<rectangle x1="10.9728" y1="6.7056" x2="11.176" y2="6.9088" layer="1"/>
<rectangle x1="11.176" y1="6.7056" x2="11.3792" y2="6.9088" layer="1"/>
<rectangle x1="11.3792" y1="6.7056" x2="11.5824" y2="6.9088" layer="1"/>
<rectangle x1="11.5824" y1="6.7056" x2="11.7856" y2="6.9088" layer="1"/>
<rectangle x1="11.7856" y1="6.7056" x2="11.9888" y2="6.9088" layer="1"/>
<rectangle x1="11.9888" y1="6.7056" x2="12.192" y2="6.9088" layer="1"/>
<rectangle x1="12.192" y1="6.7056" x2="12.3952" y2="6.9088" layer="1"/>
<rectangle x1="14.224" y1="6.7056" x2="14.4272" y2="6.9088" layer="1"/>
<rectangle x1="15.24" y1="6.7056" x2="15.4432" y2="6.9088" layer="1"/>
<rectangle x1="15.4432" y1="6.7056" x2="15.6464" y2="6.9088" layer="1"/>
<rectangle x1="15.6464" y1="6.7056" x2="15.8496" y2="6.9088" layer="1"/>
<rectangle x1="16.4592" y1="6.7056" x2="16.6624" y2="6.9088" layer="1"/>
<rectangle x1="16.6624" y1="6.7056" x2="16.8656" y2="6.9088" layer="1"/>
<rectangle x1="16.8656" y1="6.7056" x2="17.0688" y2="6.9088" layer="1"/>
<rectangle x1="8.5344" y1="6.9088" x2="8.7376" y2="7.112" layer="1"/>
<rectangle x1="8.7376" y1="6.9088" x2="8.9408" y2="7.112" layer="1"/>
<rectangle x1="8.9408" y1="6.9088" x2="9.144" y2="7.112" layer="1"/>
<rectangle x1="9.144" y1="6.9088" x2="9.3472" y2="7.112" layer="1"/>
<rectangle x1="9.3472" y1="6.9088" x2="9.5504" y2="7.112" layer="1"/>
<rectangle x1="9.5504" y1="6.9088" x2="9.7536" y2="7.112" layer="1"/>
<rectangle x1="9.7536" y1="6.9088" x2="9.9568" y2="7.112" layer="1"/>
<rectangle x1="9.9568" y1="6.9088" x2="10.16" y2="7.112" layer="1"/>
<rectangle x1="10.16" y1="6.9088" x2="10.3632" y2="7.112" layer="1"/>
<rectangle x1="10.3632" y1="6.9088" x2="10.5664" y2="7.112" layer="1"/>
<rectangle x1="10.5664" y1="6.9088" x2="10.7696" y2="7.112" layer="1"/>
<rectangle x1="10.7696" y1="6.9088" x2="10.9728" y2="7.112" layer="1"/>
<rectangle x1="10.9728" y1="6.9088" x2="11.176" y2="7.112" layer="1"/>
<rectangle x1="11.176" y1="6.9088" x2="11.3792" y2="7.112" layer="1"/>
<rectangle x1="11.3792" y1="6.9088" x2="11.5824" y2="7.112" layer="1"/>
<rectangle x1="11.5824" y1="6.9088" x2="11.7856" y2="7.112" layer="1"/>
<rectangle x1="11.7856" y1="6.9088" x2="11.9888" y2="7.112" layer="1"/>
<rectangle x1="11.9888" y1="6.9088" x2="12.192" y2="7.112" layer="1"/>
<rectangle x1="12.192" y1="6.9088" x2="12.3952" y2="7.112" layer="1"/>
<rectangle x1="14.0208" y1="6.9088" x2="14.224" y2="7.112" layer="1"/>
<rectangle x1="14.224" y1="6.9088" x2="14.4272" y2="7.112" layer="1"/>
<rectangle x1="15.6464" y1="6.9088" x2="15.8496" y2="7.112" layer="1"/>
<rectangle x1="16.4592" y1="6.9088" x2="16.6624" y2="7.112" layer="1"/>
<rectangle x1="8.7376" y1="7.112" x2="8.9408" y2="7.3152" layer="1"/>
<rectangle x1="8.9408" y1="7.112" x2="9.144" y2="7.3152" layer="1"/>
<rectangle x1="9.144" y1="7.112" x2="9.3472" y2="7.3152" layer="1"/>
<rectangle x1="9.3472" y1="7.112" x2="9.5504" y2="7.3152" layer="1"/>
<rectangle x1="9.5504" y1="7.112" x2="9.7536" y2="7.3152" layer="1"/>
<rectangle x1="9.7536" y1="7.112" x2="9.9568" y2="7.3152" layer="1"/>
<rectangle x1="9.9568" y1="7.112" x2="10.16" y2="7.3152" layer="1"/>
<rectangle x1="10.16" y1="7.112" x2="10.3632" y2="7.3152" layer="1"/>
<rectangle x1="10.3632" y1="7.112" x2="10.5664" y2="7.3152" layer="1"/>
<rectangle x1="10.5664" y1="7.112" x2="10.7696" y2="7.3152" layer="1"/>
<rectangle x1="10.7696" y1="7.112" x2="10.9728" y2="7.3152" layer="1"/>
<rectangle x1="10.9728" y1="7.112" x2="11.176" y2="7.3152" layer="1"/>
<rectangle x1="11.176" y1="7.112" x2="11.3792" y2="7.3152" layer="1"/>
<rectangle x1="11.3792" y1="7.112" x2="11.5824" y2="7.3152" layer="1"/>
<rectangle x1="11.5824" y1="7.112" x2="11.7856" y2="7.3152" layer="1"/>
<rectangle x1="11.7856" y1="7.112" x2="11.9888" y2="7.3152" layer="1"/>
<rectangle x1="11.9888" y1="7.112" x2="12.192" y2="7.3152" layer="1"/>
<rectangle x1="14.224" y1="7.112" x2="14.4272" y2="7.3152" layer="1"/>
<rectangle x1="15.24" y1="7.112" x2="15.4432" y2="7.3152" layer="1"/>
<rectangle x1="15.4432" y1="7.112" x2="15.6464" y2="7.3152" layer="1"/>
<rectangle x1="15.6464" y1="7.112" x2="15.8496" y2="7.3152" layer="1"/>
<rectangle x1="16.4592" y1="7.112" x2="16.6624" y2="7.3152" layer="1"/>
<rectangle x1="16.6624" y1="7.112" x2="16.8656" y2="7.3152" layer="1"/>
<rectangle x1="16.8656" y1="7.112" x2="17.0688" y2="7.3152" layer="1"/>
<rectangle x1="8.7376" y1="7.3152" x2="8.9408" y2="7.5184" layer="1"/>
<rectangle x1="8.9408" y1="7.3152" x2="9.144" y2="7.5184" layer="1"/>
<rectangle x1="9.144" y1="7.3152" x2="9.3472" y2="7.5184" layer="1"/>
<rectangle x1="9.3472" y1="7.3152" x2="9.5504" y2="7.5184" layer="1"/>
<rectangle x1="9.5504" y1="7.3152" x2="9.7536" y2="7.5184" layer="1"/>
<rectangle x1="9.7536" y1="7.3152" x2="9.9568" y2="7.5184" layer="1"/>
<rectangle x1="9.9568" y1="7.3152" x2="10.16" y2="7.5184" layer="1"/>
<rectangle x1="10.16" y1="7.3152" x2="10.3632" y2="7.5184" layer="1"/>
<rectangle x1="10.3632" y1="7.3152" x2="10.5664" y2="7.5184" layer="1"/>
<rectangle x1="10.5664" y1="7.3152" x2="10.7696" y2="7.5184" layer="1"/>
<rectangle x1="10.7696" y1="7.3152" x2="10.9728" y2="7.5184" layer="1"/>
<rectangle x1="10.9728" y1="7.3152" x2="11.176" y2="7.5184" layer="1"/>
<rectangle x1="11.176" y1="7.3152" x2="11.3792" y2="7.5184" layer="1"/>
<rectangle x1="11.3792" y1="7.3152" x2="11.5824" y2="7.5184" layer="1"/>
<rectangle x1="11.5824" y1="7.3152" x2="11.7856" y2="7.5184" layer="1"/>
<rectangle x1="11.7856" y1="7.3152" x2="11.9888" y2="7.5184" layer="1"/>
<rectangle x1="11.9888" y1="7.3152" x2="12.192" y2="7.5184" layer="1"/>
<rectangle x1="8.9408" y1="7.5184" x2="9.144" y2="7.7216" layer="1"/>
<rectangle x1="9.144" y1="7.5184" x2="9.3472" y2="7.7216" layer="1"/>
<rectangle x1="9.3472" y1="7.5184" x2="9.5504" y2="7.7216" layer="1"/>
<rectangle x1="9.5504" y1="7.5184" x2="9.7536" y2="7.7216" layer="1"/>
<rectangle x1="9.7536" y1="7.5184" x2="9.9568" y2="7.7216" layer="1"/>
<rectangle x1="9.9568" y1="7.5184" x2="10.16" y2="7.7216" layer="1"/>
<rectangle x1="10.16" y1="7.5184" x2="10.3632" y2="7.7216" layer="1"/>
<rectangle x1="10.3632" y1="7.5184" x2="10.5664" y2="7.7216" layer="1"/>
<rectangle x1="10.5664" y1="7.5184" x2="10.7696" y2="7.7216" layer="1"/>
<rectangle x1="10.7696" y1="7.5184" x2="10.9728" y2="7.7216" layer="1"/>
<rectangle x1="10.9728" y1="7.5184" x2="11.176" y2="7.7216" layer="1"/>
<rectangle x1="11.176" y1="7.5184" x2="11.3792" y2="7.7216" layer="1"/>
<rectangle x1="11.3792" y1="7.5184" x2="11.5824" y2="7.7216" layer="1"/>
<rectangle x1="11.5824" y1="7.5184" x2="11.7856" y2="7.7216" layer="1"/>
<rectangle x1="11.7856" y1="7.5184" x2="11.9888" y2="7.7216" layer="1"/>
<rectangle x1="8.9408" y1="7.7216" x2="9.144" y2="7.9248" layer="1"/>
<rectangle x1="9.144" y1="7.7216" x2="9.3472" y2="7.9248" layer="1"/>
<rectangle x1="9.3472" y1="7.7216" x2="9.5504" y2="7.9248" layer="1"/>
<rectangle x1="9.5504" y1="7.7216" x2="9.7536" y2="7.9248" layer="1"/>
<rectangle x1="9.7536" y1="7.7216" x2="9.9568" y2="7.9248" layer="1"/>
<rectangle x1="9.9568" y1="7.7216" x2="10.16" y2="7.9248" layer="1"/>
<rectangle x1="10.16" y1="7.7216" x2="10.3632" y2="7.9248" layer="1"/>
<rectangle x1="10.3632" y1="7.7216" x2="10.5664" y2="7.9248" layer="1"/>
<rectangle x1="10.5664" y1="7.7216" x2="10.7696" y2="7.9248" layer="1"/>
<rectangle x1="10.7696" y1="7.7216" x2="10.9728" y2="7.9248" layer="1"/>
<rectangle x1="10.9728" y1="7.7216" x2="11.176" y2="7.9248" layer="1"/>
<rectangle x1="11.176" y1="7.7216" x2="11.3792" y2="7.9248" layer="1"/>
<rectangle x1="11.3792" y1="7.7216" x2="11.5824" y2="7.9248" layer="1"/>
<rectangle x1="11.5824" y1="7.7216" x2="11.7856" y2="7.9248" layer="1"/>
<rectangle x1="11.7856" y1="7.7216" x2="11.9888" y2="7.9248" layer="1"/>
<rectangle x1="9.144" y1="7.9248" x2="9.3472" y2="8.128" layer="1"/>
<rectangle x1="9.3472" y1="7.9248" x2="9.5504" y2="8.128" layer="1"/>
<rectangle x1="9.5504" y1="7.9248" x2="9.7536" y2="8.128" layer="1"/>
<rectangle x1="9.7536" y1="7.9248" x2="9.9568" y2="8.128" layer="1"/>
<rectangle x1="9.9568" y1="7.9248" x2="10.16" y2="8.128" layer="1"/>
<rectangle x1="10.16" y1="7.9248" x2="10.3632" y2="8.128" layer="1"/>
<rectangle x1="10.3632" y1="7.9248" x2="10.5664" y2="8.128" layer="1"/>
<rectangle x1="10.5664" y1="7.9248" x2="10.7696" y2="8.128" layer="1"/>
<rectangle x1="10.7696" y1="7.9248" x2="10.9728" y2="8.128" layer="1"/>
<rectangle x1="10.9728" y1="7.9248" x2="11.176" y2="8.128" layer="1"/>
<rectangle x1="11.176" y1="7.9248" x2="11.3792" y2="8.128" layer="1"/>
<rectangle x1="11.3792" y1="7.9248" x2="11.5824" y2="8.128" layer="1"/>
<rectangle x1="11.5824" y1="7.9248" x2="11.7856" y2="8.128" layer="1"/>
<rectangle x1="9.144" y1="8.128" x2="9.3472" y2="8.3312" layer="1"/>
<rectangle x1="9.3472" y1="8.128" x2="9.5504" y2="8.3312" layer="1"/>
<rectangle x1="9.5504" y1="8.128" x2="9.7536" y2="8.3312" layer="1"/>
<rectangle x1="9.7536" y1="8.128" x2="9.9568" y2="8.3312" layer="1"/>
<rectangle x1="9.9568" y1="8.128" x2="10.16" y2="8.3312" layer="1"/>
<rectangle x1="10.16" y1="8.128" x2="10.3632" y2="8.3312" layer="1"/>
<rectangle x1="10.3632" y1="8.128" x2="10.5664" y2="8.3312" layer="1"/>
<rectangle x1="10.5664" y1="8.128" x2="10.7696" y2="8.3312" layer="1"/>
<rectangle x1="10.7696" y1="8.128" x2="10.9728" y2="8.3312" layer="1"/>
<rectangle x1="10.9728" y1="8.128" x2="11.176" y2="8.3312" layer="1"/>
<rectangle x1="11.176" y1="8.128" x2="11.3792" y2="8.3312" layer="1"/>
<rectangle x1="11.3792" y1="8.128" x2="11.5824" y2="8.3312" layer="1"/>
<rectangle x1="11.5824" y1="8.128" x2="11.7856" y2="8.3312" layer="1"/>
<rectangle x1="9.3472" y1="8.3312" x2="9.5504" y2="8.5344" layer="1"/>
<rectangle x1="9.5504" y1="8.3312" x2="9.7536" y2="8.5344" layer="1"/>
<rectangle x1="9.7536" y1="8.3312" x2="9.9568" y2="8.5344" layer="1"/>
<rectangle x1="9.9568" y1="8.3312" x2="10.16" y2="8.5344" layer="1"/>
<rectangle x1="10.16" y1="8.3312" x2="10.3632" y2="8.5344" layer="1"/>
<rectangle x1="10.3632" y1="8.3312" x2="10.5664" y2="8.5344" layer="1"/>
<rectangle x1="10.5664" y1="8.3312" x2="10.7696" y2="8.5344" layer="1"/>
<rectangle x1="10.7696" y1="8.3312" x2="10.9728" y2="8.5344" layer="1"/>
<rectangle x1="10.9728" y1="8.3312" x2="11.176" y2="8.5344" layer="1"/>
<rectangle x1="11.176" y1="8.3312" x2="11.3792" y2="8.5344" layer="1"/>
<rectangle x1="11.3792" y1="8.3312" x2="11.5824" y2="8.5344" layer="1"/>
<rectangle x1="9.3472" y1="8.5344" x2="9.5504" y2="8.7376" layer="1"/>
<rectangle x1="9.5504" y1="8.5344" x2="9.7536" y2="8.7376" layer="1"/>
<rectangle x1="9.7536" y1="8.5344" x2="9.9568" y2="8.7376" layer="1"/>
<rectangle x1="9.9568" y1="8.5344" x2="10.16" y2="8.7376" layer="1"/>
<rectangle x1="10.16" y1="8.5344" x2="10.3632" y2="8.7376" layer="1"/>
<rectangle x1="10.3632" y1="8.5344" x2="10.5664" y2="8.7376" layer="1"/>
<rectangle x1="10.5664" y1="8.5344" x2="10.7696" y2="8.7376" layer="1"/>
<rectangle x1="10.7696" y1="8.5344" x2="10.9728" y2="8.7376" layer="1"/>
<rectangle x1="10.9728" y1="8.5344" x2="11.176" y2="8.7376" layer="1"/>
<rectangle x1="11.176" y1="8.5344" x2="11.3792" y2="8.7376" layer="1"/>
<rectangle x1="11.3792" y1="8.5344" x2="11.5824" y2="8.7376" layer="1"/>
<rectangle x1="9.5504" y1="8.7376" x2="9.7536" y2="8.9408" layer="1"/>
<rectangle x1="9.7536" y1="8.7376" x2="9.9568" y2="8.9408" layer="1"/>
<rectangle x1="9.9568" y1="8.7376" x2="10.16" y2="8.9408" layer="1"/>
<rectangle x1="10.16" y1="8.7376" x2="10.3632" y2="8.9408" layer="1"/>
<rectangle x1="10.3632" y1="8.7376" x2="10.5664" y2="8.9408" layer="1"/>
<rectangle x1="10.5664" y1="8.7376" x2="10.7696" y2="8.9408" layer="1"/>
<rectangle x1="10.7696" y1="8.7376" x2="10.9728" y2="8.9408" layer="1"/>
<rectangle x1="10.9728" y1="8.7376" x2="11.176" y2="8.9408" layer="1"/>
<rectangle x1="11.176" y1="8.7376" x2="11.3792" y2="8.9408" layer="1"/>
<rectangle x1="9.5504" y1="8.9408" x2="9.7536" y2="9.144" layer="1"/>
<rectangle x1="9.7536" y1="8.9408" x2="9.9568" y2="9.144" layer="1"/>
<rectangle x1="9.9568" y1="8.9408" x2="10.16" y2="9.144" layer="1"/>
<rectangle x1="10.16" y1="8.9408" x2="10.3632" y2="9.144" layer="1"/>
<rectangle x1="10.3632" y1="8.9408" x2="10.5664" y2="9.144" layer="1"/>
<rectangle x1="10.5664" y1="8.9408" x2="10.7696" y2="9.144" layer="1"/>
<rectangle x1="10.7696" y1="8.9408" x2="10.9728" y2="9.144" layer="1"/>
<rectangle x1="10.9728" y1="8.9408" x2="11.176" y2="9.144" layer="1"/>
<rectangle x1="11.176" y1="8.9408" x2="11.3792" y2="9.144" layer="1"/>
<rectangle x1="9.7536" y1="9.144" x2="9.9568" y2="9.3472" layer="1"/>
<rectangle x1="9.9568" y1="9.144" x2="10.16" y2="9.3472" layer="1"/>
<rectangle x1="10.16" y1="9.144" x2="10.3632" y2="9.3472" layer="1"/>
<rectangle x1="10.3632" y1="9.144" x2="10.5664" y2="9.3472" layer="1"/>
<rectangle x1="10.5664" y1="9.144" x2="10.7696" y2="9.3472" layer="1"/>
<rectangle x1="10.7696" y1="9.144" x2="10.9728" y2="9.3472" layer="1"/>
<rectangle x1="10.9728" y1="9.144" x2="11.176" y2="9.3472" layer="1"/>
<rectangle x1="9.7536" y1="9.3472" x2="9.9568" y2="9.5504" layer="1"/>
<rectangle x1="9.9568" y1="9.3472" x2="10.16" y2="9.5504" layer="1"/>
<rectangle x1="10.16" y1="9.3472" x2="10.3632" y2="9.5504" layer="1"/>
<rectangle x1="10.3632" y1="9.3472" x2="10.5664" y2="9.5504" layer="1"/>
<rectangle x1="10.5664" y1="9.3472" x2="10.7696" y2="9.5504" layer="1"/>
<rectangle x1="10.7696" y1="9.3472" x2="10.9728" y2="9.5504" layer="1"/>
<rectangle x1="10.9728" y1="9.3472" x2="11.176" y2="9.5504" layer="1"/>
<rectangle x1="9.9568" y1="9.5504" x2="10.16" y2="9.7536" layer="1"/>
<rectangle x1="10.16" y1="9.5504" x2="10.3632" y2="9.7536" layer="1"/>
<rectangle x1="10.3632" y1="9.5504" x2="10.5664" y2="9.7536" layer="1"/>
<rectangle x1="10.5664" y1="9.5504" x2="10.7696" y2="9.7536" layer="1"/>
<rectangle x1="10.7696" y1="9.5504" x2="10.9728" y2="9.7536" layer="1"/>
<rectangle x1="9.9568" y1="9.7536" x2="10.16" y2="9.9568" layer="1"/>
<rectangle x1="10.16" y1="9.7536" x2="10.3632" y2="9.9568" layer="1"/>
<rectangle x1="10.3632" y1="9.7536" x2="10.5664" y2="9.9568" layer="1"/>
<rectangle x1="10.5664" y1="9.7536" x2="10.7696" y2="9.9568" layer="1"/>
<rectangle x1="10.7696" y1="9.7536" x2="10.9728" y2="9.9568" layer="1"/>
<rectangle x1="10.16" y1="9.9568" x2="10.3632" y2="10.16" layer="1"/>
<rectangle x1="10.3632" y1="9.9568" x2="10.5664" y2="10.16" layer="1"/>
<rectangle x1="10.5664" y1="9.9568" x2="10.7696" y2="10.16" layer="1"/>
<rectangle x1="10.16" y1="10.16" x2="10.3632" y2="10.3632" layer="1"/>
<rectangle x1="10.3632" y1="10.16" x2="10.5664" y2="10.3632" layer="1"/>
<rectangle x1="10.5664" y1="10.16" x2="10.7696" y2="10.3632" layer="1"/>
<rectangle x1="10.3632" y1="10.3632" x2="10.5664" y2="10.5664" layer="1"/>
<rectangle x1="10.3632" y1="10.5664" x2="10.5664" y2="10.7696" layer="1"/>
</package>
<package name="LOGO_RUN_V1">
<wire x1="1.27" y1="3.81" x2="1.27" y2="1.905" width="0.4064" layer="1"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="0" width="0.4064" layer="1"/>
<wire x1="0" y1="3.81" x2="1.27" y2="1.905" width="0.4064" layer="1"/>
<wire x1="1.27" y1="1.905" x2="2.54" y2="3.81" width="0.4064" layer="1"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="2.54" width="0.4064" layer="1"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0" width="0.4064" layer="1"/>
<wire x1="3.81" y1="3.81" x2="5.08" y2="2.54" width="0.4064" layer="1"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="3.81" x2="6.35" y2="2.54" width="0.4064" layer="1"/>
<wire x1="6.35" y1="2.54" x2="7.62" y2="1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="2.54" x2="8.89" y2="1.27" width="0.4064" layer="1"/>
<wire x1="8.89" y1="1.27" x2="7.62" y2="0" width="0.4064" layer="1"/>
</package>
<package name="LOGO_BIOMEGA_V2">
<rectangle x1="1.845" y1="0.295" x2="1.985" y2="0.305" layer="21"/>
<rectangle x1="1.845" y1="0.305" x2="1.985" y2="0.315" layer="21"/>
<rectangle x1="1.845" y1="0.315" x2="1.985" y2="0.325" layer="21"/>
<rectangle x1="1.845" y1="0.325" x2="1.985" y2="0.335" layer="21"/>
<rectangle x1="1.845" y1="0.335" x2="1.985" y2="0.345" layer="21"/>
<rectangle x1="1.845" y1="0.345" x2="1.985" y2="0.355" layer="21"/>
<rectangle x1="1.845" y1="0.355" x2="1.985" y2="0.365" layer="21"/>
<rectangle x1="1.845" y1="0.365" x2="1.985" y2="0.375" layer="21"/>
<rectangle x1="1.845" y1="0.375" x2="1.985" y2="0.385" layer="21"/>
<rectangle x1="1.845" y1="0.385" x2="1.985" y2="0.395" layer="21"/>
<rectangle x1="1.845" y1="0.395" x2="1.985" y2="0.405" layer="21"/>
<rectangle x1="1.845" y1="0.405" x2="1.985" y2="0.415" layer="21"/>
<rectangle x1="1.845" y1="0.415" x2="1.985" y2="0.425" layer="21"/>
<rectangle x1="1.845" y1="0.425" x2="1.985" y2="0.435" layer="21"/>
<rectangle x1="1.845" y1="0.435" x2="1.985" y2="0.445" layer="21"/>
<rectangle x1="1.845" y1="0.445" x2="1.985" y2="0.455" layer="21"/>
<rectangle x1="1.845" y1="0.455" x2="1.985" y2="0.465" layer="21"/>
<rectangle x1="1.845" y1="0.465" x2="1.985" y2="0.475" layer="21"/>
<rectangle x1="1.845" y1="0.475" x2="1.985" y2="0.485" layer="21"/>
<rectangle x1="1.845" y1="0.485" x2="1.985" y2="0.495" layer="21"/>
<rectangle x1="1.845" y1="0.495" x2="1.985" y2="0.505" layer="21"/>
<rectangle x1="1.845" y1="0.505" x2="1.985" y2="0.515" layer="21"/>
<rectangle x1="1.845" y1="0.515" x2="1.985" y2="0.525" layer="21"/>
<rectangle x1="1.845" y1="0.525" x2="1.985" y2="0.535" layer="21"/>
<rectangle x1="1.845" y1="0.535" x2="1.985" y2="0.545" layer="21"/>
<rectangle x1="1.845" y1="0.545" x2="1.985" y2="0.555" layer="21"/>
<rectangle x1="1.845" y1="0.555" x2="1.985" y2="0.565" layer="21"/>
<rectangle x1="1.845" y1="0.565" x2="1.985" y2="0.575" layer="21"/>
<rectangle x1="1.845" y1="0.575" x2="1.985" y2="0.585" layer="21"/>
<rectangle x1="1.845" y1="0.585" x2="1.985" y2="0.595" layer="21"/>
<rectangle x1="1.845" y1="0.595" x2="1.985" y2="0.605" layer="21"/>
<rectangle x1="1.845" y1="0.605" x2="1.985" y2="0.615" layer="21"/>
<rectangle x1="1.845" y1="0.615" x2="1.985" y2="0.625" layer="21"/>
<rectangle x1="1.845" y1="0.625" x2="1.985" y2="0.635" layer="21"/>
<rectangle x1="1.845" y1="0.635" x2="1.985" y2="0.645" layer="21"/>
<rectangle x1="1.845" y1="0.645" x2="1.985" y2="0.655" layer="21"/>
<rectangle x1="1.845" y1="0.655" x2="1.985" y2="0.665" layer="21"/>
<rectangle x1="1.845" y1="0.665" x2="1.985" y2="0.675" layer="21"/>
<rectangle x1="1.845" y1="0.675" x2="1.985" y2="0.685" layer="21"/>
<rectangle x1="1.845" y1="0.685" x2="1.985" y2="0.695" layer="21"/>
<rectangle x1="1.845" y1="0.695" x2="1.985" y2="0.705" layer="21"/>
<rectangle x1="1.845" y1="0.705" x2="1.985" y2="0.715" layer="21"/>
<rectangle x1="1.845" y1="0.715" x2="1.985" y2="0.725" layer="21"/>
<rectangle x1="1.845" y1="0.725" x2="1.985" y2="0.735" layer="21"/>
<rectangle x1="1.845" y1="0.735" x2="1.985" y2="0.745" layer="21"/>
<rectangle x1="1.845" y1="0.745" x2="1.985" y2="0.755" layer="21"/>
<rectangle x1="1.845" y1="0.755" x2="1.985" y2="0.765" layer="21"/>
<rectangle x1="1.845" y1="0.765" x2="1.985" y2="0.775" layer="21"/>
<rectangle x1="1.845" y1="0.775" x2="1.985" y2="0.785" layer="21"/>
<rectangle x1="1.845" y1="0.785" x2="1.985" y2="0.795" layer="21"/>
<rectangle x1="1.845" y1="0.795" x2="1.985" y2="0.805" layer="21"/>
<rectangle x1="1.845" y1="0.805" x2="1.985" y2="0.815" layer="21"/>
<rectangle x1="1.845" y1="0.815" x2="1.985" y2="0.825" layer="21"/>
<rectangle x1="1.845" y1="0.825" x2="1.985" y2="0.835" layer="21"/>
<rectangle x1="1.845" y1="0.835" x2="1.985" y2="0.845" layer="21"/>
<rectangle x1="1.845" y1="0.845" x2="1.985" y2="0.855" layer="21"/>
<rectangle x1="1.845" y1="0.855" x2="1.985" y2="0.865" layer="21"/>
<rectangle x1="1.845" y1="0.865" x2="1.985" y2="0.875" layer="21"/>
<rectangle x1="1.845" y1="0.875" x2="1.985" y2="0.885" layer="21"/>
<rectangle x1="1.845" y1="0.885" x2="1.985" y2="0.895" layer="21"/>
<rectangle x1="1.845" y1="0.895" x2="1.985" y2="0.905" layer="21"/>
<rectangle x1="1.845" y1="0.905" x2="1.985" y2="0.915" layer="21"/>
<rectangle x1="1.845" y1="0.915" x2="1.985" y2="0.925" layer="21"/>
<rectangle x1="1.845" y1="0.925" x2="1.985" y2="0.935" layer="21"/>
<rectangle x1="1.845" y1="0.935" x2="1.985" y2="0.945" layer="21"/>
<rectangle x1="1.845" y1="0.945" x2="1.985" y2="0.955" layer="21"/>
<rectangle x1="1.845" y1="0.955" x2="1.985" y2="0.965" layer="21"/>
<rectangle x1="1.845" y1="0.965" x2="1.985" y2="0.975" layer="21"/>
<rectangle x1="1.845" y1="0.975" x2="1.985" y2="0.985" layer="21"/>
<rectangle x1="1.845" y1="0.985" x2="1.985" y2="0.995" layer="21"/>
<rectangle x1="1.845" y1="0.995" x2="1.985" y2="1.005" layer="21"/>
<rectangle x1="1.845" y1="1.005" x2="1.985" y2="1.015" layer="21"/>
<rectangle x1="1.845" y1="1.015" x2="1.985" y2="1.025" layer="21"/>
<rectangle x1="1.845" y1="1.025" x2="1.985" y2="1.035" layer="21"/>
<rectangle x1="1.845" y1="1.035" x2="1.985" y2="1.045" layer="21"/>
<rectangle x1="1.845" y1="1.045" x2="1.985" y2="1.055" layer="21"/>
<rectangle x1="1.845" y1="1.055" x2="1.985" y2="1.065" layer="21"/>
<rectangle x1="1.845" y1="1.065" x2="1.985" y2="1.075" layer="21"/>
<rectangle x1="1.845" y1="1.075" x2="1.985" y2="1.085" layer="21"/>
<rectangle x1="1.845" y1="1.085" x2="1.985" y2="1.095" layer="21"/>
<rectangle x1="1.845" y1="1.095" x2="1.985" y2="1.105" layer="21"/>
<rectangle x1="1.845" y1="1.105" x2="1.985" y2="1.115" layer="21"/>
<rectangle x1="1.845" y1="1.115" x2="1.985" y2="1.125" layer="21"/>
<rectangle x1="1.845" y1="1.125" x2="1.985" y2="1.135" layer="21"/>
<rectangle x1="1.845" y1="1.135" x2="1.985" y2="1.145" layer="21"/>
<rectangle x1="1.845" y1="1.145" x2="1.985" y2="1.155" layer="21"/>
<rectangle x1="1.845" y1="1.155" x2="1.985" y2="1.165" layer="21"/>
<rectangle x1="1.845" y1="1.165" x2="1.985" y2="1.175" layer="21"/>
<rectangle x1="1.845" y1="1.175" x2="1.985" y2="1.185" layer="21"/>
<rectangle x1="1.845" y1="1.185" x2="1.985" y2="1.195" layer="21"/>
<rectangle x1="1.845" y1="1.195" x2="1.985" y2="1.205" layer="21"/>
<rectangle x1="1.845" y1="1.205" x2="1.985" y2="1.215" layer="21"/>
<rectangle x1="1.845" y1="1.215" x2="1.985" y2="1.225" layer="21"/>
<rectangle x1="1.845" y1="1.225" x2="1.985" y2="1.235" layer="21"/>
<rectangle x1="1.845" y1="1.235" x2="1.985" y2="1.245" layer="21"/>
<rectangle x1="1.845" y1="1.245" x2="1.985" y2="1.255" layer="21"/>
<rectangle x1="1.845" y1="1.255" x2="1.985" y2="1.265" layer="21"/>
<rectangle x1="1.845" y1="1.265" x2="1.985" y2="1.275" layer="21"/>
<rectangle x1="1.845" y1="1.275" x2="1.985" y2="1.285" layer="21"/>
<rectangle x1="1.845" y1="1.285" x2="1.985" y2="1.295" layer="21"/>
<rectangle x1="1.845" y1="1.295" x2="1.985" y2="1.305" layer="21"/>
<rectangle x1="1.845" y1="1.305" x2="1.985" y2="1.315" layer="21"/>
<rectangle x1="1.845" y1="1.315" x2="1.985" y2="1.325" layer="21"/>
<rectangle x1="1.845" y1="1.325" x2="1.985" y2="1.335" layer="21"/>
<rectangle x1="1.845" y1="1.335" x2="1.985" y2="1.345" layer="21"/>
<rectangle x1="1.845" y1="1.345" x2="1.985" y2="1.355" layer="21"/>
<rectangle x1="1.845" y1="1.355" x2="1.985" y2="1.365" layer="21"/>
<rectangle x1="1.845" y1="1.365" x2="1.985" y2="1.375" layer="21"/>
<rectangle x1="1.845" y1="1.375" x2="1.985" y2="1.385" layer="21"/>
<rectangle x1="1.845" y1="1.385" x2="1.985" y2="1.395" layer="21"/>
<rectangle x1="1.845" y1="1.395" x2="1.985" y2="1.405" layer="21"/>
<rectangle x1="1.845" y1="1.405" x2="1.985" y2="1.415" layer="21"/>
<rectangle x1="1.845" y1="1.415" x2="1.985" y2="1.425" layer="21"/>
<rectangle x1="1.845" y1="1.425" x2="1.985" y2="1.435" layer="21"/>
<rectangle x1="1.845" y1="1.435" x2="1.985" y2="1.445" layer="21"/>
<rectangle x1="1.845" y1="1.445" x2="1.985" y2="1.455" layer="21"/>
<rectangle x1="1.845" y1="1.455" x2="1.985" y2="1.465" layer="21"/>
<rectangle x1="1.845" y1="1.465" x2="1.985" y2="1.475" layer="21"/>
<rectangle x1="1.845" y1="1.475" x2="1.985" y2="1.485" layer="21"/>
<rectangle x1="1.845" y1="1.485" x2="1.985" y2="1.495" layer="21"/>
<rectangle x1="1.845" y1="1.495" x2="1.985" y2="1.505" layer="21"/>
<rectangle x1="1.845" y1="1.505" x2="1.985" y2="1.515" layer="21"/>
<rectangle x1="1.845" y1="1.515" x2="1.985" y2="1.525" layer="21"/>
<rectangle x1="1.845" y1="1.525" x2="1.985" y2="1.535" layer="21"/>
<rectangle x1="1.845" y1="1.535" x2="1.985" y2="1.545" layer="21"/>
<rectangle x1="1.845" y1="1.545" x2="1.985" y2="1.555" layer="21"/>
<rectangle x1="1.845" y1="1.555" x2="1.985" y2="1.565" layer="21"/>
<rectangle x1="1.845" y1="1.565" x2="1.985" y2="1.575" layer="21"/>
<rectangle x1="1.845" y1="1.575" x2="1.985" y2="1.585" layer="21"/>
<rectangle x1="1.845" y1="1.585" x2="1.985" y2="1.595" layer="21"/>
<rectangle x1="1.845" y1="1.595" x2="1.985" y2="1.605" layer="21"/>
<rectangle x1="1.845" y1="1.605" x2="1.985" y2="1.615" layer="21"/>
<rectangle x1="1.845" y1="1.615" x2="1.985" y2="1.625" layer="21"/>
<rectangle x1="1.845" y1="1.625" x2="1.985" y2="1.635" layer="21"/>
<rectangle x1="1.845" y1="1.635" x2="1.985" y2="1.645" layer="21"/>
<rectangle x1="1.845" y1="1.645" x2="1.985" y2="1.655" layer="21"/>
<rectangle x1="1.845" y1="1.655" x2="1.985" y2="1.665" layer="21"/>
<rectangle x1="1.845" y1="1.665" x2="1.985" y2="1.675" layer="21"/>
<rectangle x1="1.845" y1="1.675" x2="1.985" y2="1.685" layer="21"/>
<rectangle x1="1.845" y1="1.685" x2="1.985" y2="1.695" layer="21"/>
<rectangle x1="1.845" y1="1.695" x2="1.985" y2="1.705" layer="21"/>
<rectangle x1="1.845" y1="1.705" x2="1.985" y2="1.715" layer="21"/>
<rectangle x1="1.845" y1="1.715" x2="1.985" y2="1.725" layer="21"/>
<rectangle x1="1.845" y1="1.725" x2="1.985" y2="1.735" layer="21"/>
<rectangle x1="1.845" y1="1.735" x2="1.985" y2="1.745" layer="21"/>
<rectangle x1="1.845" y1="1.745" x2="1.985" y2="1.755" layer="21"/>
<rectangle x1="1.845" y1="1.755" x2="1.985" y2="1.765" layer="21"/>
<rectangle x1="1.845" y1="1.765" x2="1.985" y2="1.775" layer="21"/>
<rectangle x1="1.845" y1="1.775" x2="1.985" y2="1.785" layer="21"/>
<rectangle x1="1.845" y1="1.785" x2="1.985" y2="1.795" layer="21"/>
<rectangle x1="1.845" y1="1.795" x2="1.985" y2="1.805" layer="21"/>
<rectangle x1="1.845" y1="1.805" x2="1.985" y2="1.815" layer="21"/>
<rectangle x1="0.485" y1="1.815" x2="1.705" y2="1.825" layer="21"/>
<rectangle x1="1.845" y1="1.815" x2="1.985" y2="1.825" layer="21"/>
<rectangle x1="2.125" y1="1.815" x2="3.345" y2="1.825" layer="21"/>
<rectangle x1="0.485" y1="1.825" x2="1.705" y2="1.835" layer="21"/>
<rectangle x1="1.845" y1="1.825" x2="1.985" y2="1.835" layer="21"/>
<rectangle x1="2.125" y1="1.825" x2="3.345" y2="1.835" layer="21"/>
<rectangle x1="0.485" y1="1.835" x2="1.705" y2="1.845" layer="21"/>
<rectangle x1="1.845" y1="1.835" x2="1.985" y2="1.845" layer="21"/>
<rectangle x1="2.125" y1="1.835" x2="3.345" y2="1.845" layer="21"/>
<rectangle x1="0.485" y1="1.845" x2="1.705" y2="1.855" layer="21"/>
<rectangle x1="1.845" y1="1.845" x2="1.985" y2="1.855" layer="21"/>
<rectangle x1="2.125" y1="1.845" x2="3.345" y2="1.855" layer="21"/>
<rectangle x1="0.485" y1="1.855" x2="1.705" y2="1.865" layer="21"/>
<rectangle x1="1.845" y1="1.855" x2="1.985" y2="1.865" layer="21"/>
<rectangle x1="2.125" y1="1.855" x2="3.345" y2="1.865" layer="21"/>
<rectangle x1="0.485" y1="1.865" x2="1.705" y2="1.875" layer="21"/>
<rectangle x1="1.845" y1="1.865" x2="1.985" y2="1.875" layer="21"/>
<rectangle x1="2.125" y1="1.865" x2="3.345" y2="1.875" layer="21"/>
<rectangle x1="0.485" y1="1.875" x2="1.705" y2="1.885" layer="21"/>
<rectangle x1="1.845" y1="1.875" x2="1.985" y2="1.885" layer="21"/>
<rectangle x1="2.125" y1="1.875" x2="3.345" y2="1.885" layer="21"/>
<rectangle x1="0.485" y1="1.885" x2="1.705" y2="1.895" layer="21"/>
<rectangle x1="1.845" y1="1.885" x2="1.985" y2="1.895" layer="21"/>
<rectangle x1="2.125" y1="1.885" x2="3.345" y2="1.895" layer="21"/>
<rectangle x1="0.485" y1="1.895" x2="1.705" y2="1.905" layer="21"/>
<rectangle x1="1.845" y1="1.895" x2="1.985" y2="1.905" layer="21"/>
<rectangle x1="2.125" y1="1.895" x2="3.345" y2="1.905" layer="21"/>
<rectangle x1="0.485" y1="1.905" x2="1.705" y2="1.915" layer="21"/>
<rectangle x1="1.845" y1="1.905" x2="1.985" y2="1.915" layer="21"/>
<rectangle x1="2.125" y1="1.905" x2="3.345" y2="1.915" layer="21"/>
<rectangle x1="0.485" y1="1.915" x2="1.705" y2="1.925" layer="21"/>
<rectangle x1="1.845" y1="1.915" x2="1.985" y2="1.925" layer="21"/>
<rectangle x1="2.125" y1="1.915" x2="3.345" y2="1.925" layer="21"/>
<rectangle x1="0.485" y1="1.925" x2="1.705" y2="1.935" layer="21"/>
<rectangle x1="1.845" y1="1.925" x2="1.985" y2="1.935" layer="21"/>
<rectangle x1="2.125" y1="1.925" x2="3.345" y2="1.935" layer="21"/>
<rectangle x1="0.485" y1="1.935" x2="1.705" y2="1.945" layer="21"/>
<rectangle x1="1.845" y1="1.935" x2="1.985" y2="1.945" layer="21"/>
<rectangle x1="2.125" y1="1.935" x2="3.345" y2="1.945" layer="21"/>
<rectangle x1="0.485" y1="1.945" x2="1.705" y2="1.955" layer="21"/>
<rectangle x1="1.845" y1="1.945" x2="1.985" y2="1.955" layer="21"/>
<rectangle x1="2.125" y1="1.945" x2="3.345" y2="1.955" layer="21"/>
<rectangle x1="1.565" y1="1.955" x2="1.705" y2="1.965" layer="21"/>
<rectangle x1="1.845" y1="1.955" x2="1.985" y2="1.965" layer="21"/>
<rectangle x1="2.125" y1="1.955" x2="2.265" y2="1.965" layer="21"/>
<rectangle x1="1.565" y1="1.965" x2="1.705" y2="1.975" layer="21"/>
<rectangle x1="1.845" y1="1.965" x2="1.985" y2="1.975" layer="21"/>
<rectangle x1="2.125" y1="1.965" x2="2.265" y2="1.975" layer="21"/>
<rectangle x1="1.565" y1="1.975" x2="1.705" y2="1.985" layer="21"/>
<rectangle x1="1.845" y1="1.975" x2="1.985" y2="1.985" layer="21"/>
<rectangle x1="2.125" y1="1.975" x2="2.265" y2="1.985" layer="21"/>
<rectangle x1="1.565" y1="1.985" x2="1.705" y2="1.995" layer="21"/>
<rectangle x1="1.845" y1="1.985" x2="1.985" y2="1.995" layer="21"/>
<rectangle x1="2.125" y1="1.985" x2="2.265" y2="1.995" layer="21"/>
<rectangle x1="1.555" y1="1.995" x2="1.705" y2="2.005" layer="21"/>
<rectangle x1="1.845" y1="1.995" x2="1.985" y2="2.005" layer="21"/>
<rectangle x1="2.125" y1="1.995" x2="2.275" y2="2.005" layer="21"/>
<rectangle x1="1.535" y1="2.005" x2="1.705" y2="2.015" layer="21"/>
<rectangle x1="1.845" y1="2.005" x2="1.985" y2="2.015" layer="21"/>
<rectangle x1="2.125" y1="2.005" x2="2.295" y2="2.015" layer="21"/>
<rectangle x1="1.515" y1="2.015" x2="1.705" y2="2.025" layer="21"/>
<rectangle x1="1.845" y1="2.015" x2="1.985" y2="2.025" layer="21"/>
<rectangle x1="2.125" y1="2.015" x2="2.315" y2="2.025" layer="21"/>
<rectangle x1="1.505" y1="2.025" x2="1.705" y2="2.035" layer="21"/>
<rectangle x1="1.845" y1="2.025" x2="1.985" y2="2.035" layer="21"/>
<rectangle x1="2.125" y1="2.025" x2="2.325" y2="2.035" layer="21"/>
<rectangle x1="1.485" y1="2.035" x2="1.705" y2="2.045" layer="21"/>
<rectangle x1="1.845" y1="2.035" x2="1.985" y2="2.045" layer="21"/>
<rectangle x1="2.125" y1="2.035" x2="2.345" y2="2.045" layer="21"/>
<rectangle x1="1.465" y1="2.045" x2="1.705" y2="2.055" layer="21"/>
<rectangle x1="1.845" y1="2.045" x2="1.985" y2="2.055" layer="21"/>
<rectangle x1="2.125" y1="2.045" x2="2.365" y2="2.055" layer="21"/>
<rectangle x1="1.455" y1="2.055" x2="1.705" y2="2.065" layer="21"/>
<rectangle x1="1.845" y1="2.055" x2="1.985" y2="2.065" layer="21"/>
<rectangle x1="2.125" y1="2.055" x2="2.375" y2="2.065" layer="21"/>
<rectangle x1="1.435" y1="2.065" x2="1.705" y2="2.075" layer="21"/>
<rectangle x1="1.845" y1="2.065" x2="1.985" y2="2.075" layer="21"/>
<rectangle x1="2.125" y1="2.065" x2="2.395" y2="2.075" layer="21"/>
<rectangle x1="1.425" y1="2.075" x2="1.705" y2="2.085" layer="21"/>
<rectangle x1="1.845" y1="2.075" x2="1.985" y2="2.085" layer="21"/>
<rectangle x1="2.125" y1="2.075" x2="2.405" y2="2.085" layer="21"/>
<rectangle x1="1.415" y1="2.085" x2="1.705" y2="2.095" layer="21"/>
<rectangle x1="1.845" y1="2.085" x2="1.985" y2="2.095" layer="21"/>
<rectangle x1="2.125" y1="2.085" x2="2.415" y2="2.095" layer="21"/>
<rectangle x1="1.395" y1="2.095" x2="1.675" y2="2.105" layer="21"/>
<rectangle x1="2.155" y1="2.095" x2="2.435" y2="2.105" layer="21"/>
<rectangle x1="1.385" y1="2.105" x2="1.655" y2="2.115" layer="21"/>
<rectangle x1="2.175" y1="2.105" x2="2.445" y2="2.115" layer="21"/>
<rectangle x1="1.375" y1="2.115" x2="1.635" y2="2.125" layer="21"/>
<rectangle x1="2.195" y1="2.115" x2="2.455" y2="2.125" layer="21"/>
<rectangle x1="1.365" y1="2.125" x2="1.615" y2="2.135" layer="21"/>
<rectangle x1="2.215" y1="2.125" x2="2.465" y2="2.135" layer="21"/>
<rectangle x1="1.355" y1="2.135" x2="1.595" y2="2.145" layer="21"/>
<rectangle x1="2.235" y1="2.135" x2="2.475" y2="2.145" layer="21"/>
<rectangle x1="1.345" y1="2.145" x2="1.575" y2="2.155" layer="21"/>
<rectangle x1="2.255" y1="2.145" x2="2.485" y2="2.155" layer="21"/>
<rectangle x1="1.335" y1="2.155" x2="1.555" y2="2.165" layer="21"/>
<rectangle x1="2.275" y1="2.155" x2="2.495" y2="2.165" layer="21"/>
<rectangle x1="1.325" y1="2.165" x2="1.545" y2="2.175" layer="21"/>
<rectangle x1="2.285" y1="2.165" x2="2.505" y2="2.175" layer="21"/>
<rectangle x1="1.315" y1="2.175" x2="1.525" y2="2.185" layer="21"/>
<rectangle x1="2.305" y1="2.175" x2="2.515" y2="2.185" layer="21"/>
<rectangle x1="1.305" y1="2.185" x2="1.515" y2="2.195" layer="21"/>
<rectangle x1="2.315" y1="2.185" x2="2.525" y2="2.195" layer="21"/>
<rectangle x1="1.295" y1="2.195" x2="1.495" y2="2.205" layer="21"/>
<rectangle x1="2.335" y1="2.195" x2="2.535" y2="2.205" layer="21"/>
<rectangle x1="1.285" y1="2.205" x2="1.485" y2="2.215" layer="21"/>
<rectangle x1="2.345" y1="2.205" x2="2.545" y2="2.215" layer="21"/>
<rectangle x1="1.275" y1="2.215" x2="1.475" y2="2.225" layer="21"/>
<rectangle x1="2.355" y1="2.215" x2="2.555" y2="2.225" layer="21"/>
<rectangle x1="1.265" y1="2.225" x2="1.465" y2="2.235" layer="21"/>
<rectangle x1="2.365" y1="2.225" x2="2.565" y2="2.235" layer="21"/>
<rectangle x1="1.265" y1="2.235" x2="1.455" y2="2.245" layer="21"/>
<rectangle x1="2.375" y1="2.235" x2="2.565" y2="2.245" layer="21"/>
<rectangle x1="1.255" y1="2.245" x2="1.445" y2="2.255" layer="21"/>
<rectangle x1="2.385" y1="2.245" x2="2.575" y2="2.255" layer="21"/>
<rectangle x1="1.245" y1="2.255" x2="1.435" y2="2.265" layer="21"/>
<rectangle x1="2.395" y1="2.255" x2="2.585" y2="2.265" layer="21"/>
<rectangle x1="1.235" y1="2.265" x2="1.425" y2="2.275" layer="21"/>
<rectangle x1="2.405" y1="2.265" x2="2.595" y2="2.275" layer="21"/>
<rectangle x1="1.235" y1="2.275" x2="1.415" y2="2.285" layer="21"/>
<rectangle x1="2.415" y1="2.275" x2="2.595" y2="2.285" layer="21"/>
<rectangle x1="1.225" y1="2.285" x2="1.405" y2="2.295" layer="21"/>
<rectangle x1="2.425" y1="2.285" x2="2.605" y2="2.295" layer="21"/>
<rectangle x1="1.215" y1="2.295" x2="1.395" y2="2.305" layer="21"/>
<rectangle x1="2.435" y1="2.295" x2="2.615" y2="2.305" layer="21"/>
<rectangle x1="1.215" y1="2.305" x2="1.385" y2="2.315" layer="21"/>
<rectangle x1="2.445" y1="2.305" x2="2.615" y2="2.315" layer="21"/>
<rectangle x1="1.205" y1="2.315" x2="1.375" y2="2.325" layer="21"/>
<rectangle x1="2.455" y1="2.315" x2="2.625" y2="2.325" layer="21"/>
<rectangle x1="1.205" y1="2.325" x2="1.365" y2="2.335" layer="21"/>
<rectangle x1="2.465" y1="2.325" x2="2.625" y2="2.335" layer="21"/>
<rectangle x1="1.195" y1="2.335" x2="1.365" y2="2.345" layer="21"/>
<rectangle x1="2.465" y1="2.335" x2="2.635" y2="2.345" layer="21"/>
<rectangle x1="1.185" y1="2.345" x2="1.355" y2="2.355" layer="21"/>
<rectangle x1="2.475" y1="2.345" x2="2.645" y2="2.355" layer="21"/>
<rectangle x1="1.185" y1="2.355" x2="1.345" y2="2.365" layer="21"/>
<rectangle x1="2.485" y1="2.355" x2="2.645" y2="2.365" layer="21"/>
<rectangle x1="1.175" y1="2.365" x2="1.345" y2="2.375" layer="21"/>
<rectangle x1="2.485" y1="2.365" x2="2.655" y2="2.375" layer="21"/>
<rectangle x1="1.175" y1="2.375" x2="1.335" y2="2.385" layer="21"/>
<rectangle x1="2.495" y1="2.375" x2="2.655" y2="2.385" layer="21"/>
<rectangle x1="1.165" y1="2.385" x2="1.325" y2="2.395" layer="21"/>
<rectangle x1="2.505" y1="2.385" x2="2.665" y2="2.395" layer="21"/>
<rectangle x1="1.165" y1="2.395" x2="1.325" y2="2.405" layer="21"/>
<rectangle x1="2.505" y1="2.395" x2="2.665" y2="2.405" layer="21"/>
<rectangle x1="1.155" y1="2.405" x2="1.315" y2="2.415" layer="21"/>
<rectangle x1="2.515" y1="2.405" x2="2.675" y2="2.415" layer="21"/>
<rectangle x1="1.155" y1="2.415" x2="1.315" y2="2.425" layer="21"/>
<rectangle x1="2.515" y1="2.415" x2="2.675" y2="2.425" layer="21"/>
<rectangle x1="1.155" y1="2.425" x2="1.305" y2="2.435" layer="21"/>
<rectangle x1="2.525" y1="2.425" x2="2.675" y2="2.435" layer="21"/>
<rectangle x1="1.145" y1="2.435" x2="1.305" y2="2.445" layer="21"/>
<rectangle x1="2.525" y1="2.435" x2="2.685" y2="2.445" layer="21"/>
<rectangle x1="1.145" y1="2.445" x2="1.295" y2="2.455" layer="21"/>
<rectangle x1="2.535" y1="2.445" x2="2.685" y2="2.455" layer="21"/>
<rectangle x1="1.135" y1="2.455" x2="1.295" y2="2.465" layer="21"/>
<rectangle x1="2.535" y1="2.455" x2="2.695" y2="2.465" layer="21"/>
<rectangle x1="1.135" y1="2.465" x2="1.285" y2="2.475" layer="21"/>
<rectangle x1="2.545" y1="2.465" x2="2.695" y2="2.475" layer="21"/>
<rectangle x1="1.135" y1="2.475" x2="1.285" y2="2.485" layer="21"/>
<rectangle x1="2.545" y1="2.475" x2="2.695" y2="2.485" layer="21"/>
<rectangle x1="1.125" y1="2.485" x2="1.275" y2="2.495" layer="21"/>
<rectangle x1="2.555" y1="2.485" x2="2.705" y2="2.495" layer="21"/>
<rectangle x1="1.125" y1="2.495" x2="1.275" y2="2.505" layer="21"/>
<rectangle x1="2.555" y1="2.495" x2="2.705" y2="2.505" layer="21"/>
<rectangle x1="1.125" y1="2.505" x2="1.265" y2="2.515" layer="21"/>
<rectangle x1="2.565" y1="2.505" x2="2.705" y2="2.515" layer="21"/>
<rectangle x1="1.115" y1="2.515" x2="1.265" y2="2.525" layer="21"/>
<rectangle x1="2.565" y1="2.515" x2="2.715" y2="2.525" layer="21"/>
<rectangle x1="1.115" y1="2.525" x2="1.265" y2="2.535" layer="21"/>
<rectangle x1="2.565" y1="2.525" x2="2.715" y2="2.535" layer="21"/>
<rectangle x1="1.115" y1="2.535" x2="1.255" y2="2.545" layer="21"/>
<rectangle x1="2.575" y1="2.535" x2="2.715" y2="2.545" layer="21"/>
<rectangle x1="1.115" y1="2.545" x2="1.255" y2="2.555" layer="21"/>
<rectangle x1="2.575" y1="2.545" x2="2.715" y2="2.555" layer="21"/>
<rectangle x1="1.105" y1="2.555" x2="1.255" y2="2.565" layer="21"/>
<rectangle x1="2.575" y1="2.555" x2="2.725" y2="2.565" layer="21"/>
<rectangle x1="1.105" y1="2.565" x2="1.245" y2="2.575" layer="21"/>
<rectangle x1="2.585" y1="2.565" x2="2.725" y2="2.575" layer="21"/>
<rectangle x1="1.105" y1="2.575" x2="1.245" y2="2.585" layer="21"/>
<rectangle x1="2.585" y1="2.575" x2="2.725" y2="2.585" layer="21"/>
<rectangle x1="1.105" y1="2.585" x2="1.245" y2="2.595" layer="21"/>
<rectangle x1="2.585" y1="2.585" x2="2.725" y2="2.595" layer="21"/>
<rectangle x1="1.095" y1="2.595" x2="1.245" y2="2.605" layer="21"/>
<rectangle x1="2.585" y1="2.595" x2="2.735" y2="2.605" layer="21"/>
<rectangle x1="1.095" y1="2.605" x2="1.245" y2="2.615" layer="21"/>
<rectangle x1="2.585" y1="2.605" x2="2.735" y2="2.615" layer="21"/>
<rectangle x1="1.095" y1="2.615" x2="1.235" y2="2.625" layer="21"/>
<rectangle x1="2.595" y1="2.615" x2="2.735" y2="2.625" layer="21"/>
<rectangle x1="1.095" y1="2.625" x2="1.235" y2="2.635" layer="21"/>
<rectangle x1="2.595" y1="2.625" x2="2.735" y2="2.635" layer="21"/>
<rectangle x1="1.095" y1="2.635" x2="1.235" y2="2.645" layer="21"/>
<rectangle x1="2.595" y1="2.635" x2="2.735" y2="2.645" layer="21"/>
<rectangle x1="1.095" y1="2.645" x2="1.235" y2="2.655" layer="21"/>
<rectangle x1="2.595" y1="2.645" x2="2.735" y2="2.655" layer="21"/>
<rectangle x1="1.095" y1="2.655" x2="1.235" y2="2.665" layer="21"/>
<rectangle x1="2.595" y1="2.655" x2="2.735" y2="2.665" layer="21"/>
<rectangle x1="1.085" y1="2.665" x2="1.235" y2="2.675" layer="21"/>
<rectangle x1="2.595" y1="2.665" x2="2.745" y2="2.675" layer="21"/>
<rectangle x1="1.085" y1="2.675" x2="1.225" y2="2.685" layer="21"/>
<rectangle x1="2.605" y1="2.675" x2="2.745" y2="2.685" layer="21"/>
<rectangle x1="1.085" y1="2.685" x2="1.225" y2="2.695" layer="21"/>
<rectangle x1="2.605" y1="2.685" x2="2.745" y2="2.695" layer="21"/>
<rectangle x1="1.085" y1="2.695" x2="1.225" y2="2.705" layer="21"/>
<rectangle x1="2.605" y1="2.695" x2="2.745" y2="2.705" layer="21"/>
<rectangle x1="1.085" y1="2.705" x2="1.225" y2="2.715" layer="21"/>
<rectangle x1="2.605" y1="2.705" x2="2.745" y2="2.715" layer="21"/>
<rectangle x1="1.085" y1="2.715" x2="1.225" y2="2.725" layer="21"/>
<rectangle x1="2.605" y1="2.715" x2="2.745" y2="2.725" layer="21"/>
<rectangle x1="1.085" y1="2.725" x2="1.225" y2="2.735" layer="21"/>
<rectangle x1="2.605" y1="2.725" x2="2.745" y2="2.735" layer="21"/>
<rectangle x1="1.085" y1="2.735" x2="1.225" y2="2.745" layer="21"/>
<rectangle x1="2.605" y1="2.735" x2="2.745" y2="2.745" layer="21"/>
<rectangle x1="1.085" y1="2.745" x2="1.225" y2="2.755" layer="21"/>
<rectangle x1="2.605" y1="2.745" x2="2.745" y2="2.755" layer="21"/>
<rectangle x1="1.085" y1="2.755" x2="1.225" y2="2.765" layer="21"/>
<rectangle x1="2.605" y1="2.755" x2="2.745" y2="2.765" layer="21"/>
<rectangle x1="1.085" y1="2.765" x2="1.225" y2="2.775" layer="21"/>
<rectangle x1="2.605" y1="2.765" x2="2.745" y2="2.775" layer="21"/>
<rectangle x1="1.085" y1="2.775" x2="1.225" y2="2.785" layer="21"/>
<rectangle x1="2.605" y1="2.775" x2="2.745" y2="2.785" layer="21"/>
<rectangle x1="1.085" y1="2.785" x2="1.225" y2="2.795" layer="21"/>
<rectangle x1="2.605" y1="2.785" x2="2.745" y2="2.795" layer="21"/>
<rectangle x1="1.085" y1="2.795" x2="1.225" y2="2.805" layer="21"/>
<rectangle x1="2.605" y1="2.795" x2="2.745" y2="2.805" layer="21"/>
<rectangle x1="1.085" y1="2.805" x2="1.225" y2="2.815" layer="21"/>
<rectangle x1="2.605" y1="2.805" x2="2.745" y2="2.815" layer="21"/>
<rectangle x1="1.085" y1="2.815" x2="1.225" y2="2.825" layer="21"/>
<rectangle x1="2.605" y1="2.815" x2="2.745" y2="2.825" layer="21"/>
<rectangle x1="1.085" y1="2.825" x2="1.225" y2="2.835" layer="21"/>
<rectangle x1="2.605" y1="2.825" x2="2.745" y2="2.835" layer="21"/>
<rectangle x1="1.085" y1="2.835" x2="1.235" y2="2.845" layer="21"/>
<rectangle x1="2.595" y1="2.835" x2="2.745" y2="2.845" layer="21"/>
<rectangle x1="1.095" y1="2.845" x2="1.235" y2="2.855" layer="21"/>
<rectangle x1="2.595" y1="2.845" x2="2.735" y2="2.855" layer="21"/>
<rectangle x1="1.095" y1="2.855" x2="1.235" y2="2.865" layer="21"/>
<rectangle x1="2.595" y1="2.855" x2="2.735" y2="2.865" layer="21"/>
<rectangle x1="1.095" y1="2.865" x2="1.235" y2="2.875" layer="21"/>
<rectangle x1="2.595" y1="2.865" x2="2.735" y2="2.875" layer="21"/>
<rectangle x1="1.095" y1="2.875" x2="1.235" y2="2.885" layer="21"/>
<rectangle x1="2.595" y1="2.875" x2="2.735" y2="2.885" layer="21"/>
<rectangle x1="1.095" y1="2.885" x2="1.235" y2="2.895" layer="21"/>
<rectangle x1="2.595" y1="2.885" x2="2.735" y2="2.895" layer="21"/>
<rectangle x1="1.095" y1="2.895" x2="1.245" y2="2.905" layer="21"/>
<rectangle x1="2.585" y1="2.895" x2="2.735" y2="2.905" layer="21"/>
<rectangle x1="1.095" y1="2.905" x2="1.245" y2="2.915" layer="21"/>
<rectangle x1="2.585" y1="2.905" x2="2.735" y2="2.915" layer="21"/>
<rectangle x1="1.105" y1="2.915" x2="1.245" y2="2.925" layer="21"/>
<rectangle x1="2.585" y1="2.915" x2="2.725" y2="2.925" layer="21"/>
<rectangle x1="1.105" y1="2.925" x2="1.245" y2="2.935" layer="21"/>
<rectangle x1="2.585" y1="2.925" x2="2.725" y2="2.935" layer="21"/>
<rectangle x1="1.105" y1="2.935" x2="1.245" y2="2.945" layer="21"/>
<rectangle x1="2.585" y1="2.935" x2="2.725" y2="2.945" layer="21"/>
<rectangle x1="1.105" y1="2.945" x2="1.255" y2="2.955" layer="21"/>
<rectangle x1="2.575" y1="2.945" x2="2.725" y2="2.955" layer="21"/>
<rectangle x1="1.115" y1="2.955" x2="1.255" y2="2.965" layer="21"/>
<rectangle x1="2.575" y1="2.955" x2="2.715" y2="2.965" layer="21"/>
<rectangle x1="1.115" y1="2.965" x2="1.255" y2="2.975" layer="21"/>
<rectangle x1="2.575" y1="2.965" x2="2.715" y2="2.975" layer="21"/>
<rectangle x1="1.115" y1="2.975" x2="1.265" y2="2.985" layer="21"/>
<rectangle x1="2.565" y1="2.975" x2="2.715" y2="2.985" layer="21"/>
<rectangle x1="1.115" y1="2.985" x2="1.265" y2="2.995" layer="21"/>
<rectangle x1="2.565" y1="2.985" x2="2.715" y2="2.995" layer="21"/>
<rectangle x1="1.125" y1="2.995" x2="1.265" y2="3.005" layer="21"/>
<rectangle x1="2.565" y1="2.995" x2="2.705" y2="3.005" layer="21"/>
<rectangle x1="1.125" y1="3.005" x2="1.275" y2="3.015" layer="21"/>
<rectangle x1="2.555" y1="3.005" x2="2.705" y2="3.015" layer="21"/>
<rectangle x1="1.125" y1="3.015" x2="1.275" y2="3.025" layer="21"/>
<rectangle x1="2.555" y1="3.015" x2="2.705" y2="3.025" layer="21"/>
<rectangle x1="1.135" y1="3.025" x2="1.285" y2="3.035" layer="21"/>
<rectangle x1="2.545" y1="3.025" x2="2.695" y2="3.035" layer="21"/>
<rectangle x1="1.135" y1="3.035" x2="1.285" y2="3.045" layer="21"/>
<rectangle x1="2.545" y1="3.035" x2="2.695" y2="3.045" layer="21"/>
<rectangle x1="1.135" y1="3.045" x2="1.295" y2="3.055" layer="21"/>
<rectangle x1="2.535" y1="3.045" x2="2.695" y2="3.055" layer="21"/>
<rectangle x1="1.145" y1="3.055" x2="1.295" y2="3.065" layer="21"/>
<rectangle x1="2.535" y1="3.055" x2="2.685" y2="3.065" layer="21"/>
<rectangle x1="1.145" y1="3.065" x2="1.305" y2="3.075" layer="21"/>
<rectangle x1="2.525" y1="3.065" x2="2.685" y2="3.075" layer="21"/>
<rectangle x1="1.155" y1="3.075" x2="1.305" y2="3.085" layer="21"/>
<rectangle x1="2.525" y1="3.075" x2="2.675" y2="3.085" layer="21"/>
<rectangle x1="1.155" y1="3.085" x2="1.315" y2="3.095" layer="21"/>
<rectangle x1="2.515" y1="3.085" x2="2.675" y2="3.095" layer="21"/>
<rectangle x1="1.155" y1="3.095" x2="1.315" y2="3.105" layer="21"/>
<rectangle x1="2.515" y1="3.095" x2="2.675" y2="3.105" layer="21"/>
<rectangle x1="1.165" y1="3.105" x2="1.325" y2="3.115" layer="21"/>
<rectangle x1="2.505" y1="3.105" x2="2.665" y2="3.115" layer="21"/>
<rectangle x1="1.165" y1="3.115" x2="1.325" y2="3.125" layer="21"/>
<rectangle x1="2.505" y1="3.115" x2="2.665" y2="3.125" layer="21"/>
<rectangle x1="1.175" y1="3.125" x2="1.335" y2="3.135" layer="21"/>
<rectangle x1="2.495" y1="3.125" x2="2.655" y2="3.135" layer="21"/>
<rectangle x1="1.175" y1="3.135" x2="1.345" y2="3.145" layer="21"/>
<rectangle x1="2.485" y1="3.135" x2="2.655" y2="3.145" layer="21"/>
<rectangle x1="1.185" y1="3.145" x2="1.345" y2="3.155" layer="21"/>
<rectangle x1="2.485" y1="3.145" x2="2.645" y2="3.155" layer="21"/>
<rectangle x1="1.185" y1="3.155" x2="1.355" y2="3.165" layer="21"/>
<rectangle x1="2.475" y1="3.155" x2="2.645" y2="3.165" layer="21"/>
<rectangle x1="1.195" y1="3.165" x2="1.365" y2="3.175" layer="21"/>
<rectangle x1="2.465" y1="3.165" x2="2.635" y2="3.175" layer="21"/>
<rectangle x1="1.205" y1="3.175" x2="1.365" y2="3.185" layer="21"/>
<rectangle x1="2.465" y1="3.175" x2="2.625" y2="3.185" layer="21"/>
<rectangle x1="1.205" y1="3.185" x2="1.375" y2="3.195" layer="21"/>
<rectangle x1="2.455" y1="3.185" x2="2.625" y2="3.195" layer="21"/>
<rectangle x1="1.215" y1="3.195" x2="1.385" y2="3.205" layer="21"/>
<rectangle x1="2.445" y1="3.195" x2="2.615" y2="3.205" layer="21"/>
<rectangle x1="1.215" y1="3.205" x2="1.395" y2="3.215" layer="21"/>
<rectangle x1="2.435" y1="3.205" x2="2.615" y2="3.215" layer="21"/>
<rectangle x1="1.225" y1="3.215" x2="1.405" y2="3.225" layer="21"/>
<rectangle x1="2.425" y1="3.215" x2="2.605" y2="3.225" layer="21"/>
<rectangle x1="1.235" y1="3.225" x2="1.415" y2="3.235" layer="21"/>
<rectangle x1="2.415" y1="3.225" x2="2.595" y2="3.235" layer="21"/>
<rectangle x1="1.235" y1="3.235" x2="1.425" y2="3.245" layer="21"/>
<rectangle x1="2.405" y1="3.235" x2="2.595" y2="3.245" layer="21"/>
<rectangle x1="1.245" y1="3.245" x2="1.435" y2="3.255" layer="21"/>
<rectangle x1="2.395" y1="3.245" x2="2.585" y2="3.255" layer="21"/>
<rectangle x1="1.255" y1="3.255" x2="1.445" y2="3.265" layer="21"/>
<rectangle x1="2.385" y1="3.255" x2="2.575" y2="3.265" layer="21"/>
<rectangle x1="1.265" y1="3.265" x2="1.455" y2="3.275" layer="21"/>
<rectangle x1="2.375" y1="3.265" x2="2.565" y2="3.275" layer="21"/>
<rectangle x1="1.265" y1="3.275" x2="1.465" y2="3.285" layer="21"/>
<rectangle x1="2.365" y1="3.275" x2="2.565" y2="3.285" layer="21"/>
<rectangle x1="1.275" y1="3.285" x2="1.475" y2="3.295" layer="21"/>
<rectangle x1="2.355" y1="3.285" x2="2.555" y2="3.295" layer="21"/>
<rectangle x1="1.285" y1="3.295" x2="1.485" y2="3.305" layer="21"/>
<rectangle x1="2.345" y1="3.295" x2="2.545" y2="3.305" layer="21"/>
<rectangle x1="1.295" y1="3.305" x2="1.495" y2="3.315" layer="21"/>
<rectangle x1="2.335" y1="3.305" x2="2.535" y2="3.315" layer="21"/>
<rectangle x1="1.305" y1="3.315" x2="1.515" y2="3.325" layer="21"/>
<rectangle x1="2.315" y1="3.315" x2="2.525" y2="3.325" layer="21"/>
<rectangle x1="1.315" y1="3.325" x2="1.525" y2="3.335" layer="21"/>
<rectangle x1="2.305" y1="3.325" x2="2.515" y2="3.335" layer="21"/>
<rectangle x1="1.325" y1="3.335" x2="1.545" y2="3.345" layer="21"/>
<rectangle x1="2.285" y1="3.335" x2="2.505" y2="3.345" layer="21"/>
<rectangle x1="1.335" y1="3.345" x2="1.555" y2="3.355" layer="21"/>
<rectangle x1="2.275" y1="3.345" x2="2.495" y2="3.355" layer="21"/>
<rectangle x1="1.345" y1="3.355" x2="1.575" y2="3.365" layer="21"/>
<rectangle x1="2.255" y1="3.355" x2="2.485" y2="3.365" layer="21"/>
<rectangle x1="1.355" y1="3.365" x2="1.595" y2="3.375" layer="21"/>
<rectangle x1="2.235" y1="3.365" x2="2.475" y2="3.375" layer="21"/>
<rectangle x1="1.365" y1="3.375" x2="1.615" y2="3.385" layer="21"/>
<rectangle x1="2.215" y1="3.375" x2="2.465" y2="3.385" layer="21"/>
<rectangle x1="1.375" y1="3.385" x2="1.635" y2="3.395" layer="21"/>
<rectangle x1="2.195" y1="3.385" x2="2.455" y2="3.395" layer="21"/>
<rectangle x1="1.385" y1="3.395" x2="1.655" y2="3.405" layer="21"/>
<rectangle x1="2.175" y1="3.395" x2="2.445" y2="3.405" layer="21"/>
<rectangle x1="1.395" y1="3.405" x2="1.675" y2="3.415" layer="21"/>
<rectangle x1="2.155" y1="3.405" x2="2.435" y2="3.415" layer="21"/>
<rectangle x1="1.415" y1="3.415" x2="1.705" y2="3.425" layer="21"/>
<rectangle x1="2.125" y1="3.415" x2="2.415" y2="3.425" layer="21"/>
<rectangle x1="1.425" y1="3.425" x2="1.705" y2="3.435" layer="21"/>
<rectangle x1="2.125" y1="3.425" x2="2.405" y2="3.435" layer="21"/>
<rectangle x1="1.435" y1="3.435" x2="1.705" y2="3.445" layer="21"/>
<rectangle x1="2.125" y1="3.435" x2="2.395" y2="3.445" layer="21"/>
<rectangle x1="1.455" y1="3.445" x2="1.705" y2="3.455" layer="21"/>
<rectangle x1="2.125" y1="3.445" x2="2.375" y2="3.455" layer="21"/>
<rectangle x1="1.465" y1="3.455" x2="1.705" y2="3.465" layer="21"/>
<rectangle x1="2.125" y1="3.455" x2="2.365" y2="3.465" layer="21"/>
<rectangle x1="1.485" y1="3.465" x2="1.705" y2="3.475" layer="21"/>
<rectangle x1="2.125" y1="3.465" x2="2.345" y2="3.475" layer="21"/>
<rectangle x1="1.505" y1="3.475" x2="1.705" y2="3.485" layer="21"/>
<rectangle x1="2.125" y1="3.475" x2="2.325" y2="3.485" layer="21"/>
<rectangle x1="1.515" y1="3.485" x2="1.705" y2="3.495" layer="21"/>
<rectangle x1="2.125" y1="3.485" x2="2.315" y2="3.495" layer="21"/>
<rectangle x1="1.535" y1="3.495" x2="1.705" y2="3.505" layer="21"/>
<rectangle x1="2.125" y1="3.495" x2="2.295" y2="3.505" layer="21"/>
<rectangle x1="1.555" y1="3.505" x2="1.705" y2="3.515" layer="21"/>
<rectangle x1="2.125" y1="3.505" x2="2.275" y2="3.515" layer="21"/>
<rectangle x1="1.575" y1="3.515" x2="1.705" y2="3.525" layer="21"/>
<rectangle x1="2.125" y1="3.515" x2="2.255" y2="3.525" layer="21"/>
<rectangle x1="1.605" y1="3.525" x2="1.705" y2="3.535" layer="21"/>
<rectangle x1="2.125" y1="3.525" x2="2.225" y2="3.535" layer="21"/>
<rectangle x1="1.625" y1="3.535" x2="1.705" y2="3.545" layer="21"/>
<rectangle x1="2.125" y1="3.535" x2="2.205" y2="3.545" layer="21"/>
<rectangle x1="1.655" y1="3.545" x2="1.705" y2="3.555" layer="21"/>
<rectangle x1="2.125" y1="3.545" x2="2.175" y2="3.555" layer="21"/>
<rectangle x1="1.685" y1="3.555" x2="1.705" y2="3.565" layer="21"/>
<rectangle x1="2.125" y1="3.555" x2="2.145" y2="3.565" layer="21"/>
</package>
<package name="LOGO_QR_1">
<rectangle x1="-111.19" y1="1.22" x2="-91.39" y2="1.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="1.32" x2="-91.39" y2="1.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="1.42" x2="-91.39" y2="1.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="1.52" x2="-91.39" y2="1.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="1.62" x2="-91.39" y2="1.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="1.72" x2="-91.39" y2="1.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="1.82" x2="-91.39" y2="1.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="1.92" x2="-91.39" y2="2.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.02" x2="-91.39" y2="2.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.12" x2="-91.39" y2="2.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.22" x2="-91.39" y2="2.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.32" x2="-91.39" y2="2.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.42" x2="-91.39" y2="2.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.52" x2="-91.39" y2="2.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.62" x2="-91.39" y2="2.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.72" x2="-91.39" y2="2.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.82" x2="-91.39" y2="2.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="2.92" x2="-91.39" y2="3.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.02" x2="-91.39" y2="3.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.12" x2="-91.39" y2="3.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.22" x2="-91.39" y2="3.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.32" x2="-91.39" y2="3.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.42" x2="-91.39" y2="3.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.52" x2="-91.39" y2="3.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="2.35" x2="7.82" y2="2.45" layer="21"/>
<rectangle x1="8.42" y1="2.35" x2="10.22" y2="2.45" layer="21"/>
<rectangle x1="10.82" y1="2.35" x2="11.42" y2="2.45" layer="21"/>
<rectangle x1="12.62" y1="2.35" x2="14.42" y2="2.45" layer="21"/>
<rectangle x1="15.02" y1="2.35" x2="15.62" y2="2.45" layer="21"/>
<rectangle x1="17.42" y1="2.35" x2="18.62" y2="2.45" layer="21"/>
<rectangle x1="-93.79" y1="3.62" x2="-91.39" y2="3.72" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="3.62" x2="-97.99" y2="3.72" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="3.62" x2="-100.39" y2="3.72" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="3.62" x2="-101.59" y2="3.72" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="3.62" x2="-104.59" y2="3.72" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="3.62" x2="-105.79" y2="3.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.62" x2="-108.79" y2="3.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="2.45" x2="7.82" y2="2.55" layer="21"/>
<rectangle x1="8.42" y1="2.45" x2="10.22" y2="2.55" layer="21"/>
<rectangle x1="10.82" y1="2.45" x2="11.42" y2="2.55" layer="21"/>
<rectangle x1="12.62" y1="2.45" x2="14.42" y2="2.55" layer="21"/>
<rectangle x1="15.02" y1="2.45" x2="15.62" y2="2.55" layer="21"/>
<rectangle x1="17.42" y1="2.45" x2="18.62" y2="2.55" layer="21"/>
<rectangle x1="-93.79" y1="3.72" x2="-91.39" y2="3.82" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="3.72" x2="-97.99" y2="3.82" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="3.72" x2="-100.39" y2="3.82" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="3.72" x2="-101.59" y2="3.82" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="3.72" x2="-104.59" y2="3.82" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="3.72" x2="-105.79" y2="3.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.72" x2="-108.79" y2="3.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="2.55" x2="7.82" y2="2.65" layer="21"/>
<rectangle x1="8.42" y1="2.55" x2="10.22" y2="2.65" layer="21"/>
<rectangle x1="10.82" y1="2.55" x2="11.42" y2="2.65" layer="21"/>
<rectangle x1="12.62" y1="2.55" x2="14.42" y2="2.65" layer="21"/>
<rectangle x1="15.02" y1="2.55" x2="15.62" y2="2.65" layer="21"/>
<rectangle x1="17.42" y1="2.55" x2="18.62" y2="2.65" layer="21"/>
<rectangle x1="-93.79" y1="3.82" x2="-91.39" y2="3.92" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="3.82" x2="-97.99" y2="3.92" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="3.82" x2="-100.39" y2="3.92" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="3.82" x2="-101.59" y2="3.92" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="3.82" x2="-104.59" y2="3.92" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="3.82" x2="-105.79" y2="3.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.82" x2="-108.79" y2="3.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="2.65" x2="7.82" y2="2.75" layer="21"/>
<rectangle x1="8.42" y1="2.65" x2="10.22" y2="2.75" layer="21"/>
<rectangle x1="10.82" y1="2.65" x2="11.42" y2="2.75" layer="21"/>
<rectangle x1="12.62" y1="2.65" x2="14.42" y2="2.75" layer="21"/>
<rectangle x1="15.02" y1="2.65" x2="15.62" y2="2.75" layer="21"/>
<rectangle x1="17.42" y1="2.65" x2="18.62" y2="2.75" layer="21"/>
<rectangle x1="-93.79" y1="3.92" x2="-91.39" y2="4.02" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="3.92" x2="-97.99" y2="4.02" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="3.92" x2="-100.39" y2="4.02" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="3.92" x2="-101.59" y2="4.02" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="3.92" x2="-104.59" y2="4.02" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="3.92" x2="-105.79" y2="4.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="3.92" x2="-108.79" y2="4.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="2.75" x2="7.82" y2="2.85" layer="21"/>
<rectangle x1="8.42" y1="2.75" x2="10.22" y2="2.85" layer="21"/>
<rectangle x1="10.82" y1="2.75" x2="11.42" y2="2.85" layer="21"/>
<rectangle x1="12.62" y1="2.75" x2="14.42" y2="2.85" layer="21"/>
<rectangle x1="15.02" y1="2.75" x2="15.62" y2="2.85" layer="21"/>
<rectangle x1="17.42" y1="2.75" x2="18.62" y2="2.85" layer="21"/>
<rectangle x1="-93.79" y1="4.02" x2="-91.39" y2="4.12" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="4.02" x2="-97.99" y2="4.12" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="4.02" x2="-100.39" y2="4.12" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.02" x2="-101.59" y2="4.12" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="4.02" x2="-104.59" y2="4.12" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="4.02" x2="-105.79" y2="4.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.02" x2="-108.79" y2="4.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="2.85" x2="7.82" y2="2.95" layer="21"/>
<rectangle x1="8.42" y1="2.85" x2="10.22" y2="2.95" layer="21"/>
<rectangle x1="10.82" y1="2.85" x2="11.42" y2="2.95" layer="21"/>
<rectangle x1="12.62" y1="2.85" x2="14.42" y2="2.95" layer="21"/>
<rectangle x1="15.02" y1="2.85" x2="15.62" y2="2.95" layer="21"/>
<rectangle x1="17.42" y1="2.85" x2="18.62" y2="2.95" layer="21"/>
<rectangle x1="-93.79" y1="4.12" x2="-91.39" y2="4.22" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="4.12" x2="-97.99" y2="4.22" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="4.12" x2="-100.39" y2="4.22" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.12" x2="-101.59" y2="4.22" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="4.12" x2="-104.59" y2="4.22" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="4.12" x2="-105.79" y2="4.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.12" x2="-108.79" y2="4.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="2.95" x2="4.22" y2="3.05" layer="21"/>
<rectangle x1="7.22" y1="2.95" x2="7.82" y2="3.05" layer="21"/>
<rectangle x1="9.02" y1="2.95" x2="10.82" y2="3.05" layer="21"/>
<rectangle x1="12.62" y1="2.95" x2="13.82" y2="3.05" layer="21"/>
<rectangle x1="15.62" y1="2.95" x2="16.22" y2="3.05" layer="21"/>
<rectangle x1="17.42" y1="2.95" x2="18.02" y2="3.05" layer="21"/>
<rectangle x1="-93.79" y1="4.22" x2="-91.39" y2="4.32" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="4.22" x2="-94.39" y2="4.32" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="4.22" x2="-97.99" y2="4.32" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.22" x2="-100.99" y2="4.32" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="4.22" x2="-103.99" y2="4.32" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="4.22" x2="-106.39" y2="4.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.22" x2="-108.19" y2="4.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.05" x2="4.22" y2="3.15" layer="21"/>
<rectangle x1="7.22" y1="3.05" x2="7.82" y2="3.15" layer="21"/>
<rectangle x1="9.02" y1="3.05" x2="10.82" y2="3.15" layer="21"/>
<rectangle x1="12.62" y1="3.05" x2="13.82" y2="3.15" layer="21"/>
<rectangle x1="15.62" y1="3.05" x2="16.22" y2="3.15" layer="21"/>
<rectangle x1="17.42" y1="3.05" x2="18.02" y2="3.15" layer="21"/>
<rectangle x1="-93.79" y1="4.32" x2="-91.39" y2="4.42" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="4.32" x2="-94.39" y2="4.42" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="4.32" x2="-97.99" y2="4.42" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.32" x2="-100.99" y2="4.42" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="4.32" x2="-103.99" y2="4.42" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="4.32" x2="-106.39" y2="4.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.32" x2="-108.19" y2="4.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.15" x2="4.22" y2="3.25" layer="21"/>
<rectangle x1="7.22" y1="3.15" x2="7.82" y2="3.25" layer="21"/>
<rectangle x1="9.02" y1="3.15" x2="10.82" y2="3.25" layer="21"/>
<rectangle x1="12.62" y1="3.15" x2="13.82" y2="3.25" layer="21"/>
<rectangle x1="15.62" y1="3.15" x2="16.22" y2="3.25" layer="21"/>
<rectangle x1="17.42" y1="3.15" x2="18.02" y2="3.25" layer="21"/>
<rectangle x1="-93.79" y1="4.42" x2="-91.39" y2="4.52" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="4.42" x2="-94.39" y2="4.52" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="4.42" x2="-97.99" y2="4.52" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.42" x2="-100.99" y2="4.52" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="4.42" x2="-103.99" y2="4.52" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="4.42" x2="-106.39" y2="4.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.42" x2="-108.19" y2="4.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.25" x2="4.22" y2="3.35" layer="21"/>
<rectangle x1="7.22" y1="3.25" x2="7.82" y2="3.35" layer="21"/>
<rectangle x1="9.02" y1="3.25" x2="10.82" y2="3.35" layer="21"/>
<rectangle x1="12.62" y1="3.25" x2="13.82" y2="3.35" layer="21"/>
<rectangle x1="15.62" y1="3.25" x2="16.22" y2="3.35" layer="21"/>
<rectangle x1="17.42" y1="3.25" x2="18.02" y2="3.35" layer="21"/>
<rectangle x1="-93.79" y1="4.52" x2="-91.39" y2="4.62" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="4.52" x2="-94.39" y2="4.62" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="4.52" x2="-97.99" y2="4.62" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.52" x2="-100.99" y2="4.62" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="4.52" x2="-103.99" y2="4.62" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="4.52" x2="-106.39" y2="4.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.52" x2="-108.19" y2="4.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.35" x2="4.22" y2="3.45" layer="21"/>
<rectangle x1="7.22" y1="3.35" x2="7.82" y2="3.45" layer="21"/>
<rectangle x1="9.02" y1="3.35" x2="10.82" y2="3.45" layer="21"/>
<rectangle x1="12.62" y1="3.35" x2="13.82" y2="3.45" layer="21"/>
<rectangle x1="15.62" y1="3.35" x2="16.22" y2="3.45" layer="21"/>
<rectangle x1="17.42" y1="3.35" x2="18.02" y2="3.45" layer="21"/>
<rectangle x1="-93.79" y1="4.62" x2="-91.39" y2="4.72" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="4.62" x2="-94.39" y2="4.72" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="4.62" x2="-97.99" y2="4.72" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.62" x2="-100.99" y2="4.72" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="4.62" x2="-103.99" y2="4.72" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="4.62" x2="-106.39" y2="4.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.62" x2="-108.19" y2="4.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.45" x2="4.22" y2="3.55" layer="21"/>
<rectangle x1="7.22" y1="3.45" x2="7.82" y2="3.55" layer="21"/>
<rectangle x1="9.02" y1="3.45" x2="10.82" y2="3.55" layer="21"/>
<rectangle x1="12.62" y1="3.45" x2="13.82" y2="3.55" layer="21"/>
<rectangle x1="15.62" y1="3.45" x2="16.22" y2="3.55" layer="21"/>
<rectangle x1="17.42" y1="3.45" x2="18.02" y2="3.55" layer="21"/>
<rectangle x1="-93.79" y1="4.72" x2="-91.39" y2="4.82" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="4.72" x2="-94.39" y2="4.82" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="4.72" x2="-97.99" y2="4.82" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.72" x2="-100.99" y2="4.82" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="4.72" x2="-103.99" y2="4.82" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="4.72" x2="-106.39" y2="4.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.72" x2="-108.19" y2="4.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.55" x2="4.22" y2="3.65" layer="21"/>
<rectangle x1="4.82" y1="3.55" x2="6.62" y2="3.65" layer="21"/>
<rectangle x1="7.22" y1="3.55" x2="7.82" y2="3.65" layer="21"/>
<rectangle x1="8.42" y1="3.55" x2="9.02" y2="3.65" layer="21"/>
<rectangle x1="11.42" y1="3.55" x2="12.02" y2="3.65" layer="21"/>
<rectangle x1="12.62" y1="3.55" x2="13.22" y2="3.65" layer="21"/>
<rectangle x1="15.02" y1="3.55" x2="16.22" y2="3.65" layer="21"/>
<rectangle x1="16.82" y1="3.55" x2="17.42" y2="3.65" layer="21"/>
<rectangle x1="18.02" y1="3.55" x2="18.62" y2="3.65" layer="21"/>
<rectangle x1="-93.79" y1="4.82" x2="-91.39" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="4.82" x2="-94.39" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="4.82" x2="-96.79" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="4.82" x2="-97.99" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="4.82" x2="-99.19" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.82" x2="-102.19" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="4.82" x2="-103.39" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="4.82" x2="-106.39" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="4.82" x2="-107.59" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.82" x2="-108.79" y2="4.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.65" x2="4.22" y2="3.75" layer="21"/>
<rectangle x1="4.82" y1="3.65" x2="6.62" y2="3.75" layer="21"/>
<rectangle x1="7.22" y1="3.65" x2="7.82" y2="3.75" layer="21"/>
<rectangle x1="8.42" y1="3.65" x2="9.02" y2="3.75" layer="21"/>
<rectangle x1="11.42" y1="3.65" x2="12.02" y2="3.75" layer="21"/>
<rectangle x1="12.62" y1="3.65" x2="13.22" y2="3.75" layer="21"/>
<rectangle x1="15.02" y1="3.65" x2="16.22" y2="3.75" layer="21"/>
<rectangle x1="16.82" y1="3.65" x2="17.42" y2="3.75" layer="21"/>
<rectangle x1="18.02" y1="3.65" x2="18.62" y2="3.75" layer="21"/>
<rectangle x1="-93.79" y1="4.92" x2="-91.39" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="4.92" x2="-94.39" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="4.92" x2="-96.79" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="4.92" x2="-97.99" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="4.92" x2="-99.19" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="4.92" x2="-102.19" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="4.92" x2="-103.39" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="4.92" x2="-106.39" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="4.92" x2="-107.59" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="4.92" x2="-108.79" y2="5.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.75" x2="4.22" y2="3.85" layer="21"/>
<rectangle x1="4.82" y1="3.75" x2="6.62" y2="3.85" layer="21"/>
<rectangle x1="7.22" y1="3.75" x2="7.82" y2="3.85" layer="21"/>
<rectangle x1="8.42" y1="3.75" x2="9.02" y2="3.85" layer="21"/>
<rectangle x1="11.42" y1="3.75" x2="12.02" y2="3.85" layer="21"/>
<rectangle x1="12.62" y1="3.75" x2="13.22" y2="3.85" layer="21"/>
<rectangle x1="15.02" y1="3.75" x2="16.22" y2="3.85" layer="21"/>
<rectangle x1="16.82" y1="3.75" x2="17.42" y2="3.85" layer="21"/>
<rectangle x1="18.02" y1="3.75" x2="18.62" y2="3.85" layer="21"/>
<rectangle x1="-93.79" y1="5.02" x2="-91.39" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.02" x2="-94.39" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.02" x2="-96.79" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="5.02" x2="-97.99" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="5.02" x2="-99.19" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="5.02" x2="-102.19" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="5.02" x2="-103.39" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="5.02" x2="-106.39" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="5.02" x2="-107.59" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.02" x2="-108.79" y2="5.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.85" x2="4.22" y2="3.95" layer="21"/>
<rectangle x1="4.82" y1="3.85" x2="6.62" y2="3.95" layer="21"/>
<rectangle x1="7.22" y1="3.85" x2="7.82" y2="3.95" layer="21"/>
<rectangle x1="8.42" y1="3.85" x2="9.02" y2="3.95" layer="21"/>
<rectangle x1="11.42" y1="3.85" x2="12.02" y2="3.95" layer="21"/>
<rectangle x1="12.62" y1="3.85" x2="13.22" y2="3.95" layer="21"/>
<rectangle x1="15.02" y1="3.85" x2="16.22" y2="3.95" layer="21"/>
<rectangle x1="16.82" y1="3.85" x2="17.42" y2="3.95" layer="21"/>
<rectangle x1="18.02" y1="3.85" x2="18.62" y2="3.95" layer="21"/>
<rectangle x1="-93.79" y1="5.12" x2="-91.39" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.12" x2="-94.39" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.12" x2="-96.79" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="5.12" x2="-97.99" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="5.12" x2="-99.19" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="5.12" x2="-102.19" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="5.12" x2="-103.39" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="5.12" x2="-106.39" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="5.12" x2="-107.59" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.12" x2="-108.79" y2="5.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="3.95" x2="4.22" y2="4.05" layer="21"/>
<rectangle x1="4.82" y1="3.95" x2="6.62" y2="4.05" layer="21"/>
<rectangle x1="7.22" y1="3.95" x2="7.82" y2="4.05" layer="21"/>
<rectangle x1="8.42" y1="3.95" x2="9.02" y2="4.05" layer="21"/>
<rectangle x1="11.42" y1="3.95" x2="12.02" y2="4.05" layer="21"/>
<rectangle x1="12.62" y1="3.95" x2="13.22" y2="4.05" layer="21"/>
<rectangle x1="15.02" y1="3.95" x2="16.22" y2="4.05" layer="21"/>
<rectangle x1="16.82" y1="3.95" x2="17.42" y2="4.05" layer="21"/>
<rectangle x1="18.02" y1="3.95" x2="18.62" y2="4.05" layer="21"/>
<rectangle x1="-93.79" y1="5.22" x2="-91.39" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.22" x2="-94.39" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.22" x2="-96.79" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="5.22" x2="-97.99" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="5.22" x2="-99.19" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="5.22" x2="-102.19" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="5.22" x2="-103.39" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="5.22" x2="-106.39" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="5.22" x2="-107.59" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.22" x2="-108.79" y2="5.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.05" x2="4.22" y2="4.15" layer="21"/>
<rectangle x1="4.82" y1="4.05" x2="6.62" y2="4.15" layer="21"/>
<rectangle x1="7.22" y1="4.05" x2="7.82" y2="4.15" layer="21"/>
<rectangle x1="8.42" y1="4.05" x2="9.02" y2="4.15" layer="21"/>
<rectangle x1="11.42" y1="4.05" x2="12.02" y2="4.15" layer="21"/>
<rectangle x1="12.62" y1="4.05" x2="13.22" y2="4.15" layer="21"/>
<rectangle x1="15.02" y1="4.05" x2="16.22" y2="4.15" layer="21"/>
<rectangle x1="16.82" y1="4.05" x2="17.42" y2="4.15" layer="21"/>
<rectangle x1="18.02" y1="4.05" x2="18.62" y2="4.15" layer="21"/>
<rectangle x1="-93.79" y1="5.32" x2="-91.39" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.32" x2="-94.39" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.32" x2="-96.79" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="5.32" x2="-97.99" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="5.32" x2="-99.19" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="5.32" x2="-102.19" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="5.32" x2="-103.39" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="5.32" x2="-106.39" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="5.32" x2="-107.59" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.32" x2="-108.79" y2="5.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.15" x2="4.22" y2="4.25" layer="21"/>
<rectangle x1="4.82" y1="4.15" x2="6.62" y2="4.25" layer="21"/>
<rectangle x1="7.22" y1="4.15" x2="7.82" y2="4.25" layer="21"/>
<rectangle x1="9.02" y1="4.15" x2="10.82" y2="4.25" layer="21"/>
<rectangle x1="12.02" y1="4.15" x2="12.62" y2="4.25" layer="21"/>
<rectangle x1="15.62" y1="4.15" x2="16.22" y2="4.25" layer="21"/>
<rectangle x1="-93.79" y1="5.42" x2="-91.39" y2="5.52" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.42" x2="-94.39" y2="5.52" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.42" x2="-96.79" y2="5.52" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="5.42" x2="-97.99" y2="5.52" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="5.42" x2="-100.99" y2="5.52" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="5.42" x2="-102.79" y2="5.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.42" x2="-106.39" y2="5.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.25" x2="4.22" y2="4.35" layer="21"/>
<rectangle x1="4.82" y1="4.25" x2="6.62" y2="4.35" layer="21"/>
<rectangle x1="7.22" y1="4.25" x2="7.82" y2="4.35" layer="21"/>
<rectangle x1="9.02" y1="4.25" x2="10.82" y2="4.35" layer="21"/>
<rectangle x1="12.02" y1="4.25" x2="12.62" y2="4.35" layer="21"/>
<rectangle x1="15.62" y1="4.25" x2="16.22" y2="4.35" layer="21"/>
<rectangle x1="-93.79" y1="5.52" x2="-91.39" y2="5.62" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.52" x2="-94.39" y2="5.62" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.52" x2="-96.79" y2="5.62" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="5.52" x2="-97.99" y2="5.62" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="5.52" x2="-100.99" y2="5.62" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="5.52" x2="-102.79" y2="5.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.52" x2="-106.39" y2="5.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.35" x2="4.22" y2="4.45" layer="21"/>
<rectangle x1="4.82" y1="4.35" x2="6.62" y2="4.45" layer="21"/>
<rectangle x1="7.22" y1="4.35" x2="7.82" y2="4.45" layer="21"/>
<rectangle x1="9.02" y1="4.35" x2="10.82" y2="4.45" layer="21"/>
<rectangle x1="12.02" y1="4.35" x2="12.62" y2="4.45" layer="21"/>
<rectangle x1="15.62" y1="4.35" x2="16.22" y2="4.45" layer="21"/>
<rectangle x1="-93.79" y1="5.62" x2="-91.39" y2="5.72" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.62" x2="-94.39" y2="5.72" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.62" x2="-96.79" y2="5.72" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="5.62" x2="-97.99" y2="5.72" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="5.62" x2="-100.99" y2="5.72" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="5.62" x2="-102.79" y2="5.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.62" x2="-106.39" y2="5.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.45" x2="4.22" y2="4.55" layer="21"/>
<rectangle x1="4.82" y1="4.45" x2="6.62" y2="4.55" layer="21"/>
<rectangle x1="7.22" y1="4.45" x2="7.82" y2="4.55" layer="21"/>
<rectangle x1="9.02" y1="4.45" x2="10.82" y2="4.55" layer="21"/>
<rectangle x1="12.02" y1="4.45" x2="12.62" y2="4.55" layer="21"/>
<rectangle x1="15.62" y1="4.45" x2="16.22" y2="4.55" layer="21"/>
<rectangle x1="-93.79" y1="5.72" x2="-91.39" y2="5.82" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.72" x2="-94.39" y2="5.82" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.72" x2="-96.79" y2="5.82" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="5.72" x2="-97.99" y2="5.82" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="5.72" x2="-100.99" y2="5.82" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="5.72" x2="-102.79" y2="5.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.72" x2="-106.39" y2="5.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.55" x2="4.22" y2="4.65" layer="21"/>
<rectangle x1="4.82" y1="4.55" x2="6.62" y2="4.65" layer="21"/>
<rectangle x1="7.22" y1="4.55" x2="7.82" y2="4.65" layer="21"/>
<rectangle x1="9.02" y1="4.55" x2="10.82" y2="4.65" layer="21"/>
<rectangle x1="12.02" y1="4.55" x2="12.62" y2="4.65" layer="21"/>
<rectangle x1="15.62" y1="4.55" x2="16.22" y2="4.65" layer="21"/>
<rectangle x1="-93.79" y1="5.82" x2="-91.39" y2="5.92" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.82" x2="-94.39" y2="5.92" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.82" x2="-96.79" y2="5.92" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="5.82" x2="-97.99" y2="5.92" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="5.82" x2="-100.99" y2="5.92" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="5.82" x2="-102.79" y2="5.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.82" x2="-106.39" y2="5.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.65" x2="4.22" y2="4.75" layer="21"/>
<rectangle x1="4.82" y1="4.65" x2="6.62" y2="4.75" layer="21"/>
<rectangle x1="7.22" y1="4.65" x2="7.82" y2="4.75" layer="21"/>
<rectangle x1="9.02" y1="4.65" x2="10.82" y2="4.75" layer="21"/>
<rectangle x1="12.02" y1="4.65" x2="12.62" y2="4.75" layer="21"/>
<rectangle x1="15.62" y1="4.65" x2="16.22" y2="4.75" layer="21"/>
<rectangle x1="-93.79" y1="5.92" x2="-91.39" y2="6.02" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="5.92" x2="-94.39" y2="6.02" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="5.92" x2="-96.79" y2="6.02" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="5.92" x2="-97.99" y2="6.02" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="5.92" x2="-100.99" y2="6.02" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="5.92" x2="-102.79" y2="6.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="5.92" x2="-106.39" y2="6.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.75" x2="4.22" y2="4.85" layer="21"/>
<rectangle x1="4.82" y1="4.75" x2="6.62" y2="4.85" layer="21"/>
<rectangle x1="7.22" y1="4.75" x2="7.82" y2="4.85" layer="21"/>
<rectangle x1="8.42" y1="4.75" x2="10.82" y2="4.85" layer="21"/>
<rectangle x1="12.02" y1="4.75" x2="12.62" y2="4.85" layer="21"/>
<rectangle x1="13.22" y1="4.75" x2="16.82" y2="4.85" layer="21"/>
<rectangle x1="17.42" y1="4.75" x2="18.62" y2="4.85" layer="21"/>
<rectangle x1="-93.79" y1="6.02" x2="-91.39" y2="6.12" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="6.02" x2="-94.39" y2="6.12" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.02" x2="-96.79" y2="6.12" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="6.02" x2="-97.99" y2="6.12" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="6.02" x2="-100.99" y2="6.12" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.02" x2="-102.79" y2="6.12" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.02" x2="-106.99" y2="6.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.02" x2="-108.79" y2="6.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.85" x2="4.22" y2="4.95" layer="21"/>
<rectangle x1="4.82" y1="4.85" x2="6.62" y2="4.95" layer="21"/>
<rectangle x1="7.22" y1="4.85" x2="7.82" y2="4.95" layer="21"/>
<rectangle x1="8.42" y1="4.85" x2="10.82" y2="4.95" layer="21"/>
<rectangle x1="12.02" y1="4.85" x2="12.62" y2="4.95" layer="21"/>
<rectangle x1="13.22" y1="4.85" x2="16.82" y2="4.95" layer="21"/>
<rectangle x1="17.42" y1="4.85" x2="18.62" y2="4.95" layer="21"/>
<rectangle x1="-93.79" y1="6.12" x2="-91.39" y2="6.22" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="6.12" x2="-94.39" y2="6.22" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.12" x2="-96.79" y2="6.22" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="6.12" x2="-97.99" y2="6.22" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="6.12" x2="-100.99" y2="6.22" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.12" x2="-102.79" y2="6.22" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.12" x2="-106.99" y2="6.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.12" x2="-108.79" y2="6.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="4.95" x2="4.22" y2="5.05" layer="21"/>
<rectangle x1="4.82" y1="4.95" x2="6.62" y2="5.05" layer="21"/>
<rectangle x1="7.22" y1="4.95" x2="7.82" y2="5.05" layer="21"/>
<rectangle x1="8.42" y1="4.95" x2="10.82" y2="5.05" layer="21"/>
<rectangle x1="12.02" y1="4.95" x2="12.62" y2="5.05" layer="21"/>
<rectangle x1="13.22" y1="4.95" x2="16.82" y2="5.05" layer="21"/>
<rectangle x1="17.42" y1="4.95" x2="18.62" y2="5.05" layer="21"/>
<rectangle x1="-93.79" y1="6.22" x2="-91.39" y2="6.32" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="6.22" x2="-94.39" y2="6.32" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.22" x2="-96.79" y2="6.32" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="6.22" x2="-97.99" y2="6.32" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="6.22" x2="-100.99" y2="6.32" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.22" x2="-102.79" y2="6.32" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.22" x2="-106.99" y2="6.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.22" x2="-108.79" y2="6.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.05" x2="4.22" y2="5.15" layer="21"/>
<rectangle x1="4.82" y1="5.05" x2="6.62" y2="5.15" layer="21"/>
<rectangle x1="7.22" y1="5.05" x2="7.82" y2="5.15" layer="21"/>
<rectangle x1="8.42" y1="5.05" x2="10.82" y2="5.15" layer="21"/>
<rectangle x1="12.02" y1="5.05" x2="12.62" y2="5.15" layer="21"/>
<rectangle x1="13.22" y1="5.05" x2="16.82" y2="5.15" layer="21"/>
<rectangle x1="17.42" y1="5.05" x2="18.62" y2="5.15" layer="21"/>
<rectangle x1="-93.79" y1="6.32" x2="-91.39" y2="6.42" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="6.32" x2="-94.39" y2="6.42" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.32" x2="-96.79" y2="6.42" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="6.32" x2="-97.99" y2="6.42" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="6.32" x2="-100.99" y2="6.42" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.32" x2="-102.79" y2="6.42" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.32" x2="-106.99" y2="6.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.32" x2="-108.79" y2="6.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.15" x2="4.22" y2="5.25" layer="21"/>
<rectangle x1="4.82" y1="5.15" x2="6.62" y2="5.25" layer="21"/>
<rectangle x1="7.22" y1="5.15" x2="7.82" y2="5.25" layer="21"/>
<rectangle x1="8.42" y1="5.15" x2="10.82" y2="5.25" layer="21"/>
<rectangle x1="12.02" y1="5.15" x2="12.62" y2="5.25" layer="21"/>
<rectangle x1="13.22" y1="5.15" x2="16.82" y2="5.25" layer="21"/>
<rectangle x1="17.42" y1="5.15" x2="18.62" y2="5.25" layer="21"/>
<rectangle x1="-93.79" y1="6.42" x2="-91.39" y2="6.52" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="6.42" x2="-94.39" y2="6.52" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.42" x2="-96.79" y2="6.52" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="6.42" x2="-97.99" y2="6.52" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="6.42" x2="-100.99" y2="6.52" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.42" x2="-102.79" y2="6.52" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.42" x2="-106.99" y2="6.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.42" x2="-108.79" y2="6.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.25" x2="4.22" y2="5.35" layer="21"/>
<rectangle x1="4.82" y1="5.25" x2="6.62" y2="5.35" layer="21"/>
<rectangle x1="7.22" y1="5.25" x2="7.82" y2="5.35" layer="21"/>
<rectangle x1="8.42" y1="5.25" x2="10.82" y2="5.35" layer="21"/>
<rectangle x1="12.02" y1="5.25" x2="12.62" y2="5.35" layer="21"/>
<rectangle x1="13.22" y1="5.25" x2="16.82" y2="5.35" layer="21"/>
<rectangle x1="17.42" y1="5.25" x2="18.62" y2="5.35" layer="21"/>
<rectangle x1="-93.79" y1="6.52" x2="-91.39" y2="6.62" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="6.52" x2="-94.39" y2="6.62" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.52" x2="-96.79" y2="6.62" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="6.52" x2="-97.99" y2="6.62" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="6.52" x2="-100.99" y2="6.62" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.52" x2="-102.79" y2="6.62" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.52" x2="-106.99" y2="6.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.52" x2="-108.79" y2="6.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.35" x2="4.22" y2="5.45" layer="21"/>
<rectangle x1="7.22" y1="5.35" x2="7.82" y2="5.45" layer="21"/>
<rectangle x1="9.62" y1="5.35" x2="10.22" y2="5.45" layer="21"/>
<rectangle x1="10.82" y1="5.35" x2="12.62" y2="5.45" layer="21"/>
<rectangle x1="13.22" y1="5.35" x2="13.82" y2="5.45" layer="21"/>
<rectangle x1="15.62" y1="5.35" x2="16.22" y2="5.45" layer="21"/>
<rectangle x1="17.42" y1="5.35" x2="18.02" y2="5.45" layer="21"/>
<rectangle x1="-93.79" y1="6.62" x2="-91.39" y2="6.72" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.62" x2="-94.39" y2="6.72" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="6.62" x2="-97.99" y2="6.72" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="6.62" x2="-100.39" y2="6.72" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.62" x2="-102.79" y2="6.72" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="6.62" x2="-103.99" y2="6.72" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.62" x2="-106.39" y2="6.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.62" x2="-108.19" y2="6.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.45" x2="4.22" y2="5.55" layer="21"/>
<rectangle x1="7.22" y1="5.45" x2="7.82" y2="5.55" layer="21"/>
<rectangle x1="9.62" y1="5.45" x2="10.22" y2="5.55" layer="21"/>
<rectangle x1="10.82" y1="5.45" x2="12.62" y2="5.55" layer="21"/>
<rectangle x1="13.22" y1="5.45" x2="13.82" y2="5.55" layer="21"/>
<rectangle x1="15.62" y1="5.45" x2="16.22" y2="5.55" layer="21"/>
<rectangle x1="17.42" y1="5.45" x2="18.02" y2="5.55" layer="21"/>
<rectangle x1="-93.79" y1="6.72" x2="-91.39" y2="6.82" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.72" x2="-94.39" y2="6.82" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="6.72" x2="-97.99" y2="6.82" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="6.72" x2="-100.39" y2="6.82" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.72" x2="-102.79" y2="6.82" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="6.72" x2="-103.99" y2="6.82" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.72" x2="-106.39" y2="6.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.72" x2="-108.19" y2="6.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.55" x2="4.22" y2="5.65" layer="21"/>
<rectangle x1="7.22" y1="5.55" x2="7.82" y2="5.65" layer="21"/>
<rectangle x1="9.62" y1="5.55" x2="10.22" y2="5.65" layer="21"/>
<rectangle x1="10.82" y1="5.55" x2="12.62" y2="5.65" layer="21"/>
<rectangle x1="13.22" y1="5.55" x2="13.82" y2="5.65" layer="21"/>
<rectangle x1="15.62" y1="5.55" x2="16.22" y2="5.65" layer="21"/>
<rectangle x1="17.42" y1="5.55" x2="18.02" y2="5.65" layer="21"/>
<rectangle x1="-93.79" y1="6.82" x2="-91.39" y2="6.92" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.82" x2="-94.39" y2="6.92" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="6.82" x2="-97.99" y2="6.92" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="6.82" x2="-100.39" y2="6.92" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.82" x2="-102.79" y2="6.92" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="6.82" x2="-103.99" y2="6.92" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.82" x2="-106.39" y2="6.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.82" x2="-108.19" y2="6.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.65" x2="4.22" y2="5.75" layer="21"/>
<rectangle x1="7.22" y1="5.65" x2="7.82" y2="5.75" layer="21"/>
<rectangle x1="9.62" y1="5.65" x2="10.22" y2="5.75" layer="21"/>
<rectangle x1="10.82" y1="5.65" x2="12.62" y2="5.75" layer="21"/>
<rectangle x1="13.22" y1="5.65" x2="13.82" y2="5.75" layer="21"/>
<rectangle x1="15.62" y1="5.65" x2="16.22" y2="5.75" layer="21"/>
<rectangle x1="17.42" y1="5.65" x2="18.02" y2="5.75" layer="21"/>
<rectangle x1="-93.79" y1="6.92" x2="-91.39" y2="7.02" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="6.92" x2="-94.39" y2="7.02" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="6.92" x2="-97.99" y2="7.02" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="6.92" x2="-100.39" y2="7.02" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="6.92" x2="-102.79" y2="7.02" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="6.92" x2="-103.99" y2="7.02" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="6.92" x2="-106.39" y2="7.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="6.92" x2="-108.19" y2="7.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.75" x2="4.22" y2="5.85" layer="21"/>
<rectangle x1="7.22" y1="5.75" x2="7.82" y2="5.85" layer="21"/>
<rectangle x1="9.62" y1="5.75" x2="10.22" y2="5.85" layer="21"/>
<rectangle x1="10.82" y1="5.75" x2="12.62" y2="5.85" layer="21"/>
<rectangle x1="13.22" y1="5.75" x2="13.82" y2="5.85" layer="21"/>
<rectangle x1="15.62" y1="5.75" x2="16.22" y2="5.85" layer="21"/>
<rectangle x1="17.42" y1="5.75" x2="18.02" y2="5.85" layer="21"/>
<rectangle x1="-93.79" y1="7.02" x2="-91.39" y2="7.12" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="7.02" x2="-94.39" y2="7.12" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="7.02" x2="-97.99" y2="7.12" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="7.02" x2="-100.39" y2="7.12" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="7.02" x2="-102.79" y2="7.12" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.02" x2="-103.99" y2="7.12" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="7.02" x2="-106.39" y2="7.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.02" x2="-108.19" y2="7.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.85" x2="4.22" y2="5.95" layer="21"/>
<rectangle x1="7.22" y1="5.85" x2="7.82" y2="5.95" layer="21"/>
<rectangle x1="9.62" y1="5.85" x2="10.22" y2="5.95" layer="21"/>
<rectangle x1="10.82" y1="5.85" x2="12.62" y2="5.95" layer="21"/>
<rectangle x1="13.22" y1="5.85" x2="13.82" y2="5.95" layer="21"/>
<rectangle x1="15.62" y1="5.85" x2="16.22" y2="5.95" layer="21"/>
<rectangle x1="17.42" y1="5.85" x2="18.02" y2="5.95" layer="21"/>
<rectangle x1="-93.79" y1="7.12" x2="-91.39" y2="7.22" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="7.12" x2="-94.39" y2="7.22" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="7.12" x2="-97.99" y2="7.22" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="7.12" x2="-100.39" y2="7.22" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="7.12" x2="-102.79" y2="7.22" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.12" x2="-103.99" y2="7.22" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="7.12" x2="-106.39" y2="7.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.12" x2="-108.19" y2="7.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="5.95" x2="7.82" y2="6.05" layer="21"/>
<rectangle x1="10.22" y1="5.95" x2="11.42" y2="6.05" layer="21"/>
<rectangle x1="12.02" y1="5.95" x2="13.82" y2="6.05" layer="21"/>
<rectangle x1="14.42" y1="5.95" x2="15.02" y2="6.05" layer="21"/>
<rectangle x1="15.62" y1="5.95" x2="16.82" y2="6.05" layer="21"/>
<rectangle x1="17.42" y1="5.95" x2="18.62" y2="6.05" layer="21"/>
<rectangle x1="-93.79" y1="7.22" x2="-91.39" y2="7.32" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="7.22" x2="-97.99" y2="7.32" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="7.22" x2="-101.59" y2="7.32" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="7.22" x2="-103.99" y2="7.32" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.22" x2="-105.19" y2="7.32" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="7.22" x2="-106.99" y2="7.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.22" x2="-108.79" y2="7.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="6.05" x2="7.82" y2="6.15" layer="21"/>
<rectangle x1="10.22" y1="6.05" x2="11.42" y2="6.15" layer="21"/>
<rectangle x1="12.02" y1="6.05" x2="13.82" y2="6.15" layer="21"/>
<rectangle x1="14.42" y1="6.05" x2="15.02" y2="6.15" layer="21"/>
<rectangle x1="15.62" y1="6.05" x2="16.82" y2="6.15" layer="21"/>
<rectangle x1="17.42" y1="6.05" x2="18.62" y2="6.15" layer="21"/>
<rectangle x1="-93.79" y1="7.32" x2="-91.39" y2="7.42" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="7.32" x2="-97.99" y2="7.42" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="7.32" x2="-101.59" y2="7.42" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="7.32" x2="-103.99" y2="7.42" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.32" x2="-105.19" y2="7.42" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="7.32" x2="-106.99" y2="7.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.32" x2="-108.79" y2="7.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="6.15" x2="7.82" y2="6.25" layer="21"/>
<rectangle x1="10.22" y1="6.15" x2="11.42" y2="6.25" layer="21"/>
<rectangle x1="12.02" y1="6.15" x2="13.82" y2="6.25" layer="21"/>
<rectangle x1="14.42" y1="6.15" x2="15.02" y2="6.25" layer="21"/>
<rectangle x1="15.62" y1="6.15" x2="16.82" y2="6.25" layer="21"/>
<rectangle x1="17.42" y1="6.15" x2="18.62" y2="6.25" layer="21"/>
<rectangle x1="-93.79" y1="7.42" x2="-91.39" y2="7.52" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="7.42" x2="-97.99" y2="7.52" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="7.42" x2="-101.59" y2="7.52" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="7.42" x2="-103.99" y2="7.52" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.42" x2="-105.19" y2="7.52" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="7.42" x2="-106.99" y2="7.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.42" x2="-108.79" y2="7.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="6.25" x2="7.82" y2="6.35" layer="21"/>
<rectangle x1="10.22" y1="6.25" x2="11.42" y2="6.35" layer="21"/>
<rectangle x1="12.02" y1="6.25" x2="13.82" y2="6.35" layer="21"/>
<rectangle x1="14.42" y1="6.25" x2="15.02" y2="6.35" layer="21"/>
<rectangle x1="15.62" y1="6.25" x2="16.82" y2="6.35" layer="21"/>
<rectangle x1="17.42" y1="6.25" x2="18.62" y2="6.35" layer="21"/>
<rectangle x1="-93.79" y1="7.52" x2="-91.39" y2="7.62" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="7.52" x2="-97.99" y2="7.62" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="7.52" x2="-101.59" y2="7.62" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="7.52" x2="-103.99" y2="7.62" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.52" x2="-105.19" y2="7.62" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="7.52" x2="-106.99" y2="7.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.52" x2="-108.79" y2="7.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="6.35" x2="7.82" y2="6.45" layer="21"/>
<rectangle x1="10.22" y1="6.35" x2="11.42" y2="6.45" layer="21"/>
<rectangle x1="12.02" y1="6.35" x2="13.82" y2="6.45" layer="21"/>
<rectangle x1="14.42" y1="6.35" x2="15.02" y2="6.45" layer="21"/>
<rectangle x1="15.62" y1="6.35" x2="16.82" y2="6.45" layer="21"/>
<rectangle x1="17.42" y1="6.35" x2="18.62" y2="6.45" layer="21"/>
<rectangle x1="-93.79" y1="7.62" x2="-91.39" y2="7.72" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="7.62" x2="-97.99" y2="7.72" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="7.62" x2="-101.59" y2="7.72" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="7.62" x2="-103.99" y2="7.72" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.62" x2="-105.19" y2="7.72" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="7.62" x2="-106.99" y2="7.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.62" x2="-108.79" y2="7.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="6.45" x2="7.82" y2="6.55" layer="21"/>
<rectangle x1="10.22" y1="6.45" x2="11.42" y2="6.55" layer="21"/>
<rectangle x1="12.02" y1="6.45" x2="13.82" y2="6.55" layer="21"/>
<rectangle x1="14.42" y1="6.45" x2="15.02" y2="6.55" layer="21"/>
<rectangle x1="15.62" y1="6.45" x2="16.82" y2="6.55" layer="21"/>
<rectangle x1="17.42" y1="6.45" x2="18.62" y2="6.55" layer="21"/>
<rectangle x1="-93.79" y1="7.72" x2="-91.39" y2="7.82" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="7.72" x2="-97.99" y2="7.82" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="7.72" x2="-101.59" y2="7.82" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="7.72" x2="-103.99" y2="7.82" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.72" x2="-105.19" y2="7.82" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="7.72" x2="-106.99" y2="7.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.72" x2="-108.79" y2="7.82" layer="21" rot="R180"/>
<rectangle x1="8.42" y1="6.55" x2="9.02" y2="6.65" layer="21"/>
<rectangle x1="10.22" y1="6.55" x2="12.02" y2="6.65" layer="21"/>
<rectangle x1="12.62" y1="6.55" x2="13.82" y2="6.65" layer="21"/>
<rectangle x1="15.62" y1="6.55" x2="16.22" y2="6.65" layer="21"/>
<rectangle x1="16.82" y1="6.55" x2="17.42" y2="6.65" layer="21"/>
<rectangle x1="18.02" y1="6.55" x2="18.62" y2="6.65" layer="21"/>
<rectangle x1="-98.59" y1="7.82" x2="-91.39" y2="7.92" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="7.82" x2="-99.19" y2="7.92" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="7.82" x2="-102.19" y2="7.92" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.82" x2="-103.99" y2="7.92" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="7.82" x2="-106.39" y2="7.92" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="7.82" x2="-107.59" y2="7.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.82" x2="-108.79" y2="7.92" layer="21" rot="R180"/>
<rectangle x1="8.42" y1="6.65" x2="9.02" y2="6.75" layer="21"/>
<rectangle x1="10.22" y1="6.65" x2="12.02" y2="6.75" layer="21"/>
<rectangle x1="12.62" y1="6.65" x2="13.82" y2="6.75" layer="21"/>
<rectangle x1="15.62" y1="6.65" x2="16.22" y2="6.75" layer="21"/>
<rectangle x1="16.82" y1="6.65" x2="17.42" y2="6.75" layer="21"/>
<rectangle x1="18.02" y1="6.65" x2="18.62" y2="6.75" layer="21"/>
<rectangle x1="-98.59" y1="7.92" x2="-91.39" y2="8.02" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="7.92" x2="-99.19" y2="8.02" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="7.92" x2="-102.19" y2="8.02" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="7.92" x2="-103.99" y2="8.02" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="7.92" x2="-106.39" y2="8.02" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="7.92" x2="-107.59" y2="8.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="7.92" x2="-108.79" y2="8.02" layer="21" rot="R180"/>
<rectangle x1="8.42" y1="6.75" x2="9.02" y2="6.85" layer="21"/>
<rectangle x1="10.22" y1="6.75" x2="12.02" y2="6.85" layer="21"/>
<rectangle x1="12.62" y1="6.75" x2="13.82" y2="6.85" layer="21"/>
<rectangle x1="15.62" y1="6.75" x2="16.22" y2="6.85" layer="21"/>
<rectangle x1="16.82" y1="6.75" x2="17.42" y2="6.85" layer="21"/>
<rectangle x1="18.02" y1="6.75" x2="18.62" y2="6.85" layer="21"/>
<rectangle x1="-98.59" y1="8.02" x2="-91.39" y2="8.12" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="8.02" x2="-99.19" y2="8.12" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="8.02" x2="-102.19" y2="8.12" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="8.02" x2="-103.99" y2="8.12" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="8.02" x2="-106.39" y2="8.12" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="8.02" x2="-107.59" y2="8.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.02" x2="-108.79" y2="8.12" layer="21" rot="R180"/>
<rectangle x1="8.42" y1="6.85" x2="9.02" y2="6.95" layer="21"/>
<rectangle x1="10.22" y1="6.85" x2="12.02" y2="6.95" layer="21"/>
<rectangle x1="12.62" y1="6.85" x2="13.82" y2="6.95" layer="21"/>
<rectangle x1="15.62" y1="6.85" x2="16.22" y2="6.95" layer="21"/>
<rectangle x1="16.82" y1="6.85" x2="17.42" y2="6.95" layer="21"/>
<rectangle x1="18.02" y1="6.85" x2="18.62" y2="6.95" layer="21"/>
<rectangle x1="-98.59" y1="8.12" x2="-91.39" y2="8.22" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="8.12" x2="-99.19" y2="8.22" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="8.12" x2="-102.19" y2="8.22" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="8.12" x2="-103.99" y2="8.22" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="8.12" x2="-106.39" y2="8.22" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="8.12" x2="-107.59" y2="8.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.12" x2="-108.79" y2="8.22" layer="21" rot="R180"/>
<rectangle x1="8.42" y1="6.95" x2="9.02" y2="7.05" layer="21"/>
<rectangle x1="10.22" y1="6.95" x2="12.02" y2="7.05" layer="21"/>
<rectangle x1="12.62" y1="6.95" x2="13.82" y2="7.05" layer="21"/>
<rectangle x1="15.62" y1="6.95" x2="16.22" y2="7.05" layer="21"/>
<rectangle x1="16.82" y1="6.95" x2="17.42" y2="7.05" layer="21"/>
<rectangle x1="18.02" y1="6.95" x2="18.62" y2="7.05" layer="21"/>
<rectangle x1="-98.59" y1="8.22" x2="-91.39" y2="8.32" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="8.22" x2="-99.19" y2="8.32" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="8.22" x2="-102.19" y2="8.32" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="8.22" x2="-103.99" y2="8.32" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="8.22" x2="-106.39" y2="8.32" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="8.22" x2="-107.59" y2="8.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.22" x2="-108.79" y2="8.32" layer="21" rot="R180"/>
<rectangle x1="8.42" y1="7.05" x2="9.02" y2="7.15" layer="21"/>
<rectangle x1="10.22" y1="7.05" x2="12.02" y2="7.15" layer="21"/>
<rectangle x1="12.62" y1="7.05" x2="13.82" y2="7.15" layer="21"/>
<rectangle x1="15.62" y1="7.05" x2="16.22" y2="7.15" layer="21"/>
<rectangle x1="16.82" y1="7.05" x2="17.42" y2="7.15" layer="21"/>
<rectangle x1="18.02" y1="7.05" x2="18.62" y2="7.15" layer="21"/>
<rectangle x1="-98.59" y1="8.32" x2="-91.39" y2="8.42" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="8.32" x2="-99.19" y2="8.42" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="8.32" x2="-102.19" y2="8.42" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="8.32" x2="-103.99" y2="8.42" layer="21" rot="R180"/>
<rectangle x1="-106.99" y1="8.32" x2="-106.39" y2="8.42" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="8.32" x2="-107.59" y2="8.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.32" x2="-108.79" y2="8.42" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="7.15" x2="8.42" y2="7.25" layer="21"/>
<rectangle x1="9.02" y1="7.15" x2="10.22" y2="7.25" layer="21"/>
<rectangle x1="10.82" y1="7.15" x2="11.42" y2="7.25" layer="21"/>
<rectangle x1="13.22" y1="7.15" x2="16.22" y2="7.25" layer="21"/>
<rectangle x1="17.42" y1="7.15" x2="18.62" y2="7.25" layer="21"/>
<rectangle x1="-94.39" y1="8.42" x2="-91.39" y2="8.52" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="8.42" x2="-98.59" y2="8.52" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="8.42" x2="-100.39" y2="8.52" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="8.42" x2="-101.59" y2="8.52" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="8.42" x2="-106.39" y2="8.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.42" x2="-108.79" y2="8.52" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="7.25" x2="8.42" y2="7.35" layer="21"/>
<rectangle x1="9.02" y1="7.25" x2="10.22" y2="7.35" layer="21"/>
<rectangle x1="10.82" y1="7.25" x2="11.42" y2="7.35" layer="21"/>
<rectangle x1="13.22" y1="7.25" x2="16.22" y2="7.35" layer="21"/>
<rectangle x1="17.42" y1="7.25" x2="18.62" y2="7.35" layer="21"/>
<rectangle x1="-94.39" y1="8.52" x2="-91.39" y2="8.62" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="8.52" x2="-98.59" y2="8.62" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="8.52" x2="-100.39" y2="8.62" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="8.52" x2="-101.59" y2="8.62" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="8.52" x2="-106.39" y2="8.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.52" x2="-108.79" y2="8.62" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="7.35" x2="8.42" y2="7.45" layer="21"/>
<rectangle x1="9.02" y1="7.35" x2="10.22" y2="7.45" layer="21"/>
<rectangle x1="10.82" y1="7.35" x2="11.42" y2="7.45" layer="21"/>
<rectangle x1="13.22" y1="7.35" x2="16.22" y2="7.45" layer="21"/>
<rectangle x1="17.42" y1="7.35" x2="18.62" y2="7.45" layer="21"/>
<rectangle x1="-94.39" y1="8.62" x2="-91.39" y2="8.72" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="8.62" x2="-98.59" y2="8.72" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="8.62" x2="-100.39" y2="8.72" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="8.62" x2="-101.59" y2="8.72" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="8.62" x2="-106.39" y2="8.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.62" x2="-108.79" y2="8.72" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="7.45" x2="8.42" y2="7.55" layer="21"/>
<rectangle x1="9.02" y1="7.45" x2="10.22" y2="7.55" layer="21"/>
<rectangle x1="10.82" y1="7.45" x2="11.42" y2="7.55" layer="21"/>
<rectangle x1="13.22" y1="7.45" x2="16.22" y2="7.55" layer="21"/>
<rectangle x1="17.42" y1="7.45" x2="18.62" y2="7.55" layer="21"/>
<rectangle x1="-94.39" y1="8.72" x2="-91.39" y2="8.82" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="8.72" x2="-98.59" y2="8.82" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="8.72" x2="-100.39" y2="8.82" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="8.72" x2="-101.59" y2="8.82" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="8.72" x2="-106.39" y2="8.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.72" x2="-108.79" y2="8.82" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="7.55" x2="8.42" y2="7.65" layer="21"/>
<rectangle x1="9.02" y1="7.55" x2="10.22" y2="7.65" layer="21"/>
<rectangle x1="10.82" y1="7.55" x2="11.42" y2="7.65" layer="21"/>
<rectangle x1="13.22" y1="7.55" x2="16.22" y2="7.65" layer="21"/>
<rectangle x1="17.42" y1="7.55" x2="18.62" y2="7.65" layer="21"/>
<rectangle x1="-94.39" y1="8.82" x2="-91.39" y2="8.92" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="8.82" x2="-98.59" y2="8.92" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="8.82" x2="-100.39" y2="8.92" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="8.82" x2="-101.59" y2="8.92" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="8.82" x2="-106.39" y2="8.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.82" x2="-108.79" y2="8.92" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="7.65" x2="8.42" y2="7.75" layer="21"/>
<rectangle x1="9.02" y1="7.65" x2="10.22" y2="7.75" layer="21"/>
<rectangle x1="10.82" y1="7.65" x2="11.42" y2="7.75" layer="21"/>
<rectangle x1="13.22" y1="7.65" x2="16.22" y2="7.75" layer="21"/>
<rectangle x1="17.42" y1="7.65" x2="18.62" y2="7.75" layer="21"/>
<rectangle x1="-94.39" y1="8.92" x2="-91.39" y2="9.02" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="8.92" x2="-98.59" y2="9.02" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="8.92" x2="-100.39" y2="9.02" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="8.92" x2="-101.59" y2="9.02" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="8.92" x2="-106.39" y2="9.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="8.92" x2="-108.79" y2="9.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="7.75" x2="4.22" y2="7.85" layer="21"/>
<rectangle x1="4.82" y1="7.75" x2="6.02" y2="7.85" layer="21"/>
<rectangle x1="9.02" y1="7.75" x2="9.62" y2="7.85" layer="21"/>
<rectangle x1="10.22" y1="7.75" x2="10.82" y2="7.85" layer="21"/>
<rectangle x1="12.62" y1="7.75" x2="13.22" y2="7.85" layer="21"/>
<rectangle x1="13.82" y1="7.75" x2="14.42" y2="7.85" layer="21"/>
<rectangle x1="15.02" y1="7.75" x2="15.62" y2="7.85" layer="21"/>
<rectangle x1="17.42" y1="7.75" x2="18.02" y2="7.85" layer="21"/>
<rectangle x1="-93.79" y1="9.02" x2="-91.39" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="9.02" x2="-94.39" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="9.02" x2="-96.19" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="9.02" x2="-99.79" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="9.02" x2="-100.99" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="9.02" x2="-103.39" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="9.02" x2="-104.59" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.02" x2="-105.79" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.02" x2="-108.19" y2="9.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="7.85" x2="4.22" y2="7.95" layer="21"/>
<rectangle x1="4.82" y1="7.85" x2="6.02" y2="7.95" layer="21"/>
<rectangle x1="9.02" y1="7.85" x2="9.62" y2="7.95" layer="21"/>
<rectangle x1="10.22" y1="7.85" x2="10.82" y2="7.95" layer="21"/>
<rectangle x1="12.62" y1="7.85" x2="13.22" y2="7.95" layer="21"/>
<rectangle x1="13.82" y1="7.85" x2="14.42" y2="7.95" layer="21"/>
<rectangle x1="15.02" y1="7.85" x2="15.62" y2="7.95" layer="21"/>
<rectangle x1="17.42" y1="7.85" x2="18.02" y2="7.95" layer="21"/>
<rectangle x1="-93.79" y1="9.12" x2="-91.39" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="9.12" x2="-94.39" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="9.12" x2="-96.19" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="9.12" x2="-99.79" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="9.12" x2="-100.99" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="9.12" x2="-103.39" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="9.12" x2="-104.59" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.12" x2="-105.79" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.12" x2="-108.19" y2="9.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="7.95" x2="4.22" y2="8.05" layer="21"/>
<rectangle x1="4.82" y1="7.95" x2="6.02" y2="8.05" layer="21"/>
<rectangle x1="9.02" y1="7.95" x2="9.62" y2="8.05" layer="21"/>
<rectangle x1="10.22" y1="7.95" x2="10.82" y2="8.05" layer="21"/>
<rectangle x1="12.62" y1="7.95" x2="13.22" y2="8.05" layer="21"/>
<rectangle x1="13.82" y1="7.95" x2="14.42" y2="8.05" layer="21"/>
<rectangle x1="15.02" y1="7.95" x2="15.62" y2="8.05" layer="21"/>
<rectangle x1="17.42" y1="7.95" x2="18.02" y2="8.05" layer="21"/>
<rectangle x1="-93.79" y1="9.22" x2="-91.39" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="9.22" x2="-94.39" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="9.22" x2="-96.19" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="9.22" x2="-99.79" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="9.22" x2="-100.99" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="9.22" x2="-103.39" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="9.22" x2="-104.59" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.22" x2="-105.79" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.22" x2="-108.19" y2="9.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="8.05" x2="4.22" y2="8.15" layer="21"/>
<rectangle x1="4.82" y1="8.05" x2="6.02" y2="8.15" layer="21"/>
<rectangle x1="9.02" y1="8.05" x2="9.62" y2="8.15" layer="21"/>
<rectangle x1="10.22" y1="8.05" x2="10.82" y2="8.15" layer="21"/>
<rectangle x1="12.62" y1="8.05" x2="13.22" y2="8.15" layer="21"/>
<rectangle x1="13.82" y1="8.05" x2="14.42" y2="8.15" layer="21"/>
<rectangle x1="15.02" y1="8.05" x2="15.62" y2="8.15" layer="21"/>
<rectangle x1="17.42" y1="8.05" x2="18.02" y2="8.15" layer="21"/>
<rectangle x1="-93.79" y1="9.32" x2="-91.39" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="9.32" x2="-94.39" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="9.32" x2="-96.19" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="9.32" x2="-99.79" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="9.32" x2="-100.99" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="9.32" x2="-103.39" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="9.32" x2="-104.59" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.32" x2="-105.79" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.32" x2="-108.19" y2="9.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="8.15" x2="4.22" y2="8.25" layer="21"/>
<rectangle x1="4.82" y1="8.15" x2="6.02" y2="8.25" layer="21"/>
<rectangle x1="9.02" y1="8.15" x2="9.62" y2="8.25" layer="21"/>
<rectangle x1="10.22" y1="8.15" x2="10.82" y2="8.25" layer="21"/>
<rectangle x1="12.62" y1="8.15" x2="13.22" y2="8.25" layer="21"/>
<rectangle x1="13.82" y1="8.15" x2="14.42" y2="8.25" layer="21"/>
<rectangle x1="15.02" y1="8.15" x2="15.62" y2="8.25" layer="21"/>
<rectangle x1="17.42" y1="8.15" x2="18.02" y2="8.25" layer="21"/>
<rectangle x1="-93.79" y1="9.42" x2="-91.39" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="9.42" x2="-94.39" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="9.42" x2="-96.19" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="9.42" x2="-99.79" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="9.42" x2="-100.99" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="9.42" x2="-103.39" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="9.42" x2="-104.59" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.42" x2="-105.79" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.42" x2="-108.19" y2="9.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="8.25" x2="4.22" y2="8.35" layer="21"/>
<rectangle x1="4.82" y1="8.25" x2="6.02" y2="8.35" layer="21"/>
<rectangle x1="9.02" y1="8.25" x2="9.62" y2="8.35" layer="21"/>
<rectangle x1="10.22" y1="8.25" x2="10.82" y2="8.35" layer="21"/>
<rectangle x1="12.62" y1="8.25" x2="13.22" y2="8.35" layer="21"/>
<rectangle x1="13.82" y1="8.25" x2="14.42" y2="8.35" layer="21"/>
<rectangle x1="15.02" y1="8.25" x2="15.62" y2="8.35" layer="21"/>
<rectangle x1="17.42" y1="8.25" x2="18.02" y2="8.35" layer="21"/>
<rectangle x1="-93.79" y1="9.52" x2="-91.39" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="9.52" x2="-94.39" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="9.52" x2="-96.19" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="9.52" x2="-99.79" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="9.52" x2="-100.99" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="9.52" x2="-103.39" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="9.52" x2="-104.59" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.52" x2="-105.79" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.52" x2="-108.19" y2="9.62" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="8.35" x2="6.02" y2="8.45" layer="21"/>
<rectangle x1="7.22" y1="8.35" x2="9.02" y2="8.45" layer="21"/>
<rectangle x1="11.42" y1="8.35" x2="12.02" y2="8.45" layer="21"/>
<rectangle x1="14.42" y1="8.35" x2="15.02" y2="8.45" layer="21"/>
<rectangle x1="16.22" y1="8.35" x2="16.82" y2="8.45" layer="21"/>
<rectangle x1="17.42" y1="8.35" x2="18.62" y2="8.45" layer="21"/>
<rectangle x1="-94.39" y1="9.62" x2="-91.39" y2="9.72" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="9.62" x2="-96.19" y2="9.72" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="9.62" x2="-99.19" y2="9.72" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="9.62" x2="-102.19" y2="9.72" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="9.62" x2="-105.19" y2="9.72" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.62" x2="-106.99" y2="9.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.62" x2="-108.79" y2="9.72" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="8.45" x2="6.02" y2="8.55" layer="21"/>
<rectangle x1="7.22" y1="8.45" x2="9.02" y2="8.55" layer="21"/>
<rectangle x1="11.42" y1="8.45" x2="12.02" y2="8.55" layer="21"/>
<rectangle x1="14.42" y1="8.45" x2="15.02" y2="8.55" layer="21"/>
<rectangle x1="16.22" y1="8.45" x2="16.82" y2="8.55" layer="21"/>
<rectangle x1="17.42" y1="8.45" x2="18.62" y2="8.55" layer="21"/>
<rectangle x1="-94.39" y1="9.72" x2="-91.39" y2="9.82" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="9.72" x2="-96.19" y2="9.82" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="9.72" x2="-99.19" y2="9.82" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="9.72" x2="-102.19" y2="9.82" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="9.72" x2="-105.19" y2="9.82" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.72" x2="-106.99" y2="9.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.72" x2="-108.79" y2="9.82" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="8.55" x2="6.02" y2="8.65" layer="21"/>
<rectangle x1="7.22" y1="8.55" x2="9.02" y2="8.65" layer="21"/>
<rectangle x1="11.42" y1="8.55" x2="12.02" y2="8.65" layer="21"/>
<rectangle x1="14.42" y1="8.55" x2="15.02" y2="8.65" layer="21"/>
<rectangle x1="16.22" y1="8.55" x2="16.82" y2="8.65" layer="21"/>
<rectangle x1="17.42" y1="8.55" x2="18.62" y2="8.65" layer="21"/>
<rectangle x1="-94.39" y1="9.82" x2="-91.39" y2="9.92" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="9.82" x2="-96.19" y2="9.92" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="9.82" x2="-99.19" y2="9.92" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="9.82" x2="-102.19" y2="9.92" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="9.82" x2="-105.19" y2="9.92" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.82" x2="-106.99" y2="9.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.82" x2="-108.79" y2="9.92" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="8.65" x2="6.02" y2="8.75" layer="21"/>
<rectangle x1="7.22" y1="8.65" x2="9.02" y2="8.75" layer="21"/>
<rectangle x1="11.42" y1="8.65" x2="12.02" y2="8.75" layer="21"/>
<rectangle x1="14.42" y1="8.65" x2="15.02" y2="8.75" layer="21"/>
<rectangle x1="16.22" y1="8.65" x2="16.82" y2="8.75" layer="21"/>
<rectangle x1="17.42" y1="8.65" x2="18.62" y2="8.75" layer="21"/>
<rectangle x1="-94.39" y1="9.92" x2="-91.39" y2="10.02" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="9.92" x2="-96.19" y2="10.02" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="9.92" x2="-99.19" y2="10.02" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="9.92" x2="-102.19" y2="10.02" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="9.92" x2="-105.19" y2="10.02" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="9.92" x2="-106.99" y2="10.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="9.92" x2="-108.79" y2="10.02" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="8.75" x2="6.02" y2="8.85" layer="21"/>
<rectangle x1="7.22" y1="8.75" x2="9.02" y2="8.85" layer="21"/>
<rectangle x1="11.42" y1="8.75" x2="12.02" y2="8.85" layer="21"/>
<rectangle x1="14.42" y1="8.75" x2="15.02" y2="8.85" layer="21"/>
<rectangle x1="16.22" y1="8.75" x2="16.82" y2="8.85" layer="21"/>
<rectangle x1="17.42" y1="8.75" x2="18.62" y2="8.85" layer="21"/>
<rectangle x1="-94.39" y1="10.02" x2="-91.39" y2="10.12" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="10.02" x2="-96.19" y2="10.12" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="10.02" x2="-99.19" y2="10.12" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="10.02" x2="-102.19" y2="10.12" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="10.02" x2="-105.19" y2="10.12" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="10.02" x2="-106.99" y2="10.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.02" x2="-108.79" y2="10.12" layer="21" rot="R180"/>
<rectangle x1="4.22" y1="8.85" x2="6.02" y2="8.95" layer="21"/>
<rectangle x1="7.22" y1="8.85" x2="9.02" y2="8.95" layer="21"/>
<rectangle x1="11.42" y1="8.85" x2="12.02" y2="8.95" layer="21"/>
<rectangle x1="14.42" y1="8.85" x2="15.02" y2="8.95" layer="21"/>
<rectangle x1="16.22" y1="8.85" x2="16.82" y2="8.95" layer="21"/>
<rectangle x1="17.42" y1="8.85" x2="18.62" y2="8.95" layer="21"/>
<rectangle x1="-94.39" y1="10.12" x2="-91.39" y2="10.22" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="10.12" x2="-96.19" y2="10.22" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="10.12" x2="-99.19" y2="10.22" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="10.12" x2="-102.19" y2="10.22" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="10.12" x2="-105.19" y2="10.22" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="10.12" x2="-106.99" y2="10.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.12" x2="-108.79" y2="10.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="8.95" x2="5.42" y2="9.05" layer="21"/>
<rectangle x1="6.02" y1="8.95" x2="7.22" y2="9.05" layer="21"/>
<rectangle x1="7.82" y1="8.95" x2="8.42" y2="9.05" layer="21"/>
<rectangle x1="9.02" y1="8.95" x2="9.62" y2="9.05" layer="21"/>
<rectangle x1="10.22" y1="8.95" x2="10.82" y2="9.05" layer="21"/>
<rectangle x1="13.22" y1="8.95" x2="14.42" y2="9.05" layer="21"/>
<rectangle x1="18.02" y1="8.95" x2="18.62" y2="9.05" layer="21"/>
<rectangle x1="-93.79" y1="10.22" x2="-91.39" y2="10.32" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="10.22" x2="-95.59" y2="10.32" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="10.22" x2="-97.39" y2="10.32" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="10.22" x2="-98.59" y2="10.32" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="10.22" x2="-99.79" y2="10.32" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="10.22" x2="-100.99" y2="10.32" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="10.22" x2="-104.59" y2="10.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.22" x2="-108.79" y2="10.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="9.05" x2="5.42" y2="9.15" layer="21"/>
<rectangle x1="6.02" y1="9.05" x2="7.22" y2="9.15" layer="21"/>
<rectangle x1="7.82" y1="9.05" x2="8.42" y2="9.15" layer="21"/>
<rectangle x1="9.02" y1="9.05" x2="9.62" y2="9.15" layer="21"/>
<rectangle x1="10.22" y1="9.05" x2="10.82" y2="9.15" layer="21"/>
<rectangle x1="13.22" y1="9.05" x2="14.42" y2="9.15" layer="21"/>
<rectangle x1="18.02" y1="9.05" x2="18.62" y2="9.15" layer="21"/>
<rectangle x1="-93.79" y1="10.32" x2="-91.39" y2="10.42" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="10.32" x2="-95.59" y2="10.42" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="10.32" x2="-97.39" y2="10.42" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="10.32" x2="-98.59" y2="10.42" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="10.32" x2="-99.79" y2="10.42" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="10.32" x2="-100.99" y2="10.42" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="10.32" x2="-104.59" y2="10.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.32" x2="-108.79" y2="10.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="9.15" x2="5.42" y2="9.25" layer="21"/>
<rectangle x1="6.02" y1="9.15" x2="7.22" y2="9.25" layer="21"/>
<rectangle x1="7.82" y1="9.15" x2="8.42" y2="9.25" layer="21"/>
<rectangle x1="9.02" y1="9.15" x2="9.62" y2="9.25" layer="21"/>
<rectangle x1="10.22" y1="9.15" x2="10.82" y2="9.25" layer="21"/>
<rectangle x1="13.22" y1="9.15" x2="14.42" y2="9.25" layer="21"/>
<rectangle x1="18.02" y1="9.15" x2="18.62" y2="9.25" layer="21"/>
<rectangle x1="-93.79" y1="10.42" x2="-91.39" y2="10.52" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="10.42" x2="-95.59" y2="10.52" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="10.42" x2="-97.39" y2="10.52" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="10.42" x2="-98.59" y2="10.52" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="10.42" x2="-99.79" y2="10.52" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="10.42" x2="-100.99" y2="10.52" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="10.42" x2="-104.59" y2="10.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.42" x2="-108.79" y2="10.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="9.25" x2="5.42" y2="9.35" layer="21"/>
<rectangle x1="6.02" y1="9.25" x2="7.22" y2="9.35" layer="21"/>
<rectangle x1="7.82" y1="9.25" x2="8.42" y2="9.35" layer="21"/>
<rectangle x1="9.02" y1="9.25" x2="9.62" y2="9.35" layer="21"/>
<rectangle x1="10.22" y1="9.25" x2="10.82" y2="9.35" layer="21"/>
<rectangle x1="13.22" y1="9.25" x2="14.42" y2="9.35" layer="21"/>
<rectangle x1="18.02" y1="9.25" x2="18.62" y2="9.35" layer="21"/>
<rectangle x1="-93.79" y1="10.52" x2="-91.39" y2="10.62" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="10.52" x2="-95.59" y2="10.62" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="10.52" x2="-97.39" y2="10.62" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="10.52" x2="-98.59" y2="10.62" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="10.52" x2="-99.79" y2="10.62" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="10.52" x2="-100.99" y2="10.62" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="10.52" x2="-104.59" y2="10.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.52" x2="-108.79" y2="10.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="9.35" x2="5.42" y2="9.45" layer="21"/>
<rectangle x1="6.02" y1="9.35" x2="7.22" y2="9.45" layer="21"/>
<rectangle x1="7.82" y1="9.35" x2="8.42" y2="9.45" layer="21"/>
<rectangle x1="9.02" y1="9.35" x2="9.62" y2="9.45" layer="21"/>
<rectangle x1="10.22" y1="9.35" x2="10.82" y2="9.45" layer="21"/>
<rectangle x1="13.22" y1="9.35" x2="14.42" y2="9.45" layer="21"/>
<rectangle x1="18.02" y1="9.35" x2="18.62" y2="9.45" layer="21"/>
<rectangle x1="-93.79" y1="10.62" x2="-91.39" y2="10.72" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="10.62" x2="-95.59" y2="10.72" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="10.62" x2="-97.39" y2="10.72" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="10.62" x2="-98.59" y2="10.72" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="10.62" x2="-99.79" y2="10.72" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="10.62" x2="-100.99" y2="10.72" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="10.62" x2="-104.59" y2="10.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.62" x2="-108.79" y2="10.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="9.45" x2="5.42" y2="9.55" layer="21"/>
<rectangle x1="6.02" y1="9.45" x2="7.22" y2="9.55" layer="21"/>
<rectangle x1="7.82" y1="9.45" x2="8.42" y2="9.55" layer="21"/>
<rectangle x1="9.02" y1="9.45" x2="9.62" y2="9.55" layer="21"/>
<rectangle x1="10.22" y1="9.45" x2="10.82" y2="9.55" layer="21"/>
<rectangle x1="13.22" y1="9.45" x2="14.42" y2="9.55" layer="21"/>
<rectangle x1="18.02" y1="9.45" x2="18.62" y2="9.55" layer="21"/>
<rectangle x1="-93.79" y1="10.72" x2="-91.39" y2="10.82" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="10.72" x2="-95.59" y2="10.82" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="10.72" x2="-97.39" y2="10.82" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="10.72" x2="-98.59" y2="10.82" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="10.72" x2="-99.79" y2="10.82" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="10.72" x2="-100.99" y2="10.82" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="10.72" x2="-104.59" y2="10.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.72" x2="-108.79" y2="10.82" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="9.55" x2="6.02" y2="9.65" layer="21"/>
<rectangle x1="6.62" y1="9.55" x2="8.42" y2="9.65" layer="21"/>
<rectangle x1="9.02" y1="9.55" x2="9.62" y2="9.65" layer="21"/>
<rectangle x1="10.22" y1="9.55" x2="10.82" y2="9.65" layer="21"/>
<rectangle x1="12.02" y1="9.55" x2="15.02" y2="9.65" layer="21"/>
<rectangle x1="18.02" y1="9.55" x2="18.62" y2="9.65" layer="21"/>
<rectangle x1="-94.99" y1="10.82" x2="-91.39" y2="10.92" layer="21" rot="R180"/>
<rectangle x1="-96.79" y1="10.82" x2="-96.19" y2="10.92" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="10.82" x2="-98.59" y2="10.92" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="10.82" x2="-99.79" y2="10.92" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="10.82" x2="-100.99" y2="10.92" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="10.82" x2="-105.19" y2="10.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.82" x2="-108.79" y2="10.92" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="9.65" x2="6.02" y2="9.75" layer="21"/>
<rectangle x1="6.62" y1="9.65" x2="8.42" y2="9.75" layer="21"/>
<rectangle x1="9.02" y1="9.65" x2="9.62" y2="9.75" layer="21"/>
<rectangle x1="10.22" y1="9.65" x2="10.82" y2="9.75" layer="21"/>
<rectangle x1="12.02" y1="9.65" x2="15.02" y2="9.75" layer="21"/>
<rectangle x1="18.02" y1="9.65" x2="18.62" y2="9.75" layer="21"/>
<rectangle x1="-94.99" y1="10.92" x2="-91.39" y2="11.02" layer="21" rot="R180"/>
<rectangle x1="-96.79" y1="10.92" x2="-96.19" y2="11.02" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="10.92" x2="-98.59" y2="11.02" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="10.92" x2="-99.79" y2="11.02" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="10.92" x2="-100.99" y2="11.02" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="10.92" x2="-105.19" y2="11.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="10.92" x2="-108.79" y2="11.02" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="9.75" x2="6.02" y2="9.85" layer="21"/>
<rectangle x1="6.62" y1="9.75" x2="8.42" y2="9.85" layer="21"/>
<rectangle x1="9.02" y1="9.75" x2="9.62" y2="9.85" layer="21"/>
<rectangle x1="10.22" y1="9.75" x2="10.82" y2="9.85" layer="21"/>
<rectangle x1="12.02" y1="9.75" x2="15.02" y2="9.85" layer="21"/>
<rectangle x1="18.02" y1="9.75" x2="18.62" y2="9.85" layer="21"/>
<rectangle x1="-94.99" y1="11.02" x2="-91.39" y2="11.12" layer="21" rot="R180"/>
<rectangle x1="-96.79" y1="11.02" x2="-96.19" y2="11.12" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="11.02" x2="-98.59" y2="11.12" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="11.02" x2="-99.79" y2="11.12" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="11.02" x2="-100.99" y2="11.12" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="11.02" x2="-105.19" y2="11.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.02" x2="-108.79" y2="11.12" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="9.85" x2="6.02" y2="9.95" layer="21"/>
<rectangle x1="6.62" y1="9.85" x2="8.42" y2="9.95" layer="21"/>
<rectangle x1="9.02" y1="9.85" x2="9.62" y2="9.95" layer="21"/>
<rectangle x1="10.22" y1="9.85" x2="10.82" y2="9.95" layer="21"/>
<rectangle x1="12.02" y1="9.85" x2="15.02" y2="9.95" layer="21"/>
<rectangle x1="18.02" y1="9.85" x2="18.62" y2="9.95" layer="21"/>
<rectangle x1="-94.99" y1="11.12" x2="-91.39" y2="11.22" layer="21" rot="R180"/>
<rectangle x1="-96.79" y1="11.12" x2="-96.19" y2="11.22" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="11.12" x2="-98.59" y2="11.22" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="11.12" x2="-99.79" y2="11.22" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="11.12" x2="-100.99" y2="11.22" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="11.12" x2="-105.19" y2="11.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.12" x2="-108.79" y2="11.22" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="9.95" x2="6.02" y2="10.05" layer="21"/>
<rectangle x1="6.62" y1="9.95" x2="8.42" y2="10.05" layer="21"/>
<rectangle x1="9.02" y1="9.95" x2="9.62" y2="10.05" layer="21"/>
<rectangle x1="10.22" y1="9.95" x2="10.82" y2="10.05" layer="21"/>
<rectangle x1="12.02" y1="9.95" x2="15.02" y2="10.05" layer="21"/>
<rectangle x1="18.02" y1="9.95" x2="18.62" y2="10.05" layer="21"/>
<rectangle x1="-94.99" y1="11.22" x2="-91.39" y2="11.32" layer="21" rot="R180"/>
<rectangle x1="-96.79" y1="11.22" x2="-96.19" y2="11.32" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="11.22" x2="-98.59" y2="11.32" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="11.22" x2="-99.79" y2="11.32" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="11.22" x2="-100.99" y2="11.32" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="11.22" x2="-105.19" y2="11.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.22" x2="-108.79" y2="11.32" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="10.05" x2="6.02" y2="10.15" layer="21"/>
<rectangle x1="6.62" y1="10.05" x2="8.42" y2="10.15" layer="21"/>
<rectangle x1="9.02" y1="10.05" x2="9.62" y2="10.15" layer="21"/>
<rectangle x1="10.22" y1="10.05" x2="10.82" y2="10.15" layer="21"/>
<rectangle x1="12.02" y1="10.05" x2="15.02" y2="10.15" layer="21"/>
<rectangle x1="18.02" y1="10.05" x2="18.62" y2="10.15" layer="21"/>
<rectangle x1="-94.99" y1="11.32" x2="-91.39" y2="11.42" layer="21" rot="R180"/>
<rectangle x1="-96.79" y1="11.32" x2="-96.19" y2="11.42" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="11.32" x2="-98.59" y2="11.42" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="11.32" x2="-99.79" y2="11.42" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="11.32" x2="-100.99" y2="11.42" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="11.32" x2="-105.19" y2="11.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.32" x2="-108.79" y2="11.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="10.15" x2="5.42" y2="10.25" layer="21"/>
<rectangle x1="6.02" y1="10.15" x2="7.22" y2="10.25" layer="21"/>
<rectangle x1="9.62" y1="10.15" x2="10.22" y2="10.25" layer="21"/>
<rectangle x1="11.42" y1="10.15" x2="13.22" y2="10.25" layer="21"/>
<rectangle x1="15.02" y1="10.15" x2="15.62" y2="10.25" layer="21"/>
<rectangle x1="17.42" y1="10.15" x2="18.62" y2="10.25" layer="21"/>
<rectangle x1="-93.79" y1="11.42" x2="-91.39" y2="11.52" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="11.42" x2="-95.59" y2="11.52" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="11.42" x2="-97.39" y2="11.52" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="11.42" x2="-100.39" y2="11.52" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="11.42" x2="-103.39" y2="11.52" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="11.42" x2="-105.79" y2="11.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.42" x2="-108.79" y2="11.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="10.25" x2="5.42" y2="10.35" layer="21"/>
<rectangle x1="6.02" y1="10.25" x2="7.22" y2="10.35" layer="21"/>
<rectangle x1="9.62" y1="10.25" x2="10.22" y2="10.35" layer="21"/>
<rectangle x1="11.42" y1="10.25" x2="13.22" y2="10.35" layer="21"/>
<rectangle x1="15.02" y1="10.25" x2="15.62" y2="10.35" layer="21"/>
<rectangle x1="17.42" y1="10.25" x2="18.62" y2="10.35" layer="21"/>
<rectangle x1="-93.79" y1="11.52" x2="-91.39" y2="11.62" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="11.52" x2="-95.59" y2="11.62" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="11.52" x2="-97.39" y2="11.62" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="11.52" x2="-100.39" y2="11.62" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="11.52" x2="-103.39" y2="11.62" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="11.52" x2="-105.79" y2="11.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.52" x2="-108.79" y2="11.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="10.35" x2="5.42" y2="10.45" layer="21"/>
<rectangle x1="6.02" y1="10.35" x2="7.22" y2="10.45" layer="21"/>
<rectangle x1="9.62" y1="10.35" x2="10.22" y2="10.45" layer="21"/>
<rectangle x1="11.42" y1="10.35" x2="13.22" y2="10.45" layer="21"/>
<rectangle x1="15.02" y1="10.35" x2="15.62" y2="10.45" layer="21"/>
<rectangle x1="17.42" y1="10.35" x2="18.62" y2="10.45" layer="21"/>
<rectangle x1="-93.79" y1="11.62" x2="-91.39" y2="11.72" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="11.62" x2="-95.59" y2="11.72" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="11.62" x2="-97.39" y2="11.72" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="11.62" x2="-100.39" y2="11.72" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="11.62" x2="-103.39" y2="11.72" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="11.62" x2="-105.79" y2="11.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.62" x2="-108.79" y2="11.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="10.45" x2="5.42" y2="10.55" layer="21"/>
<rectangle x1="6.02" y1="10.45" x2="7.22" y2="10.55" layer="21"/>
<rectangle x1="9.62" y1="10.45" x2="10.22" y2="10.55" layer="21"/>
<rectangle x1="11.42" y1="10.45" x2="13.22" y2="10.55" layer="21"/>
<rectangle x1="15.02" y1="10.45" x2="15.62" y2="10.55" layer="21"/>
<rectangle x1="17.42" y1="10.45" x2="18.62" y2="10.55" layer="21"/>
<rectangle x1="-93.79" y1="11.72" x2="-91.39" y2="11.82" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="11.72" x2="-95.59" y2="11.82" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="11.72" x2="-97.39" y2="11.82" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="11.72" x2="-100.39" y2="11.82" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="11.72" x2="-103.39" y2="11.82" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="11.72" x2="-105.79" y2="11.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.72" x2="-108.79" y2="11.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="10.55" x2="5.42" y2="10.65" layer="21"/>
<rectangle x1="6.02" y1="10.55" x2="7.22" y2="10.65" layer="21"/>
<rectangle x1="9.62" y1="10.55" x2="10.22" y2="10.65" layer="21"/>
<rectangle x1="11.42" y1="10.55" x2="13.22" y2="10.65" layer="21"/>
<rectangle x1="15.02" y1="10.55" x2="15.62" y2="10.65" layer="21"/>
<rectangle x1="17.42" y1="10.55" x2="18.62" y2="10.65" layer="21"/>
<rectangle x1="-93.79" y1="11.82" x2="-91.39" y2="11.92" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="11.82" x2="-95.59" y2="11.92" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="11.82" x2="-97.39" y2="11.92" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="11.82" x2="-100.39" y2="11.92" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="11.82" x2="-103.39" y2="11.92" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="11.82" x2="-105.79" y2="11.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.82" x2="-108.79" y2="11.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="10.65" x2="5.42" y2="10.75" layer="21"/>
<rectangle x1="6.02" y1="10.65" x2="7.22" y2="10.75" layer="21"/>
<rectangle x1="9.62" y1="10.65" x2="10.22" y2="10.75" layer="21"/>
<rectangle x1="11.42" y1="10.65" x2="13.22" y2="10.75" layer="21"/>
<rectangle x1="15.02" y1="10.65" x2="15.62" y2="10.75" layer="21"/>
<rectangle x1="17.42" y1="10.65" x2="18.62" y2="10.75" layer="21"/>
<rectangle x1="-93.79" y1="11.92" x2="-91.39" y2="12.02" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="11.92" x2="-95.59" y2="12.02" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="11.92" x2="-97.39" y2="12.02" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="11.92" x2="-100.39" y2="12.02" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="11.92" x2="-103.39" y2="12.02" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="11.92" x2="-105.79" y2="12.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="11.92" x2="-108.79" y2="12.02" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="10.75" x2="5.42" y2="10.85" layer="21"/>
<rectangle x1="6.02" y1="10.75" x2="7.82" y2="10.85" layer="21"/>
<rectangle x1="8.42" y1="10.75" x2="11.42" y2="10.85" layer="21"/>
<rectangle x1="13.82" y1="10.75" x2="14.42" y2="10.85" layer="21"/>
<rectangle x1="15.02" y1="10.75" x2="18.62" y2="10.85" layer="21"/>
<rectangle x1="-94.99" y1="12.02" x2="-91.39" y2="12.12" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="12.02" x2="-95.59" y2="12.12" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="12.02" x2="-97.99" y2="12.12" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="12.02" x2="-101.59" y2="12.12" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="12.02" x2="-104.59" y2="12.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.02" x2="-108.79" y2="12.12" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="10.85" x2="5.42" y2="10.95" layer="21"/>
<rectangle x1="6.02" y1="10.85" x2="7.82" y2="10.95" layer="21"/>
<rectangle x1="8.42" y1="10.85" x2="11.42" y2="10.95" layer="21"/>
<rectangle x1="13.82" y1="10.85" x2="14.42" y2="10.95" layer="21"/>
<rectangle x1="15.02" y1="10.85" x2="18.62" y2="10.95" layer="21"/>
<rectangle x1="-94.99" y1="12.12" x2="-91.39" y2="12.22" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="12.12" x2="-95.59" y2="12.22" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="12.12" x2="-97.99" y2="12.22" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="12.12" x2="-101.59" y2="12.22" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="12.12" x2="-104.59" y2="12.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.12" x2="-108.79" y2="12.22" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="10.95" x2="5.42" y2="11.05" layer="21"/>
<rectangle x1="6.02" y1="10.95" x2="7.82" y2="11.05" layer="21"/>
<rectangle x1="8.42" y1="10.95" x2="11.42" y2="11.05" layer="21"/>
<rectangle x1="13.82" y1="10.95" x2="14.42" y2="11.05" layer="21"/>
<rectangle x1="15.02" y1="10.95" x2="18.62" y2="11.05" layer="21"/>
<rectangle x1="-94.99" y1="12.22" x2="-91.39" y2="12.32" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="12.22" x2="-95.59" y2="12.32" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="12.22" x2="-97.99" y2="12.32" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="12.22" x2="-101.59" y2="12.32" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="12.22" x2="-104.59" y2="12.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.22" x2="-108.79" y2="12.32" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="11.05" x2="5.42" y2="11.15" layer="21"/>
<rectangle x1="6.02" y1="11.05" x2="7.82" y2="11.15" layer="21"/>
<rectangle x1="8.42" y1="11.05" x2="11.42" y2="11.15" layer="21"/>
<rectangle x1="13.82" y1="11.05" x2="14.42" y2="11.15" layer="21"/>
<rectangle x1="15.02" y1="11.05" x2="18.62" y2="11.15" layer="21"/>
<rectangle x1="-94.99" y1="12.32" x2="-91.39" y2="12.42" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="12.32" x2="-95.59" y2="12.42" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="12.32" x2="-97.99" y2="12.42" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="12.32" x2="-101.59" y2="12.42" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="12.32" x2="-104.59" y2="12.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.32" x2="-108.79" y2="12.42" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="11.15" x2="5.42" y2="11.25" layer="21"/>
<rectangle x1="6.02" y1="11.15" x2="7.82" y2="11.25" layer="21"/>
<rectangle x1="8.42" y1="11.15" x2="11.42" y2="11.25" layer="21"/>
<rectangle x1="13.82" y1="11.15" x2="14.42" y2="11.25" layer="21"/>
<rectangle x1="15.02" y1="11.15" x2="18.62" y2="11.25" layer="21"/>
<rectangle x1="-94.99" y1="12.42" x2="-91.39" y2="12.52" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="12.42" x2="-95.59" y2="12.52" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="12.42" x2="-97.99" y2="12.52" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="12.42" x2="-101.59" y2="12.52" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="12.42" x2="-104.59" y2="12.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.42" x2="-108.79" y2="12.52" layer="21" rot="R180"/>
<rectangle x1="4.82" y1="11.25" x2="5.42" y2="11.35" layer="21"/>
<rectangle x1="6.02" y1="11.25" x2="7.82" y2="11.35" layer="21"/>
<rectangle x1="8.42" y1="11.25" x2="11.42" y2="11.35" layer="21"/>
<rectangle x1="13.82" y1="11.25" x2="14.42" y2="11.35" layer="21"/>
<rectangle x1="15.02" y1="11.25" x2="18.62" y2="11.35" layer="21"/>
<rectangle x1="-94.99" y1="12.52" x2="-91.39" y2="12.62" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="12.52" x2="-95.59" y2="12.62" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="12.52" x2="-97.99" y2="12.62" layer="21" rot="R180"/>
<rectangle x1="-103.99" y1="12.52" x2="-101.59" y2="12.62" layer="21" rot="R180"/>
<rectangle x1="-105.19" y1="12.52" x2="-104.59" y2="12.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.52" x2="-108.79" y2="12.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="11.35" x2="4.22" y2="11.45" layer="21"/>
<rectangle x1="5.42" y1="11.35" x2="7.22" y2="11.45" layer="21"/>
<rectangle x1="7.82" y1="11.35" x2="11.42" y2="11.45" layer="21"/>
<rectangle x1="13.22" y1="11.35" x2="15.02" y2="11.45" layer="21"/>
<rectangle x1="16.22" y1="11.35" x2="18.62" y2="11.45" layer="21"/>
<rectangle x1="-93.79" y1="12.62" x2="-91.39" y2="12.72" layer="21" rot="R180"/>
<rectangle x1="-95.59" y1="12.62" x2="-94.39" y2="12.72" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="12.62" x2="-97.39" y2="12.72" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="12.62" x2="-101.59" y2="12.72" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="12.62" x2="-105.19" y2="12.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.62" x2="-108.79" y2="12.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="11.45" x2="4.22" y2="11.55" layer="21"/>
<rectangle x1="5.42" y1="11.45" x2="7.22" y2="11.55" layer="21"/>
<rectangle x1="7.82" y1="11.45" x2="11.42" y2="11.55" layer="21"/>
<rectangle x1="13.22" y1="11.45" x2="15.02" y2="11.55" layer="21"/>
<rectangle x1="16.22" y1="11.45" x2="18.62" y2="11.55" layer="21"/>
<rectangle x1="-93.79" y1="12.72" x2="-91.39" y2="12.82" layer="21" rot="R180"/>
<rectangle x1="-95.59" y1="12.72" x2="-94.39" y2="12.82" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="12.72" x2="-97.39" y2="12.82" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="12.72" x2="-101.59" y2="12.82" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="12.72" x2="-105.19" y2="12.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.72" x2="-108.79" y2="12.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="11.55" x2="4.22" y2="11.65" layer="21"/>
<rectangle x1="5.42" y1="11.55" x2="7.22" y2="11.65" layer="21"/>
<rectangle x1="7.82" y1="11.55" x2="11.42" y2="11.65" layer="21"/>
<rectangle x1="13.22" y1="11.55" x2="15.02" y2="11.65" layer="21"/>
<rectangle x1="16.22" y1="11.55" x2="18.62" y2="11.65" layer="21"/>
<rectangle x1="-93.79" y1="12.82" x2="-91.39" y2="12.92" layer="21" rot="R180"/>
<rectangle x1="-95.59" y1="12.82" x2="-94.39" y2="12.92" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="12.82" x2="-97.39" y2="12.92" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="12.82" x2="-101.59" y2="12.92" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="12.82" x2="-105.19" y2="12.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.82" x2="-108.79" y2="12.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="11.65" x2="4.22" y2="11.75" layer="21"/>
<rectangle x1="5.42" y1="11.65" x2="7.22" y2="11.75" layer="21"/>
<rectangle x1="7.82" y1="11.65" x2="11.42" y2="11.75" layer="21"/>
<rectangle x1="13.22" y1="11.65" x2="15.02" y2="11.75" layer="21"/>
<rectangle x1="16.22" y1="11.65" x2="18.62" y2="11.75" layer="21"/>
<rectangle x1="-93.79" y1="12.92" x2="-91.39" y2="13.02" layer="21" rot="R180"/>
<rectangle x1="-95.59" y1="12.92" x2="-94.39" y2="13.02" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="12.92" x2="-97.39" y2="13.02" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="12.92" x2="-101.59" y2="13.02" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="12.92" x2="-105.19" y2="13.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="12.92" x2="-108.79" y2="13.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="11.75" x2="4.22" y2="11.85" layer="21"/>
<rectangle x1="5.42" y1="11.75" x2="7.22" y2="11.85" layer="21"/>
<rectangle x1="7.82" y1="11.75" x2="11.42" y2="11.85" layer="21"/>
<rectangle x1="13.22" y1="11.75" x2="15.02" y2="11.85" layer="21"/>
<rectangle x1="16.22" y1="11.75" x2="18.62" y2="11.85" layer="21"/>
<rectangle x1="-93.79" y1="13.02" x2="-91.39" y2="13.12" layer="21" rot="R180"/>
<rectangle x1="-95.59" y1="13.02" x2="-94.39" y2="13.12" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="13.02" x2="-97.39" y2="13.12" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="13.02" x2="-101.59" y2="13.12" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="13.02" x2="-105.19" y2="13.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.02" x2="-108.79" y2="13.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="11.85" x2="4.22" y2="11.95" layer="21"/>
<rectangle x1="5.42" y1="11.85" x2="7.22" y2="11.95" layer="21"/>
<rectangle x1="7.82" y1="11.85" x2="11.42" y2="11.95" layer="21"/>
<rectangle x1="13.22" y1="11.85" x2="15.02" y2="11.95" layer="21"/>
<rectangle x1="16.22" y1="11.85" x2="18.62" y2="11.95" layer="21"/>
<rectangle x1="-93.79" y1="13.12" x2="-91.39" y2="13.22" layer="21" rot="R180"/>
<rectangle x1="-95.59" y1="13.12" x2="-94.39" y2="13.22" layer="21" rot="R180"/>
<rectangle x1="-97.99" y1="13.12" x2="-97.39" y2="13.22" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="13.12" x2="-101.59" y2="13.22" layer="21" rot="R180"/>
<rectangle x1="-106.39" y1="13.12" x2="-105.19" y2="13.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.12" x2="-108.79" y2="13.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="11.95" x2="4.22" y2="12.05" layer="21"/>
<rectangle x1="4.82" y1="11.95" x2="5.42" y2="12.05" layer="21"/>
<rectangle x1="6.02" y1="11.95" x2="6.62" y2="12.05" layer="21"/>
<rectangle x1="7.22" y1="11.95" x2="7.82" y2="12.05" layer="21"/>
<rectangle x1="9.62" y1="11.95" x2="10.82" y2="12.05" layer="21"/>
<rectangle x1="11.42" y1="11.95" x2="12.02" y2="12.05" layer="21"/>
<rectangle x1="12.62" y1="11.95" x2="13.82" y2="12.05" layer="21"/>
<rectangle x1="15.62" y1="11.95" x2="16.22" y2="12.05" layer="21"/>
<rectangle x1="17.42" y1="11.95" x2="18.02" y2="12.05" layer="21"/>
<rectangle x1="-93.79" y1="13.22" x2="-91.39" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="13.22" x2="-94.39" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="13.22" x2="-95.59" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="13.22" x2="-96.79" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="13.22" x2="-97.99" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="13.22" x2="-100.99" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="13.22" x2="-102.19" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="13.22" x2="-103.99" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="13.22" x2="-106.39" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.22" x2="-108.19" y2="13.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="12.05" x2="4.22" y2="12.15" layer="21"/>
<rectangle x1="4.82" y1="12.05" x2="5.42" y2="12.15" layer="21"/>
<rectangle x1="6.02" y1="12.05" x2="6.62" y2="12.15" layer="21"/>
<rectangle x1="7.22" y1="12.05" x2="7.82" y2="12.15" layer="21"/>
<rectangle x1="9.62" y1="12.05" x2="10.82" y2="12.15" layer="21"/>
<rectangle x1="11.42" y1="12.05" x2="12.02" y2="12.15" layer="21"/>
<rectangle x1="12.62" y1="12.05" x2="13.82" y2="12.15" layer="21"/>
<rectangle x1="15.62" y1="12.05" x2="16.22" y2="12.15" layer="21"/>
<rectangle x1="17.42" y1="12.05" x2="18.02" y2="12.15" layer="21"/>
<rectangle x1="-93.79" y1="13.32" x2="-91.39" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="13.32" x2="-94.39" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="13.32" x2="-95.59" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="13.32" x2="-96.79" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="13.32" x2="-97.99" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="13.32" x2="-100.99" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="13.32" x2="-102.19" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="13.32" x2="-103.99" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="13.32" x2="-106.39" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.32" x2="-108.19" y2="13.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="12.15" x2="4.22" y2="12.25" layer="21"/>
<rectangle x1="4.82" y1="12.15" x2="5.42" y2="12.25" layer="21"/>
<rectangle x1="6.02" y1="12.15" x2="6.62" y2="12.25" layer="21"/>
<rectangle x1="7.22" y1="12.15" x2="7.82" y2="12.25" layer="21"/>
<rectangle x1="9.62" y1="12.15" x2="10.82" y2="12.25" layer="21"/>
<rectangle x1="11.42" y1="12.15" x2="12.02" y2="12.25" layer="21"/>
<rectangle x1="12.62" y1="12.15" x2="13.82" y2="12.25" layer="21"/>
<rectangle x1="15.62" y1="12.15" x2="16.22" y2="12.25" layer="21"/>
<rectangle x1="17.42" y1="12.15" x2="18.02" y2="12.25" layer="21"/>
<rectangle x1="-93.79" y1="13.42" x2="-91.39" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="13.42" x2="-94.39" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="13.42" x2="-95.59" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="13.42" x2="-96.79" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="13.42" x2="-97.99" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="13.42" x2="-100.99" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="13.42" x2="-102.19" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="13.42" x2="-103.99" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="13.42" x2="-106.39" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.42" x2="-108.19" y2="13.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="12.25" x2="4.22" y2="12.35" layer="21"/>
<rectangle x1="4.82" y1="12.25" x2="5.42" y2="12.35" layer="21"/>
<rectangle x1="6.02" y1="12.25" x2="6.62" y2="12.35" layer="21"/>
<rectangle x1="7.22" y1="12.25" x2="7.82" y2="12.35" layer="21"/>
<rectangle x1="9.62" y1="12.25" x2="10.82" y2="12.35" layer="21"/>
<rectangle x1="11.42" y1="12.25" x2="12.02" y2="12.35" layer="21"/>
<rectangle x1="12.62" y1="12.25" x2="13.82" y2="12.35" layer="21"/>
<rectangle x1="15.62" y1="12.25" x2="16.22" y2="12.35" layer="21"/>
<rectangle x1="17.42" y1="12.25" x2="18.02" y2="12.35" layer="21"/>
<rectangle x1="-93.79" y1="13.52" x2="-91.39" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="13.52" x2="-94.39" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="13.52" x2="-95.59" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="13.52" x2="-96.79" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="13.52" x2="-97.99" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="13.52" x2="-100.99" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="13.52" x2="-102.19" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="13.52" x2="-103.99" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="13.52" x2="-106.39" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.52" x2="-108.19" y2="13.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="12.35" x2="4.22" y2="12.45" layer="21"/>
<rectangle x1="4.82" y1="12.35" x2="5.42" y2="12.45" layer="21"/>
<rectangle x1="6.02" y1="12.35" x2="6.62" y2="12.45" layer="21"/>
<rectangle x1="7.22" y1="12.35" x2="7.82" y2="12.45" layer="21"/>
<rectangle x1="9.62" y1="12.35" x2="10.82" y2="12.45" layer="21"/>
<rectangle x1="11.42" y1="12.35" x2="12.02" y2="12.45" layer="21"/>
<rectangle x1="12.62" y1="12.35" x2="13.82" y2="12.45" layer="21"/>
<rectangle x1="15.62" y1="12.35" x2="16.22" y2="12.45" layer="21"/>
<rectangle x1="17.42" y1="12.35" x2="18.02" y2="12.45" layer="21"/>
<rectangle x1="-93.79" y1="13.62" x2="-91.39" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="13.62" x2="-94.39" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="13.62" x2="-95.59" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="13.62" x2="-96.79" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="13.62" x2="-97.99" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="13.62" x2="-100.99" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="13.62" x2="-102.19" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="13.62" x2="-103.99" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="13.62" x2="-106.39" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.62" x2="-108.19" y2="13.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="12.45" x2="4.22" y2="12.55" layer="21"/>
<rectangle x1="4.82" y1="12.45" x2="5.42" y2="12.55" layer="21"/>
<rectangle x1="6.02" y1="12.45" x2="6.62" y2="12.55" layer="21"/>
<rectangle x1="7.22" y1="12.45" x2="7.82" y2="12.55" layer="21"/>
<rectangle x1="9.62" y1="12.45" x2="10.82" y2="12.55" layer="21"/>
<rectangle x1="11.42" y1="12.45" x2="12.02" y2="12.55" layer="21"/>
<rectangle x1="12.62" y1="12.45" x2="13.82" y2="12.55" layer="21"/>
<rectangle x1="15.62" y1="12.45" x2="16.22" y2="12.55" layer="21"/>
<rectangle x1="17.42" y1="12.45" x2="18.02" y2="12.55" layer="21"/>
<rectangle x1="-93.79" y1="13.72" x2="-91.39" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="13.72" x2="-94.39" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-96.19" y1="13.72" x2="-95.59" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="13.72" x2="-96.79" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="13.72" x2="-97.99" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="13.72" x2="-100.99" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="13.72" x2="-102.19" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="13.72" x2="-103.99" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-107.59" y1="13.72" x2="-106.39" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.72" x2="-108.19" y2="13.82" layer="21" rot="R180"/>
<rectangle x1="9.02" y1="12.55" x2="10.22" y2="12.65" layer="21"/>
<rectangle x1="10.82" y1="12.55" x2="13.82" y2="12.65" layer="21"/>
<rectangle x1="-99.19" y1="13.82" x2="-91.39" y2="13.92" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="13.82" x2="-100.39" y2="13.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.82" x2="-103.99" y2="13.92" layer="21" rot="R180"/>
<rectangle x1="9.02" y1="12.65" x2="10.22" y2="12.75" layer="21"/>
<rectangle x1="10.82" y1="12.65" x2="13.82" y2="12.75" layer="21"/>
<rectangle x1="-99.19" y1="13.92" x2="-91.39" y2="14.02" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="13.92" x2="-100.39" y2="14.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="13.92" x2="-103.99" y2="14.02" layer="21" rot="R180"/>
<rectangle x1="9.02" y1="12.75" x2="10.22" y2="12.85" layer="21"/>
<rectangle x1="10.82" y1="12.75" x2="13.82" y2="12.85" layer="21"/>
<rectangle x1="-99.19" y1="14.02" x2="-91.39" y2="14.12" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.02" x2="-100.39" y2="14.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.02" x2="-103.99" y2="14.12" layer="21" rot="R180"/>
<rectangle x1="9.02" y1="12.85" x2="10.22" y2="12.95" layer="21"/>
<rectangle x1="10.82" y1="12.85" x2="13.82" y2="12.95" layer="21"/>
<rectangle x1="-99.19" y1="14.12" x2="-91.39" y2="14.22" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.12" x2="-100.39" y2="14.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.12" x2="-103.99" y2="14.22" layer="21" rot="R180"/>
<rectangle x1="9.02" y1="12.95" x2="10.22" y2="13.05" layer="21"/>
<rectangle x1="10.82" y1="12.95" x2="13.82" y2="13.05" layer="21"/>
<rectangle x1="-99.19" y1="14.22" x2="-91.39" y2="14.32" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.22" x2="-100.39" y2="14.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.22" x2="-103.99" y2="14.32" layer="21" rot="R180"/>
<rectangle x1="9.02" y1="13.05" x2="10.22" y2="13.15" layer="21"/>
<rectangle x1="10.82" y1="13.05" x2="13.82" y2="13.15" layer="21"/>
<rectangle x1="-99.19" y1="14.32" x2="-91.39" y2="14.42" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.32" x2="-100.39" y2="14.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.32" x2="-103.99" y2="14.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.15" x2="7.82" y2="13.25" layer="21"/>
<rectangle x1="8.42" y1="13.15" x2="9.02" y2="13.25" layer="21"/>
<rectangle x1="9.62" y1="13.15" x2="10.22" y2="13.25" layer="21"/>
<rectangle x1="10.82" y1="13.15" x2="11.42" y2="13.25" layer="21"/>
<rectangle x1="12.02" y1="13.15" x2="12.62" y2="13.25" layer="21"/>
<rectangle x1="13.22" y1="13.15" x2="13.82" y2="13.25" layer="21"/>
<rectangle x1="14.42" y1="13.15" x2="18.62" y2="13.25" layer="21"/>
<rectangle x1="-93.79" y1="14.42" x2="-91.39" y2="14.52" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="14.42" x2="-97.99" y2="14.52" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="14.42" x2="-99.19" y2="14.52" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.42" x2="-100.39" y2="14.52" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="14.42" x2="-101.59" y2="14.52" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="14.42" x2="-102.79" y2="14.52" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="14.42" x2="-103.99" y2="14.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.42" x2="-108.79" y2="14.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.25" x2="7.82" y2="13.35" layer="21"/>
<rectangle x1="8.42" y1="13.25" x2="9.02" y2="13.35" layer="21"/>
<rectangle x1="9.62" y1="13.25" x2="10.22" y2="13.35" layer="21"/>
<rectangle x1="10.82" y1="13.25" x2="11.42" y2="13.35" layer="21"/>
<rectangle x1="12.02" y1="13.25" x2="12.62" y2="13.35" layer="21"/>
<rectangle x1="13.22" y1="13.25" x2="13.82" y2="13.35" layer="21"/>
<rectangle x1="14.42" y1="13.25" x2="18.62" y2="13.35" layer="21"/>
<rectangle x1="-93.79" y1="14.52" x2="-91.39" y2="14.62" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="14.52" x2="-97.99" y2="14.62" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="14.52" x2="-99.19" y2="14.62" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.52" x2="-100.39" y2="14.62" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="14.52" x2="-101.59" y2="14.62" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="14.52" x2="-102.79" y2="14.62" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="14.52" x2="-103.99" y2="14.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.52" x2="-108.79" y2="14.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.35" x2="7.82" y2="13.45" layer="21"/>
<rectangle x1="8.42" y1="13.35" x2="9.02" y2="13.45" layer="21"/>
<rectangle x1="9.62" y1="13.35" x2="10.22" y2="13.45" layer="21"/>
<rectangle x1="10.82" y1="13.35" x2="11.42" y2="13.45" layer="21"/>
<rectangle x1="12.02" y1="13.35" x2="12.62" y2="13.45" layer="21"/>
<rectangle x1="13.22" y1="13.35" x2="13.82" y2="13.45" layer="21"/>
<rectangle x1="14.42" y1="13.35" x2="18.62" y2="13.45" layer="21"/>
<rectangle x1="-93.79" y1="14.62" x2="-91.39" y2="14.72" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="14.62" x2="-97.99" y2="14.72" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="14.62" x2="-99.19" y2="14.72" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.62" x2="-100.39" y2="14.72" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="14.62" x2="-101.59" y2="14.72" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="14.62" x2="-102.79" y2="14.72" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="14.62" x2="-103.99" y2="14.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.62" x2="-108.79" y2="14.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.45" x2="7.82" y2="13.55" layer="21"/>
<rectangle x1="8.42" y1="13.45" x2="9.02" y2="13.55" layer="21"/>
<rectangle x1="9.62" y1="13.45" x2="10.22" y2="13.55" layer="21"/>
<rectangle x1="10.82" y1="13.45" x2="11.42" y2="13.55" layer="21"/>
<rectangle x1="12.02" y1="13.45" x2="12.62" y2="13.55" layer="21"/>
<rectangle x1="13.22" y1="13.45" x2="13.82" y2="13.55" layer="21"/>
<rectangle x1="14.42" y1="13.45" x2="18.62" y2="13.55" layer="21"/>
<rectangle x1="-93.79" y1="14.72" x2="-91.39" y2="14.82" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="14.72" x2="-97.99" y2="14.82" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="14.72" x2="-99.19" y2="14.82" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.72" x2="-100.39" y2="14.82" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="14.72" x2="-101.59" y2="14.82" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="14.72" x2="-102.79" y2="14.82" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="14.72" x2="-103.99" y2="14.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.72" x2="-108.79" y2="14.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.55" x2="7.82" y2="13.65" layer="21"/>
<rectangle x1="8.42" y1="13.55" x2="9.02" y2="13.65" layer="21"/>
<rectangle x1="9.62" y1="13.55" x2="10.22" y2="13.65" layer="21"/>
<rectangle x1="10.82" y1="13.55" x2="11.42" y2="13.65" layer="21"/>
<rectangle x1="12.02" y1="13.55" x2="12.62" y2="13.65" layer="21"/>
<rectangle x1="13.22" y1="13.55" x2="13.82" y2="13.65" layer="21"/>
<rectangle x1="14.42" y1="13.55" x2="18.62" y2="13.65" layer="21"/>
<rectangle x1="-93.79" y1="14.82" x2="-91.39" y2="14.92" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="14.82" x2="-97.99" y2="14.92" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="14.82" x2="-99.19" y2="14.92" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.82" x2="-100.39" y2="14.92" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="14.82" x2="-101.59" y2="14.92" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="14.82" x2="-102.79" y2="14.92" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="14.82" x2="-103.99" y2="14.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.82" x2="-108.79" y2="14.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.65" x2="7.82" y2="13.75" layer="21"/>
<rectangle x1="8.42" y1="13.65" x2="9.02" y2="13.75" layer="21"/>
<rectangle x1="9.62" y1="13.65" x2="10.22" y2="13.75" layer="21"/>
<rectangle x1="10.82" y1="13.65" x2="11.42" y2="13.75" layer="21"/>
<rectangle x1="12.02" y1="13.65" x2="12.62" y2="13.75" layer="21"/>
<rectangle x1="13.22" y1="13.65" x2="13.82" y2="13.75" layer="21"/>
<rectangle x1="14.42" y1="13.65" x2="18.62" y2="13.75" layer="21"/>
<rectangle x1="-93.79" y1="14.92" x2="-91.39" y2="15.02" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="14.92" x2="-97.99" y2="15.02" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="14.92" x2="-99.19" y2="15.02" layer="21" rot="R180"/>
<rectangle x1="-100.99" y1="14.92" x2="-100.39" y2="15.02" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="14.92" x2="-101.59" y2="15.02" layer="21" rot="R180"/>
<rectangle x1="-103.39" y1="14.92" x2="-102.79" y2="15.02" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="14.92" x2="-103.99" y2="15.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="14.92" x2="-108.79" y2="15.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.75" x2="4.22" y2="13.85" layer="21"/>
<rectangle x1="7.22" y1="13.75" x2="7.82" y2="13.85" layer="21"/>
<rectangle x1="9.02" y1="13.75" x2="9.62" y2="13.85" layer="21"/>
<rectangle x1="12.02" y1="13.75" x2="13.22" y2="13.85" layer="21"/>
<rectangle x1="14.42" y1="13.75" x2="15.02" y2="13.85" layer="21"/>
<rectangle x1="18.02" y1="13.75" x2="18.62" y2="13.85" layer="21"/>
<rectangle x1="-93.79" y1="15.02" x2="-91.39" y2="15.12" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.02" x2="-94.39" y2="15.12" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="15.02" x2="-97.99" y2="15.12" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="15.02" x2="-99.79" y2="15.12" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.02" x2="-103.39" y2="15.12" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.02" x2="-105.19" y2="15.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.02" x2="-108.79" y2="15.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.85" x2="4.22" y2="13.95" layer="21"/>
<rectangle x1="7.22" y1="13.85" x2="7.82" y2="13.95" layer="21"/>
<rectangle x1="9.02" y1="13.85" x2="9.62" y2="13.95" layer="21"/>
<rectangle x1="12.02" y1="13.85" x2="13.22" y2="13.95" layer="21"/>
<rectangle x1="14.42" y1="13.85" x2="15.02" y2="13.95" layer="21"/>
<rectangle x1="18.02" y1="13.85" x2="18.62" y2="13.95" layer="21"/>
<rectangle x1="-93.79" y1="15.12" x2="-91.39" y2="15.22" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.12" x2="-94.39" y2="15.22" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="15.12" x2="-97.99" y2="15.22" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="15.12" x2="-99.79" y2="15.22" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.12" x2="-103.39" y2="15.22" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.12" x2="-105.19" y2="15.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.12" x2="-108.79" y2="15.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="13.95" x2="4.22" y2="14.05" layer="21"/>
<rectangle x1="7.22" y1="13.95" x2="7.82" y2="14.05" layer="21"/>
<rectangle x1="9.02" y1="13.95" x2="9.62" y2="14.05" layer="21"/>
<rectangle x1="12.02" y1="13.95" x2="13.22" y2="14.05" layer="21"/>
<rectangle x1="14.42" y1="13.95" x2="15.02" y2="14.05" layer="21"/>
<rectangle x1="18.02" y1="13.95" x2="18.62" y2="14.05" layer="21"/>
<rectangle x1="-93.79" y1="15.22" x2="-91.39" y2="15.32" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.22" x2="-94.39" y2="15.32" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="15.22" x2="-97.99" y2="15.32" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="15.22" x2="-99.79" y2="15.32" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.22" x2="-103.39" y2="15.32" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.22" x2="-105.19" y2="15.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.22" x2="-108.79" y2="15.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.05" x2="4.22" y2="14.15" layer="21"/>
<rectangle x1="7.22" y1="14.05" x2="7.82" y2="14.15" layer="21"/>
<rectangle x1="9.02" y1="14.05" x2="9.62" y2="14.15" layer="21"/>
<rectangle x1="12.02" y1="14.05" x2="13.22" y2="14.15" layer="21"/>
<rectangle x1="14.42" y1="14.05" x2="15.02" y2="14.15" layer="21"/>
<rectangle x1="18.02" y1="14.05" x2="18.62" y2="14.15" layer="21"/>
<rectangle x1="-93.79" y1="15.32" x2="-91.39" y2="15.42" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.32" x2="-94.39" y2="15.42" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="15.32" x2="-97.99" y2="15.42" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="15.32" x2="-99.79" y2="15.42" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.32" x2="-103.39" y2="15.42" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.32" x2="-105.19" y2="15.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.32" x2="-108.79" y2="15.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.15" x2="4.22" y2="14.25" layer="21"/>
<rectangle x1="7.22" y1="14.15" x2="7.82" y2="14.25" layer="21"/>
<rectangle x1="9.02" y1="14.15" x2="9.62" y2="14.25" layer="21"/>
<rectangle x1="12.02" y1="14.15" x2="13.22" y2="14.25" layer="21"/>
<rectangle x1="14.42" y1="14.15" x2="15.02" y2="14.25" layer="21"/>
<rectangle x1="18.02" y1="14.15" x2="18.62" y2="14.25" layer="21"/>
<rectangle x1="-93.79" y1="15.42" x2="-91.39" y2="15.52" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.42" x2="-94.39" y2="15.52" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="15.42" x2="-97.99" y2="15.52" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="15.42" x2="-99.79" y2="15.52" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.42" x2="-103.39" y2="15.52" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.42" x2="-105.19" y2="15.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.42" x2="-108.79" y2="15.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.25" x2="4.22" y2="14.35" layer="21"/>
<rectangle x1="7.22" y1="14.25" x2="7.82" y2="14.35" layer="21"/>
<rectangle x1="9.02" y1="14.25" x2="9.62" y2="14.35" layer="21"/>
<rectangle x1="12.02" y1="14.25" x2="13.22" y2="14.35" layer="21"/>
<rectangle x1="14.42" y1="14.25" x2="15.02" y2="14.35" layer="21"/>
<rectangle x1="18.02" y1="14.25" x2="18.62" y2="14.35" layer="21"/>
<rectangle x1="-93.79" y1="15.52" x2="-91.39" y2="15.62" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.52" x2="-94.39" y2="15.62" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="15.52" x2="-97.99" y2="15.62" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="15.52" x2="-99.79" y2="15.62" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.52" x2="-103.39" y2="15.62" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.52" x2="-105.19" y2="15.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.52" x2="-108.79" y2="15.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.35" x2="4.22" y2="14.45" layer="21"/>
<rectangle x1="4.82" y1="14.35" x2="6.62" y2="14.45" layer="21"/>
<rectangle x1="7.22" y1="14.35" x2="7.82" y2="14.45" layer="21"/>
<rectangle x1="8.42" y1="14.35" x2="9.62" y2="14.45" layer="21"/>
<rectangle x1="11.42" y1="14.35" x2="13.82" y2="14.45" layer="21"/>
<rectangle x1="14.42" y1="14.35" x2="15.02" y2="14.45" layer="21"/>
<rectangle x1="15.62" y1="14.35" x2="17.42" y2="14.45" layer="21"/>
<rectangle x1="18.02" y1="14.35" x2="18.62" y2="14.45" layer="21"/>
<rectangle x1="-93.79" y1="15.62" x2="-91.39" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="15.62" x2="-94.39" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.62" x2="-96.79" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="15.62" x2="-97.99" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="15.62" x2="-99.79" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.62" x2="-103.99" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="15.62" x2="-105.19" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.62" x2="-107.59" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.62" x2="-108.79" y2="15.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.45" x2="4.22" y2="14.55" layer="21"/>
<rectangle x1="4.82" y1="14.45" x2="6.62" y2="14.55" layer="21"/>
<rectangle x1="7.22" y1="14.45" x2="7.82" y2="14.55" layer="21"/>
<rectangle x1="8.42" y1="14.45" x2="9.62" y2="14.55" layer="21"/>
<rectangle x1="11.42" y1="14.45" x2="13.82" y2="14.55" layer="21"/>
<rectangle x1="14.42" y1="14.45" x2="15.02" y2="14.55" layer="21"/>
<rectangle x1="15.62" y1="14.45" x2="17.42" y2="14.55" layer="21"/>
<rectangle x1="18.02" y1="14.45" x2="18.62" y2="14.55" layer="21"/>
<rectangle x1="-93.79" y1="15.72" x2="-91.39" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="15.72" x2="-94.39" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.72" x2="-96.79" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="15.72" x2="-97.99" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="15.72" x2="-99.79" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.72" x2="-103.99" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="15.72" x2="-105.19" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.72" x2="-107.59" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.72" x2="-108.79" y2="15.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.55" x2="4.22" y2="14.65" layer="21"/>
<rectangle x1="4.82" y1="14.55" x2="6.62" y2="14.65" layer="21"/>
<rectangle x1="7.22" y1="14.55" x2="7.82" y2="14.65" layer="21"/>
<rectangle x1="8.42" y1="14.55" x2="9.62" y2="14.65" layer="21"/>
<rectangle x1="11.42" y1="14.55" x2="13.82" y2="14.65" layer="21"/>
<rectangle x1="14.42" y1="14.55" x2="15.02" y2="14.65" layer="21"/>
<rectangle x1="15.62" y1="14.55" x2="17.42" y2="14.65" layer="21"/>
<rectangle x1="18.02" y1="14.55" x2="18.62" y2="14.65" layer="21"/>
<rectangle x1="-93.79" y1="15.82" x2="-91.39" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="15.82" x2="-94.39" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.82" x2="-96.79" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="15.82" x2="-97.99" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="15.82" x2="-99.79" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.82" x2="-103.99" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="15.82" x2="-105.19" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.82" x2="-107.59" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.82" x2="-108.79" y2="15.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.65" x2="4.22" y2="14.75" layer="21"/>
<rectangle x1="4.82" y1="14.65" x2="6.62" y2="14.75" layer="21"/>
<rectangle x1="7.22" y1="14.65" x2="7.82" y2="14.75" layer="21"/>
<rectangle x1="8.42" y1="14.65" x2="9.62" y2="14.75" layer="21"/>
<rectangle x1="11.42" y1="14.65" x2="13.82" y2="14.75" layer="21"/>
<rectangle x1="14.42" y1="14.65" x2="15.02" y2="14.75" layer="21"/>
<rectangle x1="15.62" y1="14.65" x2="17.42" y2="14.75" layer="21"/>
<rectangle x1="18.02" y1="14.65" x2="18.62" y2="14.75" layer="21"/>
<rectangle x1="-93.79" y1="15.92" x2="-91.39" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="15.92" x2="-94.39" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="15.92" x2="-96.79" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="15.92" x2="-97.99" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="15.92" x2="-99.79" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="15.92" x2="-103.99" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="15.92" x2="-105.19" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="15.92" x2="-107.59" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="15.92" x2="-108.79" y2="16.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.75" x2="4.22" y2="14.85" layer="21"/>
<rectangle x1="4.82" y1="14.75" x2="6.62" y2="14.85" layer="21"/>
<rectangle x1="7.22" y1="14.75" x2="7.82" y2="14.85" layer="21"/>
<rectangle x1="8.42" y1="14.75" x2="9.62" y2="14.85" layer="21"/>
<rectangle x1="11.42" y1="14.75" x2="13.82" y2="14.85" layer="21"/>
<rectangle x1="14.42" y1="14.75" x2="15.02" y2="14.85" layer="21"/>
<rectangle x1="15.62" y1="14.75" x2="17.42" y2="14.85" layer="21"/>
<rectangle x1="18.02" y1="14.75" x2="18.62" y2="14.85" layer="21"/>
<rectangle x1="-93.79" y1="16.02" x2="-91.39" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.02" x2="-94.39" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.02" x2="-96.79" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="16.02" x2="-97.99" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="16.02" x2="-99.79" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.02" x2="-103.99" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.02" x2="-105.19" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.02" x2="-107.59" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.02" x2="-108.79" y2="16.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.85" x2="4.22" y2="14.95" layer="21"/>
<rectangle x1="4.82" y1="14.85" x2="6.62" y2="14.95" layer="21"/>
<rectangle x1="7.22" y1="14.85" x2="7.82" y2="14.95" layer="21"/>
<rectangle x1="8.42" y1="14.85" x2="9.62" y2="14.95" layer="21"/>
<rectangle x1="11.42" y1="14.85" x2="13.82" y2="14.95" layer="21"/>
<rectangle x1="14.42" y1="14.85" x2="15.02" y2="14.95" layer="21"/>
<rectangle x1="15.62" y1="14.85" x2="17.42" y2="14.95" layer="21"/>
<rectangle x1="18.02" y1="14.85" x2="18.62" y2="14.95" layer="21"/>
<rectangle x1="-93.79" y1="16.12" x2="-91.39" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.12" x2="-94.39" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.12" x2="-96.79" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="16.12" x2="-97.99" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="-101.59" y1="16.12" x2="-99.79" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.12" x2="-103.99" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.12" x2="-105.19" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.12" x2="-107.59" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.12" x2="-108.79" y2="16.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="14.95" x2="4.22" y2="15.05" layer="21"/>
<rectangle x1="4.82" y1="14.95" x2="6.62" y2="15.05" layer="21"/>
<rectangle x1="7.22" y1="14.95" x2="7.82" y2="15.05" layer="21"/>
<rectangle x1="10.22" y1="14.95" x2="11.42" y2="15.05" layer="21"/>
<rectangle x1="12.02" y1="14.95" x2="12.62" y2="15.05" layer="21"/>
<rectangle x1="14.42" y1="14.95" x2="15.02" y2="15.05" layer="21"/>
<rectangle x1="15.62" y1="14.95" x2="17.42" y2="15.05" layer="21"/>
<rectangle x1="18.02" y1="14.95" x2="18.62" y2="15.05" layer="21"/>
<rectangle x1="-93.79" y1="16.22" x2="-91.39" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.22" x2="-94.39" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.22" x2="-96.79" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="16.22" x2="-97.99" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="16.22" x2="-101.59" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.22" x2="-102.79" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.22" x2="-105.19" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.22" x2="-107.59" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.22" x2="-108.79" y2="16.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.05" x2="4.22" y2="15.15" layer="21"/>
<rectangle x1="4.82" y1="15.05" x2="6.62" y2="15.15" layer="21"/>
<rectangle x1="7.22" y1="15.05" x2="7.82" y2="15.15" layer="21"/>
<rectangle x1="10.22" y1="15.05" x2="11.42" y2="15.15" layer="21"/>
<rectangle x1="12.02" y1="15.05" x2="12.62" y2="15.15" layer="21"/>
<rectangle x1="14.42" y1="15.05" x2="15.02" y2="15.15" layer="21"/>
<rectangle x1="15.62" y1="15.05" x2="17.42" y2="15.15" layer="21"/>
<rectangle x1="18.02" y1="15.05" x2="18.62" y2="15.15" layer="21"/>
<rectangle x1="-93.79" y1="16.32" x2="-91.39" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.32" x2="-94.39" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.32" x2="-96.79" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="16.32" x2="-97.99" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="16.32" x2="-101.59" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.32" x2="-102.79" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.32" x2="-105.19" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.32" x2="-107.59" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.32" x2="-108.79" y2="16.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.15" x2="4.22" y2="15.25" layer="21"/>
<rectangle x1="4.82" y1="15.15" x2="6.62" y2="15.25" layer="21"/>
<rectangle x1="7.22" y1="15.15" x2="7.82" y2="15.25" layer="21"/>
<rectangle x1="10.22" y1="15.15" x2="11.42" y2="15.25" layer="21"/>
<rectangle x1="12.02" y1="15.15" x2="12.62" y2="15.25" layer="21"/>
<rectangle x1="14.42" y1="15.15" x2="15.02" y2="15.25" layer="21"/>
<rectangle x1="15.62" y1="15.15" x2="17.42" y2="15.25" layer="21"/>
<rectangle x1="18.02" y1="15.15" x2="18.62" y2="15.25" layer="21"/>
<rectangle x1="-93.79" y1="16.42" x2="-91.39" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.42" x2="-94.39" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.42" x2="-96.79" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="16.42" x2="-97.99" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="16.42" x2="-101.59" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.42" x2="-102.79" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.42" x2="-105.19" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.42" x2="-107.59" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.42" x2="-108.79" y2="16.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.25" x2="4.22" y2="15.35" layer="21"/>
<rectangle x1="4.82" y1="15.25" x2="6.62" y2="15.35" layer="21"/>
<rectangle x1="7.22" y1="15.25" x2="7.82" y2="15.35" layer="21"/>
<rectangle x1="10.22" y1="15.25" x2="11.42" y2="15.35" layer="21"/>
<rectangle x1="12.02" y1="15.25" x2="12.62" y2="15.35" layer="21"/>
<rectangle x1="14.42" y1="15.25" x2="15.02" y2="15.35" layer="21"/>
<rectangle x1="15.62" y1="15.25" x2="17.42" y2="15.35" layer="21"/>
<rectangle x1="18.02" y1="15.25" x2="18.62" y2="15.35" layer="21"/>
<rectangle x1="-93.79" y1="16.52" x2="-91.39" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.52" x2="-94.39" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.52" x2="-96.79" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="16.52" x2="-97.99" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="16.52" x2="-101.59" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.52" x2="-102.79" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.52" x2="-105.19" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.52" x2="-107.59" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.52" x2="-108.79" y2="16.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.35" x2="4.22" y2="15.45" layer="21"/>
<rectangle x1="4.82" y1="15.35" x2="6.62" y2="15.45" layer="21"/>
<rectangle x1="7.22" y1="15.35" x2="7.82" y2="15.45" layer="21"/>
<rectangle x1="10.22" y1="15.35" x2="11.42" y2="15.45" layer="21"/>
<rectangle x1="12.02" y1="15.35" x2="12.62" y2="15.45" layer="21"/>
<rectangle x1="14.42" y1="15.35" x2="15.02" y2="15.45" layer="21"/>
<rectangle x1="15.62" y1="15.35" x2="17.42" y2="15.45" layer="21"/>
<rectangle x1="18.02" y1="15.35" x2="18.62" y2="15.45" layer="21"/>
<rectangle x1="-93.79" y1="16.62" x2="-91.39" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.62" x2="-94.39" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.62" x2="-96.79" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="16.62" x2="-97.99" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="16.62" x2="-101.59" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.62" x2="-102.79" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.62" x2="-105.19" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.62" x2="-107.59" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.62" x2="-108.79" y2="16.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.45" x2="4.22" y2="15.55" layer="21"/>
<rectangle x1="4.82" y1="15.45" x2="6.62" y2="15.55" layer="21"/>
<rectangle x1="7.22" y1="15.45" x2="7.82" y2="15.55" layer="21"/>
<rectangle x1="10.22" y1="15.45" x2="11.42" y2="15.55" layer="21"/>
<rectangle x1="12.02" y1="15.45" x2="12.62" y2="15.55" layer="21"/>
<rectangle x1="14.42" y1="15.45" x2="15.02" y2="15.55" layer="21"/>
<rectangle x1="15.62" y1="15.45" x2="17.42" y2="15.55" layer="21"/>
<rectangle x1="18.02" y1="15.45" x2="18.62" y2="15.55" layer="21"/>
<rectangle x1="-93.79" y1="16.72" x2="-91.39" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.72" x2="-94.39" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.72" x2="-96.79" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="-100.39" y1="16.72" x2="-97.99" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="16.72" x2="-101.59" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.72" x2="-102.79" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.72" x2="-105.19" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.72" x2="-107.59" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.72" x2="-108.79" y2="16.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.55" x2="4.22" y2="15.65" layer="21"/>
<rectangle x1="4.82" y1="15.55" x2="6.62" y2="15.65" layer="21"/>
<rectangle x1="7.22" y1="15.55" x2="7.82" y2="15.65" layer="21"/>
<rectangle x1="9.02" y1="15.55" x2="11.42" y2="15.65" layer="21"/>
<rectangle x1="12.62" y1="15.55" x2="13.22" y2="15.65" layer="21"/>
<rectangle x1="14.42" y1="15.55" x2="15.02" y2="15.65" layer="21"/>
<rectangle x1="15.62" y1="15.55" x2="17.42" y2="15.65" layer="21"/>
<rectangle x1="18.02" y1="15.55" x2="18.62" y2="15.65" layer="21"/>
<rectangle x1="-93.79" y1="16.82" x2="-91.39" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.82" x2="-94.39" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.82" x2="-96.79" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="16.82" x2="-97.99" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="16.82" x2="-101.59" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.82" x2="-103.39" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.82" x2="-105.19" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.82" x2="-107.59" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.82" x2="-108.79" y2="16.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.65" x2="4.22" y2="15.75" layer="21"/>
<rectangle x1="4.82" y1="15.65" x2="6.62" y2="15.75" layer="21"/>
<rectangle x1="7.22" y1="15.65" x2="7.82" y2="15.75" layer="21"/>
<rectangle x1="9.02" y1="15.65" x2="11.42" y2="15.75" layer="21"/>
<rectangle x1="12.62" y1="15.65" x2="13.22" y2="15.75" layer="21"/>
<rectangle x1="14.42" y1="15.65" x2="15.02" y2="15.75" layer="21"/>
<rectangle x1="15.62" y1="15.65" x2="17.42" y2="15.75" layer="21"/>
<rectangle x1="18.02" y1="15.65" x2="18.62" y2="15.75" layer="21"/>
<rectangle x1="-93.79" y1="16.92" x2="-91.39" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="16.92" x2="-94.39" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="16.92" x2="-96.79" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="16.92" x2="-97.99" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="16.92" x2="-101.59" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="16.92" x2="-103.39" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="16.92" x2="-105.19" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="16.92" x2="-107.59" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="16.92" x2="-108.79" y2="17.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.75" x2="4.22" y2="15.85" layer="21"/>
<rectangle x1="4.82" y1="15.75" x2="6.62" y2="15.85" layer="21"/>
<rectangle x1="7.22" y1="15.75" x2="7.82" y2="15.85" layer="21"/>
<rectangle x1="9.02" y1="15.75" x2="11.42" y2="15.85" layer="21"/>
<rectangle x1="12.62" y1="15.75" x2="13.22" y2="15.85" layer="21"/>
<rectangle x1="14.42" y1="15.75" x2="15.02" y2="15.85" layer="21"/>
<rectangle x1="15.62" y1="15.75" x2="17.42" y2="15.85" layer="21"/>
<rectangle x1="18.02" y1="15.75" x2="18.62" y2="15.85" layer="21"/>
<rectangle x1="-93.79" y1="17.02" x2="-91.39" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="17.02" x2="-94.39" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.02" x2="-96.79" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="17.02" x2="-97.99" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.02" x2="-101.59" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.02" x2="-103.39" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="17.02" x2="-105.19" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.02" x2="-107.59" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.02" x2="-108.79" y2="17.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.85" x2="4.22" y2="15.95" layer="21"/>
<rectangle x1="4.82" y1="15.85" x2="6.62" y2="15.95" layer="21"/>
<rectangle x1="7.22" y1="15.85" x2="7.82" y2="15.95" layer="21"/>
<rectangle x1="9.02" y1="15.85" x2="11.42" y2="15.95" layer="21"/>
<rectangle x1="12.62" y1="15.85" x2="13.22" y2="15.95" layer="21"/>
<rectangle x1="14.42" y1="15.85" x2="15.02" y2="15.95" layer="21"/>
<rectangle x1="15.62" y1="15.85" x2="17.42" y2="15.95" layer="21"/>
<rectangle x1="18.02" y1="15.85" x2="18.62" y2="15.95" layer="21"/>
<rectangle x1="-93.79" y1="17.12" x2="-91.39" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="17.12" x2="-94.39" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.12" x2="-96.79" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="17.12" x2="-97.99" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.12" x2="-101.59" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.12" x2="-103.39" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="17.12" x2="-105.19" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.12" x2="-107.59" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.12" x2="-108.79" y2="17.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="15.95" x2="4.22" y2="16.05" layer="21"/>
<rectangle x1="4.82" y1="15.95" x2="6.62" y2="16.05" layer="21"/>
<rectangle x1="7.22" y1="15.95" x2="7.82" y2="16.05" layer="21"/>
<rectangle x1="9.02" y1="15.95" x2="11.42" y2="16.05" layer="21"/>
<rectangle x1="12.62" y1="15.95" x2="13.22" y2="16.05" layer="21"/>
<rectangle x1="14.42" y1="15.95" x2="15.02" y2="16.05" layer="21"/>
<rectangle x1="15.62" y1="15.95" x2="17.42" y2="16.05" layer="21"/>
<rectangle x1="18.02" y1="15.95" x2="18.62" y2="16.05" layer="21"/>
<rectangle x1="-93.79" y1="17.22" x2="-91.39" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="17.22" x2="-94.39" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.22" x2="-96.79" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="17.22" x2="-97.99" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.22" x2="-101.59" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.22" x2="-103.39" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="17.22" x2="-105.19" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.22" x2="-107.59" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.22" x2="-108.79" y2="17.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.05" x2="4.22" y2="16.15" layer="21"/>
<rectangle x1="4.82" y1="16.05" x2="6.62" y2="16.15" layer="21"/>
<rectangle x1="7.22" y1="16.05" x2="7.82" y2="16.15" layer="21"/>
<rectangle x1="9.02" y1="16.05" x2="11.42" y2="16.15" layer="21"/>
<rectangle x1="12.62" y1="16.05" x2="13.22" y2="16.15" layer="21"/>
<rectangle x1="14.42" y1="16.05" x2="15.02" y2="16.15" layer="21"/>
<rectangle x1="15.62" y1="16.05" x2="17.42" y2="16.15" layer="21"/>
<rectangle x1="18.02" y1="16.05" x2="18.62" y2="16.15" layer="21"/>
<rectangle x1="-93.79" y1="17.32" x2="-91.39" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="-94.99" y1="17.32" x2="-94.39" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.32" x2="-96.79" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="-99.19" y1="17.32" x2="-97.99" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.32" x2="-101.59" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.32" x2="-103.39" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="-105.79" y1="17.32" x2="-105.19" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.32" x2="-107.59" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.32" x2="-108.79" y2="17.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.15" x2="4.22" y2="16.25" layer="21"/>
<rectangle x1="7.22" y1="16.15" x2="7.82" y2="16.25" layer="21"/>
<rectangle x1="8.42" y1="16.15" x2="10.22" y2="16.25" layer="21"/>
<rectangle x1="12.62" y1="16.15" x2="13.22" y2="16.25" layer="21"/>
<rectangle x1="14.42" y1="16.15" x2="15.02" y2="16.25" layer="21"/>
<rectangle x1="18.02" y1="16.15" x2="18.62" y2="16.25" layer="21"/>
<rectangle x1="-93.79" y1="17.42" x2="-91.39" y2="17.52" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.42" x2="-94.39" y2="17.52" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="17.42" x2="-97.99" y2="17.52" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.42" x2="-100.39" y2="17.52" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.42" x2="-103.39" y2="17.52" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.42" x2="-105.19" y2="17.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.42" x2="-108.79" y2="17.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.25" x2="4.22" y2="16.35" layer="21"/>
<rectangle x1="7.22" y1="16.25" x2="7.82" y2="16.35" layer="21"/>
<rectangle x1="8.42" y1="16.25" x2="10.22" y2="16.35" layer="21"/>
<rectangle x1="12.62" y1="16.25" x2="13.22" y2="16.35" layer="21"/>
<rectangle x1="14.42" y1="16.25" x2="15.02" y2="16.35" layer="21"/>
<rectangle x1="18.02" y1="16.25" x2="18.62" y2="16.35" layer="21"/>
<rectangle x1="-93.79" y1="17.52" x2="-91.39" y2="17.62" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.52" x2="-94.39" y2="17.62" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="17.52" x2="-97.99" y2="17.62" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.52" x2="-100.39" y2="17.62" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.52" x2="-103.39" y2="17.62" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.52" x2="-105.19" y2="17.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.52" x2="-108.79" y2="17.62" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.35" x2="4.22" y2="16.45" layer="21"/>
<rectangle x1="7.22" y1="16.35" x2="7.82" y2="16.45" layer="21"/>
<rectangle x1="8.42" y1="16.35" x2="10.22" y2="16.45" layer="21"/>
<rectangle x1="12.62" y1="16.35" x2="13.22" y2="16.45" layer="21"/>
<rectangle x1="14.42" y1="16.35" x2="15.02" y2="16.45" layer="21"/>
<rectangle x1="18.02" y1="16.35" x2="18.62" y2="16.45" layer="21"/>
<rectangle x1="-93.79" y1="17.62" x2="-91.39" y2="17.72" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.62" x2="-94.39" y2="17.72" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="17.62" x2="-97.99" y2="17.72" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.62" x2="-100.39" y2="17.72" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.62" x2="-103.39" y2="17.72" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.62" x2="-105.19" y2="17.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.62" x2="-108.79" y2="17.72" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.45" x2="4.22" y2="16.55" layer="21"/>
<rectangle x1="7.22" y1="16.45" x2="7.82" y2="16.55" layer="21"/>
<rectangle x1="8.42" y1="16.45" x2="10.22" y2="16.55" layer="21"/>
<rectangle x1="12.62" y1="16.45" x2="13.22" y2="16.55" layer="21"/>
<rectangle x1="14.42" y1="16.45" x2="15.02" y2="16.55" layer="21"/>
<rectangle x1="18.02" y1="16.45" x2="18.62" y2="16.55" layer="21"/>
<rectangle x1="-93.79" y1="17.72" x2="-91.39" y2="17.82" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.72" x2="-94.39" y2="17.82" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="17.72" x2="-97.99" y2="17.82" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.72" x2="-100.39" y2="17.82" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.72" x2="-103.39" y2="17.82" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.72" x2="-105.19" y2="17.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.72" x2="-108.79" y2="17.82" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.55" x2="4.22" y2="16.65" layer="21"/>
<rectangle x1="7.22" y1="16.55" x2="7.82" y2="16.65" layer="21"/>
<rectangle x1="8.42" y1="16.55" x2="10.22" y2="16.65" layer="21"/>
<rectangle x1="12.62" y1="16.55" x2="13.22" y2="16.65" layer="21"/>
<rectangle x1="14.42" y1="16.55" x2="15.02" y2="16.65" layer="21"/>
<rectangle x1="18.02" y1="16.55" x2="18.62" y2="16.65" layer="21"/>
<rectangle x1="-93.79" y1="17.82" x2="-91.39" y2="17.92" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.82" x2="-94.39" y2="17.92" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="17.82" x2="-97.99" y2="17.92" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.82" x2="-100.39" y2="17.92" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.82" x2="-103.39" y2="17.92" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.82" x2="-105.19" y2="17.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.82" x2="-108.79" y2="17.92" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.65" x2="4.22" y2="16.75" layer="21"/>
<rectangle x1="7.22" y1="16.65" x2="7.82" y2="16.75" layer="21"/>
<rectangle x1="8.42" y1="16.65" x2="10.22" y2="16.75" layer="21"/>
<rectangle x1="12.62" y1="16.65" x2="13.22" y2="16.75" layer="21"/>
<rectangle x1="14.42" y1="16.65" x2="15.02" y2="16.75" layer="21"/>
<rectangle x1="18.02" y1="16.65" x2="18.62" y2="16.75" layer="21"/>
<rectangle x1="-93.79" y1="17.92" x2="-91.39" y2="18.02" layer="21" rot="R180"/>
<rectangle x1="-97.39" y1="17.92" x2="-94.39" y2="18.02" layer="21" rot="R180"/>
<rectangle x1="-98.59" y1="17.92" x2="-97.99" y2="18.02" layer="21" rot="R180"/>
<rectangle x1="-102.79" y1="17.92" x2="-100.39" y2="18.02" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="17.92" x2="-103.39" y2="18.02" layer="21" rot="R180"/>
<rectangle x1="-108.19" y1="17.92" x2="-105.19" y2="18.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="17.92" x2="-108.79" y2="18.02" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.75" x2="7.82" y2="16.85" layer="21"/>
<rectangle x1="9.62" y1="16.75" x2="10.82" y2="16.85" layer="21"/>
<rectangle x1="12.02" y1="16.75" x2="13.22" y2="16.85" layer="21"/>
<rectangle x1="14.42" y1="16.75" x2="18.62" y2="16.85" layer="21"/>
<rectangle x1="-93.79" y1="18.02" x2="-91.39" y2="18.12" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="18.02" x2="-97.99" y2="18.12" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="18.02" x2="-100.99" y2="18.12" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="18.02" x2="-103.39" y2="18.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.02" x2="-108.79" y2="18.12" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.85" x2="7.82" y2="16.95" layer="21"/>
<rectangle x1="9.62" y1="16.85" x2="10.82" y2="16.95" layer="21"/>
<rectangle x1="12.02" y1="16.85" x2="13.22" y2="16.95" layer="21"/>
<rectangle x1="14.42" y1="16.85" x2="18.62" y2="16.95" layer="21"/>
<rectangle x1="-93.79" y1="18.12" x2="-91.39" y2="18.22" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="18.12" x2="-97.99" y2="18.22" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="18.12" x2="-100.99" y2="18.22" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="18.12" x2="-103.39" y2="18.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.12" x2="-108.79" y2="18.22" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="16.95" x2="7.82" y2="17.05" layer="21"/>
<rectangle x1="9.62" y1="16.95" x2="10.82" y2="17.05" layer="21"/>
<rectangle x1="12.02" y1="16.95" x2="13.22" y2="17.05" layer="21"/>
<rectangle x1="14.42" y1="16.95" x2="18.62" y2="17.05" layer="21"/>
<rectangle x1="-93.79" y1="18.22" x2="-91.39" y2="18.32" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="18.22" x2="-97.99" y2="18.32" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="18.22" x2="-100.99" y2="18.32" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="18.22" x2="-103.39" y2="18.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.22" x2="-108.79" y2="18.32" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="17.05" x2="7.82" y2="17.15" layer="21"/>
<rectangle x1="9.62" y1="17.05" x2="10.82" y2="17.15" layer="21"/>
<rectangle x1="12.02" y1="17.05" x2="13.22" y2="17.15" layer="21"/>
<rectangle x1="14.42" y1="17.05" x2="18.62" y2="17.15" layer="21"/>
<rectangle x1="-93.79" y1="18.32" x2="-91.39" y2="18.42" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="18.32" x2="-97.99" y2="18.42" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="18.32" x2="-100.99" y2="18.42" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="18.32" x2="-103.39" y2="18.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.32" x2="-108.79" y2="18.42" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="17.15" x2="7.82" y2="17.25" layer="21"/>
<rectangle x1="9.62" y1="17.15" x2="10.82" y2="17.25" layer="21"/>
<rectangle x1="12.02" y1="17.15" x2="13.22" y2="17.25" layer="21"/>
<rectangle x1="14.42" y1="17.15" x2="18.62" y2="17.25" layer="21"/>
<rectangle x1="-93.79" y1="18.42" x2="-91.39" y2="18.52" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="18.42" x2="-97.99" y2="18.52" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="18.42" x2="-100.99" y2="18.52" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="18.42" x2="-103.39" y2="18.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.42" x2="-108.79" y2="18.52" layer="21" rot="R180"/>
<rectangle x1="3.62" y1="17.25" x2="7.82" y2="17.35" layer="21"/>
<rectangle x1="9.62" y1="17.25" x2="10.82" y2="17.35" layer="21"/>
<rectangle x1="12.02" y1="17.25" x2="13.22" y2="17.35" layer="21"/>
<rectangle x1="14.42" y1="17.25" x2="18.62" y2="17.35" layer="21"/>
<rectangle x1="-93.79" y1="18.52" x2="-91.39" y2="18.62" layer="21" rot="R180"/>
<rectangle x1="-99.79" y1="18.52" x2="-97.99" y2="18.62" layer="21" rot="R180"/>
<rectangle x1="-102.19" y1="18.52" x2="-100.99" y2="18.62" layer="21" rot="R180"/>
<rectangle x1="-104.59" y1="18.52" x2="-103.39" y2="18.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.52" x2="-108.79" y2="18.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.62" x2="-91.39" y2="18.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.72" x2="-91.39" y2="18.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.82" x2="-91.39" y2="18.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="18.92" x2="-91.39" y2="19.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.02" x2="-91.39" y2="19.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.12" x2="-91.39" y2="19.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.22" x2="-91.39" y2="19.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.32" x2="-91.39" y2="19.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.42" x2="-91.39" y2="19.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.52" x2="-91.39" y2="19.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.62" x2="-91.39" y2="19.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.72" x2="-91.39" y2="19.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.82" x2="-91.39" y2="19.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="19.92" x2="-91.39" y2="20.02" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.02" x2="-91.39" y2="20.12" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.12" x2="-91.39" y2="20.22" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.22" x2="-91.39" y2="20.32" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.32" x2="-91.39" y2="20.42" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.42" x2="-91.39" y2="20.52" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.52" x2="-91.39" y2="20.62" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.62" x2="-91.39" y2="20.72" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.72" x2="-91.39" y2="20.82" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.82" x2="-91.39" y2="20.92" layer="21" rot="R180"/>
<rectangle x1="-111.19" y1="20.92" x2="-91.39" y2="21.02" layer="21" rot="R180"/>
<text x="-91.44" y="0.77" size="0.2" layer="21" font="vector">D:/program/EAGLE 7.5.0/projects/examples/PCB-EAGLE/qr-code.bmp</text>
</package>
<package name="LOGO_OMEGA">
<rectangle x1="-0.1" y1="-0.8" x2="0.1" y2="0.5" layer="21"/>
<rectangle x1="-1" y1="-0.1" x2="-0.5" y2="0.1" layer="21"/>
<rectangle x1="-0.5" y1="-0.1" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="-0.1" y1="0.5" x2="0.1" y2="1.1" layer="21"/>
<rectangle x1="-0.4" y1="0.6" x2="0.4" y2="0.8" layer="21"/>
<rectangle x1="0.3" y1="-0.1" x2="0.5" y2="0.3" layer="21"/>
<rectangle x1="0.5" y1="-0.1" x2="1" y2="0.1" layer="21"/>
<polygon width="0" layer="21">
<vertex x="-0.3" y="0.3"/>
<vertex x="-0.7" y="0.7"/>
<vertex x="0" y="1.4"/>
<vertex x="0.7" y="0.7"/>
<vertex x="0.3" y="0.3"/>
<vertex x="0.5" y="0.2"/>
<vertex x="1" y="0.7"/>
<vertex x="0" y="1.7"/>
<vertex x="-1" y="0.7"/>
<vertex x="-0.5" y="0.2"/>
</polygon>
<rectangle x1="-1" y1="-0.5" x2="1" y2="-0.3" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="REZONATOR-4PIN">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="0" y="5.08" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<text x="0.254" y="3.048" size="1.778" layer="96" font="vector" align="center">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="S@1" x="-2.54" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="S@2" x="2.54" y="-7.62" visible="pad" length="short" rot="R90"/>
</symbol>
<symbol name="BATT">
<wire x1="-0.635" y1="-1.905" x2="0" y2="-1.905" width="0.4064" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-1.905" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="0" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="2.54" y2="-0.635" width="0.4064" layer="94"/>
<text x="5.08" y="0.635" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="+" x="0" y="2.54" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="ANT">
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="2.54" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="0" width="0.254" layer="94"/>
<pin name="SIG" x="0" y="0" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="GND" x="2.54" y="0" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="97"/>
</symbol>
<symbol name="LOGO_NOMAD_V1">
<text x="0" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="REZONATOR-4PIN" prefix="ZQ" uservalue="yes">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="REZONATOR-4PIN" x="0" y="0"/>
</gates>
<devices>
<device name="HC18U-H" package="HC18U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="S@1" pad="M"/>
<connect gate="G$1" pin="S@2" pad="M1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68SMX" package="86SMX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="S@1" pad="2"/>
<connect gate="G$1" pin="S@2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="6344860" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MM20SS" package="MM20SS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
<connect gate="G$1" pin="S@1" pad="2"/>
<connect gate="G$1" pin="S@2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MM39SL" package="MM39SL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
<connect gate="G$1" pin="S@1" pad="2"/>
<connect gate="G$1" pin="S@2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="CTS406" package="CTS406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="S@1" pad="2"/>
<connect gate="G$1" pin="S@2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SJK-7M" package="SJK-7M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
<connect gate="G$1" pin="S@1" pad="2"/>
<connect gate="G$1" pin="S@2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="CRYSTAL-SMD-3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="S@1" pad="2"/>
<connect gate="G$1" pin="S@2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CR2032H" prefix="G">
<description>&lt;b&gt;LI BATTERY&lt;/b&gt; Varta</description>
<gates>
<gate name="1" symbol="BATT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SN2032">
<connects>
<connect gate="1" pin="+" pad="+"/>
<connect gate="1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ANT-2.4GHZ" prefix="ANT">
<gates>
<gate name="G$1" symbol="ANT" x="-2.54" y="0"/>
</gates>
<devices>
<device name="-1" package="RN-ANT">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SIG" pad="FEED"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2" package="ANTENNA-2.4GHZ-IFA">
<connects>
<connect gate="G$1" pin="GND" pad="G"/>
<connect gate="G$1" pin="SIG" pad="A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3" package="ANTENNA-2.4GHZ-MEANTRED">
<connects>
<connect gate="G$1" pin="GND" pad="G"/>
<connect gate="G$1" pin="SIG" pad="A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4" package="OLIMEX_ANT_ESP8266_ANT">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SIG" pad="ANT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="ANT2-SMD-9.5X2X1.2MM">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SIG" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="FIDUCIAL">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LOGO" prefix="LOGO" uservalue="yes">
<gates>
<gate name="G$1" symbol="LOGO_NOMAD_V1" x="7.62" y="0"/>
</gates>
<devices>
<device name="1" package="LOGO_NOMAD_V1">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2" package="LOGO_TRIFORS_V1">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3" package="LOGO_RUN_V1">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5" package="LOGO_BIOMEGA_V2">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4" package="LOGO_QR_1">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6" package="LOGO_OMEGA">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_leds">
<packages>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805-NNN">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.525" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1" dx="1.2" dy="1.2" layer="1"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<wire x1="-0.9" y1="-1.9" x2="0.9" y2="-1.9" width="0.2" layer="21"/>
<wire x1="0.9" y1="-1.9" x2="0.9" y2="0.15" width="0.2" layer="21"/>
<wire x1="0.9" y1="0.15" x2="0.9" y2="1.7" width="0.2" layer="21"/>
<wire x1="0.7" y1="1.9" x2="-0.7" y2="1.9" width="0.2" layer="21"/>
<wire x1="-0.9" y1="1.7" x2="-0.9" y2="0.15" width="0.2" layer="21"/>
<wire x1="-0.9" y1="0.15" x2="-0.9" y2="-1.9" width="0.2" layer="21"/>
<wire x1="-0.9" y1="0.15" x2="0.9" y2="0.15" width="0.2" layer="21"/>
<wire x1="-0.7" y1="1.9" x2="-0.9" y2="1.7" width="0.2" layer="21"/>
<wire x1="0.7" y1="1.9" x2="0.9" y2="1.7" width="0.2" layer="21"/>
<rectangle x1="-0.625" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="21"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.016" layer="25" ratio="20" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.016" layer="27" ratio="20" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="LED_SMD5730">
<wire x1="2.5" y1="-1.5" x2="-1.2" y2="-1.5" width="0.254" layer="21"/>
<smd name="C" x="-2.5" y="0" dx="1.2" dy="1.7" layer="1"/>
<smd name="A1" x="2.5" y="0" dx="1.2" dy="1.7" layer="1"/>
<smd name="A2" x="0.3" y="0" dx="2.25" dy="1.7" layer="1"/>
<text x="0" y="0" size="1.016" layer="21" ratio="15">&gt;NAME</text>
<wire x1="-1.2" y1="-1.5" x2="-2.5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-1.5" x2="-2.5" y2="-1.1" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-1.1" x2="-1.6" y2="-1.1" width="0.254" layer="21"/>
<wire x1="-1.6" y1="-1.1" x2="-1.6" y2="1.1" width="0.254" layer="21"/>
<wire x1="-1.6" y1="1.1" x2="-2.5" y2="1.1" width="0.254" layer="21"/>
<wire x1="-2.5" y1="1.1" x2="-2.5" y2="1.5" width="0.254" layer="21"/>
<wire x1="-2.5" y1="1.5" x2="-1.2" y2="1.5" width="0.254" layer="21"/>
<wire x1="-1.2" y1="1.5" x2="2.5" y2="1.5" width="0.254" layer="21"/>
<wire x1="2.5" y1="1.5" x2="2.5" y2="1.1" width="0.254" layer="21"/>
<wire x1="2.5" y1="-1.5" x2="2.5" y2="-1.1" width="0.254" layer="21"/>
<wire x1="-1.2" y1="1.5" x2="-1.2" y2="-1.5" width="0.254" layer="21"/>
</package>
<package name="KA-3528ASYC">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.2" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.2" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.5" y="0" dx="1.3" dy="2.2" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.3" dy="2.2" layer="1"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.016" layer="27" ratio="20">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<wire x1="-0.5" y1="0.6" x2="-0.5" y2="-0.6" width="0.2" layer="21"/>
<wire x1="-0.5" y1="-0.6" x2="0.3" y2="0" width="0.2" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.5" y2="0.6" width="0.2" layer="21"/>
<wire x1="0.5" y1="0.6" x2="0.5" y2="-0.6" width="0.2" layer="21"/>
<wire x1="-1.6" y1="-1.5" x2="1.6" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-1.6" y1="1.5" x2="1.6" y2="1.5" width="0.2" layer="21"/>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.2" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0" y="1.5" size="1.2" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2" layer="51" curve="-286.260205"/>
<circle x="0" y="0" radius="2.54" width="0.2" layer="51"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.7" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.7" layer="1"/>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<text x="0" y="0" size="0.6" layer="25" font="vector" ratio="12" rot="R90" align="center">&gt;NAME</text>
<rectangle x1="-0.5" y1="0.08" x2="0.5" y2="0.28" layer="21"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<smd name="A" x="-1" y="0" dx="1.3" dy="1" layer="1" rot="R90"/>
<smd name="C" x="1" y="0" dx="1.3" dy="1" layer="1" rot="R90"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1.8" y1="0.9" x2="-0.4" y2="0.9" width="0.2" layer="21"/>
<wire x1="0.2" y1="0.9" x2="1.8" y2="0.9" width="0.2" layer="21"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.2" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="0.2" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.8" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.2" layer="21"/>
<polygon width="0" layer="51">
<vertex x="0.2" y="0"/>
<vertex x="-0.3" y="0.5"/>
<vertex x="-0.3" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.2" y="0.5"/>
<vertex x="0.2" y="-0.5"/>
<vertex x="0.3" y="-0.5"/>
<vertex x="0.3" y="0.5"/>
</polygon>
<text x="0" y="0" size="0.4" layer="27" font="vector" align="center">&gt;VALUE</text>
<wire x1="0.2" y1="0.9" x2="0.2" y2="-0.9" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95" font="vector" rot="R90" align="center">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96" font="vector" rot="R90" align="center">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="HL" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805-NNN">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TTW" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5730" package="LED_SMD5730">
<connects>
<connect gate="G$1" pin="A" pad="A1"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_inductor">
<packages>
<package name="COIL_V2">
<wire x1="-2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.905" y2="1.905" width="0.127" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="1.905" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.905" width="0.127" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<smd name="2" x="1.905" y="0" dx="3.302" dy="2.032" layer="1" rot="R90"/>
<text x="-1.27" y="3.175" size="0.6096" layer="21" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="0.6096" layer="21" font="vector">&gt;VALUE</text>
</package>
<package name="CDRH125">
<description>&lt;b&gt;COILCRAFT&lt;/b&gt;</description>
<wire x1="-3.175" y1="3.175" x2="3.175" y2="3.175" width="0.2" layer="21" curve="-90"/>
<wire x1="3.175" y1="-3.175" x2="-3.175" y2="-3.175" width="0.2" layer="21" curve="-90"/>
<smd name="1" x="-4.4" y="0" dx="6" dy="5.5" layer="1"/>
<smd name="2" x="4.4" y="0" dx="6" dy="5.5" layer="1"/>
<text x="0.025" y="-1.985" size="1" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="0" y="0" size="1.5" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<wire x1="-6" y1="6" x2="6" y2="6" width="0.2" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="3" width="0.2" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="3" width="0.2" layer="21"/>
<wire x1="-6" y1="-3" x2="-6" y2="-6" width="0.2" layer="21"/>
<wire x1="-6" y1="-6" x2="6" y2="-6" width="0.2" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="-3" width="0.2" layer="21"/>
<wire x1="-5.5" y1="4.5" x2="-4.5" y2="5.5" width="0.2" layer="21"/>
<wire x1="4.5" y1="5.5" x2="5.5" y2="4.5" width="0.2" layer="21"/>
<wire x1="5.5" y1="-4.5" x2="4.5" y2="-5.5" width="0.2" layer="21"/>
<wire x1="-5.5" y1="-4.5" x2="-4.5" y2="-5.5" width="0.2" layer="21"/>
<wire x1="-6" y1="-3" x2="-7.7" y2="-3" width="0.2" layer="21"/>
<wire x1="-7.7" y1="-3" x2="-7.7" y2="3" width="0.2" layer="21"/>
<wire x1="-7.7" y1="3" x2="-6" y2="3" width="0.2" layer="21"/>
<wire x1="6" y1="-3" x2="7.7" y2="-3" width="0.2" layer="21"/>
<wire x1="7.7" y1="-3" x2="7.7" y2="3" width="0.2" layer="21"/>
<wire x1="7.7" y1="3" x2="6" y2="3" width="0.2" layer="21"/>
<wire x1="-4" y1="3" x2="-3" y2="4" width="0.2" layer="21" curve="-180"/>
<wire x1="3" y1="4" x2="4" y2="3" width="0.2" layer="21" curve="-180"/>
<wire x1="4" y1="-3" x2="3" y2="-4" width="0.2" layer="21" curve="-180"/>
<wire x1="-3" y1="-4" x2="-4" y2="-3" width="0.2" layer="21" curve="-180"/>
</package>
<package name="COIL_V3">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<text x="-0.635" y="1.905" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="0" y="0" size="0.4" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="-0.275" y2="0.25" width="0.127" layer="51"/>
<wire x1="-0.275" y1="0.25" x2="0.275" y2="0.25" width="0.127" layer="51"/>
<wire x1="0.275" y1="0.25" x2="0.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.127" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="0.275" y2="-0.25" width="0.127" layer="51"/>
<wire x1="0.275" y1="-0.25" x2="-0.275" y2="-0.25" width="0.127" layer="51"/>
<wire x1="-0.275" y1="-0.25" x2="-0.5" y2="-0.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.127" layer="51"/>
<wire x1="0" y1="-0.3" x2="0" y2="0.3" width="0.15" layer="21"/>
</package>
<package name="DR1030">
<description>&lt;b&gt;DR1030 Series Low Profile Power Inductors&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.cooperbussmann.com/pdf/c73e479f-f867-4c64-ae26-9c1f23814163.pdf"&gt; Data sheet&lt;/a&gt;</description>
<wire x1="5.05" y1="3.6057" x2="5.05" y2="-4.2986" width="0.2032" layer="21"/>
<wire x1="5.05" y1="-4.2986" x2="4.1986" y2="-5.15" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.1986" y1="-5.15" x2="-3.5057" y2="-5.15" width="0.2032" layer="51"/>
<wire x1="-3.5057" y1="-5.15" x2="-5.05" y2="-3.6057" width="0.2032" layer="21"/>
<wire x1="-5.05" y1="-3.6057" x2="-5.05" y2="4.2308" width="0.2032" layer="21"/>
<wire x1="-5.05" y1="4.2308" x2="-4.1308" y2="5.15" width="0.2032" layer="21"/>
<wire x1="-4.1308" y1="5.15" x2="3.5057" y2="5.15" width="0.2032" layer="51"/>
<wire x1="3.5057" y1="5.15" x2="5.05" y2="3.6057" width="0.2032" layer="21"/>
<circle x="4.6" y="4.7" radius="0.5099" width="0" layer="21"/>
<circle x="-4.6" y="-4.7" radius="0.5099" width="0" layer="21"/>
<circle x="0" y="0" radius="3.3" width="0.2032" layer="21"/>
<smd name="1" x="0" y="4.7" dx="3.3" dy="2.2" layer="1"/>
<smd name="2" x="0" y="-4.7" dx="3.3" dy="2.2" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="0" size="0.8" layer="27" font="vector" align="center">&gt;VALUE</text>
</package>
<package name="DR74">
<description>&lt;b&gt;High Power Density, High Efficiency, Shielded Inductors&lt;/b&gt;&lt;p&gt;
Source: coiltronics_dr_series.pdf</description>
<wire x1="-3.7" y1="3.7" x2="3.7" y2="3.7" width="0.2032" layer="21"/>
<wire x1="3.7" y1="3.7" x2="3.7" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="3.7" y1="-3.7" x2="-3.7" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-3.7" x2="-3.7" y2="3.7" width="0.2032" layer="21"/>
<wire x1="0" y1="2.975" x2="-2.4" y2="1.75" width="0.2032" layer="21" curve="53.855356"/>
<wire x1="0" y1="2.975" x2="2.4" y2="1.75" width="0.2032" layer="21" curve="-53.855356"/>
<wire x1="0" y1="-2.975" x2="2.4" y2="-1.75" width="0.2032" layer="21" curve="53.855356"/>
<wire x1="0" y1="-2.975" x2="-2.4" y2="-1.75" width="0.2032" layer="21" curve="-53.855356"/>
<circle x="0" y="0" radius="2.975" width="0.2032" layer="51"/>
<smd name="1" x="-3" y="0" dx="2.5" dy="3.25" layer="1"/>
<smd name="2" x="3" y="0" dx="2.5" dy="3.25" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-3.5" y="-5.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8215" y1="1.984" x2="-1.7215" y2="2.559" layer="21" rot="R45"/>
<rectangle x1="-2.8215" y1="-2.559" x2="-1.7215" y2="-1.984" layer="21" rot="R135"/>
<rectangle x1="1.7215" y1="-2.559" x2="2.8215" y2="-1.984" layer="21" rot="R225"/>
<rectangle x1="1.7215" y1="1.984" x2="2.8215" y2="2.559" layer="21" rot="R315"/>
</package>
<package name="ED16">
<description>&lt;B&gt;INDUCTOR&lt;/B&gt;</description>
<wire x1="-4.445" y1="0.508" x2="-7.62" y2="1.143" width="0.6096" layer="51"/>
<wire x1="3.429" y1="2.794" x2="6.096" y2="4.826" width="0.6096" layer="21"/>
<wire x1="4.318" y1="1.397" x2="7.366" y2="2.54" width="0.6096" layer="21"/>
<wire x1="3.937" y1="2.159" x2="6.858" y2="3.683" width="0.6096" layer="21"/>
<wire x1="4.445" y1="0.635" x2="7.62" y2="1.27" width="0.6096" layer="21"/>
<wire x1="2.921" y1="3.302" x2="5.207" y2="5.715" width="0.6096" layer="21"/>
<wire x1="2.413" y1="3.683" x2="4.191" y2="6.477" width="0.6096" layer="21"/>
<wire x1="1.778" y1="4.064" x2="3.048" y2="7.112" width="0.6096" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-7.239" y2="2.54" width="0.6096" layer="51"/>
<wire x1="-4.064" y1="-0.127" x2="-7.62" y2="-1.143" width="0.6096" layer="51"/>
<wire x1="-4.064" y1="-1.905" x2="-6.858" y2="-3.683" width="0.6096" layer="21"/>
<wire x1="4.445" y1="-0.127" x2="7.747" y2="-0.127" width="0.6096" layer="21"/>
<wire x1="4.445" y1="-0.889" x2="7.62" y2="-1.651" width="0.6096" layer="21"/>
<wire x1="4.191" y1="-1.651" x2="7.112" y2="-3.048" width="0.6096" layer="21"/>
<wire x1="3.81" y1="-2.413" x2="6.477" y2="-4.191" width="0.6096" layer="21"/>
<wire x1="3.302" y1="-3.048" x2="5.715" y2="-5.207" width="0.6096" layer="21"/>
<wire x1="2.667" y1="-3.556" x2="4.826" y2="-6.096" width="0.6096" layer="21"/>
<wire x1="2.032" y1="-3.937" x2="3.683" y2="-6.858" width="0.6096" layer="21"/>
<wire x1="1.27" y1="-4.318" x2="2.413" y2="-7.366" width="0.6096" layer="21"/>
<wire x1="0.381" y1="-4.445" x2="0.889" y2="-7.62" width="0.6096" layer="21"/>
<wire x1="-0.381" y1="-4.445" x2="-0.508" y2="-7.747" width="0.6096" layer="21"/>
<wire x1="-1.143" y1="-4.318" x2="-1.778" y2="-7.493" width="0.6096" layer="21"/>
<wire x1="-1.778" y1="-4.064" x2="-3.048" y2="-7.112" width="0.6096" layer="21"/>
<wire x1="-2.54" y1="-3.683" x2="-4.318" y2="-6.477" width="0.6096" layer="21"/>
<wire x1="-3.175" y1="-3.175" x2="-5.334" y2="-5.715" width="0.6096" layer="21"/>
<wire x1="-3.683" y1="-2.54" x2="-6.223" y2="-4.699" width="0.6096" layer="21"/>
<wire x1="-4.318" y1="-1.016" x2="-7.366" y2="-2.413" width="0.6096" layer="51"/>
<wire x1="1.143" y1="4.318" x2="1.778" y2="7.493" width="0.6096" layer="21"/>
<wire x1="0.381" y1="4.445" x2="0.508" y2="7.747" width="0.6096" layer="21"/>
<wire x1="-0.381" y1="4.445" x2="-0.889" y2="7.62" width="0.6096" layer="21"/>
<wire x1="-1.143" y1="4.318" x2="-2.286" y2="7.366" width="0.6096" layer="21"/>
<wire x1="-1.778" y1="4.064" x2="-3.302" y2="6.985" width="0.6096" layer="21"/>
<wire x1="-2.413" y1="3.683" x2="-4.318" y2="6.35" width="0.6096" layer="21"/>
<wire x1="-3.048" y1="3.175" x2="-5.334" y2="5.588" width="0.6096" layer="21"/>
<wire x1="-3.556" y1="2.54" x2="-6.223" y2="4.572" width="0.6096" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-6.858" y2="3.429" width="0.6096" layer="21"/>
<wire x1="-4.4519" y1="-1.5038" x2="-4.4519" y2="1.5038" width="0.1524" layer="21" curve="322.67126"/>
<wire x1="-7.338" y1="-1.5161" x2="-7.338" y2="1.5161" width="0.1524" layer="21" curve="336.652815"/>
<wire x1="-7.338" y1="1.5161" x2="-7.338" y2="-1.5161" width="0.1524" layer="51" curve="23.347185"/>
<wire x1="-4.4519" y1="1.5038" x2="-4.4519" y2="-1.5038" width="0.1524" layer="51" curve="37.32874"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-8.255" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="8.255" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.286" y="-0.635" size="1.27" layer="21" ratio="10">DÝ0,6</text>
</package>
<package name="SDR0403">
<smd name="1" x="0" y="1.5" dx="4" dy="1" layer="1" stop="no" cream="no"/>
<smd name="2" x="0" y="-1.5" dx="4" dy="1" layer="1" stop="no" cream="no"/>
<polygon width="0.254" layer="1">
<vertex x="-2.123" y="0.877"/>
<vertex x="-2.123" y="1.923" curve="-90"/>
<vertex x="-1.623" y="2.423"/>
<vertex x="1.623" y="2.423" curve="-90"/>
<vertex x="2.123" y="1.923"/>
<vertex x="2.123" y="0.877"/>
</polygon>
<polygon width="0.254" layer="1">
<vertex x="2.123" y="-0.877"/>
<vertex x="2.123" y="-1.923" curve="-90"/>
<vertex x="1.623" y="-2.423"/>
<vertex x="-1.623" y="-2.423" curve="-90"/>
<vertex x="-2.123" y="-1.923"/>
<vertex x="-2.123" y="-0.877"/>
</polygon>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.1" layer="21"/>
<wire x1="2" y1="-0.75" x2="2" y2="0.75" width="0.1" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.1" layer="21" curve="-138.88791"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.1" layer="21" curve="138.88791"/>
<text x="-1" y="0.25" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1" y="-0.5" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.159" y1="0.889" x2="2.159" y2="2.413" layer="29"/>
<rectangle x1="-2.159" y1="-2.413" x2="2.159" y2="-0.889" layer="29"/>
</package>
<package name="UNI-DR127-IHLP2525">
<description>&lt;b&gt;High Power Density, High Efficiency, Shielded Inductors&lt;/b&gt;&lt;p&gt;
Source: coiltronics_dr_series.pdf</description>
<wire x1="-6.5" y1="6.5" x2="6.5" y2="6.5" width="0.2" layer="21"/>
<wire x1="6.525" y1="-6.5" x2="6.5" y2="-6.5" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="-6.5" y2="-6.5" width="0.2032" layer="21"/>
<wire x1="-4.5254" y1="3.783" x2="-3.8006" y2="4.5431" width="1.016" layer="21" curve="-202.065586" cap="flat"/>
<wire x1="-3.783" y1="-4.5254" x2="-4.5431" y2="-3.8006" width="1.016" layer="21" curve="-202.065586" cap="flat"/>
<wire x1="4.5254" y1="-3.783" x2="3.8006" y2="-4.5431" width="1.016" layer="21" curve="-202.065586" cap="flat"/>
<wire x1="3.783" y1="4.5254" x2="4.5431" y2="3.8006" width="1.016" layer="21" curve="-202.065586" cap="flat"/>
<wire x1="0" y1="5.9" x2="4.9" y2="3.275" width="0.2032" layer="21" curve="-56.209779"/>
<wire x1="0" y1="5.9" x2="-4.95" y2="3.225" width="0.2032" layer="21" curve="56.95663"/>
<wire x1="0" y1="-5.9" x2="-4.9" y2="-3.275" width="0.2032" layer="21" curve="-56.209779"/>
<wire x1="0" y1="-5.9" x2="4.95" y2="-3.225" width="0.2032" layer="21" curve="56.95663"/>
<smd name="1" x="-5" y="0" dx="6.5" dy="5.5" layer="1"/>
<smd name="2" x="5" y="0" dx="6.5" dy="5.5" layer="1"/>
<text x="0" y="0" size="1.5" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="-3" y="-4.5" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-6.5" y1="6.5" x2="6.525" y2="6.5" width="0.2032" layer="51"/>
<wire x1="6.525" y1="6.5" x2="6.525" y2="-6.5" width="0.2032" layer="51"/>
<wire x1="6.525" y1="-6.5" x2="-6.5" y2="-6.5" width="0.2032" layer="51"/>
<wire x1="-4.5254" y1="3.783" x2="-3.8006" y2="4.5431" width="1.016" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="-3.783" y1="-4.5254" x2="-4.5431" y2="-3.8006" width="1.016" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="4.5254" y1="-3.783" x2="3.8006" y2="-4.5431" width="1.016" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="3.783" y1="4.5254" x2="4.5431" y2="3.8006" width="1.016" layer="51" curve="-202.065586" cap="flat"/>
<wire x1="0" y1="5.9" x2="4.9" y2="3.275" width="0.2032" layer="51" curve="-56.209779"/>
<wire x1="0" y1="5.9" x2="-4.95" y2="3.225" width="0.2032" layer="51" curve="56.95663"/>
<wire x1="0" y1="-5.9" x2="-4.9" y2="-3.275" width="0.2032" layer="51" curve="-56.209779"/>
<wire x1="0" y1="-5.9" x2="4.95" y2="-3.225" width="0.2032" layer="51" curve="56.95663"/>
<circle x="0" y="0" radius="5.9" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="6.5" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="6.5" x2="-6.5" y2="3.1" width="0.2" layer="21"/>
<wire x1="-6.5" y1="3.1" x2="-8.6" y2="3.1" width="0.2" layer="21"/>
<wire x1="-8.6" y1="3.1" x2="-8.6" y2="-3.1" width="0.2" layer="21"/>
<wire x1="-8.6" y1="-3.1" x2="-6.5" y2="-3.1" width="0.2" layer="21"/>
<wire x1="-6.5" y1="-3.1" x2="-6.5" y2="-6.5" width="0.2" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="6.5" y2="-3.1" width="0.2" layer="21"/>
<wire x1="6.5" y1="-3.1" x2="8.6" y2="-3.1" width="0.2" layer="21"/>
<wire x1="8.6" y1="-3.1" x2="8.6" y2="3.1" width="0.2" layer="21"/>
<wire x1="8.6" y1="3.1" x2="6.5" y2="3.1" width="0.2" layer="21"/>
<wire x1="6.5" y1="3.1" x2="6.5" y2="6.5" width="0.2" layer="21"/>
<rectangle x1="-7.5" y1="-3" x2="-6.5" y2="3" layer="51"/>
<rectangle x1="6.5" y1="-3" x2="7.5" y2="3" layer="51"/>
</package>
<package name="RCH114">
<circle x="0" y="0" radius="5" width="0.1" layer="51"/>
<circle x="0" y="0" radius="5.3" width="0.2" layer="21"/>
<pad name="1" x="0" y="-2.5" drill="0.9" diameter="1.5"/>
<pad name="2" x="0" y="2.5" drill="0.9" diameter="1.5"/>
</package>
<package name="L0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-1" y="0" dx="1.3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1" layer="1" rot="R90"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-1" size="0.4" layer="27" font="vector" align="center">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1.8" y1="0.9" x2="-0.4" y2="0.9" width="0.2" layer="21"/>
<wire x1="0.4" y1="0.9" x2="1.8" y2="0.9" width="0.2" layer="21"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.2" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="0.4" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-0.4" y1="-0.9" x2="-1.8" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.2" layer="21"/>
</package>
<package name="L1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="2" x="1.5" y="0" dx="1" dy="1.6" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="1" dy="1.6" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.016" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="-1.1" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-0.8" x2="1.1" y2="-0.8" width="0.127" layer="51"/>
<wire x1="1.1" y1="-0.8" x2="1.6" y2="-0.8" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.8" x2="1.1" y2="0.8" width="0.127" layer="51"/>
<wire x1="1.1" y1="0.8" x2="-1.1" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.1" y1="0.8" x2="-1.6" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.1" y1="0.8" x2="-1.1" y2="-0.8" width="0.127" layer="51"/>
<wire x1="1.1" y1="0.8" x2="1.1" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.9" y1="1.1" x2="-2.2" y2="1.1" width="0.2" layer="21"/>
<wire x1="-2.2" y1="1.1" x2="-2.2" y2="0.4" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-0.4" x2="-2.2" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-2.2" y1="-1.1" x2="-0.9" y2="-1.1" width="0.2" layer="21"/>
<wire x1="0.9" y1="-1.1" x2="2.2" y2="-1.1" width="0.2" layer="21"/>
<wire x1="2.2" y1="-1.1" x2="2.2" y2="-0.4" width="0.2" layer="21"/>
<wire x1="2.2" y1="0.4" x2="2.2" y2="1.1" width="0.2" layer="21"/>
<wire x1="2.2" y1="1.1" x2="1" y2="1.1" width="0.2" layer="21"/>
</package>
<package name="L0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.9" layer="1"/>
<text x="0" y="0" size="0.7" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="0" y="-1" size="0.8" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="-0.5" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0.4" x2="0.5" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.5" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="0.5" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.5" y1="-0.4" x2="-0.5" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0.4" x2="-0.5" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.5" y1="0.4" x2="0.5" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.7" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.7" x2="1.4" y2="0.7" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.7" x2="-0.5" y2="-0.7" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.7" x2="0.5" y2="-0.7" width="0.2" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-0.5" y2="0.7" width="0.2" layer="21"/>
<wire x1="0.5" y1="0.7" x2="1.4" y2="0.7" width="0.2" layer="21"/>
</package>
<package name="IHLP2525">
<smd name="1" x="-3" y="0" dx="3" dy="3.5" layer="1"/>
<smd name="2" x="3" y="0" dx="3" dy="3.5" layer="1"/>
<text x="0" y="0" size="1.2" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-3" y1="3.4" x2="3" y2="3.4" width="0.127" layer="51"/>
<wire x1="3.5" y1="3" x2="3.5" y2="-3" width="0.127" layer="51"/>
<wire x1="3" y1="-3.4" x2="-3" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-2.9" x2="-3.5" y2="3" width="0.127" layer="51"/>
<wire x1="-3.5" y1="3" x2="-3" y2="3.4" width="0.127" layer="51" curve="-90"/>
<wire x1="3" y1="3.4" x2="3.5" y2="3" width="0.127" layer="51" curve="-90"/>
<wire x1="-3" y1="-3.4" x2="-3.5" y2="-2.9" width="0.127" layer="51" curve="-90"/>
<wire x1="3.5" y1="-3" x2="3" y2="-3.4" width="0.127" layer="51" curve="-90"/>
<wire x1="-4.8" y1="2" x2="-3.9" y2="2" width="0.2" layer="21"/>
<wire x1="-3.9" y1="2" x2="-3.9" y2="3" width="0.2" layer="21"/>
<wire x1="-3.9" y1="3" x2="-3.1" y2="3.8" width="0.2" layer="21" curve="-90"/>
<wire x1="-3.1" y1="3.8" x2="3.1" y2="3.8" width="0.2" layer="21"/>
<wire x1="3.1" y1="3.8" x2="3.9" y2="3" width="0.2" layer="21" curve="-90"/>
<wire x1="3.9" y1="3" x2="3.9" y2="2" width="0.2" layer="21"/>
<wire x1="3.9" y1="2" x2="4.8" y2="2" width="0.2" layer="21"/>
<wire x1="4.8" y1="2" x2="4.8" y2="-2" width="0.2" layer="21"/>
<wire x1="4.8" y1="-2" x2="3.9" y2="-2" width="0.2" layer="21"/>
<wire x1="3.9" y1="-2" x2="3.9" y2="-3" width="0.2" layer="21"/>
<wire x1="3.9" y1="-3" x2="3.1" y2="-3.8" width="0.2" layer="21" curve="-90"/>
<wire x1="3.1" y1="-3.8" x2="-3.1" y2="-3.8" width="0.2" layer="21"/>
<wire x1="-3.1" y1="-3.8" x2="-3.9" y2="-2.9" width="0.2" layer="21" curve="-90"/>
<wire x1="-3.9" y1="-2.9" x2="-3.9" y2="-2" width="0.2" layer="21"/>
<wire x1="-3.9" y1="-2" x2="-4.8" y2="-2" width="0.2" layer="21"/>
<wire x1="-4.8" y1="-2" x2="-4.8" y2="2" width="0.2" layer="21"/>
<rectangle x1="-3.81" y1="-1.27" x2="-2.54" y2="1.27" layer="51"/>
<rectangle x1="2.54" y1="-1.27" x2="3.81" y2="1.27" layer="51"/>
</package>
<package name="DR127">
<description>&lt;b&gt;High Power Density, High Efficiency, Shielded Inductors&lt;/b&gt;&lt;p&gt;
Source: coiltronics_dr_series.pdf</description>
<wire x1="-6.5" y1="6.5" x2="6.525" y2="6.5" width="0.2032" layer="21"/>
<wire x1="6.525" y1="6.5" x2="6.525" y2="-6.5" width="0.2032" layer="21"/>
<wire x1="6.525" y1="-6.5" x2="-6.5" y2="-6.5" width="0.2032" layer="21"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="6.5" width="0.2032" layer="21"/>
<wire x1="-4.5254" y1="3.783" x2="-3.8006" y2="4.5431" width="1.016" layer="21" curve="-202.065586" cap="flat"/>
<wire x1="-3.783" y1="-4.5254" x2="-4.5431" y2="-3.8006" width="1.016" layer="21" curve="-202.065586" cap="flat"/>
<wire x1="4.5254" y1="-3.783" x2="3.8006" y2="-4.5431" width="1.016" layer="21" curve="-202.065586" cap="flat"/>
<wire x1="3.783" y1="4.5254" x2="4.5431" y2="3.8006" width="1.016" layer="21" curve="-202.065586" cap="flat"/>
<wire x1="0" y1="5.9" x2="4.9" y2="3.275" width="0.2032" layer="21" curve="-56.209779"/>
<wire x1="0" y1="5.9" x2="-4.95" y2="3.225" width="0.2032" layer="21" curve="56.95663"/>
<wire x1="0" y1="-5.9" x2="-4.9" y2="-3.275" width="0.2032" layer="21" curve="-56.209779"/>
<wire x1="0" y1="-5.9" x2="4.95" y2="-3.225" width="0.2032" layer="21" curve="56.95663"/>
<circle x="0" y="0" radius="5.9" width="0.2032" layer="51"/>
<smd name="1" x="-5" y="0" dx="3.9" dy="5.5" layer="1"/>
<smd name="2" x="5" y="0" dx="3.9" dy="5.5" layer="1"/>
<text x="0" y="0" size="1.5" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="-3" y="-4.5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RCH110">
<circle x="0" y="0" radius="5" width="0.1" layer="51"/>
<circle x="0" y="0" radius="5.3" width="0.2" layer="21"/>
<wire x1="-0.4" y1="-4.9" x2="-0.4" y2="-3.9" width="0.1" layer="51"/>
<wire x1="-0.4" y1="-3.9" x2="0" y2="-3.5" width="0.1" layer="51" curve="-90"/>
<wire x1="0" y1="-3.5" x2="0.4" y2="-3.9" width="0.1" layer="51" curve="-90"/>
<wire x1="0.4" y1="-3.9" x2="0.4" y2="-4.9" width="0.1" layer="51"/>
<pad name="1" x="2" y="-2.5" drill="0.9" diameter="1.5"/>
<pad name="2" x="2" y="2.5" drill="0.9" diameter="1.5"/>
<pad name="3" x="-2" y="2.5" drill="0.9" diameter="1.5"/>
<pad name="4" x="-2" y="-2.5" drill="0.9" diameter="1.5"/>
<wire x1="0.4" y1="4.9" x2="0.4" y2="3.9" width="0.1" layer="51"/>
<wire x1="0.4" y1="3.9" x2="0" y2="3.5" width="0.1" layer="51" curve="-90"/>
<wire x1="0" y1="3.5" x2="-0.4" y2="3.9" width="0.1" layer="51" curve="-90"/>
<wire x1="-0.4" y1="3.9" x2="-0.4" y2="4.9" width="0.1" layer="51"/>
<circle x="0.7" y="-1.9" radius="0.4242625" width="0.1" layer="51"/>
</package>
<package name="RCL_L3216C">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.27" y1="0.896" x2="1.27" y2="0.896" width="0.1016" layer="51"/>
<wire x1="-1.27" y1="-0.883" x2="1.27" y2="-0.883" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="0.896" x2="0.762" y2="0.896" width="0.254" layer="21"/>
<wire x1="-0.762" y1="-0.883" x2="0.762" y2="-0.883" width="0.254" layer="21"/>
<rectangle x1="-1.7526" y1="-0.9525" x2="-1.2525" y2="0.9474" layer="51"/>
<rectangle x1="1.2446" y1="-0.9525" x2="1.7447" y2="0.9474" layer="51"/>
<rectangle x1="-0.4001" y1="-0.5999" x2="0.4001" y2="0.5999" layer="35"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.524" y="1.143" size="1.27" layer="25" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-2.413" size="1.27" layer="27" ratio="20">&gt;VALUE</text>
</package>
<package name="RCL_L3225P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
precision wire wound</description>
<circle x="0" y="0" radius="0.7117" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.676" y1="0.845" x2="1.676" y2="0.845" width="0.1524" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-1.676" y1="0.838" x2="-1.676" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="-1.168" y1="0.838" x2="-1.168" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.168" y1="0.838" x2="1.168" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.676" y1="0.838" x2="1.676" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.676" y1="-0.845" x2="-1.676" y2="-0.845" width="0.1524" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.8" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.8" dy="2" layer="1"/>
<text x="-1.397" y="1.27" size="1.27" layer="25" ratio="20">&gt;NAME</text>
<text x="-1.397" y="-2.54" size="1.27" layer="27" ratio="20">&gt;VALUE</text>
</package>
<package name="RCL_L5038P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt; &lt;p&gt;
precision wire wound</description>
<circle x="0" y="0" radius="1.4732" width="0.1524" layer="51"/>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-2.286" y1="1.853" x2="2.311" y2="1.853" width="0.254" layer="21"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.286" y1="-1.856" x2="2.311" y2="-1.856" width="0.254" layer="21"/>
<wire x1="2.389" y1="-1.27" x2="2.389" y2="1.27" width="0.1016" layer="51"/>
<wire x1="-2.386" y1="-1.27" x2="-2.386" y2="1.27" width="0.1016" layer="51"/>
<wire x1="1.602" y1="-1.854" x2="1.602" y2="1.854" width="0.1016" layer="51"/>
<wire x1="-1.624" y1="-1.854" x2="-1.624" y2="1.854" width="0.1016" layer="51"/>
<wire x1="-2.31" y1="-1.854" x2="-2.31" y2="1.854" width="0.1016" layer="51"/>
<wire x1="2.313" y1="-1.854" x2="2.313" y2="1.854" width="0.1016" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<smd name="1" x="-2.2" y="0" dx="1.4" dy="2.8" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.4" dy="2.8" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-2.159" y="-3.429" size="1.27" layer="27" ratio="20">&gt;VALUE</text>
</package>
<package name="EC24">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.2" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.2" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.2" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.2" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.2" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.2" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.2" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.2" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.2" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.2" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.2" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.2" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.2" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.2" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.2" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.2" layer="21"/>
<pad name="1" x="-5" y="0" drill="0.8" diameter="1.4" shape="octagon"/>
<pad name="2" x="5" y="0" drill="0.8" diameter="1.4" shape="octagon"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="L_1">
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-0.889" y1="0" x2="0.889" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="0" y="2.54" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="%1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="%2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L_1" x="0" y="0"/>
</gates>
<devices>
<device name="CDRH125" package="CDRH125">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2" package="COIL_V2">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3" package="COIL_V3">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0" package="EC24">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L0805" package="L0805">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L1206" package="L1206">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L0402" package="L0402">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L0603" package="L0603">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DR1030" package="DR1030">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DR74" package="DR74">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ED16" package="ED16">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IHLP2525" package="IHLP2525">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SDR0403" package="SDR0403">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DR127" package="DR127">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="UNI-DR127-IHLP2525">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RCH110" package="RCH110">
<connects>
<connect gate="G$1" pin="%1" pad="1 2"/>
<connect gate="G$1" pin="%2" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RCH114" package="RCH114">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-L3216C" package="RCL_L3216C">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-L3225P" package="RCL_L3225P">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-L5038P" package="RCL_L5038P">
<connects>
<connect gate="G$1" pin="%1" pad="1"/>
<connect gate="G$1" pin="%2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_modules">
<packages>
<package name="NOKIA-1202_LCD">
<wire x1="-17.78" y1="17.78" x2="17.78" y2="17.78" width="0.127" layer="21"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="-13.97" width="0.127" layer="21"/>
<wire x1="17.78" y1="-13.97" x2="-17.78" y2="-13.97" width="0.127" layer="21"/>
<wire x1="-17.78" y1="-13.97" x2="-17.78" y2="17.78" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-12.7" x2="16.51" y2="-12.7" width="0.127" layer="51"/>
<wire x1="16.51" y1="-12.7" x2="16.51" y2="10.16" width="0.127" layer="51"/>
<wire x1="16.51" y1="10.16" x2="-16.51" y2="10.16" width="0.127" layer="51"/>
<wire x1="-16.51" y1="10.16" x2="-16.51" y2="-12.7" width="0.127" layer="51"/>
<wire x1="-10.03" y1="19.92" x2="-1.54" y2="19.92" width="0.127" layer="21"/>
<wire x1="-1.54" y1="19.92" x2="-1.54" y2="24.13" width="0.127" layer="21"/>
<wire x1="-1.54" y1="24.13" x2="-10.03" y2="24.13" width="0.127" layer="21"/>
<wire x1="-10.03" y1="24.13" x2="-10.03" y2="19.92" width="0.127" layer="21"/>
<smd name="0" x="-9.09" y="22.044" dx="0.32" dy="3" layer="1"/>
<smd name="1" x="-8.49" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="2" x="-7.89" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="3" x="-7.29" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="4" x="-6.69" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="5" x="-6.09" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="6" x="-5.49" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="7" x="-4.89" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="8" x="-4.29" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="9" x="-3.69" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="10" x="-3.09" y="22.044" dx="0.32" dy="2.8" layer="1"/>
<smd name="11" x="-2.49" y="22.044" dx="0.32" dy="3" layer="1"/>
<text x="0" y="0" size="2" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-14.82" y="-10.76" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="NOKIA-1202_LCD-BOTTOM-PAD">
<wire x1="-17.78" y1="17.78" x2="17.78" y2="17.78" width="0.127" layer="21"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="-13.97" width="0.127" layer="21"/>
<wire x1="17.78" y1="-13.97" x2="-17.78" y2="-13.97" width="0.127" layer="21"/>
<wire x1="-17.78" y1="-13.97" x2="-17.78" y2="17.78" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-12.7" x2="16.51" y2="-12.7" width="0.127" layer="51"/>
<wire x1="16.51" y1="-12.7" x2="16.51" y2="10.16" width="0.127" layer="51"/>
<wire x1="16.51" y1="10.16" x2="-16.51" y2="10.16" width="0.127" layer="51"/>
<wire x1="-16.51" y1="10.16" x2="-16.51" y2="-12.7" width="0.127" layer="51"/>
<wire x1="-10.03" y1="17.08" x2="-1.54" y2="17.08" width="0.127" layer="22"/>
<wire x1="-1.54" y1="17.08" x2="-1.54" y2="12.87" width="0.127" layer="22"/>
<wire x1="-1.54" y1="12.87" x2="-10.03" y2="12.87" width="0.127" layer="22"/>
<wire x1="-10.03" y1="12.87" x2="-10.03" y2="17.08" width="0.127" layer="22"/>
<smd name="0" x="-9.09" y="14.956" dx="0.32" dy="3" layer="16"/>
<smd name="1" x="-8.49" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="2" x="-7.89" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="3" x="-7.29" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="4" x="-6.69" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="5" x="-6.09" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="6" x="-5.49" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="7" x="-4.89" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="8" x="-4.29" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="9" x="-3.69" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="10" x="-3.09" y="14.956" dx="0.32" dy="2.8" layer="16"/>
<smd name="11" x="-2.49" y="14.956" dx="0.32" dy="3" layer="16"/>
<text x="0" y="0" size="2" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-14.82" y="-10.76" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="NRF24L01_SMD">
<wire x1="0" y1="0" x2="0" y2="12.2" width="0.2" layer="21"/>
<wire x1="0" y1="0.16" x2="0" y2="17.78" width="0.1" layer="51"/>
<wire x1="0" y1="17.78" x2="11.43" y2="17.78" width="0.1" layer="51"/>
<wire x1="11.43" y1="17.78" x2="11.43" y2="0.16" width="0.1" layer="51"/>
<wire x1="11.4" y1="12.2" x2="11.4" y2="0" width="0.2" layer="21"/>
<wire x1="0" y1="12.2" x2="11.4" y2="12.2" width="0.2" layer="21"/>
<wire x1="0" y1="0" x2="0.6" y2="0" width="0.2" layer="21"/>
<wire x1="11.4" y1="0" x2="10.8" y2="0" width="0.2" layer="21"/>
<wire x1="1.27" y1="12.16" x2="1.27" y2="16.51" width="0.1" layer="51"/>
<wire x1="1.27" y1="16.51" x2="2.54" y2="16.51" width="0.1" layer="51"/>
<wire x1="2.54" y1="16.51" x2="2.54" y2="12.7" width="0.1" layer="51"/>
<wire x1="2.54" y1="12.7" x2="3.81" y2="12.7" width="0.1" layer="51"/>
<wire x1="3.81" y1="12.7" x2="3.81" y2="16.51" width="0.1" layer="51"/>
<wire x1="3.81" y1="16.51" x2="5.08" y2="16.51" width="0.1" layer="51"/>
<wire x1="5.08" y1="16.51" x2="5.08" y2="12.7" width="0.1" layer="51"/>
<wire x1="5.08" y1="12.7" x2="6.35" y2="12.7" width="0.1" layer="51"/>
<wire x1="6.35" y1="12.7" x2="6.35" y2="16.51" width="0.1" layer="51"/>
<wire x1="6.35" y1="16.51" x2="7.62" y2="16.51" width="0.1" layer="51"/>
<wire x1="7.62" y1="16.51" x2="7.62" y2="12.7" width="0.1" layer="51"/>
<wire x1="7.62" y1="12.7" x2="8.89" y2="12.7" width="0.1" layer="51"/>
<wire x1="8.89" y1="12.7" x2="8.89" y2="16.51" width="0.1" layer="51"/>
<wire x1="8.89" y1="16.51" x2="10.16" y2="16.51" width="0.1" layer="51"/>
<wire x1="10.16" y1="16.51" x2="10.16" y2="12.7" width="0.1" layer="51"/>
<smd name="1" x="1.27" y="0.635" dx="1.8" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="0.635" dx="1.8" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="3.81" y="0.635" dx="1.8" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="5.08" y="0.635" dx="1.8" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="6.35" y="0.635" dx="1.8" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="7.62" y="0.635" dx="1.8" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="8.89" y="0.635" dx="1.8" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="10.16" y="0.635" dx="1.8" dy="0.6" layer="1" rot="R90"/>
<text x="6" y="6" size="1.5" layer="25" font="vector" ratio="12" rot="R180" align="center">&gt;NAME</text>
<text x="-0.635" y="8.89" size="0.8128" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<smd name="9" x="10.1" y="10.9" dx="2" dy="2" layer="1"/>
<polygon width="0.1" layer="51">
<vertex x="0.6" y="1.8"/>
<vertex x="10.8" y="1.8"/>
<vertex x="10.8" y="0.2"/>
<vertex x="11.2" y="0.2"/>
<vertex x="11.2" y="9.6"/>
<vertex x="8.8" y="9.6"/>
<vertex x="8.8" y="12"/>
<vertex x="0.2" y="12"/>
<vertex x="0.2" y="0.2"/>
<vertex x="0.6" y="0.2"/>
</polygon>
<wire x1="1.9" y1="1.6" x2="1.9" y2="-0.3" width="0.2" layer="21"/>
<wire x1="3.2" y1="1.6" x2="3.2" y2="-0.3" width="0.2" layer="21"/>
<wire x1="4.4" y1="1.6" x2="4.4" y2="-0.3" width="0.2" layer="21"/>
<wire x1="5.7" y1="1.6" x2="5.7" y2="-0.3" width="0.2" layer="21"/>
<wire x1="7" y1="1.6" x2="7" y2="-0.3" width="0.2" layer="21"/>
<wire x1="8.3" y1="1.6" x2="8.3" y2="-0.3" width="0.2" layer="21"/>
<wire x1="9.5" y1="1.6" x2="9.5" y2="-0.3" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="NOKIA-1202_LCD">
<wire x1="0" y1="0" x2="50.8" y2="0" width="0.254" layer="94"/>
<wire x1="50.8" y1="0" x2="50.8" y2="-38.1" width="0.254" layer="94"/>
<wire x1="50.8" y1="-38.1" x2="0" y2="-38.1" width="0.254" layer="94"/>
<wire x1="0" y1="-38.1" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="!CS" x="-5.08" y="-10.16" length="middle" direction="in" function="dot"/>
<pin name="!RST" x="-5.08" y="-7.62" length="middle" direction="in" function="dot"/>
<pin name="GND@1" x="-5.08" y="-12.7" length="middle" direction="pwr" swaplevel="1"/>
<pin name="GND@2" x="-5.08" y="-25.4" length="middle" swaplevel="1"/>
<pin name="LEDA" x="-5.08" y="-30.48" length="middle" direction="pas"/>
<pin name="LEDK" x="-5.08" y="-27.94" length="middle" direction="pas"/>
<pin name="SCLK" x="-5.08" y="-17.78" length="middle" function="clk"/>
<pin name="SDIN" x="-5.08" y="-15.24" length="middle"/>
<pin name="VDD" x="-5.08" y="-22.86" length="middle"/>
<pin name="VDDI" x="-5.08" y="-20.32" length="middle"/>
<text x="0" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-40.64" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="12.7" y1="-2.54" x2="48.26" y2="-2.54" width="0.254" layer="94"/>
<wire x1="48.26" y1="-2.54" x2="48.26" y2="-35.56" width="0.254" layer="94"/>
<wire x1="48.26" y1="-35.56" x2="12.7" y2="-35.56" width="0.254" layer="94"/>
<wire x1="12.7" y1="-35.56" x2="12.7" y2="-2.54" width="0.254" layer="94"/>
<text x="30.48" y="-17.78" size="2.54" layer="94" font="vector" ratio="15" align="center">LCD 64 * 96 </text>
</symbol>
<symbol name="NRF24L01_SMD">
<wire x1="0" y1="0" x2="30.48" y2="0" width="0.254" layer="94"/>
<wire x1="30.48" y1="0" x2="30.48" y2="-22.86" width="0.254" layer="94"/>
<wire x1="30.48" y1="-22.86" x2="0" y2="-22.86" width="0.254" layer="94"/>
<wire x1="0" y1="-22.86" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="25.4" y1="-5.08" x2="25.4" y2="-7.62" width="0.254" layer="94"/>
<wire x1="25.4" y1="-7.62" x2="25.4" y2="-12.7" width="0.254" layer="94"/>
<wire x1="27.94" y1="-5.08" x2="25.4" y2="-7.62" width="0.254" layer="94"/>
<wire x1="25.4" y1="-7.62" x2="22.86" y2="-5.08" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="-5.08" y="-2.54" length="middle"/>
<pin name="GND@1" x="-5.08" y="-5.08" length="middle"/>
<pin name="CE" x="-5.08" y="-7.62" length="middle"/>
<pin name="/CS" x="-5.08" y="-10.16" length="middle"/>
<pin name="SCK" x="-5.08" y="-12.7" length="middle"/>
<pin name="MOSI" x="-5.08" y="-15.24" length="middle"/>
<pin name="MISO" x="-5.08" y="-17.78" length="middle"/>
<pin name="/IRQ" x="-5.08" y="-20.32" length="middle"/>
<pin name="GND@2" x="35.56" y="-20.32" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LCD-NOKIA-1202" prefix="LCD">
<gates>
<gate name="G$1" symbol="NOKIA-1202_LCD" x="0" y="0"/>
</gates>
<devices>
<device name="-1" package="NOKIA-1202_LCD">
<connects>
<connect gate="G$1" pin="!CS" pad="2"/>
<connect gate="G$1" pin="!RST" pad="1"/>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="8"/>
<connect gate="G$1" pin="LEDA" pad="10"/>
<connect gate="G$1" pin="LEDK" pad="9"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="SDIN" pad="4"/>
<connect gate="G$1" pin="VDD" pad="7"/>
<connect gate="G$1" pin="VDDI" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2" package="NOKIA-1202_LCD-BOTTOM-PAD">
<connects>
<connect gate="G$1" pin="!CS" pad="2"/>
<connect gate="G$1" pin="!RST" pad="1"/>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="8"/>
<connect gate="G$1" pin="LEDA" pad="10"/>
<connect gate="G$1" pin="LEDK" pad="9"/>
<connect gate="G$1" pin="SCLK" pad="5"/>
<connect gate="G$1" pin="SDIN" pad="4"/>
<connect gate="G$1" pin="VDD" pad="7"/>
<connect gate="G$1" pin="VDDI" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NRF24L01_SMD" prefix="A" uservalue="yes">
<gates>
<gate name="G$1" symbol="NRF24L01_SMD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NRF24L01_SMD">
<connects>
<connect gate="G$1" pin="/CS" pad="4"/>
<connect gate="G$1" pin="/IRQ" pad="8"/>
<connect gate="G$1" pin="CE" pad="3"/>
<connect gate="G$1" pin="GND@1" pad="2"/>
<connect gate="G$1" pin="GND@2" pad="9"/>
<connect gate="G$1" pin="MISO" pad="7"/>
<connect gate="G$1" pin="MOSI" pad="6"/>
<connect gate="G$1" pin="SCK" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="!ht3_switchers">
<packages>
<package name="SWITCH_B3F-40XX">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="1.651" x2="1.651" y2="1.651" width="0.0508" layer="21"/>
<wire x1="1.651" y1="-1.651" x2="1.651" y2="1.651" width="0.0508" layer="21"/>
<wire x1="1.651" y1="-1.651" x2="-1.651" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="-1.651" y1="1.651" x2="-1.651" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="-1.016" y1="6.096" x2="-1.016" y2="6.477" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="6.096" x2="1.016" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="6.477" x2="1.016" y2="6.477" width="0.1524" layer="21"/>
<wire x1="1.016" y1="6.477" x2="1.016" y2="6.096" width="0.1524" layer="21"/>
<wire x1="6.096" y1="5.08" x2="5.08" y2="6.096" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.096" x2="1.778" y2="6.096" width="0.1524" layer="21"/>
<wire x1="6.096" y1="5.08" x2="6.096" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.096" y1="1.143" x2="6.096" y2="3.81" width="0.1524" layer="51"/>
<wire x1="6.096" y1="1.143" x2="6.096" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.81" x2="6.096" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-6.096" x2="6.096" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-5.08" x2="6.096" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.143" x2="-6.096" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="1.143" x2="-6.096" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.81" x2="-6.096" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-3.81" x2="-6.096" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-5.08" x2="-5.08" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-6.096" x2="-1.778" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.096" x2="-1.778" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.096" x2="-6.096" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="5.08" x2="-6.096" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-6.477" x2="1.016" y2="-6.477" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-6.477" x2="-1.016" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-6.096" x2="1.016" y2="-6.477" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.096" x2="4.826" y2="6.35" width="0.1524" layer="21"/>
<wire x1="4.826" y1="6.35" x2="1.778" y2="6.35" width="0.1524" layer="21"/>
<wire x1="1.778" y1="6.35" x2="1.778" y2="6.096" width="0.1524" layer="21"/>
<wire x1="1.778" y1="6.096" x2="1.016" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="6.35" x2="-1.778" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="6.096" x2="-1.016" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="6.35" x2="-4.826" y2="6.35" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.096" x2="-4.826" y2="6.35" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-6.096" x2="1.016" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-6.096" x2="1.778" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-6.096" x2="-1.778" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-6.096" x2="-1.016" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-6.35" x2="-4.826" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-6.35" x2="-5.08" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-6.096" x2="1.778" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-6.096" x2="5.08" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-6.35" x2="1.778" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-6.096" x2="4.826" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-4.572" y2="2.54" width="0.1524" layer="51"/>
<wire x1="2.921" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="-2.54" x2="-4.572" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.921" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="2.54" x2="-4.572" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="2.54" x2="-2.921" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="0.762" x2="-5.08" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="-0.762" x2="-4.572" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="-2.54" x2="-2.921" y2="-2.54" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="3.556" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.016" width="0.0508" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.0508" layer="21"/>
<circle x="-4.572" y="2.54" radius="0.127" width="0.1524" layer="51"/>
<circle x="-4.572" y="-2.54" radius="0.127" width="0.1524" layer="51"/>
<pad name="3" x="-6.2484" y="-2.4892" drill="1.1938" shape="long"/>
<pad name="4" x="6.2484" y="-2.4892" drill="1.1938" shape="long"/>
<pad name="1" x="-6.2484" y="2.4892" drill="1.1938" shape="long"/>
<pad name="2" x="6.2484" y="2.4892" drill="1.1938" shape="long"/>
<text x="-5.08" y="6.985" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.207" y="3.302" size="1.27" layer="21" ratio="10">1</text>
<text x="3.937" y="3.302" size="1.27" layer="21" ratio="10">2</text>
<text x="-5.207" y="-4.699" size="1.27" layer="21" ratio="10">3</text>
<text x="4.064" y="-4.699" size="1.27" layer="21" ratio="10">4</text>
<rectangle x1="6.096" y1="-2.921" x2="6.604" y2="-2.032" layer="51"/>
<rectangle x1="6.096" y1="2.032" x2="6.604" y2="2.921" layer="51"/>
<rectangle x1="-6.604" y1="2.032" x2="-6.096" y2="2.921" layer="51"/>
<rectangle x1="-6.604" y1="-2.921" x2="-6.096" y2="-2.032" layer="51"/>
<rectangle x1="2.286" y1="5.842" x2="4.445" y2="6.35" layer="21"/>
<rectangle x1="-4.445" y1="5.842" x2="-2.286" y2="6.35" layer="21"/>
<rectangle x1="2.286" y1="-6.35" x2="4.445" y2="-5.842" layer="21"/>
<rectangle x1="-4.445" y1="-6.35" x2="-2.286" y2="-5.842" layer="21"/>
<hole x="0" y="-4.4958" drill="1.8034"/>
<hole x="0" y="4.4958" drill="1.8034"/>
</package>
<package name="SWITCH_SMD_B3F-40XX">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="1.905" width="0.1524" layer="51"/>
<wire x1="1.905" y1="-1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="1.651" x2="1.651" y2="1.651" width="0.0508" layer="51"/>
<wire x1="1.651" y1="-1.651" x2="1.651" y2="1.651" width="0.0508" layer="51"/>
<wire x1="1.651" y1="-1.651" x2="-1.651" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-1.651" y1="1.651" x2="-1.651" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-1.016" y1="6.096" x2="-1.016" y2="6.477" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="6.096" x2="1.016" y2="6.096" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="6.477" x2="1.016" y2="6.477" width="0.1524" layer="51"/>
<wire x1="1.016" y1="6.477" x2="1.016" y2="6.096" width="0.1524" layer="51"/>
<wire x1="6.096" y1="5.08" x2="5.08" y2="6.096" width="0.1524" layer="51"/>
<wire x1="5.08" y1="6.096" x2="1.778" y2="6.096" width="0.1524" layer="51"/>
<wire x1="6.096" y1="5.08" x2="6.096" y2="3.81" width="0.1524" layer="51"/>
<wire x1="6.096" y1="1.143" x2="6.096" y2="3.81" width="0.1524" layer="51"/>
<wire x1="6.096" y1="1.143" x2="6.096" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="6.096" y1="-3.81" x2="6.096" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-6.096" x2="6.096" y2="-5.08" width="0.1524" layer="51"/>
<wire x1="6.096" y1="-5.08" x2="6.096" y2="-3.81" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="1.143" x2="-6.096" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="1.143" x2="-6.096" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-3.81" x2="-6.096" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-3.81" x2="-6.096" y2="-5.08" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="-5.08" x2="-5.08" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="-6.096" x2="-1.778" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="6.096" x2="-1.778" y2="6.096" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="6.096" x2="-6.096" y2="5.08" width="0.1524" layer="51"/>
<wire x1="-6.096" y1="5.08" x2="-6.096" y2="3.81" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-6.477" x2="1.016" y2="-6.477" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-6.477" x2="-1.016" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="1.016" y1="-6.096" x2="1.016" y2="-6.477" width="0.1524" layer="51"/>
<wire x1="5.08" y1="6.096" x2="4.826" y2="6.35" width="0.1524" layer="51"/>
<wire x1="4.826" y1="6.35" x2="1.778" y2="6.35" width="0.1524" layer="51"/>
<wire x1="1.778" y1="6.35" x2="1.778" y2="6.096" width="0.1524" layer="51"/>
<wire x1="1.778" y1="6.096" x2="1.016" y2="6.096" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="6.35" x2="-1.778" y2="6.096" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="6.096" x2="-1.016" y2="6.096" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="6.35" x2="-4.826" y2="6.35" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="6.096" x2="-4.826" y2="6.35" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-6.096" x2="1.016" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="1.016" y1="-6.096" x2="1.778" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-6.096" x2="-1.778" y2="-6.35" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-6.096" x2="-1.016" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-6.35" x2="-4.826" y2="-6.35" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-6.35" x2="-5.08" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="1.778" y1="-6.096" x2="1.778" y2="-6.35" width="0.1524" layer="51"/>
<wire x1="1.778" y1="-6.096" x2="5.08" y2="-6.096" width="0.1524" layer="51"/>
<wire x1="4.826" y1="-6.35" x2="1.778" y2="-6.35" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-6.096" x2="4.826" y2="-6.35" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="2.54" x2="-4.572" y2="2.54" width="0.1524" layer="51"/>
<wire x1="2.921" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="-2.54" x2="-4.572" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.921" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="2.54" x2="-4.572" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="2.54" x2="-2.921" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="0.762" x2="-5.08" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="-0.762" x2="-4.572" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="-2.54" x2="-2.921" y2="-2.54" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="3.556" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="1.016" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.0508" layer="51"/>
<circle x="-4.572" y="2.54" radius="0.127" width="0.1524" layer="51"/>
<circle x="-4.572" y="-2.54" radius="0.127" width="0.1524" layer="51"/>
<smd name="1" x="-6.35" y="2.54" dx="2" dy="1.5" layer="1" rot="R90"/>
<smd name="2" x="6.35" y="2.54" dx="2" dy="1.5" layer="1" rot="R90"/>
<smd name="3" x="-6.35" y="-2.54" dx="2" dy="1.5" layer="1" rot="R90"/>
<smd name="4" x="6.35" y="-2.54" dx="2" dy="1.5" layer="1" rot="R90"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="10" align="center">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-5.207" y="3.302" size="1.27" layer="51" ratio="10">1</text>
<text x="3.937" y="3.302" size="1.27" layer="51" ratio="10">2</text>
<text x="-5.207" y="-4.699" size="1.27" layer="51" ratio="10">3</text>
<text x="4.064" y="-4.699" size="1.27" layer="51" ratio="10">4</text>
<rectangle x1="6.096" y1="-2.921" x2="6.604" y2="-2.032" layer="51"/>
<rectangle x1="6.096" y1="2.032" x2="6.604" y2="2.921" layer="51"/>
<rectangle x1="-6.604" y1="2.032" x2="-6.096" y2="2.921" layer="51"/>
<rectangle x1="-6.604" y1="-2.921" x2="-6.096" y2="-2.032" layer="51"/>
<rectangle x1="2.286" y1="5.842" x2="4.445" y2="6.35" layer="51"/>
<rectangle x1="-4.445" y1="5.842" x2="-2.286" y2="6.35" layer="51"/>
<rectangle x1="2.286" y1="-6.35" x2="4.445" y2="-5.842" layer="51"/>
<rectangle x1="-4.445" y1="-6.35" x2="-2.286" y2="-5.842" layer="51"/>
<hole x="0" y="-4.4958" drill="1.8034"/>
<hole x="0" y="4.4958" drill="1.8034"/>
<wire x1="-6.3" y1="3.8" x2="-6.3" y2="5.2" width="0.2" layer="21"/>
<wire x1="-6.3" y1="5.2" x2="-4.9" y2="6.6" width="0.2" layer="21"/>
<wire x1="-4.9" y1="6.6" x2="4.9" y2="6.6" width="0.2" layer="21"/>
<wire x1="4.9" y1="6.6" x2="6.3" y2="5.2" width="0.2" layer="21"/>
<wire x1="6.3" y1="5.2" x2="6.3" y2="3.8" width="0.2" layer="21"/>
<wire x1="6.3" y1="1.3" x2="6.3" y2="-1.3" width="0.2" layer="21"/>
<wire x1="6.3" y1="-3.8" x2="6.3" y2="-5.2" width="0.2" layer="21"/>
<wire x1="6.3" y1="-5.2" x2="4.9" y2="-6.6" width="0.2" layer="21"/>
<wire x1="4.9" y1="-6.6" x2="-4.9" y2="-6.6" width="0.2" layer="21"/>
<wire x1="-4.9" y1="-6.6" x2="-6.3" y2="-5.2" width="0.2" layer="21"/>
<wire x1="-6.3" y1="-5.2" x2="-6.3" y2="-3.8" width="0.2" layer="21"/>
<wire x1="-6.3" y1="-1.3" x2="-6.3" y2="1.3" width="0.2" layer="21"/>
</package>
<package name="EVQ-Q2">
<wire x1="-3.3" y1="3" x2="3.3" y2="3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3" x2="3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-3" x2="-3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3" x2="-3.3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5033" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<smd name="B" x="-3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="B'" x="3.4" y="2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A'" x="3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<smd name="A" x="-3.4" y="-2" dx="3.2" dy="1.2" layer="1"/>
<text x="-3" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TACTB-64K-F">
<wire x1="-2.5" y1="0.9" x2="-2.5" y2="-0.9" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-2.5" x2="1.2" y2="-2.5" width="0.2" layer="21"/>
<wire x1="2.5" y1="-0.9" x2="2.5" y2="0.9" width="0.2" layer="21"/>
<wire x1="1.2" y1="2.5" x2="-1.2" y2="2.5" width="0.2" layer="21"/>
<circle x="0" y="0" radius="1.6" width="0.2" layer="21"/>
<smd name="4" x="-3.1" y="1.8" dx="1.8" dy="1.1" layer="1"/>
<smd name="1" x="-3.1" y="-1.8" dx="1.8" dy="1.1" layer="1"/>
<smd name="2" x="3.1" y="-1.8" dx="1.8" dy="1.1" layer="1"/>
<smd name="3" x="3.1" y="1.8" dx="1.8" dy="1.1" layer="1"/>
<text x="-1" y="-0.4" size="0.8" layer="25" font="vector" ratio="15">&gt;NAME</text>
<wire x1="-1.2" y1="2.5" x2="-1.8" y2="1.9" width="0.2" layer="21"/>
<wire x1="1.2" y1="2.5" x2="1.8" y2="1.9" width="0.2" layer="21"/>
<wire x1="1.2" y1="-2.5" x2="1.8" y2="-1.9" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-2.5" x2="-1.8" y2="-1.9" width="0.2" layer="21"/>
</package>
<package name="SKSCLDE010">
<wire x1="1.75" y1="1.4" x2="-1.75" y2="1.4" width="0.1" layer="21"/>
<wire x1="-1.75" y1="1.4" x2="-1.75" y2="-1.4" width="0.1" layer="21"/>
<wire x1="-1.75" y1="-1.4" x2="-0.8" y2="-1.4" width="0.1" layer="21"/>
<wire x1="-0.8" y1="-1.4" x2="0.8" y2="-1.4" width="0.1" layer="21"/>
<wire x1="0.8" y1="-1.4" x2="1.75" y2="-1.4" width="0.1" layer="21"/>
<wire x1="1.75" y1="-1.4" x2="1.75" y2="1.4" width="0.1" layer="21"/>
<wire x1="0.8" y1="-2.1" x2="-0.8" y2="-2.1" width="0.1" layer="21"/>
<wire x1="-0.8" y1="-2.1" x2="-0.8" y2="-1.4" width="0.1" layer="21"/>
<wire x1="0.8" y1="-2.1" x2="0.8" y2="-1.4" width="0.1" layer="21"/>
<smd name="1" x="-1.8" y="-0.65" dx="1.4" dy="0.9" layer="1"/>
<smd name="2" x="1.8" y="-0.65" dx="1.4" dy="0.9" layer="1"/>
<smd name="3" x="-1.8" y="0.65" dx="1.4" dy="0.9" layer="1"/>
<smd name="4" x="1.8" y="0.65" dx="1.4" dy="0.9" layer="1"/>
<text x="0" y="0" size="0.8" layer="25" font="vector" align="center">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0.9" drill="0.55"/>
<hole x="0" y="-0.9" drill="0.55"/>
</package>
<package name="SMD-TAKTSW">
<circle x="0" y="0" radius="1.7038" width="0.254" layer="51"/>
<circle x="-2.032" y="2.032" radius="0.3592" width="0.254" layer="51"/>
<circle x="2.032" y="2.032" radius="0.3592" width="0.254" layer="51"/>
<circle x="2.032" y="-2.032" radius="0.3592" width="0.254" layer="51"/>
<circle x="-2.032" y="-2.032" radius="0.3592" width="0.254" layer="51"/>
<wire x1="-2.921" y1="2.921" x2="-2.921" y2="-2.921" width="0.254" layer="51"/>
<wire x1="-2.921" y1="-2.921" x2="2.921" y2="-2.921" width="0.254" layer="51"/>
<wire x1="2.921" y1="-2.921" x2="2.921" y2="2.921" width="0.254" layer="51"/>
<wire x1="2.921" y1="2.921" x2="-2.921" y2="2.921" width="0.254" layer="51"/>
<smd name="1" x="-2.4" y="-3.7" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="2.4" y="-3.7" dx="1.3" dy="1.5" layer="1"/>
<smd name="3" x="2.4" y="3.7" dx="1.3" dy="1.5" layer="1"/>
<smd name="4" x="-2.4" y="3.7" dx="1.3" dy="1.5" layer="1"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="-3.175" y="-6.35" size="1.27" layer="27" ratio="20">&gt;VALUE</text>
<wire x1="1.5" y1="-3.2" x2="-1.5" y2="-3.2" width="0.2" layer="21"/>
<wire x1="1.5" y1="3.2" x2="-1.5" y2="3.2" width="0.2" layer="21"/>
<wire x1="-3.2" y1="-2.6" x2="-3.2" y2="2.6" width="0.2" layer="21"/>
<wire x1="3.2" y1="2.6" x2="3.2" y2="-2.6" width="0.2" layer="21"/>
</package>
<package name="SW_TACT_SMALL">
<wire x1="-1.2183" y1="2.182" x2="-1.1335" y2="2.182" width="0.1524" layer="51"/>
<wire x1="-1.1335" y1="2.182" x2="1.2183" y2="2.182" width="0.1524" layer="51"/>
<wire x1="1.2183" y1="2.182" x2="2.182" y2="1.2183" width="0.1524" layer="51"/>
<wire x1="2.182" y1="-1.2183" x2="1.2183" y2="-2.182" width="0.1524" layer="51"/>
<wire x1="-1.2183" y1="-2.182" x2="-2.182" y2="-1.2183" width="0.1524" layer="51"/>
<wire x1="-2.182" y1="1.2183" x2="-1.2183" y2="2.182" width="0.1524" layer="51"/>
<wire x1="-1.1335" y1="2.182" x2="-1.1335" y2="2.5" width="0.1524" layer="51"/>
<wire x1="-1.1335" y1="2.5" x2="1.1335" y2="2.5" width="0.1524" layer="51"/>
<wire x1="1.1335" y1="2.5" x2="1.1335" y2="2.181" width="0.1524" layer="51"/>
<wire x1="1.2183" y1="-2.182" x2="1.1335" y2="-2.182" width="0.1524" layer="51"/>
<wire x1="1.1335" y1="-2.182" x2="-1.2183" y2="-2.182" width="0.1524" layer="51"/>
<wire x1="1.1335" y1="-2.182" x2="1.1335" y2="-2.5" width="0.1524" layer="51"/>
<wire x1="1.1335" y1="-2.5" x2="-1.1335" y2="-2.5" width="0.1524" layer="51"/>
<wire x1="-1.1335" y1="-2.5" x2="-1.1335" y2="-2.181" width="0.1524" layer="51"/>
<wire x1="-2.182" y1="-1.2183" x2="-2.182" y2="-1.1335" width="0.1524" layer="51"/>
<wire x1="-2.182" y1="-1.1335" x2="-2.182" y2="1.2183" width="0.1524" layer="51"/>
<wire x1="-2.182" y1="-1.1335" x2="-2.5" y2="-1.1335" width="0.1524" layer="51"/>
<wire x1="-2.5" y1="-1.1335" x2="-2.5" y2="1.1335" width="0.1524" layer="51"/>
<wire x1="-2.5" y1="1.1335" x2="-2.181" y2="1.1335" width="0.1524" layer="51"/>
<wire x1="2.182" y1="1.2183" x2="2.182" y2="1.1335" width="0.1524" layer="51"/>
<wire x1="2.182" y1="1.1335" x2="2.182" y2="-1.2183" width="0.1524" layer="51"/>
<wire x1="2.182" y1="1.1335" x2="2.5" y2="1.1335" width="0.1524" layer="51"/>
<wire x1="2.5" y1="1.1335" x2="2.5" y2="-1.1335" width="0.1524" layer="51"/>
<wire x1="2.5" y1="-1.1335" x2="2.181" y2="-1.1335" width="0.1524" layer="51"/>
<wire x1="-1.065" y1="1.812" x2="1.065" y2="1.812" width="0.0762" layer="51"/>
<wire x1="1.065" y1="1.812" x2="1.812" y2="1.065" width="0.0762" layer="51"/>
<wire x1="-1.812" y1="-1.065" x2="-1.812" y2="1.065" width="0.0762" layer="51"/>
<wire x1="-1.812" y1="1.065" x2="-1.065" y2="1.812" width="0.0762" layer="51"/>
<wire x1="1.065" y1="-1.812" x2="-1.065" y2="-1.812" width="0.0762" layer="51"/>
<wire x1="-1.065" y1="-1.812" x2="-1.812" y2="-1.065" width="0.0762" layer="51"/>
<wire x1="1.812" y1="1.065" x2="1.812" y2="-1.065" width="0.0762" layer="51"/>
<wire x1="1.812" y1="-1.065" x2="1.065" y2="-1.812" width="0.0762" layer="51"/>
<wire x1="-0.9958" y1="1.645" x2="0.9958" y2="1.645" width="0.0762" layer="51"/>
<wire x1="0.9958" y1="1.645" x2="1.645" y2="0.9958" width="0.0762" layer="51"/>
<wire x1="-1.645" y1="-0.9958" x2="-1.645" y2="0.9958" width="0.0762" layer="51"/>
<wire x1="-1.645" y1="0.9958" x2="-0.9958" y2="1.645" width="0.0762" layer="51"/>
<wire x1="0.9958" y1="-1.645" x2="-0.9958" y2="-1.645" width="0.0762" layer="51"/>
<wire x1="-0.9958" y1="-1.645" x2="-1.645" y2="-0.9958" width="0.0762" layer="51"/>
<wire x1="1.645" y1="0.9958" x2="1.645" y2="-0.9958" width="0.0762" layer="51"/>
<wire x1="1.645" y1="-0.9958" x2="0.9958" y2="-1.645" width="0.0762" layer="51"/>
<circle x="0" y="0" radius="1" width="0.0762" layer="51"/>
<circle x="0" y="0" radius="0.865" width="0.0762" layer="51"/>
<smd name="1" x="-3.1" y="1.9" dx="1" dy="1.7" layer="1" rot="R90"/>
<smd name="2" x="3.1" y="1.9" dx="1" dy="1.7" layer="1" rot="R90"/>
<smd name="3" x="-3.1" y="-1.9" dx="1" dy="1.7" layer="1" rot="R90"/>
<smd name="4" x="3.1" y="-1.9" dx="1" dy="1.7" layer="1" rot="R90"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<polygon width="0.0762" layer="51">
<vertex x="1.3003" y="2.1"/>
<vertex x="3.3" y="2.1"/>
<vertex x="3.3" y="1.6"/>
<vertex x="1.8003" y="1.6"/>
</polygon>
<polygon width="0.0762" layer="51">
<vertex x="-1.3003" y="-2.1"/>
<vertex x="-3.3" y="-2.1"/>
<vertex x="-3.3" y="-1.6"/>
<vertex x="-1.8003" y="-1.6"/>
</polygon>
<polygon width="0.0762" layer="51">
<vertex x="-1.3003" y="2.1"/>
<vertex x="-3.3" y="2.1"/>
<vertex x="-3.3" y="1.6"/>
<vertex x="-1.8003" y="1.6"/>
</polygon>
<polygon width="0.0762" layer="51">
<vertex x="1.3003" y="-2.1"/>
<vertex x="3.3" y="-2.1"/>
<vertex x="3.3" y="-1.6"/>
<vertex x="1.8003" y="-1.6"/>
</polygon>
<wire x1="-2.6" y1="1.1" x2="-2.6" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-2.6" y1="-1.1" x2="-2.2" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-2.1" x2="-1.2" y2="-2.6" width="0.2" layer="21"/>
<wire x1="-1.2" y1="-2.6" x2="1.2" y2="-2.6" width="0.2" layer="21"/>
<wire x1="1.2" y1="-2.6" x2="1.2" y2="-2.1" width="0.2" layer="21"/>
<wire x1="2.2" y1="-1.1" x2="2.6" y2="-1.1" width="0.2" layer="21"/>
<wire x1="2.6" y1="-1.1" x2="2.6" y2="1.1" width="0.2" layer="21"/>
<wire x1="2.6" y1="1.1" x2="2.2" y2="1.1" width="0.2" layer="21"/>
<wire x1="1.2" y1="2.1" x2="1.2" y2="2.6" width="0.2" layer="21"/>
<wire x1="1.2" y1="2.6" x2="-1.2" y2="2.6" width="0.2" layer="21"/>
<wire x1="-1.2" y1="2.6" x2="-1.2" y2="2.1" width="0.2" layer="21"/>
<wire x1="-2.2" y1="1.1" x2="-2.6" y2="1.1" width="0.2" layer="21"/>
</package>
<package name="TACTILE_SWITCH_SMD">
<wire x1="-1.54" y1="-2.54" x2="-2.54" y2="-1.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.24" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.54" x2="-1.54" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.54" y1="2.54" x2="1.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.54" y1="2.54" x2="2.54" y2="1.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.24" x2="2.54" y2="-1.24" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.54" x2="1.54" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.54" y1="-2.54" x2="-1.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.54" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="4" x="2.54" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<text x="-0.889" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD-2">
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.3" y1="2.2" x2="-1.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-2.2" x2="1.3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="0.8" x2="1.8" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="1.8" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="-2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<text x="-0.889" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="SW-TAKT1">
<circle x="0" y="0" radius="1.7038" width="0.254" layer="51"/>
<circle x="-2.032" y="2.032" radius="0.3592" width="0.254" layer="51"/>
<circle x="2.032" y="2.032" radius="0.3592" width="0.254" layer="51"/>
<circle x="2.032" y="-2.032" radius="0.3592" width="0.254" layer="51"/>
<circle x="-2.032" y="-2.032" radius="0.3592" width="0.254" layer="51"/>
<wire x1="-2.921" y1="2.921" x2="-2.921" y2="-2.921" width="0.254" layer="51"/>
<wire x1="-2.921" y1="-2.921" x2="2.921" y2="-2.921" width="0.254" layer="51"/>
<wire x1="2.921" y1="-2.921" x2="2.921" y2="2.921" width="0.254" layer="51"/>
<wire x1="2.921" y1="2.921" x2="-2.921" y2="2.921" width="0.254" layer="51"/>
<pad name="1" x="-2.3" y="-3.8" drill="1.0922" diameter="2.159"/>
<pad name="2" x="2.3" y="-3.8" drill="1.0922" diameter="2.159"/>
<pad name="3" x="2.3" y="3.8" drill="1.0922" diameter="2.159"/>
<pad name="4" x="-2.3" y="3.8" drill="1.0922" diameter="2.159"/>
<text x="0" y="0" size="1" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<wire x1="-3.3" y1="2.9" x2="-3.3" y2="-2.9" width="0.2" layer="21"/>
<wire x1="3.3" y1="-2.9" x2="3.3" y2="2.9" width="0.2" layer="21"/>
<wire x1="1" y1="3.3" x2="-0.9" y2="3.3" width="0.2" layer="21"/>
<wire x1="1" y1="-3.3" x2="-1" y2="-3.3" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SWITCH_G2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SWITCH_G2" prefix="SW">
<description>B3F-40XX
OMRON SWITCH</description>
<gates>
<gate name="G$1" symbol="SWITCH_G2" x="0" y="0"/>
</gates>
<devices>
<device name="-DIP" package="SWITCH_B3F-40XX">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMD" package="SWITCH_SMD_B3F-40XX">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="Q1" package="EVQ-Q2">
<connects>
<connect gate="G$1" pin="P" pad="A"/>
<connect gate="G$1" pin="P1" pad="A'"/>
<connect gate="G$1" pin="S" pad="B"/>
<connect gate="G$1" pin="S1" pad="B'"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4" package="TACTB-64K-F">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SKSCLDE010" package="SKSCLDE010">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-TAKTSW" package="SMD-TAKTSW">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="4"/>
<connect gate="G$1" pin="S" pad="2"/>
<connect gate="G$1" pin="S1" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SM" package="SW_TACT_SMALL">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="233" package="TACTILE_SWITCH_SMD">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="444444444" package="TACTILE_SWITCH_SMD-2">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-YUU" package="SW-TAKT1">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SKSCLDE010">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U2" library="!ht3_ic" deviceset="MCP170X" device="SOT89" value="MCP1703-3002"/>
<part name="ZQ3" library="!ht3_misc" deviceset="REZONATOR-4PIN" device="-SJK-7M" value="32768Hz"/>
<part name="ZQ1" library="!ht3_misc" deviceset="REZONATOR-4PIN" device="-3225" value="16MHz"/>
<part name="C5" library="!ht3_passive" deviceset="C-EU" device="C0402" value="12pF"/>
<part name="C9" library="!ht3_passive" deviceset="C-EU" device="C0402" value="12pF"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="C13" library="!ht3_passive" deviceset="C-EU" device="C0402" value="15pF"/>
<part name="C16" library="!ht3_passive" deviceset="C-EU" device="C0402" value="15pF"/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="C8" library="!ht3_passive" deviceset="C-EU" device="C0603" value="10-100nF"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="R10" library="!ht3_passive" deviceset="R-EU" device="0603" value="10K"/>
<part name="HL2" library="!ht3_leds" deviceset="LED" device="0603" value="BLUE"/>
<part name="GND52" library="supply1" deviceset="GND" device=""/>
<part name="R16" library="!ht3_passive" deviceset="R-EU" device="0603" value="1K"/>
<part name="G1" library="!ht3_misc" deviceset="CR2032H" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="C12" library="!ht3_passive" deviceset="C-EU" device="C0402" value="100nF"/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="C10" library="!ht3_passive" deviceset="C-EU" device="C0603" value="4.7uF"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="C14" library="!ht3_passive" deviceset="C-EU" device="C0402" value="33nF"/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="GND45" library="supply1" deviceset="GND" device=""/>
<part name="X3" library="!ht3_connectors" deviceset="CON-4P" device="PLS2-4" value="n.m."/>
<part name="U3" library="!ht3_ic" deviceset="NRF8001" device="" value="nRF8001"/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="ANT1" library="!ht3_misc" deviceset="ANT-2.4GHZ" device="-2"/>
<part name="GND56" library="supply1" deviceset="GND" device=""/>
<part name="C23" library="!ht3_passive" deviceset="C-EU" device="C0402" value="2.2nF/X7R/10%"/>
<part name="C25" library="!ht3_passive" deviceset="C-EU" device="C0402" value="4.7pF/NP0/+-0.25pF"/>
<part name="L6" library="!ht3_inductor" deviceset="L" device="L0402" value="3.9nH/5%"/>
<part name="L5" library="!ht3_inductor" deviceset="L" device="L0402" value="8.2nH/5%"/>
<part name="L7" library="!ht3_inductor" deviceset="L" device="L0402" value="5.6nH/5%"/>
<part name="C26" library="!ht3_passive" deviceset="C-EU" device="C0402" value="1.8pF/NP0/+-0.1pF"/>
<part name="GND51" library="supply1" deviceset="GND" device=""/>
<part name="GND55" library="supply1" deviceset="GND" device=""/>
<part name="C27" library="!ht3_passive" deviceset="C-EU" device="C0402" value="1.2pF/NP0/+-0.1pF"/>
<part name="GND54" library="supply1" deviceset="GND" device=""/>
<part name="R15" library="!ht3_passive" deviceset="R-EU" device="0603" value="22K"/>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="C20" library="!ht3_passive" deviceset="C-EU" device="C0402" value="1nF"/>
<part name="L4" library="!ht3_inductor" deviceset="L" device="L0402" value="10uH/5%"/>
<part name="L3" library="!ht3_inductor" deviceset="L" device="L0603" value="15nH/5%"/>
<part name="C19" library="!ht3_passive" deviceset="C-EU" device="C0603" value="1uF"/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="C18" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="GND68" library="supply1" deviceset="GND" device=""/>
<part name="GND63" library="supply1" deviceset="GND" device=""/>
<part name="GND65" library="supply1" deviceset="GND" device=""/>
<part name="U5" library="!ht3_ic" deviceset="MPU-6050" device="" value="MPU 6050/9255"/>
<part name="C30" library="!ht3_passive" deviceset="C-EU" device="C0603" value="2.2uF"/>
<part name="C31" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND58" library="supply1" deviceset="GND" device=""/>
<part name="GND64" library="supply1" deviceset="GND" device=""/>
<part name="C29" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND62" library="supply1" deviceset="GND" device=""/>
<part name="FIDUCIAL1" library="!ht3_misc" deviceset="FIDUCIAL" device=""/>
<part name="FIDUCIAL2" library="!ht3_misc" deviceset="FIDUCIAL" device=""/>
<part name="FIDUCIAL3" library="!ht3_misc" deviceset="FIDUCIAL" device=""/>
<part name="C28" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND57" library="supply1" deviceset="GND" device=""/>
<part name="GND59" library="supply1" deviceset="GND" device=""/>
<part name="U7" library="!ht3_ic" deviceset="BH1750" device=""/>
<part name="GND60" library="supply1" deviceset="GND" device=""/>
<part name="U6" library="!ht3_ic" deviceset="BME280" device=""/>
<part name="C32" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND66" library="supply1" deviceset="GND" device=""/>
<part name="R20" library="!ht3_passive" deviceset="R-EU" device="0603" value="1K"/>
<part name="HL1" library="!ht3_leds" deviceset="LED" device="0603" value="RED"/>
<part name="U1" library="!ht3_ic" deviceset="LTC4054" device="" value="STC4054"/>
<part name="GND42" library="supply1" deviceset="GND" device=""/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="GND53" library="supply1" deviceset="GND" device=""/>
<part name="GND48" library="supply1" deviceset="GND" device=""/>
<part name="GND43" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="L2" library="!ht3_inductor" deviceset="L" device="L0603" value="43uH"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="C6" library="!ht3_passive" deviceset="C-EU" device="C0402" value="20pF"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="C11" library="!ht3_passive" deviceset="C-EU" device="C0402" value="20pF"/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="R4" library="!ht3_passive" deviceset="R-EU" device="0603" value="2-10K"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="R5" library="!ht3_passive" deviceset="R-EU" device="0603" value="1-2M"/>
<part name="R6" library="!ht3_passive" deviceset="R-EU" device="0603" value="1M"/>
<part name="C1" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="R7" library="!ht3_passive" deviceset="R-EU" device="0603" value="2K"/>
<part name="R8" library="!ht3_passive" deviceset="R-EU" device="0603" value="200K"/>
<part name="X2" library="!ht3_connectors" deviceset="CON-2P" device="-PLS2-2" value="n.m."/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="C3" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="C4" library="!ht3_passive" deviceset="C-EU" device="C0805" value="4uF"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="X1" library="!ht3_connectors" deviceset="USB-5S" device="-MICRO" value="ESB228110100Z"/>
<part name="VT1" library="!ht3_transistor" deviceset="FET_P" device="-SOT23" value="IRLML6401"/>
<part name="U4" library="!ht3_ic" deviceset="STM32F10XC" device="" value="STM32F103C8"/>
<part name="LCD1" library="!ht3_modules" deviceset="LCD-NOKIA-1202" device="-1" value="LCD-NOKIA-1202-1"/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="VT2" library="!ht3_transistor" deviceset="FET_N" device="SOT23" value="IRLML0060"/>
<part name="R13" library="!ht3_passive" deviceset="R-EU" device="0603" value="180"/>
<part name="R12" library="!ht3_passive" deviceset="R-EU" device="0603" value="10K"/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="R11" library="!ht3_passive" deviceset="R-EU" device="0603" value="100"/>
<part name="C15" library="!ht3_passive" deviceset="C-EU" device="C0805" value="2.2uF"/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="L1" library="!ht3_inductor" deviceset="L" device="L0805" value="BLM18AG102SN1D"/>
<part name="C22" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND47" library="supply1" deviceset="GND" device=""/>
<part name="C17" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="C21" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND46" library="supply1" deviceset="GND" device=""/>
<part name="R1" library="!ht3_passive" deviceset="R-EU" device="0603" value="33"/>
<part name="R2" library="!ht3_passive" deviceset="R-EU" device="0603" value="33"/>
<part name="C7" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="ZQ2" library="!ht3_misc" deviceset="REZONATOR-4PIN" device="-3225" value="12MHz"/>
<part name="SW1" library="!ht3_switchers" deviceset="SWITCH_G2" device="SM"/>
<part name="U8" library="!ht3_ic" deviceset="24C0X" device="-SO8" value="24FR02"/>
<part name="GND67" library="supply1" deviceset="GND" device=""/>
<part name="GND61" library="supply1" deviceset="GND" device=""/>
<part name="R9" library="!ht3_passive" deviceset="R-EU" device="0603" value="0"/>
<part name="X6" library="!ht3_connectors" deviceset="SWD-5" device="-PLS2-5"/>
<part name="X4" library="!ht3_connectors" deviceset="CON-4P" device="PLS2-4" value="n.m."/>
<part name="R14" library="!ht3_passive" deviceset="R-EU" device="0603" value="0"/>
<part name="R19" library="!ht3_passive" deviceset="R-EU" device="0603" value="1K"/>
<part name="X5" library="!ht3_connectors" deviceset="I2C" device="-PLS2-4" value="I2C-PLS2-4"/>
<part name="GND50" library="supply1" deviceset="GND" device=""/>
<part name="R17" library="!ht3_passive" deviceset="R-EU" device="0603" value="4.7K"/>
<part name="R18" library="!ht3_passive" deviceset="R-EU" device="0603" value="4.7K"/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="R3" library="!ht3_passive" deviceset="R-EU" device="0603" value="2K"/>
<part name="A1" library="!ht3_modules" deviceset="NRF24L01_SMD" device="" value="NRF24L01+"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="C2" library="!ht3_passive" deviceset="C-EU" device="C0603" value="1uF"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="LOGO1" library="!ht3_misc" deviceset="LOGO" device="6"/>
<part name="C24" library="!ht3_passive" deviceset="C-EU" device="C0603" value="100nF"/>
<part name="GND49" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="174.752" y="10.414" size="5.08" layer="94" font="vector">A/G/M</text>
<text x="170.18" y="-96.52" size="5.08" layer="94" font="vector">P/H/t</text>
<text x="172.72" y="99.06" size="5.08" layer="94" font="vector">L</text>
<text x="-2.54" y="-134.62" size="1.778" layer="102">The Nokia 1202 has 96 × 68 pixels.</text>
<text x="-12.7" y="-147.32" size="1.778" layer="102">http://vrtp.ru/screenshots/3590_schema3.jpg</text>
<wire x1="-127" y1="60.96" x2="-114.3" y2="60.96" width="0.3048" layer="94"/>
<wire x1="-114.3" y1="60.96" x2="-114.3" y2="53.34" width="0.3048" layer="94"/>
<wire x1="-114.3" y1="53.34" x2="-127" y2="53.34" width="0.3048" layer="94"/>
<text x="-124.46" y="55.88" size="1.778" layer="94" font="vector" align="center-left">+BATT</text>
<text x="-124.46" y="58.42" size="1.778" layer="94" font="vector" align="center-left">-BATT</text>
<wire x1="-30.48" y1="-38.1" x2="-43.18" y2="-38.1" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-38.1" x2="-43.18" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-50.8" x2="-30.48" y2="-50.8" width="0.1524" layer="94"/>
<text x="-33.02" y="-40.64" size="1.778" layer="94" font="vector" align="center-right">GPIO0</text>
<text x="-33.02" y="-43.18" size="1.778" layer="94" font="vector" align="center-right">GPIO1</text>
<text x="-33.02" y="-45.72" size="1.778" layer="94" font="vector" align="center-right">GPIO2</text>
<text x="-33.02" y="-48.26" size="1.778" layer="94" font="vector" align="center-right">GPIO3</text>
<wire x1="-30.48" y1="-58.42" x2="-43.18" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-58.42" x2="-43.18" y2="-71.12" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-71.12" x2="-30.48" y2="-71.12" width="0.1524" layer="94"/>
<text x="-33.02" y="-60.96" size="1.778" layer="94" font="vector" align="center-right">GPIO4</text>
<text x="-33.02" y="-63.5" size="1.778" layer="94" font="vector" align="center-right">GPIO5</text>
<text x="-33.02" y="-66.04" size="1.778" layer="94" font="vector" align="center-right">GPIO6</text>
<text x="-33.02" y="-68.58" size="1.778" layer="94" font="vector" align="center-right">GPIO7</text>
<frame x1="-246.38" y1="-160.02" x2="248.92" y2="170.18" columns="8" rows="5" layer="94"/>
</plain>
<instances>
<instance part="U2" gate="G$1" x="-124.46" y="10.16"/>
<instance part="ZQ3" gate="G$1" x="-27.94" y="134.62"/>
<instance part="ZQ1" gate="G$1" x="-68.58" y="134.62"/>
<instance part="C5" gate="G$1" x="-81.28" y="127"/>
<instance part="C9" gate="G$1" x="-55.88" y="127"/>
<instance part="GND13" gate="1" x="-81.28" y="116.84" rot="MR0"/>
<instance part="GND21" gate="1" x="-55.88" y="116.84" rot="MR0"/>
<instance part="C13" gate="G$1" x="-40.64" y="127"/>
<instance part="C16" gate="G$1" x="-15.24" y="127"/>
<instance part="GND27" gate="1" x="-40.64" y="116.84" rot="MR0"/>
<instance part="GND33" gate="1" x="-15.24" y="116.84" rot="MR0"/>
<instance part="C8" gate="G$1" x="-60.96" y="-60.96"/>
<instance part="GND19" gate="1" x="-60.96" y="-71.12" rot="MR0"/>
<instance part="R10" gate="G$1" x="-68.58" y="-55.88"/>
<instance part="HL2" gate="G$1" x="86.36" y="-53.34" rot="MR0"/>
<instance part="GND52" gate="1" x="86.36" y="-63.5"/>
<instance part="R16" gate="G$1" x="76.2" y="-48.26"/>
<instance part="G1" gate="1" x="-124.46" y="43.18"/>
<instance part="GND8" gate="1" x="-124.46" y="33.02" rot="MR0"/>
<instance part="C12" gate="G$1" x="-43.18" y="93.98"/>
<instance part="GND25" gate="1" x="-43.18" y="83.82" rot="MR0"/>
<instance part="C10" gate="G$1" x="-55.88" y="93.98"/>
<instance part="GND22" gate="1" x="-55.88" y="83.82"/>
<instance part="C14" gate="G$1" x="-30.48" y="93.98"/>
<instance part="GND29" gate="1" x="-30.48" y="83.82" rot="MR0"/>
<instance part="GND45" gate="1" x="43.18" y="83.82" rot="MR0"/>
<instance part="X3" gate="G$1" x="-30.48" y="-38.1" rot="MR0"/>
<instance part="U3" gate="G$1" x="-2.54" y="116.84"/>
<instance part="GND36" gate="1" x="-10.16" y="83.82" rot="MR0"/>
<instance part="GND40" gate="1" x="12.7" y="129.54" rot="MR180"/>
<instance part="ANT1" gate="G$1" x="127" y="132.08"/>
<instance part="GND56" gate="1" x="129.54" y="127"/>
<instance part="C23" gate="G$1" x="78.74" y="91.44"/>
<instance part="C25" gate="G$1" x="86.36" y="83.82"/>
<instance part="L6" gate="G$1" x="78.74" y="116.84"/>
<instance part="L5" gate="G$1" x="71.12" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="73.66" y="110.49" size="1.778" layer="95" font="vector" rot="R180" align="center"/>
<attribute name="VALUE" x="78.232" y="108.204" size="1.778" layer="96" font="vector" rot="R180" align="center"/>
</instance>
<instance part="L7" gate="G$1" x="78.74" y="101.6"/>
<instance part="C26" gate="G$1" x="99.06" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="95.504" y="120.142" size="1.778" layer="95" font="vector" align="center-left"/>
<attribute name="VALUE" x="90.932" y="113.538" size="1.778" layer="96" font="vector" align="center-left"/>
</instance>
<instance part="GND51" gate="1" x="86.36" y="71.12"/>
<instance part="GND55" gate="1" x="124.46" y="86.36"/>
<instance part="C27" gate="G$1" x="111.76" y="124.46"/>
<instance part="GND54" gate="1" x="111.76" y="132.08" rot="R180"/>
<instance part="R15" gate="G$1" x="33.02" y="129.54"/>
<instance part="GND44" gate="1" x="40.64" y="124.46" rot="MR0"/>
<instance part="C20" gate="G$1" x="40.64" y="137.16"/>
<instance part="L4" gate="G$1" x="0" y="132.08" rot="R90"/>
<instance part="L3" gate="G$1" x="0" y="149.86" rot="R90"/>
<instance part="C19" gate="G$1" x="7.62" y="152.4"/>
<instance part="GND39" gate="1" x="7.62" y="142.24" rot="MR0"/>
<instance part="C18" gate="G$1" x="-2.54" y="66.04"/>
<instance part="GND38" gate="1" x="-2.54" y="55.88"/>
<instance part="GND68" gate="1" x="215.9" y="20.32" rot="MR270"/>
<instance part="GND63" gate="1" x="185.42" y="-20.32" rot="MR0"/>
<instance part="GND65" gate="1" x="190.5" y="-33.02" rot="MR0"/>
<instance part="U5" gate="G$1" x="167.64" y="33.02"/>
<instance part="C30" gate="G$1" x="190.5" y="45.72"/>
<instance part="C31" gate="G$1" x="190.5" y="-22.86"/>
<instance part="GND58" gate="1" x="157.48" y="20.32" rot="MR90"/>
<instance part="GND64" gate="1" x="190.5" y="53.34" rot="MR180"/>
<instance part="C29" gate="G$1" x="170.18" y="-17.78"/>
<instance part="GND62" gate="1" x="170.18" y="-27.94" rot="MR0"/>
<instance part="FIDUCIAL1" gate="G$1" x="-228.6" y="-12.7"/>
<instance part="FIDUCIAL2" gate="G$1" x="-223.52" y="-12.7"/>
<instance part="FIDUCIAL3" gate="G$1" x="-218.44" y="-12.7"/>
<instance part="C28" gate="G$1" x="149.86" y="-78.74"/>
<instance part="GND57" gate="1" x="149.86" y="-88.9" rot="MR0"/>
<instance part="GND59" gate="1" x="162.56" y="-88.9" rot="MR0"/>
<instance part="U7" gate="G$1" x="172.72" y="119.38"/>
<instance part="GND60" gate="1" x="165.1" y="106.68"/>
<instance part="U6" gate="G$1" x="170.18" y="-71.12"/>
<instance part="C32" gate="G$1" x="198.12" y="104.14"/>
<instance part="GND66" gate="1" x="198.12" y="93.98"/>
<instance part="R20" gate="G$1" x="187.96" y="124.46" rot="MR0"/>
<instance part="HL1" gate="G$1" x="-142.24" y="15.24"/>
<instance part="U1" gate="G$1" x="-170.18" y="10.16"/>
<instance part="GND42" gate="1" x="20.32" y="27.94" rot="R180"/>
<instance part="GND41" gate="1" x="12.7" y="27.94" rot="R180"/>
<instance part="GND53" gate="1" x="91.44" y="30.48" rot="R270"/>
<instance part="GND48" gate="1" x="68.58" y="-7.62" rot="R90"/>
<instance part="GND43" gate="1" x="35.56" y="-66.04"/>
<instance part="GND9" gate="1" x="-116.84" y="-12.7"/>
<instance part="GND5" gate="1" x="-139.7" y="-7.62"/>
<instance part="GND1" gate="1" x="-210.82" y="-10.16"/>
<instance part="GND18" gate="1" x="-60.96" y="-45.72"/>
<instance part="L2" gate="G$1" x="-68.58" y="-27.94"/>
<instance part="GND10" gate="1" x="-106.68" y="-101.6"/>
<instance part="C6" gate="G$1" x="-73.66" y="-7.62"/>
<instance part="GND14" gate="1" x="-73.66" y="-17.78"/>
<instance part="C11" gate="G$1" x="-50.8" y="-7.62"/>
<instance part="GND23" gate="1" x="-50.8" y="-17.78"/>
<instance part="R4" gate="G$1" x="-177.8" y="-5.08" rot="R90"/>
<instance part="GND2" gate="1" x="-177.8" y="-15.24"/>
<instance part="R5" gate="G$1" x="-175.26" y="-33.02" rot="R90"/>
<instance part="R6" gate="G$1" x="-175.26" y="-48.26" rot="R90"/>
<instance part="C1" gate="G$1" x="-167.64" y="-45.72"/>
<instance part="R7" gate="G$1" x="-160.02" y="20.32"/>
<instance part="R8" gate="G$1" x="-116.84" y="-48.26" rot="R90"/>
<instance part="X2" gate="G$1" x="-127" y="60.96" rot="MR0"/>
<instance part="GND6" gate="1" x="-137.16" y="58.42" rot="R270"/>
<instance part="C3" gate="G$1" x="-134.62" y="2.54"/>
<instance part="C4" gate="G$1" x="-101.6" y="2.54"/>
<instance part="GND11" gate="1" x="-101.6" y="-7.62"/>
<instance part="GND7" gate="1" x="-134.62" y="-7.62"/>
<instance part="X1" gate="G$1" x="-233.68" y="10.16"/>
<instance part="VT1" gate="G$1" x="-109.22" y="-43.18" rot="R90"/>
<instance part="U4" gate="G$1" x="-2.54" y="7.62"/>
<instance part="LCD1" gate="G$1" x="-2.54" y="-88.9"/>
<instance part="GND34" gate="1" x="-15.24" y="-114.3" rot="MR90"/>
<instance part="GND35" gate="1" x="-12.7" y="-101.6" rot="MR90"/>
<instance part="GND26" gate="1" x="-43.18" y="-134.62"/>
<instance part="VT2" gate="G$1" x="-45.72" y="-124.46"/>
<instance part="R13" gate="G$1" x="-27.94" y="-116.84"/>
<instance part="R12" gate="G$1" x="-50.8" y="-134.62" rot="R90"/>
<instance part="GND24" gate="1" x="-50.8" y="-144.78"/>
<instance part="R11" gate="G$1" x="-58.42" y="-127"/>
<instance part="C15" gate="G$1" x="-20.32" y="-124.46"/>
<instance part="GND32" gate="1" x="-20.32" y="-134.62" rot="MR0"/>
<instance part="L1" gate="G$1" x="-205.74" y="7.62" smashed="yes">
<attribute name="NAME" x="-205.74" y="12.7" size="1.778" layer="95" font="vector" align="center"/>
<attribute name="VALUE" x="-203.2" y="10.16" size="1.778" layer="96" font="vector" rot="R180" align="center"/>
</instance>
<instance part="C22" gate="G$1" x="58.42" y="2.54"/>
<instance part="GND47" gate="1" x="58.42" y="10.16" rot="R180"/>
<instance part="C17" gate="G$1" x="-10.16" y="22.86"/>
<instance part="GND37" gate="1" x="-10.16" y="30.48" rot="R180"/>
<instance part="C21" gate="G$1" x="43.18" y="-53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="42.672" y="-50.8" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="41.148" y="-57.658" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND46" gate="1" x="50.8" y="-58.42"/>
<instance part="R1" gate="G$1" x="-205.74" y="5.08" smashed="yes">
<attribute name="NAME" x="-213.106" y="5.334" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="-202.692" y="5.334" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R2" gate="G$1" x="-205.74" y="2.54" smashed="yes">
<attribute name="NAME" x="-213.106" y="2.794" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="-202.692" y="2.794" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C7" gate="G$1" x="-60.96" y="-35.56"/>
<instance part="ZQ2" gate="G$1" x="-60.96" y="0"/>
<instance part="SW1" gate="G$1" x="-116.84" y="-88.9"/>
<instance part="U8" gate="G$1" x="172.72" y="-124.46"/>
<instance part="GND67" gate="1" x="200.66" y="-88.9"/>
<instance part="GND61" gate="1" x="165.1" y="-139.7"/>
<instance part="R9" gate="G$1" x="-101.6" y="-33.02" rot="R90"/>
<instance part="X6" gate="G$1" x="101.6" y="40.64"/>
<instance part="X4" gate="G$1" x="-30.48" y="-58.42" rot="MR0"/>
<instance part="R14" gate="G$1" x="22.86" y="157.48"/>
<instance part="R19" gate="G$1" x="185.42" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="183.896" y="58.42" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="188.722" y="58.674" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="X5" gate="G$1" x="86.36" y="-88.9"/>
<instance part="GND50" gate="1" x="78.74" y="-104.14" rot="MR0"/>
<instance part="R17" gate="G$1" x="91.44" y="-124.46"/>
<instance part="R18" gate="G$1" x="91.44" y="-132.08"/>
<instance part="GND31" gate="1" x="-22.86" y="-22.86" rot="R270"/>
<instance part="GND3" gate="1" x="-175.26" y="-60.96"/>
<instance part="R3" gate="G$1" x="-198.12" y="-5.08" rot="R90"/>
<instance part="A1" gate="G$1" x="-127" y="111.76"/>
<instance part="GND12" gate="1" x="-88.9" y="86.36"/>
<instance part="GND4" gate="1" x="-152.4" y="106.68" rot="R270"/>
<instance part="C2" gate="G$1" x="-142.24" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="-143.256" y="117.856" size="1.778" layer="95" font="vector" align="center-left"/>
<attribute name="VALUE" x="-143.002" y="110.998" size="1.778" layer="96" font="vector" align="center-left"/>
</instance>
<instance part="GND15" gate="1" x="-71.12" y="116.84" rot="MR0"/>
<instance part="GND16" gate="1" x="-66.04" y="116.84" rot="MR0"/>
<instance part="GND30" gate="1" x="-25.4" y="116.84" rot="MR0"/>
<instance part="GND28" gate="1" x="-30.48" y="116.84" rot="MR0"/>
<instance part="GND17" gate="1" x="-63.5" y="-17.78" rot="MR0"/>
<instance part="GND20" gate="1" x="-58.42" y="-17.78" rot="MR0"/>
<instance part="LOGO1" gate="G$1" x="-233.68" y="-20.32"/>
<instance part="C24" gate="G$1" x="78.74" y="-81.28"/>
<instance part="GND49" gate="1" x="73.66" y="-76.2" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="-81.28" y1="121.92" x2="-81.28" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="-55.88" y1="121.92" x2="-55.88" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="-40.64" y1="121.92" x2="-40.64" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="-15.24" y1="121.92" x2="-15.24" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="-60.96" y1="-66.04" x2="-60.96" y2="-68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND52" gate="1" pin="GND"/>
<pinref part="HL2" gate="G$1" pin="C"/>
<wire x1="86.36" y1="-60.96" x2="86.36" y2="-58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="G1" gate="1" pin="-"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-124.46" y1="38.1" x2="-124.46" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="-43.18" y1="88.9" x2="-43.18" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="-55.88" y1="88.9" x2="-55.88" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="86.36" x2="-30.48" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND45" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="VSS@1"/>
<wire x1="40.64" y1="88.9" x2="43.18" y2="88.9" width="0.1524" layer="91"/>
<wire x1="43.18" y1="88.9" x2="43.18" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VSS@2"/>
<wire x1="40.64" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="43.18" y1="91.44" x2="43.18" y2="88.9" width="0.1524" layer="91"/>
<junction x="43.18" y="88.9"/>
<pinref part="U3" gate="G$1" pin="VSS@3"/>
<wire x1="40.64" y1="104.14" x2="43.18" y2="104.14" width="0.1524" layer="91"/>
<wire x1="43.18" y1="104.14" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<junction x="43.18" y="91.44"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VSS@7"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="-7.62" y1="88.9" x2="-10.16" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="88.9" x2="-10.16" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VSS@4"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="12.7" y1="121.92" x2="12.7" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VSS@5"/>
<wire x1="12.7" y1="124.46" x2="12.7" y2="127" width="0.1524" layer="91"/>
<wire x1="12.7" y1="124.46" x2="10.16" y2="124.46" width="0.1524" layer="91"/>
<wire x1="10.16" y1="124.46" x2="10.16" y2="121.92" width="0.1524" layer="91"/>
<junction x="12.7" y="124.46"/>
</segment>
<segment>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="129.54" y1="129.54" x2="129.54" y2="132.08" width="0.1524" layer="91"/>
<pinref part="ANT1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND51" gate="1" pin="GND"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="86.36" y1="73.66" x2="86.36" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="86.36" y1="76.2" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<wire x1="86.36" y1="76.2" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<wire x1="78.74" y1="76.2" x2="78.74" y2="86.36" width="0.1524" layer="91"/>
<junction x="86.36" y="76.2"/>
</segment>
<segment>
<pinref part="GND55" gate="1" pin="GND"/>
<wire x1="127" y1="91.44" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
<wire x1="124.46" y1="91.44" x2="124.46" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="111.76" y1="129.54" x2="111.76" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="38.1" y1="129.54" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
<wire x1="40.64" y1="129.54" x2="40.64" y2="127" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="40.64" y1="132.08" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
<junction x="40.64" y="129.54"/>
</segment>
<segment>
<pinref part="GND39" gate="1" pin="GND"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="7.62" y1="144.78" x2="7.62" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="-2.54" y1="60.96" x2="-2.54" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND63" gate="1" pin="GND"/>
<wire x1="185.42" y1="-10.16" x2="185.42" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="AD0"/>
<pinref part="U5" gate="G$1" pin="FSYNC"/>
<wire x1="185.42" y1="-12.7" x2="185.42" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-10.16" x2="190.5" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-12.7" x2="185.42" y2="-12.7" width="0.1524" layer="91"/>
<junction x="185.42" y="-12.7"/>
</segment>
<segment>
<pinref part="GND68" gate="1" pin="GND"/>
<wire x1="213.36" y1="20.32" x2="210.82" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND65" gate="1" pin="GND"/>
<wire x1="190.5" y1="-27.94" x2="190.5" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND64" gate="1" pin="GND"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="190.5" y1="50.8" x2="190.5" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="GND62" gate="1" pin="GND"/>
<wire x1="170.18" y1="-25.4" x2="170.18" y2="-22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND57" gate="1" pin="GND"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="149.86" y1="-86.36" x2="149.86" y2="-83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="165.1" y1="-83.82" x2="162.56" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-83.82" x2="162.56" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-81.28" x2="162.56" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-81.28" x2="162.56" y2="-83.82" width="0.1524" layer="91"/>
<junction x="162.56" y="-83.82"/>
<pinref part="U6" gate="G$1" pin="GND@2"/>
<pinref part="U6" gate="G$1" pin="GND@1"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="GND"/>
<pinref part="GND60" gate="1" pin="GND"/>
<wire x1="167.64" y1="111.76" x2="165.1" y2="111.76" width="0.1524" layer="91"/>
<wire x1="165.1" y1="111.76" x2="165.1" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SDO/ADR"/>
<pinref part="GND67" gate="1" pin="GND"/>
<wire x1="198.12" y1="-78.74" x2="200.66" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="-78.74" x2="200.66" y2="-86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND66" gate="1" pin="GND"/>
<wire x1="198.12" y1="99.06" x2="198.12" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="20.32" y1="25.4" x2="20.32" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="BOOT0"/>
</segment>
<segment>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="12.7" y1="25.4" x2="12.7" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VSS@3"/>
</segment>
<segment>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="35.56" y1="-63.5" x2="35.56" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VSS@1"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="-116.84" y1="-10.16" x2="-116.84" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND53" gate="1" pin="GND"/>
<wire x1="96.52" y1="30.48" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<pinref part="X6" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="55.88" y1="-7.62" x2="66.04" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VSS@2"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-60.96" y1="-40.64" x2="-60.96" y2="-43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="-73.66" y1="-12.7" x2="-73.66" y2="-15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="-50.8" y1="-12.7" x2="-50.8" y2="-15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="-177.8" y1="-10.16" x2="-177.8" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="-144.78" y1="5.08" x2="-139.7" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="5.08" x2="-139.7" y2="-5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="X2" gate="G$1" pin="1"/>
<wire x1="-134.62" y1="58.42" x2="-127" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-134.62" y1="-5.08" x2="-134.62" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="-101.6" y1="-2.54" x2="-101.6" y2="-5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="SHIELD"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-213.36" y1="-5.08" x2="-210.82" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-5.08" x2="-210.82" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="-213.36" y1="-2.54" x2="-210.82" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-2.54" x2="-210.82" y2="-5.08" width="0.1524" layer="91"/>
<junction x="-210.82" y="-5.08"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="-43.18" y1="-132.08" x2="-43.18" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="VT2" gate="G$1" pin="S"/>
</segment>
<segment>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="-12.7" y1="-114.3" x2="-7.62" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="GND@2"/>
</segment>
<segment>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="-10.16" y1="-101.6" x2="-7.62" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="GND@1"/>
</segment>
<segment>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="-142.24" x2="-50.8" y2="-139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="-20.32" y1="-129.54" x2="-20.32" y2="-132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="58.42" y1="5.08" x2="58.42" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="-10.16" y1="25.4" x2="-10.16" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="48.26" y1="-53.34" x2="50.8" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-53.34" x2="50.8" y2="-55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="VSS"/>
<pinref part="GND61" gate="1" pin="GND"/>
<wire x1="167.64" y1="-134.62" x2="165.1" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-134.62" x2="165.1" y2="-137.16" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="A2"/>
<wire x1="167.64" y1="-132.08" x2="165.1" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-132.08" x2="165.1" y2="-134.62" width="0.1524" layer="91"/>
<junction x="165.1" y="-134.62"/>
<pinref part="U8" gate="G$1" pin="A1"/>
<wire x1="167.64" y1="-129.54" x2="165.1" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-129.54" x2="165.1" y2="-132.08" width="0.1524" layer="91"/>
<junction x="165.1" y="-132.08"/>
<pinref part="U8" gate="G$1" pin="A0"/>
<wire x1="167.64" y1="-127" x2="165.1" y2="-127" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-127" x2="165.1" y2="-129.54" width="0.1524" layer="91"/>
<junction x="165.1" y="-129.54"/>
<pinref part="U8" gate="G$1" pin="WP"/>
<wire x1="193.04" y1="-129.54" x2="195.58" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-129.54" x2="195.58" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-119.38" x2="165.1" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-119.38" x2="165.1" y2="-127" width="0.1524" layer="91"/>
<junction x="165.1" y="-127"/>
</segment>
<segment>
<wire x1="-116.84" y1="-93.98" x2="-116.84" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-93.98" x2="-114.3" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-96.52" x2="-116.84" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-114.3" y="-96.52"/>
<wire x1="-114.3" y1="-96.52" x2="-106.68" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-91.44" x2="-106.68" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="-91.44" x2="-106.68" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-106.68" y="-91.44"/>
<wire x1="-109.22" y1="-86.36" x2="-106.68" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="-86.36" x2="-106.68" y2="-91.44" width="0.1524" layer="91"/>
<junction x="-106.68" y="-96.52"/>
<pinref part="SW1" gate="G$1" pin="P"/>
<pinref part="SW1" gate="G$1" pin="P1"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="-106.68" y1="-96.52" x2="-106.68" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="CLKIN"/>
<wire x1="162.56" y1="20.32" x2="160.02" y2="20.32" width="0.1524" layer="91"/>
<pinref part="GND58" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X5" gate="G$1" pin="GND"/>
<pinref part="GND50" gate="1" pin="GND"/>
<wire x1="81.28" y1="-99.06" x2="78.74" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-99.06" x2="78.74" y2="-101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-20.32" y1="-22.86" x2="-7.62" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VSSA"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="-53.34" x2="-175.26" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-175.26" y1="-55.88" x2="-175.26" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="-50.8" x2="-167.64" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="-55.88" x2="-175.26" y2="-55.88" width="0.1524" layer="91"/>
<junction x="-175.26" y="-55.88"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="GND@1"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="-132.08" y1="106.68" x2="-147.32" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-147.32" y1="106.68" x2="-149.86" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="114.3" x2="-147.32" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="114.3" x2="-147.32" y2="106.68" width="0.1524" layer="91"/>
<junction x="-147.32" y="106.68"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="ZQ1" gate="G$1" pin="S@1"/>
<wire x1="-71.12" y1="119.38" x2="-71.12" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ZQ1" gate="G$1" pin="S@2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="-66.04" y1="119.38" x2="-66.04" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="ZQ3" gate="G$1" pin="S@2"/>
<wire x1="-25.4" y1="119.38" x2="-25.4" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="ZQ3" gate="G$1" pin="S@1"/>
<wire x1="-30.48" y1="119.38" x2="-30.48" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="ZQ2" gate="G$1" pin="S@1"/>
<wire x1="-63.5" y1="-15.24" x2="-63.5" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="ZQ2" gate="G$1" pin="S@2"/>
<wire x1="-58.42" y1="-15.24" x2="-58.42" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND49" gate="1" pin="GND"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="76.2" y1="-76.2" x2="78.74" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-76.2" x2="78.74" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RTCO" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="XL2"/>
<wire x1="-7.62" y1="99.06" x2="-10.16" y2="99.06" width="0.1524" layer="91"/>
<label x="-10.16" y="99.06" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="129.54" x2="-15.24" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="134.62" x2="-12.7" y2="134.62" width="0.1524" layer="91"/>
<label x="-12.7" y="134.62" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="ZQ3" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="134.62" x2="-15.24" y2="134.62" width="0.1524" layer="91"/>
<junction x="-15.24" y="134.62"/>
</segment>
</net>
<net name="RTCI" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="XL1"/>
<wire x1="-7.62" y1="96.52" x2="-10.16" y2="96.52" width="0.1524" layer="91"/>
<label x="-10.16" y="96.52" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="129.54" x2="-40.64" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="134.62" x2="-40.64" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="142.24" x2="-40.64" y2="142.24" width="0.1524" layer="91"/>
<label x="-12.7" y="142.24" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="ZQ3" gate="G$1" pin="1"/>
<wire x1="-30.48" y1="134.62" x2="-40.64" y2="134.62" width="0.1524" layer="91"/>
<junction x="-40.64" y="134.62"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="HL2" gate="G$1" pin="A"/>
<wire x1="81.28" y1="-48.26" x2="86.36" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-48.26" x2="86.36" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="/RESET" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="-55.88" x2="-60.96" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-55.88" x2="-60.96" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-55.88" x2="-58.42" y2="-55.88" width="0.1524" layer="91"/>
<junction x="-60.96" y="-55.88"/>
<label x="-58.42" y="-55.88" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="/RESET"/>
<wire x1="40.64" y1="93.98" x2="45.72" y2="93.98" width="0.1524" layer="91"/>
<label x="45.72" y="93.98" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="-7.62" y1="-20.32" x2="-10.16" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="NRST"/>
<label x="-10.16" y="-20.32" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X6" gate="G$1" pin="RESET"/>
<wire x1="96.52" y1="35.56" x2="93.98" y2="35.56" width="0.1524" layer="91"/>
<label x="93.98" y="35.56" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="104.14" x2="-43.18" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="DEC2"/>
<wire x1="-7.62" y1="104.14" x2="-43.18" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XC2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="XC2"/>
<wire x1="20.32" y1="121.92" x2="20.32" y2="124.46" width="0.1524" layer="91"/>
<label x="20.32" y="124.46" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="ZQ1" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="134.62" x2="-55.88" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="129.54" x2="-55.88" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="134.62" x2="-55.88" y2="137.16" width="0.1524" layer="91"/>
<junction x="-55.88" y="134.62"/>
<wire x1="-55.88" y1="137.16" x2="-53.34" y2="137.16" width="0.1524" layer="91"/>
<label x="-53.34" y="137.16" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="XC1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="XC1"/>
<wire x1="17.78" y1="121.92" x2="17.78" y2="124.46" width="0.1524" layer="91"/>
<label x="17.78" y="124.46" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="ZQ1" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="134.62" x2="-71.12" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="129.54" x2="-81.28" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="142.24" x2="-81.28" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="142.24" x2="-81.28" y2="134.62" width="0.1524" layer="91"/>
<junction x="-81.28" y="134.62"/>
<label x="-53.34" y="142.24" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="124.46" y1="93.98" x2="127" y2="93.98" width="0.1524" layer="91"/>
<wire x1="111.76" y1="116.84" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<wire x1="124.46" y1="116.84" x2="127" y2="116.84" width="0.1524" layer="91"/>
<wire x1="127" y1="116.84" x2="127" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="101.6" y1="116.84" x2="111.76" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="111.76" y1="119.38" x2="111.76" y2="116.84" width="0.1524" layer="91"/>
<junction x="111.76" y="116.84"/>
<wire x1="124.46" y1="93.98" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<junction x="124.46" y="116.84"/>
<pinref part="ANT1" gate="G$1" pin="SIG"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="%2"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="83.82" y1="116.84" x2="93.98" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AVDD" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="AVDD@1"/>
<wire x1="22.86" y1="121.92" x2="22.86" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="22.86" y1="142.24" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<wire x1="40.64" y1="142.24" x2="40.64" y2="139.7" width="0.1524" layer="91"/>
<wire x1="40.64" y1="142.24" x2="53.34" y2="142.24" width="0.1524" layer="91"/>
<wire x1="53.34" y1="142.24" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<junction x="40.64" y="142.24"/>
<pinref part="U3" gate="G$1" pin="AVDD@0"/>
<wire x1="53.34" y1="106.68" x2="40.64" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="AVDD@2"/>
<wire x1="15.24" y1="121.92" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="%2"/>
<wire x1="15.24" y1="142.24" x2="15.24" y2="157.48" width="0.1524" layer="91"/>
<wire x1="15.24" y1="157.48" x2="7.62" y2="157.48" width="0.1524" layer="91"/>
<wire x1="7.62" y1="157.48" x2="0" y2="157.48" width="0.1524" layer="91"/>
<wire x1="0" y1="157.48" x2="0" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="7.62" y1="154.94" x2="7.62" y2="157.48" width="0.1524" layer="91"/>
<junction x="7.62" y="157.48"/>
<wire x1="22.86" y1="142.24" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
<junction x="22.86" y="142.24"/>
<junction x="15.24" y="142.24"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="17.78" y1="157.48" x2="15.24" y2="157.48" width="0.1524" layer="91"/>
<junction x="15.24" y="157.48"/>
</segment>
</net>
<net name="IERF" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="IREF"/>
<wire x1="27.94" y1="129.54" x2="25.4" y2="129.54" width="0.1524" layer="91"/>
<wire x1="25.4" y1="129.54" x2="25.4" y2="121.92" width="0.1524" layer="91"/>
<wire x1="25.4" y1="129.54" x2="25.4" y2="132.08" width="0.1524" layer="91"/>
<junction x="25.4" y="129.54"/>
<label x="25.4" y="132.08" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="106.68" x2="-55.88" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VDD@1"/>
<wire x1="-7.62" y1="106.68" x2="-55.88" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="106.68" x2="-58.42" y2="106.68" width="0.1524" layer="91"/>
<junction x="-55.88" y="106.68"/>
<label x="-58.42" y="106.68" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="71.12" x2="-2.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="71.12" x2="-5.08" y2="71.12" width="0.1524" layer="91"/>
<label x="-5.08" y="71.12" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="U3" gate="G$1" pin="VDD@2"/>
<wire x1="-2.54" y1="71.12" x2="7.62" y2="71.12" width="0.1524" layer="91"/>
<wire x1="7.62" y1="71.12" x2="7.62" y2="73.66" width="0.1524" layer="91"/>
<junction x="-2.54" y="71.12"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="%1"/>
<wire x1="-73.66" y1="-27.94" x2="-76.2" y2="-27.94" width="0.1524" layer="91"/>
<label x="-76.2" y="-27.94" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="38.1" y1="-50.8" x2="38.1" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VDD@1"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="38.1" y1="-53.34" x2="38.1" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-53.34" x2="38.1" y2="-53.34" width="0.1524" layer="91"/>
<junction x="38.1" y="-53.34"/>
<label x="38.1" y="-55.88" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="55.88" y1="-5.08" x2="58.42" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VDD@2"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="58.42" y1="-5.08" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-2.54" x2="58.42" y2="-5.08" width="0.1524" layer="91"/>
<junction x="58.42" y="-5.08"/>
<label x="60.96" y="-5.08" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="10.16" y1="12.7" x2="10.16" y2="15.24" width="0.1524" layer="91"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-5.08" x2="-10.16" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.1524" layer="91"/>
<junction x="10.16" y="15.24"/>
<pinref part="U4" gate="G$1" pin="VBAT"/>
<pinref part="U4" gate="G$1" pin="VDD@3"/>
<label x="10.16" y="17.78" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="17.78" x2="-10.16" y2="15.24" width="0.1524" layer="91"/>
<junction x="-10.16" y="15.24"/>
</segment>
<segment>
<label x="-99.06" y="7.62" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="7.62" x2="-101.6" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="7.62" x2="-101.6" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="7.62" x2="-99.06" y2="7.62" width="0.1524" layer="91"/>
<junction x="-101.6" y="7.62"/>
<pinref part="U2" gate="G$1" pin="VOUT\"/>
<wire x1="-111.76" y1="20.32" x2="-101.6" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="20.32" x2="-101.6" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-116.84" y1="-40.64" x2="-114.3" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-43.18" x2="-116.84" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-116.84" y="-40.64"/>
<pinref part="VT1" gate="G$1" pin="S"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="-101.6" y1="-27.94" x2="-101.6" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="-20.32" x2="-116.84" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-20.32" x2="-116.84" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-40.64" x2="-119.38" y2="-40.64" width="0.1524" layer="91"/>
<label x="-119.38" y="-40.64" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-55.88" x2="-76.2" y2="-55.88" width="0.1524" layer="91"/>
<label x="-76.2" y="-55.88" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X6" gate="G$1" pin="VDD"/>
<wire x1="96.52" y1="38.1" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
<label x="93.98" y="38.1" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="27.94" y1="157.48" x2="30.48" y2="157.48" width="0.1524" layer="91"/>
<label x="30.48" y="157.48" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="86.36" y1="-132.08" x2="83.82" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-132.08" x2="83.82" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-124.46" x2="86.36" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-124.46" x2="81.28" y2="-124.46" width="0.1524" layer="91"/>
<junction x="83.82" y="-124.46"/>
<label x="81.28" y="-124.46" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="VDD"/>
<wire x1="-132.08" y1="109.22" x2="-134.62" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="109.22" x2="-134.62" y2="114.3" width="0.1524" layer="91"/>
<label x="-134.62" y="116.84" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<wire x1="-134.62" y1="114.3" x2="-134.62" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="114.3" x2="-137.16" y2="114.3" width="0.1524" layer="91"/>
<junction x="-134.62" y="114.3"/>
<pinref part="C2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-7.62" y1="-109.22" x2="-10.16" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-109.22" x2="-10.16" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-111.76" x2="-7.62" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-109.22" x2="-20.32" y2="-109.22" width="0.1524" layer="91"/>
<junction x="-10.16" y="-109.22"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-109.22" x2="-30.48" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-121.92" x2="-20.32" y2="-119.38" width="0.1524" layer="91"/>
<junction x="-20.32" y="-109.22"/>
<pinref part="LCD1" gate="G$1" pin="VDD"/>
<pinref part="LCD1" gate="G$1" pin="VDDI"/>
<pinref part="LCD1" gate="G$1" pin="LEDA"/>
<wire x1="-20.32" y1="-119.38" x2="-20.32" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-119.38" x2="-20.32" y2="-119.38" width="0.1524" layer="91"/>
<junction x="-20.32" y="-119.38"/>
<label x="-30.48" y="-109.22" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="%1"/>
<pinref part="L4" gate="G$1" pin="%2"/>
<wire x1="0" y1="144.78" x2="0" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="%1"/>
<wire x1="0" y1="127" x2="0" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="DCDC"/>
<wire x1="0" y1="124.46" x2="7.62" y2="124.46" width="0.1524" layer="91"/>
<wire x1="7.62" y1="124.46" x2="7.62" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="DEC1"/>
<wire x1="-7.62" y1="101.6" x2="-30.48" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="101.6" x2="-30.48" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="187.96" y1="-10.16" x2="187.96" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-17.78" x2="190.5" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-17.78" x2="190.5" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="REGOUT"/>
<pinref part="C31" gate="G$1" pin="1"/>
</segment>
</net>
<net name="I2C_INT" class="0">
<segment>
<wire x1="193.04" y1="-10.16" x2="193.04" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="193.04" y1="-12.7" x2="195.58" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="INT"/>
<label x="195.58" y="-12.7" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB7"/>
<wire x1="22.86" y1="12.7" x2="22.86" y2="15.24" width="0.1524" layer="91"/>
<label x="22.86" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="CPOUT"/>
<wire x1="190.5" y1="40.64" x2="190.5" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDD_I2C" class="0">
<segment>
<wire x1="210.82" y1="7.62" x2="213.36" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VDD"/>
<label x="213.36" y="7.62" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VLOGIC"/>
<wire x1="182.88" y1="-10.16" x2="182.88" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="170.18" y1="-15.24" x2="170.18" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-12.7" x2="182.88" y2="-12.7" width="0.1524" layer="91"/>
<label x="167.64" y="-12.7" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<wire x1="167.64" y1="-12.7" x2="170.18" y2="-12.7" width="0.1524" layer="91"/>
<junction x="170.18" y="-12.7"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="149.86" y1="-76.2" x2="149.86" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VDDIO"/>
<wire x1="162.56" y1="-76.2" x2="165.1" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VDD"/>
<wire x1="165.1" y1="-73.66" x2="162.56" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-73.66" x2="162.56" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-73.66" x2="149.86" y2="-73.66" width="0.1524" layer="91"/>
<junction x="162.56" y="-73.66"/>
<wire x1="149.86" y1="-73.66" x2="144.78" y2="-73.66" width="0.1524" layer="91"/>
<junction x="149.86" y="-73.66"/>
<label x="144.78" y="-73.66" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="U6" gate="G$1" pin="CS"/>
<wire x1="198.12" y1="-81.28" x2="213.36" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-81.28" x2="213.36" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-66.04" x2="162.56" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-66.04" x2="162.56" y2="-73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="ADDR"/>
<wire x1="167.64" y1="114.3" x2="165.1" y2="114.3" width="0.1524" layer="91"/>
<wire x1="165.1" y1="114.3" x2="165.1" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VCC"/>
<wire x1="165.1" y1="116.84" x2="167.64" y2="116.84" width="0.1524" layer="91"/>
<wire x1="165.1" y1="116.84" x2="157.48" y2="116.84" width="0.1524" layer="91"/>
<junction x="165.1" y="116.84"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="182.88" y1="124.46" x2="165.1" y2="124.46" width="0.1524" layer="91"/>
<wire x1="165.1" y1="124.46" x2="165.1" y2="116.84" width="0.1524" layer="91"/>
<label x="157.48" y="116.84" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="VCC"/>
<wire x1="193.04" y1="-127" x2="198.12" y2="-127" width="0.1524" layer="91"/>
<label x="198.12" y="-127" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="185.42" y1="66.04" x2="185.42" y2="68.58" width="0.1524" layer="91"/>
<label x="185.42" y="68.58" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="-101.6" y1="-40.64" x2="-99.06" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="-40.64" x2="-101.6" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-101.6" y="-40.64"/>
<pinref part="VT1" gate="G$1" pin="D"/>
<label x="-99.06" y="-40.64" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-101.6" y1="-38.1" x2="-101.6" y2="-40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="G$1" pin="VCC"/>
<wire x1="81.28" y1="-91.44" x2="78.74" y2="-91.44" width="0.1524" layer="91"/>
<label x="76.2" y="-91.44" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="78.74" y1="-91.44" x2="76.2" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-86.36" x2="78.74" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-88.9" x2="78.74" y2="-91.44" width="0.1524" layer="91"/>
<junction x="78.74" y="-91.44"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="193.04" y1="124.46" x2="198.12" y2="124.46" width="0.1524" layer="91"/>
<wire x1="198.12" y1="124.46" x2="198.12" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="DVI"/>
<wire x1="198.12" y1="114.3" x2="195.58" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="198.12" y1="114.3" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<junction x="198.12" y="114.3"/>
</segment>
</net>
<net name="PA1" class="0">
<segment>
<wire x1="-10.16" y1="-30.48" x2="-7.62" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA1"/>
<pinref part="X3" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="-43.18" x2="-17.78" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-43.18" x2="-17.78" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-30.48" x2="-10.16" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDDA" class="0">
<segment>
<wire x1="-7.62" y1="-25.4" x2="-10.16" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VDDA"/>
<label x="-10.16" y="-25.4" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="%2"/>
<wire x1="-63.5" y1="-27.94" x2="-60.96" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-60.96" y="-27.94"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-60.96" y1="-33.02" x2="-60.96" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-27.94" x2="-58.42" y2="-27.94" width="0.1524" layer="91"/>
<label x="-58.42" y="-27.94" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="55.88" y1="-10.16" x2="93.98" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-10.16" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<wire x1="93.98" y1="27.94" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA13"/>
<pinref part="X6" gate="G$1" pin="SWDAT"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="96.52" y1="33.02" x2="38.1" y2="33.02" width="0.1524" layer="91"/>
<wire x1="38.1" y1="33.02" x2="38.1" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA14"/>
<pinref part="X6" gate="G$1" pin="SWCLK"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="-175.26" y1="7.62" x2="-177.8" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-165.1" y1="20.32" x2="-177.8" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="20.32" x2="-177.8" y2="7.62" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="%2"/>
<wire x1="-200.66" y1="7.62" x2="-177.8" y2="7.62" width="0.1524" layer="91"/>
<junction x="-177.8" y="7.62"/>
</segment>
</net>
<net name="OSCI" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="0" x2="-73.66" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-15.24" x2="-35.56" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-15.24" x2="-35.56" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="7.62" x2="-73.66" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="7.62" x2="-73.66" y2="0" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="OSC-IN"/>
<pinref part="ZQ2" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="0" x2="-73.66" y2="0" width="0.1524" layer="91"/>
<junction x="-73.66" y="0"/>
</segment>
</net>
<net name="OSCO" class="0">
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="0" x2="-50.8" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="0" x2="-38.1" y2="0" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="0" x2="-38.1" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-50.8" y="0"/>
<wire x1="-38.1" y1="-17.78" x2="-7.62" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="OSC-OUT"/>
<pinref part="ZQ2" gate="G$1" pin="2"/>
<wire x1="-58.42" y1="0" x2="-50.8" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BAT" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-175.26" y1="-27.94" x2="-175.26" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-175.26" y1="-25.4" x2="-177.8" y2="-25.4" width="0.1524" layer="91"/>
<label x="-177.8" y="-25.4" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="BAT"/>
<wire x1="-134.62" y1="7.62" x2="-129.54" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="7.62" x2="-134.62" y2="7.62" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="2"/>
<wire x1="-127" y1="55.88" x2="-134.62" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="55.88" x2="-134.62" y2="53.34" width="0.1524" layer="91"/>
<junction x="-134.62" y="7.62"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-134.62" y1="53.34" x2="-134.62" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="48.26" x2="-134.62" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="20.32" x2="-134.62" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="5.08" x2="-134.62" y2="7.62" width="0.1524" layer="91"/>
<junction x="-134.62" y="7.62"/>
<pinref part="U2" gate="G$1" pin="VIN"/>
<pinref part="G1" gate="1" pin="+"/>
<wire x1="-124.46" y1="45.72" x2="-124.46" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="48.26" x2="-134.62" y2="48.26" width="0.1524" layer="91"/>
<junction x="-134.62" y="48.26"/>
<wire x1="-124.46" y1="48.26" x2="-121.92" y2="48.26" width="0.1524" layer="91" style="longdash"/>
<junction x="-124.46" y="48.26"/>
<label x="-121.92" y="48.26" size="1.27" layer="95" font="vector" xref="yes"/>
<wire x1="-121.92" y1="20.32" x2="-134.62" y2="20.32" width="0.1524" layer="91"/>
<junction x="-134.62" y="20.32"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PROG"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-175.26" y1="2.54" x2="-177.8" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="2.54" x2="-177.8" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="HL1" gate="G$1" pin="A"/>
<wire x1="-154.94" y1="20.32" x2="-142.24" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="20.32" x2="-142.24" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CHRG"/>
<wire x1="-142.24" y1="2.54" x2="-144.78" y2="2.54" width="0.1524" layer="91"/>
<pinref part="HL1" gate="G$1" pin="C"/>
<wire x1="-142.24" y1="2.54" x2="-142.24" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PERIF_ON" class="0">
<segment>
<wire x1="-111.76" y1="-45.72" x2="-111.76" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-111.76" y1="-55.88" x2="-116.84" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-55.88" x2="-116.84" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="-55.88" x2="-119.38" y2="-55.88" width="0.1524" layer="91"/>
<junction x="-116.84" y="-55.88"/>
<pinref part="VT1" gate="G$1" pin="G"/>
<label x="-119.38" y="-55.88" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB1"/>
<wire x1="25.4" y1="-50.8" x2="25.4" y2="-53.34" width="0.1524" layer="91" style="longdash"/>
<label x="25.4" y="-53.34" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
</net>
<net name="ADC_BAT" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="-43.18" x2="-175.26" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-175.26" y1="-40.64" x2="-175.26" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-175.26" y1="-40.64" x2="-167.64" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-175.26" y="-40.64"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-167.64" y1="-40.64" x2="-165.1" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="-43.18" x2="-167.64" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-167.64" y="-40.64"/>
<label x="-165.1" y="-40.64" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB0"/>
<wire x1="22.86" y1="-50.8" x2="22.86" y2="-53.34" width="0.1524" layer="91" style="longdash"/>
<label x="22.86" y="-53.34" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
</net>
<net name="PA0" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="PA0"/>
<wire x1="-7.62" y1="-27.94" x2="-10.16" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-27.94" x2="-20.32" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-27.94" x2="-20.32" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-40.64" x2="-30.48" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB_D_P" class="0">
<segment>
<wire x1="55.88" y1="-12.7" x2="58.42" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA12"/>
<label x="58.42" y="-12.7" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-200.66" y1="2.54" x2="-198.12" y2="2.54" width="0.1524" layer="91"/>
<label x="-190.5" y="2.54" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-198.12" y1="2.54" x2="-190.5" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="0" x2="-198.12" y2="2.54" width="0.1524" layer="91"/>
<junction x="-198.12" y="2.54"/>
</segment>
</net>
<net name="KEY_PWR" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="PC13-TAMPER"/>
<wire x1="-7.62" y1="-7.62" x2="-10.16" y2="-7.62" width="0.1524" layer="91"/>
<label x="-10.16" y="-7.62" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-116.84" y1="-83.82" x2="-116.84" y2="-81.28" width="0.1524" layer="91"/>
<label x="-116.84" y="-78.74" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<pinref part="SW1" gate="G$1" pin="S"/>
<pinref part="SW1" gate="G$1" pin="S1"/>
<wire x1="-116.84" y1="-81.28" x2="-116.84" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-83.82" x2="-114.3" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="-81.28" x2="-116.84" y2="-81.28" width="0.1524" layer="91"/>
<junction x="-116.84" y="-81.28"/>
</segment>
</net>
<net name="LCD_SCK" class="0">
<segment>
<wire x1="-7.62" y1="-106.68" x2="-17.78" y2="-106.68" width="0.1524" layer="91"/>
<label x="-17.78" y="-106.68" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="LCD1" gate="G$1" pin="SCLK"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB3"/>
<wire x1="33.02" y1="12.7" x2="33.02" y2="15.24" width="0.1524" layer="91"/>
<label x="33.02" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="LCD_DAT" class="0">
<segment>
<wire x1="-7.62" y1="-104.14" x2="-17.78" y2="-104.14" width="0.1524" layer="91"/>
<label x="-17.78" y="-104.14" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="LCD1" gate="G$1" pin="SDIN"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB4"/>
<wire x1="30.48" y1="12.7" x2="30.48" y2="15.24" width="0.1524" layer="91"/>
<label x="30.48" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="LCD_CS" class="0">
<segment>
<wire x1="-7.62" y1="-99.06" x2="-17.78" y2="-99.06" width="0.1524" layer="91"/>
<label x="-17.78" y="-99.06" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="LCD1" gate="G$1" pin="!CS"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB5"/>
<wire x1="27.94" y1="12.7" x2="27.94" y2="15.24" width="0.1524" layer="91"/>
<label x="27.94" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="LCD_RES" class="0">
<segment>
<wire x1="-7.62" y1="-96.52" x2="-17.78" y2="-96.52" width="0.1524" layer="91"/>
<label x="-17.78" y="-96.52" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="LCD1" gate="G$1" pin="!RST"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB6"/>
<wire x1="25.4" y1="12.7" x2="25.4" y2="15.24" width="0.1524" layer="91"/>
<label x="25.4" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="VT2" gate="G$1" pin="G"/>
<wire x1="-48.26" y1="-127" x2="-50.8" y2="-127" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="-50.8" y1="-129.54" x2="-50.8" y2="-127" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="-127" x2="-50.8" y2="-127" width="0.1524" layer="91"/>
<junction x="-50.8" y="-127"/>
</segment>
</net>
<net name="LCD_ON" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="-127" x2="-66.04" y2="-127" width="0.1524" layer="91"/>
<label x="-66.04" y="-127" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PA15"/>
<wire x1="35.56" y1="12.7" x2="35.56" y2="15.24" width="0.1524" layer="91"/>
<label x="35.56" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="VT2" gate="G$1" pin="D"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="-33.02" y1="-116.84" x2="-43.18" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-116.84" x2="-43.18" y2="-119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="VBUS"/>
<pinref part="L1" gate="G$1" pin="%1"/>
<wire x1="-210.82" y1="7.62" x2="-213.36" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D_P" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="X1" gate="G$1" pin="D+"/>
<wire x1="-210.82" y1="2.54" x2="-213.36" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D_N" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="X1" gate="G$1" pin="D-"/>
<wire x1="-210.82" y1="5.08" x2="-213.36" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="LEDK"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="-116.84" x2="-22.86" y2="-116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ACTIVE" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="ACTIVE"/>
<wire x1="-7.62" y1="93.98" x2="-10.16" y2="93.98" width="0.1524" layer="91"/>
<label x="-10.16" y="93.98" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB2/BOOT1"/>
<wire x1="27.94" y1="-50.8" x2="27.94" y2="-53.34" width="0.1524" layer="91"/>
<label x="27.94" y="-53.34" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="CE"/>
<wire x1="-132.08" y1="104.14" x2="-134.62" y2="104.14" width="0.1524" layer="91"/>
<label x="-134.62" y="104.14" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IRQ" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="/RYDN"/>
<wire x1="25.4" y1="73.66" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<label x="25.4" y="71.12" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PA8"/>
<wire x1="55.88" y1="-22.86" x2="58.42" y2="-22.86" width="0.1524" layer="91"/>
<label x="58.42" y="-22.86" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="/IRQ"/>
<wire x1="-132.08" y1="91.44" x2="-134.62" y2="91.44" width="0.1524" layer="91"/>
<label x="-134.62" y="91.44" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="MISO"/>
<wire x1="20.32" y1="73.66" x2="20.32" y2="71.12" width="0.1524" layer="91"/>
<label x="20.32" y="71.12" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB14"/>
<wire x1="55.88" y1="-27.94" x2="58.42" y2="-27.94" width="0.1524" layer="91"/>
<label x="58.42" y="-27.94" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="MISO"/>
<wire x1="-132.08" y1="93.98" x2="-134.62" y2="93.98" width="0.1524" layer="91"/>
<label x="-134.62" y="93.98" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="MOSI"/>
<wire x1="17.78" y1="73.66" x2="17.78" y2="71.12" width="0.1524" layer="91"/>
<label x="17.78" y="71.12" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB15"/>
<wire x1="55.88" y1="-25.4" x2="58.42" y2="-25.4" width="0.1524" layer="91"/>
<label x="58.42" y="-25.4" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="MOSI"/>
<wire x1="-132.08" y1="96.52" x2="-134.62" y2="96.52" width="0.1524" layer="91"/>
<label x="-134.62" y="96.52" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SCK"/>
<wire x1="12.7" y1="73.66" x2="12.7" y2="71.12" width="0.1524" layer="91"/>
<label x="12.7" y="71.12" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB13"/>
<wire x1="55.88" y1="-30.48" x2="58.42" y2="-30.48" width="0.1524" layer="91"/>
<label x="58.42" y="-30.48" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="SCK"/>
<wire x1="-132.08" y1="99.06" x2="-134.62" y2="99.06" width="0.1524" layer="91"/>
<label x="-134.62" y="99.06" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RXD"/>
<wire x1="10.16" y1="73.66" x2="10.16" y2="71.12" width="0.1524" layer="91"/>
<label x="10.16" y="71.12" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="58.42" y1="-20.32" x2="55.88" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA9"/>
<label x="58.42" y="-20.32" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="TXD"/>
<wire x1="-7.62" y1="91.44" x2="-10.16" y2="91.44" width="0.1524" layer="91"/>
<label x="-10.16" y="91.44" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="55.88" y1="-17.78" x2="58.42" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA10"/>
<label x="58.42" y="-17.78" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="PA2" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="PA2"/>
<wire x1="-7.62" y1="-33.02" x2="-15.24" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-33.02" x2="-15.24" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="3"/>
<wire x1="-15.24" y1="-45.72" x2="-30.48" y2="-45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PA3" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="PA3"/>
<wire x1="10.16" y1="-50.8" x2="10.16" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-53.34" x2="-15.24" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-53.34" x2="-15.24" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="4"/>
<wire x1="-15.24" y1="-48.26" x2="-30.48" y2="-48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="71.12" y1="-48.26" x2="68.58" y2="-48.26" width="0.1524" layer="91"/>
<label x="68.58" y="-48.26" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB8"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="15.24" width="0.1524" layer="91"/>
<label x="17.78" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
<label x="17.78" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
<net name="PA5" class="0">
<segment>
<pinref part="X4" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="-63.5" x2="-17.78" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA5"/>
<wire x1="-17.78" y1="-63.5" x2="15.24" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-63.5" x2="15.24" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PA4" class="0">
<segment>
<pinref part="X4" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-60.96" x2="-30.48" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA4"/>
<wire x1="-20.32" y1="-60.96" x2="12.7" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-60.96" x2="12.7" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PA6" class="0">
<segment>
<pinref part="X4" gate="G$1" pin="3"/>
<wire x1="-15.24" y1="-66.04" x2="-30.48" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA6"/>
<wire x1="-15.24" y1="-66.04" x2="17.78" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-66.04" x2="17.78" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PA7" class="0">
<segment>
<pinref part="X4" gate="G$1" pin="4"/>
<wire x1="-15.24" y1="-68.58" x2="-30.48" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA7"/>
<wire x1="-15.24" y1="-68.58" x2="20.32" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-68.58" x2="20.32" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="RESV3"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="185.42" y1="38.1" x2="185.42" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ANT2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="ANT2"/>
<wire x1="40.64" y1="101.6" x2="45.72" y2="101.6" width="0.1524" layer="91"/>
<label x="45.72" y="101.6" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="L6" gate="G$1" pin="%1"/>
<pinref part="L5" gate="G$1" pin="%2"/>
<wire x1="71.12" y1="116.84" x2="73.66" y2="116.84" width="0.1524" layer="91"/>
<wire x1="71.12" y1="114.3" x2="71.12" y2="116.84" width="0.1524" layer="91"/>
<wire x1="71.12" y1="116.84" x2="68.58" y2="116.84" width="0.1524" layer="91"/>
<junction x="71.12" y="116.84"/>
<label x="68.58" y="116.84" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VDD_PA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VDD_PA"/>
<wire x1="40.64" y1="96.52" x2="45.72" y2="96.52" width="0.1524" layer="91"/>
<label x="45.72" y="96.52" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="78.74" y1="96.52" x2="78.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="78.74" y1="96.52" x2="86.36" y2="96.52" width="0.1524" layer="91"/>
<wire x1="86.36" y1="96.52" x2="86.36" y2="86.36" width="0.1524" layer="91"/>
<junction x="78.74" y="96.52"/>
<wire x1="86.36" y1="96.52" x2="86.36" y2="101.6" width="0.1524" layer="91"/>
<junction x="86.36" y="96.52"/>
<pinref part="L7" gate="G$1" pin="%2"/>
<wire x1="86.36" y1="101.6" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="96.52" x2="76.2" y2="96.52" width="0.1524" layer="91"/>
<wire x1="76.2" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<label x="68.58" y="96.52" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ANT1" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="%1"/>
<wire x1="68.58" y1="101.6" x2="71.12" y2="101.6" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="%1"/>
<wire x1="71.12" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<wire x1="71.12" y1="104.14" x2="71.12" y2="101.6" width="0.1524" layer="91"/>
<junction x="71.12" y="101.6"/>
<label x="68.58" y="101.6" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="ANT1"/>
<wire x1="40.64" y1="99.06" x2="45.72" y2="99.06" width="0.1524" layer="91"/>
<label x="45.72" y="99.06" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="/REQN"/>
<wire x1="15.24" y1="73.66" x2="15.24" y2="71.12" width="0.1524" layer="91"/>
<label x="15.24" y="71.12" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB12"/>
<wire x1="55.88" y1="-33.02" x2="58.42" y2="-33.02" width="0.1524" layer="91"/>
<label x="58.42" y="-33.02" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="/CS"/>
<wire x1="-132.08" y1="101.6" x2="-134.62" y2="101.6" width="0.1524" layer="91"/>
<label x="-134.62" y="101.6" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="99.06" y1="-124.46" x2="96.52" y2="-124.46" width="0.1524" layer="91"/>
<label x="99.06" y="-124.46" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="SDA"/>
<wire x1="195.58" y1="111.76" x2="200.66" y2="111.76" width="0.1524" layer="91"/>
<label x="200.66" y="111.76" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SDI/SDA"/>
<wire x1="198.12" y1="-73.66" x2="200.66" y2="-73.66" width="0.1524" layer="91"/>
<label x="200.66" y="-73.66" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="SDA"/>
<wire x1="193.04" y1="-134.62" x2="195.58" y2="-134.62" width="0.1524" layer="91"/>
<label x="195.58" y="-134.62" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="X5" gate="G$1" pin="SDA"/>
<wire x1="81.28" y1="-93.98" x2="78.74" y2="-93.98" width="0.1524" layer="91"/>
<label x="78.74" y="-93.98" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="180.34" y1="38.1" x2="180.34" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="SDA"/>
<label x="180.34" y="40.64" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB11"/>
<wire x1="33.02" y1="-50.8" x2="33.02" y2="-53.34" width="0.1524" layer="91"/>
<label x="33.02" y="-53.34" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-132.08" x2="99.06" y2="-132.08" width="0.1524" layer="91"/>
<label x="99.06" y="-132.08" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="SCL"/>
<wire x1="195.58" y1="116.84" x2="200.66" y2="116.84" width="0.1524" layer="91"/>
<label x="200.66" y="116.84" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SCK/SCL"/>
<wire x1="198.12" y1="-76.2" x2="200.66" y2="-76.2" width="0.1524" layer="91"/>
<label x="200.66" y="-76.2" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="SCL"/>
<wire x1="193.04" y1="-132.08" x2="195.58" y2="-132.08" width="0.1524" layer="91"/>
<label x="195.58" y="-132.08" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="X5" gate="G$1" pin="SCL"/>
<wire x1="81.28" y1="-96.52" x2="78.74" y2="-96.52" width="0.1524" layer="91"/>
<label x="78.74" y="-96.52" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="182.88" y1="38.1" x2="182.88" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="SCL"/>
<label x="182.88" y="40.64" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB10"/>
<wire x1="30.48" y1="-50.8" x2="30.48" y2="-53.34" width="0.1524" layer="91"/>
<label x="30.48" y="-53.34" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
</net>
<net name="USB_D_N" class="0">
<segment>
<wire x1="55.88" y1="-15.24" x2="58.42" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PA11"/>
<label x="58.42" y="-15.24" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-200.66" y1="5.08" x2="-190.5" y2="5.08" width="0.1524" layer="91"/>
<label x="-190.5" y="5.08" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="USB_ON" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-198.12" y1="-10.16" x2="-198.12" y2="-12.7" width="0.1524" layer="91"/>
<label x="-198.12" y="-12.7" size="1.27" layer="95" font="vector" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="PB9"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
<label x="15.24" y="15.24" size="1.27" layer="95" font="vector" rot="R90" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
